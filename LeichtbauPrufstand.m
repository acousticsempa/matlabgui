function LeichtbauPrufstand()

% Function adds to correct folders to the path and starts the program, so
% that the user only needs to run this function from the correct folder and
% it will automatically add the correct data

close all
clearvars
clearvars -global

%% %%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configurations
global Configs
% Figures
global fStart

% Enable debugger stop
dbstop('if','error');

%% %%%%%%%%%%%%%%%%%%% LOAD THE FOLDERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the file path
path = mfilename('fullpath');

% Define the separators used to descrive the folder structure
if ispc
    sep = '\';
else
    sep = '/';
end

% Find the last occurrence of the separator
indx = find(path==sep,1,'last');
% Throw error if empty
if isempty(indx)
    error('Parent folder not found!');
end

% Define the root folder of the project
RootFolder = path(1:indx-1);

% Move to the project folder
cd(RootFolder);

% Add the project folder to the matlab path
addpath(RootFolder);


%% %%%%%%%%%%%%%%%%% DEFINE FOLDERS TO ADD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write the names of the folders (and subfolders) to be added to the path
FoldersToAdd = {'acoustics',...
                'buttons','dropdowns','sliders',...
                'figures','tabs','panels','tables',...
                'excel','exports',...
                'plots','prints',...
                'transitions',...
                'getters','handyFunc'};
            
% Add the folders and subfolders to the path
for i=1:numel(FoldersToAdd)
    % Add the folder to the matlab path
    addpath([RootFolder,sep,FoldersToAdd{i}]);
end         
          
%% %%%%%%%%%%%%%% CONFIGURATION FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Launch the configuration script that defines default values and retrieve
% the values from a configuration XML file stored in the application folder
GUIConfigurations();

% Add certain folders only in testing cases
if Configs.Testing
    FoldersToAdd = {'tmp',['tmp',sep,'database']};
    % Add the folders and subfolders to the path
    for i=1:numel(FoldersToAdd)
        % Add the folder to the matlab path
        addpath([RootFolder,sep,FoldersToAdd{i}]);
    end
end

% Tell the user where the configuration file is stored
uiwait(msgbox(['The configuration file for the LeichtbauPrufstand',...
               ' can be found at',10,...
               'the following address:',10,13,10,...
                RootFolder,sep,'configs.xml',10,13,10,...
               'To specify the Excel databases and other configurations',...
               ' please modify this file.'],...
               'LeichtbauPrufstand - Initialization','modal'));      

%% %%%%%%%%%%%%%%%%%%%%%% START DIARY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Clear diary file
diary(Configs.Diary);
% Hide the file
fileattrib(Configs.Diary,'+w +h','');
diary('on');

%% %%%%%%%%%%%%%%%%% IMPORT DATA (DATA/KNOTEN/BAUTEIL...) %%%%%%%%%%%%%%%%%
% Checking of hashes is done in the import file itself
% Import the Data / Knoten / Bauteile / Ubertragung / Vorsatz Data
pD = ImportExcelData();
pK = ImportKnoten();
pB = ImportBauteile();
pU = ImportUbertragung();
pV = ImportVorsatz();
pVU = ImportVUbertragung();

% Check that all imports worked, if not return
if ~all([pD,pK,pB,pU,pV,pVU])
    return;
end

%% %%%%%%%%%%%%%%% SHOW THE STARTING FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the Start figure
fStart_Build();
% Make the figure visible
fStart.Visible = 'on';

%% %%%%%%%%%%%%%%%% BUILD THE GUI INTERFACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% VIEWERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fViewDatasets_Build();
    % Populate the dropdowns of the ViewDatasets figure with the airborne data
    popDropdowns('Airborne');
    % Build the tabs
    BuildDataTab();

%%%%%%%%%%%%%%%%%%%% VIEWERS / MODIFIERS / DELETERS %%%%%%%%%%%%%%%%%%%%%%
pKnoten_Build();
pBauteil_Build();
pUbertragung_Build();
pVorsatz_Build();
pVUbertragung_Build();
% Populate them
pKnoten_Populate(0);
pBauteil_Populate(0);
pUbertragung_Populate(0);
pVorsatz_Populate(0);
pVUbertragung_Populate(0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%% CREATORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fCreateKnoten_Build();
fCreateBauteil_Build();
fCreateVorsatz_Build();

%%%%%%%%%%%%%%%%%%%%%%%%%%% CALCULATORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fCompareDatasets_Build();
    BuildComparisonTab();
    BuildAveragingTab();
    BuildEnergyTab();
    BuildLevelTab();
fCalcVertSituation_Build();
fCalcHorizSituation_Build();
fSituationPlots_Build();

%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fExportOriginalDB_Build();
fExportUbertragung_Build();
fExportVUbertragung_Build();

end