# Function for installing the LeichtbauPrufstand application on a remote
# computer in either the C:\Program Files (x86) folder or in the
# C:\Program Files folder under a new folder called LeichtbauPrufstand
#
# The application pulls the code from the git repository online using the
# credentials for the matteo.empa@gmail.com account

# Give the function acces to parameters
[CmdletBinding()]
# Retrieve the parameters (elevetad -flag)
Param([switch] $elevated)

# Set the execution policy to bypass, change back at the end
Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Bypass -Force;

# Function for testing if we are Admin
function Test-Admin {
  $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
  $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

# Check if we are Admin, if we are and the elevated flag is true, it means we
# we already tried to elevate the process and it didn't work, so we should just
# exit. If the elevated flag is false, it means we have jet to try to elevate
# the process
If ((Test-Admin) -eq $false)  {
    If ($elevated){}
    Else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition));
    }
Exit;
}

# ########################## ACTUAL CODE #####################################

# ######################### VARIABLES ########################################
# Define repository
$Repository = "https://bitbucket.org/matteoempa/matlabgui"
$Branch = "master";
$Repo = "$($Repository)/get/$($Branch).zip";
# Define folder name
$LBPS_dir = "LeichtbauPrufstand";
# XML Configurations file
$XMLConfigs = "configs.xml";
# XML Versioning file
$XMLVersion = "version.xml";
# Application shortcut
$Runner = "LeichtbauPrufstand.bat";
$ProgramEntry = "LeichtbauPrufstand.m";
# Present time
$Now = Get-Date -UFormat "%Y/%m/%d %H:%M";
# #############################################################################

# Check if the C:\Program Files (x86) exists
if (Test-Path -Path "C:\Program Files (x86)") {
  # Move to this folder
  $Program_Dir = "C:\Program Files (x86)";
} else {
  $Program_Dir = "C:\Program Files";
}
# Move to the correct location
Set-Location -Path $Program_Dir;

# Tell the user what we are doing
Clear-Host;
Write-Host "==============================================================================="
Write-Host "==============================================================================="
Write-Host "====================== LeichtbauPrufstand - Installer ========================="
Write-Host "==============================================================================="
Write-Host "==============================================================================="
Write-Host ""
Write-Host "-------------------------------------------------------------------------------"
Write-Host "Maintainer: Matteo Berchier (matteo.berchier@empa.ch)"
Write-Host "Repository: $($Repository)"
Write-Host ""
Write-Host ""
Write-Host "The installer will download the necessary application files from the repository"
Write-Host "and install them in the $($Program_Dir)\LeichtbauPrufstand folder."
Write-Host "The installer will also create a shortcut on the desktop for launching Matlab"
Write-Host "and the LeichtbauPrufstand application."
Write-Host ""
Write-Host "Please report bugs at the following link: "
Write-Host "       $($Repository)/issues/new"
Write-Host "At the link mentioned above you will also find a detailed guideline explaining"
Write-Host "how to properly report bugs."
Write-Host "-------------------------------------------------------------------------------"
Write-Host ""
Write-Host

# Check if the LeichtbauPrufstand folder exists
$LeichtbauPrufstand = "$($Program_Dir)\$($LBPS_dir)";
if (Test-Path -Path $LeichtbauPrufstand) {
  # Check if the configs.xml file exists and if it exist move to the Program_Dir
  if (Test-Path -Path $LeichtbauPrufstand/$XMLConfigs -PathType Leaf){
    Move-Item -Path $LeichtbauPrufstand/$XMLConfigs -Destination $Program_Dir
  }
  Remove-Item $LeichtbauPrufstand -Force -Recurse;
}

# Create the LeichtbauPrufstand directory in any case
New-Item -ItemType Directory -Force -Path $LeichtbauPrufstand | Out-Null;
# Change the permission on the newly created folder
$permission = "$env:UserDomain\$env:UserName","FullControl","Allow";
$rule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission;
$ACL = Get-Acl -Path $LeichtbauPrufstand;
$ACL.SetAccessRule($rule);
$ACL | Set-Acl -Path $LeichtbauPrufstand;
# Move into the newly created Directory
Set-Location -Path $LeichtbauPrufstand;

# Download the current master of the repository
Write-Host "Downloading the repository...."
$Store = "$($LeichtbauPrufstand)\MatlabGUI.zip";
$client = New-Object System.Net.WebClient;
$client.DownloadFile($Repo,$Store);
Write-Host "Download of repository completed!"

# Unzip the file store the content of the folder in the
Write-Host "Unzipping of repository...."
$shell = New-Object -com shell.application;
$ZipFile = $shell.NameSpace($Store);
Foreach ($item in $ZipFile.Items()){
  $shell.NameSpace($LeichtbauPrufstand).CopyHere($item);
}
Write-Host "Unzipping of repository completed!"

# Wait for action to finish
Start-Sleep -Seconds 2

# Delete the ZIP-Folder
Remove-Item -Path $Store -Force;

# Move the contents of the unzipped folder one level up
$FolderName = Get-ChildItem -Path $LeichtbauPrufstand;
$ZipFolder = "$($LeichtbauPrufstand)\$($FolderName)";

# Go through all the items in the Zip-Folder and transfer them
Write-Host "Move of data into correct folders...."
$items = Get-ChildItem -Path $ZipFolder;
Foreach ($item in $items){
  Move-Item "$($ZipFolder)\$($item)" $LeichtbauPrufstand -Force
}
# Copy the XML file back into the folder
if (Test-Path -Path $Program_Dir/$XMLConfigs -PathType Leaf){
  Move-Item -Path $Program_Dir/$XMLConfigs -Destination $LeichtbauPrufstand
}
Write-Host "Move of data into correct folders completed!"

# Delete the extracted folder
Remove-Item -Path $ZipFolder -Force;

# ####################### LAUNCHER ###########################################
# Write the bat file needed to run the application on double click of the link
$stream = [System.IO.StreamWriter] "$($LeichtbauPrufstand)\$($Runner)";
$stream.WriteLine("# Script takes care of launching the LeichtbauPrufstand application");
$stream.WriteLine("matlab -r ""run('$($LeichtbauPrufstand)\$($ProgramEntry)')""");
$stream.close();

# Create the link to the LeichtbauPrufstand.bat file on the Desktop
Write-Host "Creation of a Desktop-Link for the program...."
$Link = "$($env:userprofile)\Desktop\LeichtbauPrufstand.lnk";
$Target = "$($LeichtbauPrufstand)\LeichtbauPrufstand.bat";
$shell = New-Object -ComObject WScript.Shell;
$Shortcut = $shell.CreateShortcut($Link);
$Shortcut.TargetPath=$Target;
$Shortcut.IconLocation="$($LeichtbauPrufstand)\installer\LeichtbauPrufstand.ico,0"
$Shortcut.Save();

# ####################### VERSIONING ##########################################
# Save an XML file with the date when the application was created
$stream = [System.IO.StreamWriter] "$($LeichtbauPrufstand)\$($XMLVersion)";
$stream.WriteLine('<?xml version="1.0" encoding="utf-8"?>');
$stream.WriteLine("<application>");
$stream.WriteLine("   <installed>$($Now)</installed>");
$stream.WriteLine("   <branch>$($Branch)</branch>");
$stream.WriteLine("</application>");
$stream.close();

# Ask the user to press a key to terminate the Program
Write-Host "-------------------------------------------------------------------------------"
Write-Host ""
Write-Host "Program has been installed successfully under the directory:"
Write-Host "        $($LeichtbauPrufstand)"
Write-Host ""
Write-Host "To use the program:"
Write-Host "     1) Double-click the LeichtbauPrufstand link on the Desktop"
Write-Host "          --> ATTENTION: this will start a NEW Matlab session each time!!!"
Write-Host "     2) Open Matlab and run the script:"
Write-Host "              $($LeichtbauPrufstand)\$($ProgramEntry)"
Write-Host ""
Write-Host ""
Write-Host "To terminate the installer just close the terminal window."
Write-Host ""
Write-Host "==============================================================================="
Write-Host ""
Write-Host ""
Write-Host ""
Write-Host ""

# ######################## FINISHING UP ######################################
# Change back the execution policy
Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Restricted -Force;

# Exit the Program
Exit;
