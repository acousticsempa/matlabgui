:: Installer script for the LeichtbauPrufstand application

:: Silence the program
@echo off

:: Call the powershell to execute the powershell-installer for the
:: LeichtbauPrufstand application
powershell -executionpolicy bypass -file LeichtbauPrufstand.ps1
