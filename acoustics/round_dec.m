function [TL_round] = round_dec(TL_ori, ndec)
% Function that rounds TL values to ndec-spaces according to ISO 717 (Annotation 1, Page 10).
    
factor = 10.^ndec;
TL_round = fix(abs(TL_ori).*factor + 0.5)./factor;
    if (abs(TL_ori)~=TL_ori)
    TL_round = -1*TL_round;
    end
end
