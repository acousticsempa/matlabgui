function [RwCtr] = RwCtr_rating(Freq, TL, freqRange)

% Determine R_w + Ctr_freqRange  from transmission losses (TL) according to 
% ISO 717-1 for 50-3150Hz, 50-5000Hz, 100-3150Hz, 100-5000Hz
% %% INPUTS %%
% Freq:      Array with the measurement frequencies [nx1]
% TL:        Array with frquency dependent transmission losses [mxn]
% freqRange: Array with the frequency range  
% 
% %% OUTPUT %%
% RwCtr:     Array [mx1] with R_w + Ctr_freqRange values for each dataset 
%            in TL

%% Input control
if length(Freq) ~= size(TL,2)
    errordlg('Frequency does not match TL!','Error');
    return;
end

%% Define local variables (switch cases)
% Define the reference frequencies for L_ij 
FreqRef = [  50,   63,   80,  100,  125,  160,  200,  250,  315,...
            400,  500,  630,  800, 1000, 1250, 1600, 2000, 2500,...
           3150, 4000, 5000];
% Define the Spectrum for the calculation of Ctr for every frequency range
L_ij = -[25, 23, 21, 20, 20, 18, 16, 15, 14, 13, 12, 11, 9, 8, 9, 10,...
         11, 13, 15, 16, 18];
% Define the frequency range start and end
freqStart = min(freqRange);
freqEnd = max(freqRange);

%% Perform the actual calculations
% Round data to single decimal
TL_round = fix(TL*10 + 0.5)/10;

% Find index of TL for the specified frequency range 
indxTLStart = find(Freq == freqStart);
indxTLEnd = find(Freq == freqEnd);

% Retrieve the reduced TL array
TL = TL_round(:,indxTLStart:indxTLEnd);

% Find index of L_ij_ref for specified frequency range
indxLStart = find(FreqRef == freqStart);
indxLEnd = find(FreqRef == freqEnd);

% Retrieve the reduced L array
L_ij = L_ij(indxLStart:indxLEnd);

% Retrieve the number of datasets being processed
m = size(TL, 1);

% Correct the dimensions of the L_ij matrix to the size of TL
L_ij_m = repmat(L_ij, m, 1);

% Calculate the Rw + Ctr_freqRange value
RwCtr = -10.*log10(nansum(10.^((L_ij_m - TL)/10), 2));

% Round data to integer acc. to ISO 717
RwCtr = fix(RwCtr + 0.5);

end