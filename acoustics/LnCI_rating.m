function [LnCI] = LnCI_rating(Freq, Ln, freqRange)

% Determine Ln + CI_freqRange from (Ln) according to ISO 717-1 for all 
% frequency ranges
% %% INPUTS %%
% Freq:      Array with the measurement frequencies [nx1]
% Ln:        Array with frquency dependent Ln-values [mxn]
% freqRange: Array with the frequency range  
% 
% %% OUTPUT %%
% LnCI:     Array [mx1] with R_w + C_freqRange values for each dataset 
%            in TL

%% Input control
if length(Freq) ~= size(Ln,2)
    errordlg('Frequency vector does not match size of L_n!','Error');
    return;
end

%% Define local variables
% Define the frequency range start and end
freqStart = min(freqRange);
freqEnd = max(freqRange);

%% Perform the actual calculations
% Round data to single decimal
Ln_round = fix(Ln*10 + 0.5)/10;

% Find index of Ln for specified frequency range 
indxLnStart = find(Freq == freqStart);
indxLnEnd = find(Freq == freqEnd);

% Retrieve only relevant range from Ln
Ln = Ln_round(:,indxLnStart:indxLnEnd);

% Calculate the LnCI value
LnCI = 10.*log10(nansum(10.^(Ln/10),2));

% Round data to integer acc. to ISO 717
LnCI = fix(LnCI + 0.5);

end