function out = SNRCValues(freq,values,typeID)

% Function calculates the SNR value and the C-Values when the user passes
% in a frequency array and a corresponding values array as well as the
% typeID of the values.
% TypeID:
%   1:     'Luftschall'
%   2:     'Trittschall'

% First round the values to one decimal place
values = fix(10*values + 0.5)/10;

if typeID==1
    %% LUFTSCHALL
    % Calculate new Rw value
    [~,Rw] = Rw_rating(freq,values,1);
    
    % Calculate Rw + C_(100-3150) and Rw + Ctr_(100-3150)
    RwC = RwC_rating(freq,values,[100,3150]);
    RwCtr = RwCtr_rating(freq,values,[100,3150]);
    % Calculate Rw + C_50-3150 and Rw + Ctr_50-3150
    RwC_50_3150 =   RwC_rating(freq,values,[50,3150]);
    RwCtr_50_3150 = RwCtr_rating(freq,values,[50,3150]);
    % Calculate Rw + C_50-5000 and Rw + Ctr_50-5000
    RwC_50_5000 =   RwC_rating(freq,values,[50,5000]);
    RwCtr_50_5000 = RwCtr_rating(freq,values,[50,5000]);
    % Calculate Rw + C_100-5000 and Rw + Ctr_100-5000
    RwC_100_5000 =   RwC_rating(freq,values,[100,5000]);
    RwCtr_100_5000 = RwCtr_rating(freq,values,[100,5000]);
    
    % Round the Rw value for calculation of C-values
    RwRound = floor(Rw);

    % Calculate the C values using the RwRound value
    C = RwC - RwRound;
    Ctr = RwCtr - RwRound;
    C_50_3150 = RwC_50_3150 - RwRound;
    Ctr_50_3150 = RwCtr_50_3150 - RwRound;
    C_50_5000 = RwC_50_5000 - RwRound;
    Ctr_50_5000 = RwCtr_50_5000 - RwRound;
    C_100_5000 = RwC_100_5000 - RwRound;
    Ctr_100_5000 = RwCtr_100_5000 - RwRound;
    
    % Prepare the output structure
    out.SNR = Rw;
    out.C = [C, Ctr, C_50_3150, Ctr_50_3150, C_50_5000, Ctr_50_5000,...
             C_100_5000, Ctr_100_5000];
         
elseif typeID==2
    %% TRITTSCHALL
    % Calculate new Ln value
    [~,Lnw] = Lnw_rating(freq,values,1);
    
    % Calculate the new C_I and C_I-50-2500
    LnwCI = LnCI_rating(freq,values,[100,3150]);
    LnwCI_50_2500 = LnCI_rating(freq,values,[50,2500]);
    
    % Round Lnw value for calculation of C-values
    LnwRound = ceil(Lnw);
    
    % Calculate the CI and CI_50-2500 values using the LnwRound value
    CI = LnwCI - LnwRound;
    CI_50_2500 = LnwCI_50_2500 - LnwRound;
    
    % Prepare the output structure
    out.SNR = Lnw;
    out.C = [CI, CI_50_2500];
else
    %% UNKNOWN TYPE-ID
    error('TypeID not valid for this function!');
end

end