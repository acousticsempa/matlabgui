function [varargout] = Rw_rating(Freq, TL, dec)
% Determine Rw (SNR) from transmission losses (TL) according to ISO 717-1
% %%% INPUT %%%
% Freq:     Array (nx1) with measurement frequencies
% TL:       Array (mxn) with frequency dependant transmission losses in the
%           columns
% dec:      boolean to indicate whether the calculations should be
%           performed in decimal (0.1) steps as well
%
% %%% OUTPUT %%%
% varg(1):  Rw value in integer steps (1.0)
% varg(2):  Rw value in decimal steps (0.1), returned only if the dec flag
%           is set to 1

%% Input control
if numel(Freq) ~= size(TL,2)
    errordlg('Frequency does not match TL!','Error');
    return;
end

%% Initialize local variables
Ref_curve = [33, 36, 39, 42, 45, 48, 51, 52, 53, 54, 55, 56, 56, 56, 56, 56];

%% Perform the actual calculations
% Round data to single decimal
TL_round = fix(TL*10 + 0.5)/10;

% Find transmission losses for frequency range 100Hz to 3150 Hz
freqStart = 100;
freqEnd = 3150;
indxStart = find(Freq == freqStart);
indxEnd = find(Freq == freqEnd);

% Retrieve the value according to the selected frequency range
TL_100_3150 = TL_round(:,indxStart:indxEnd);
Freq_red = Freq(indxStart:indxEnd);

% Calculate the number of datasets being passed in
m = size(TL_100_3150, 1);

% Initialize arrays for calculations
Rw = NaN(m, 1);
Rw_dec = NaN(m, 1);

% Repeat operation for each row in the TL array
for i = 1 : m
    % Check if the number of NaN values in the current row of the TL array
    % is different than 16
    if sum(isnan(TL_100_3150(i,:))) ~= 16
        % Initilize the computations for the current row of the TL array
        % by shifting the reference curve by 100dB and initializing the 
        % sum of deviations to 50 and the shifter s to 0
        Ref_base = Ref_curve + 100;
        sum_deviations = 50;
        s = 0;
        
        % Iterate until the sum of deviations is lower or equal to 32
        while sum_deviations > 32
            % Update the shifter
            s = s + 1;
            % Shift reference spectrum by s dB
            Ref_shifted = Ref_base - s;
            
            % Calculate deviations and the sum of deviations
            deviations = TL_100_3150(i,:) - Ref_shifted;
            sum_deviations = -sum(deviations(deviations <= 0));
        end
        
        % Read the value of the shifted reference curve at the 500Hz frequency
        Rw(i) = Ref_shifted(8);
        
        % If the decimal option has been selected perform the reference
        % curve shifting for the last 1.1dB step
        if dec == 1
            % Move the shifter 1.1dB back
            s = s - 1.1;
            % Reset sum of deviations to enter while-loop
            sum_deviations = 50;
            
            % Iterate until the sum of deviations is lower or equal to 32
            while sum_deviations > 32
                % Update the shifter value
                s = s + 0.1;
                % Shift reference spectum by s dB
                Ref_shifted = Ref_base - s;
                
                % Calculate deviations and sum of deviations
                deviations = TL_100_3150(i,:) - Ref_shifted;
                sum_deviations = -sum(deviations(deviations <= 0));
            end
            
            % Read the value of the shifted reference curve at the 500Hz 
            % frequency
            Rw_dec(i) = round_dec(Ref_shifted(8), 1);
        end
    end
end

%% Prepare the output variables
% First the integer Rw value
varargout{1} = Rw;

% If the decimal flag has been selected return also the decimal Rw value
if dec == 1
    varargout{2} = Rw_dec;
end
end