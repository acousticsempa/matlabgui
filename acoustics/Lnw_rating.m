function [varargout] = Lnw_rating(Freq, Ln, dec)
% Determine Lnw (SNR) from transmission losses (TL) according to ISO 717-2
% %%% INPUT %%%
% Freq:     Array (nx1) with measurement frequencies
% Ln:       Array (mxn) with frequency dependant transmission losses in the
%           columns
% dec:      boolean to indicate whether the calculations should be
%           performed in decimal (0.1) steps as well
%
% %%% OUTPUT %%%
% varg(1):  Lnw value in integer steps (1.0)
% varg(2):  Lnw value in decimal steps (0.1), returned only if the dec flag
%           is set to 1

%% Input control
if length(Freq) ~= size(Ln,2)
    errordlg('Frequency vector does not match size of Ln-vector!','Error');
    return;
end

%% Initialize local variables
Ref_curve = [62, 62, 62, 62, 62, 62, 61, 60, 59, 58, 57, 54, 51, 48, 45, 42];

%% Perform the actual calculations
% Round data to single decimal
Ln_round = fix(Ln*10 + 0.5)/10;

% Find transmission losses (TL) from a given frequency range
freqStart = 100;
freqEnd = 3150;
indxStart = find(Freq == freqStart);
indxEnd = find(Freq == freqEnd);

% Retrieve the values according to the frequency range selected
Ln_100_3150 = Ln_round(:,indxStart:indxEnd);
Freq_red = Freq(indxStart:indxEnd);

% Calculate the number of datasets being passed in
m = size(Ln_100_3150, 1);

% Initialize arrays for calculations
Lnw = NaN(m, 1);
Lnw_dec = NaN(m, 1);

% Repeat operation for each row in Ln
for i = 1:m
    % Check if the number of NaN values in the current row of the Ln array
    % is different than 16
    if sum(isnan(Ln_100_3150(i,:)))~=16        
        % Initilize the computations for the current row of the Ln array
        % by shifting the reference curve by 62dB and initializing the 
        % sum of deviations to 50 and the shifter s to 0   
        Ref_base = Ref_curve - 62;
        sum_deviations = 50;
        s = 0;
        
        % Iterate until the sum of deviations is lower or equal to 32
        while sum_deviations > 32   
            % Update the shifter
            s = s + 1;
            % Shift reference spectrum by s dB
            Ref_shifted = Ref_base + s;    
            
            % Calculate deviations and sum of deviations
            deviations = Ln_100_3150(i,:) - Ref_shifted;
            sum_deviations = sum(deviations(deviations > 0));
        end    
        
        % Read the value of the shifted reference curve at the 500Hz frequency
        Lnw(i) = Ref_shifted(8);
        
        % If the decimal option has been selected perform the reference
        % curve shifting for the last 1.1dB step
        if dec == 1            
            % Move the shifter 1.1dB back
            s = s - 1.1;
            % Reset sum of deviations to enter while-loop
            sum_deviations = 50;
            
            % Iterate until the sum of deviations is lower or equal to 32
            while sum_deviations > 32
                % Update the shifter value
                s = s + 0.1;
                % Shift reference spectum by s dB
                Ref_shifted = Ref_base + s; 
                
                % Calculate deviations and sum of deviations
                deviations = Ln_100_3150(i,:) - Ref_shifted;
                sum_deviations = sum(deviations(deviations > 0));
            end
           
            % Read the value of the shifted reference curve at the 500Hz 
            % frequency
            Lnw_dec(i) = Ref_shifted(8);            
        end        
    end
end

%% Prepare the output variables
% First the integer Lnw value
varargout{1} = Lnw;

% If the decimal flag has been selected return also the decimal Lnw value
if dec == 1
    varargout{2} = Lnw_dec;
end
end