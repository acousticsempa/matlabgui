function [RwC] = RwC_rating(Freq, TL, freqRange)

% Determine R_w + C_freqRange  from transmission losses (TL) according to 
% ISO 717-1 for 50-3150Hz, 50-5000Hz, 100-3150Hz, 100-5000Hz
% %% INPUTS %%
% Freq:      Array with the measurement frequencies [nx1]
% TL:        Array with frquency dependent transmission losses [mxn]
% freqRange: Array with the frequency range  
% 
% %% OUTPUT %%
% RwCtr:     Array [mx1] with R_w + C_freqRange values for each dataset 
%            in TL

%% Input control
if numel(Freq) ~= size(TL,2)
    errordlg('Frequency does not match TL!','Error','modal');
    return;
end

%% Define use cases
freqRange = [min(freqRange),max(freqRange)];
% Switch cases
if prod(freqRange == [50,3150])
    % Use case 1
    freqRangeCase = 1;
elseif prod(freqRange == [50,5000])
    % Use case 2
    freqRangeCase = 2;
elseif prod(freqRange == [100,3150])
    % Use case 3
    freqRangeCase = 3;
elseif prod(freqRange == [100,5000])
    % Use case 4
    freqRangeCase = 4;
else
    % Use case unknown, throw error
    errordlg('Frequency range not implemented in this function!','Error','modal');
    return;
end

%% Define local variables (switch cases)
switch freqRangeCase
    case 1
        % Frequency range 50-3150 Hz
        L_ij = -[40, 36, 33, 29, 26, 23, 21, 19, 17, 15, 13, 12, 11, 10,...
                  9,  9,  9,  9,  9];
        freqStart = 50;
        freqEnd = 3150;
    case 2
        % Frequency range 50-5000 Hz
        L_ij = -[41, 37, 34, 30, 27, 24, 22, 20, 18, 16, 14, 13, 12, 11,...
                 10, 10, 10, 10, 10, 10, 10];
        freqStart = 50;
        freqEnd = 5000;
    case 3
        % Frequency range 100-3150 Hz
        L_ij = -[29, 26, 23, 21, 19, 17, 15, 13, 12, 11, 10, 9, 9, 9, 9, 9];
        freqStart = 100;
        freqEnd = 3150;
    case 4
        % Frequency range 100-5000 Hz
        L_ij = -[30, 27, 24, 22, 20, 18, 16, 14, 13, 12, 11, 10, 10, 10,...
                 10, 10, 10, 10];
        freqStart = 100;
        freqEnd = 5000;
end            

%% Perform the actual calculations
% Round data to single decimal
TL_round = fix(TL*10 + 0.5)/10;

% Find index corresponding to certain a certain frequency range
indxStart = find(Freq == freqStart);
indxEnd = find(Freq == freqEnd);

% Retrieve the transmission losses for the given frequency range
TL = TL_round(:,indxStart:indxEnd);

% Retrieve the number of datasets that are being analyzed
m = size(TL, 1);

% Match the matrix dimensions of Lij to match the dimensions of
% TL by repeating the matrix vertically
L_ij_m = repmat(L_ij, m, 1);

% Calculate the Rw_CfreqRange value
RwC = -10*log10(nansum(10.^((L_ij_m - TL)/10), 2));

% Round output value to integer acc. to ISO 717
RwC = fix(RwC + 0.5);

end