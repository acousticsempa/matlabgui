function selPrufObj(src,callbackdata)

% Function is called when the user selects one of the elements in the
% PrufObj dropdown menu

%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global dropLists

%%%%%%%%%%%%%%%% RESET SUBURDINATE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%
dd = findobj('Tag','ShieldingDD');
dd.Value = 1;
dd = findobj('Tag','ReportDD');
dd.Value = 1;

% Check if the SelectedIDs fields have been initialized
if ~isfield(dropLists.PrufObj,'SelectedIDs')
    errordlg('Please select a Project first!','Selection Error','modal');
    return
end

%%%%%%%%%%%%%%% PREPARE THE SHIELDING AND REPORT DD %%%%%%%%%%%%%%%%%%
% Retrieve the value that has been selected in the project dropdown
% menu and in this dropdown menu
dd = findobj('Tag','ProjectDD');
selectedProject = dd.Value;
selectedPrufObj = dropLists.PrufObj.SelectedIDs(src.Value);

% Find the datasets that have as Project selectedProject and as
% PrufObj selectedPrufObj
bool = and(dropLists.Project.ID==selectedProject, ...
           dropLists.PrufObj.ID==selectedPrufObj);

%%%%%%%%%%%%%% UPDATE THE Shielding DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated Shielding IDs
indShielding = dropLists.Shielding.ID(bool);
indShielding = unique(indShielding);

% Update the Shielding dropdown with the list of unique Shielding that
% have selectedProject as Project and selectedPrufObj as PrufObj
dd = findobj('Tag','ShieldingDD');
dd.String = dropLists.Shielding.Unique(indShielding);

% Store this choice of indexes in the dropLists structed
dropLists.Shielding.SelectedIDs = indShielding;

%%%%%%%%%%%%%% UPDATE THE Report DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated Report IDs
indReport = dropLists.Report.ID(bool);
indReport = unique(indReport);

% Update the Report dropdown with the list of unique Reports that
% have selectedProject as Project and selectedPrufObj as PrufObj
dd = findobj('Tag','ReportDD');
dd.String = dropLists.Report.Unique(indReport);

% Store this choice of indexes in the dropLists structed
dropLists.Report.SelectedIDs = indReport;

%%%%%%%%%%%% UPDATE THE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pr = findobj('Tag','TestDescriptionTXT');
pr.String = ' ';
pr = findobj('Tag','TestObjectTXT');
pr.String = ' ';
end