function selShielding(src,callbackdata)

% Function is called when the user selects one of the elements in the
% Shielding dropdown menu

%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global dropLists

%%%%%%%%%%%%%%%% RESET SUBURDINATE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dd = findobj('Tag','ReportDD');
dd.Value = 1;

% Check if the SelectedIDs fields have been initialized
if ~isfield(dropLists.PrufObj,'SelectedIDs') || ...
   ~isfield(dropLists.Shielding,'SelectedIDs')
    errordlg('Please select a Project and Prufobjekt-Nummer first!',...
             'Selection Error','modal');
    return
end

%%%%%%%%%%%%%%% PREPARE THE REPORT DD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the value that has been selected in the project dropdown
% menu, the prufobj menu and in this dropdown menu
dd = findobj('Tag','ProjectDD');
selectedProject = dd.Value;
dd = findobj('Tag','PrufObjDD');
selectedPrufObj = dropLists.PrufObj.SelectedIDs(dd.Value);
selectedShield = dropLists.Shielding.SelectedIDs(src.Value);

% Find the datasets that have as Project selectedProject, as
% PrufObj selectedPrufObj and as Shielding selectedShielding
bool = and(and(dropLists.Project.ID==selectedProject, ...
               dropLists.PrufObj.ID==selectedPrufObj),...
               dropLists.Shielding.ID==selectedShield);

%%%%%%%%%%%%%% UPDATE THE Report DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated Report IDs
indReport = dropLists.Report.ID(bool);
indReport = unique(indReport);

% Update the Report dropdown with the list of unique Reports that
% have selectedProject as Project and selectedPrufObj as PrufObj
dd = findobj('Tag','ReportDD');
dd.String = dropLists.Report.Unique(indReport);

% Store this choice of indexes in the dropLists structed
dropLists.Report.SelectedIDs = indReport;

%%%%%%%%%%%% UPDATE THE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pr = findobj('Tag','TestDescriptionTXT');
pr.String = ' ';
pr = findobj('Tag','TestObjectTXT');
pr.String = ' ';
end