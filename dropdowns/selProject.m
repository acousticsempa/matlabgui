function selProject(src,callbackdata)

% Function is called when the user selects one of the elements in the
% Project dropdown menu

%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global dropLists

%%%%%%%%%%%%%%%% RESET THE SUBORDINATE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%
dd = findobj('Tag','PrufObjDD');
dd.Value = 1;
dd = findobj('Tag','ShieldingDD');
dd.Value = 1; dd.String = ' ';
dd = findobj('Tag','ReportDD');
dd.Value = 1;

% Retrieve the Project value selected in the menu
selectedProject = src.Value;
% Find which datasets that have as Project the selectedProject
boolProject = (dropLists.Project.ID == selectedProject);

%%%%%%%%%%%%%% UPDATE THE PrufObj DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated PrufObjs IDs
indPrufObj = dropLists.PrufObj.ID(boolProject);
indPrufObj = unique(indPrufObj);

% Update the PrufObj dropdown with the list of unique PrufObjects that 
% have selectedProject as Project
dd = findobj('Tag','PrufObjDD');
dd.String = dropLists.PrufObj.Unique(indPrufObj);

% Store this choice of IDs selected for the PrufObj DD in dropLists
dropLists.PrufObj.SelectedIDs = indPrufObj;

%%%%%%%%%%%%%% UPDATE THE Report DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated Report IDs
indReport = dropLists.Report.ID(boolProject);
indReport = unique(indReport);

% Update the Report dropdown with the list of reports that have as Project
% selectedProject
dd = findobj('Tag','ReportDD');
dd.String = dropLists.Report.Unique(indReport);

% Store the choice of IDs selected for the report DD in dropLists
dropLists.Report.SelectedIDs = indReport;

%%%%%%%%%%%% UPDATE THE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pr = findobj('Tag','TestDescriptionTXT');
pr.String = ' ';
pr = findobj('Tag','TestObjectTXT');
pr.String = ' ';
end