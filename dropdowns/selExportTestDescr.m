function selExportTestDescr(src,~)

% Function is called when the user selects one of the elements in the
% Test description dropdown export menu in the fExportOriginalDB figure

% Global variables 
global fExportOriginalDB
global ExportOriginalDBData

%%%%%%%%%%%%%%%% RESET THE SUBORDINATE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the index of the selected item in the dropdown menu
selTestDescr = src.Value;

% Retrieve the corresponding test description
str = ExportOriginalDBData.TestDescrDD{selTestDescr,1};

% Retrieve the text menu
txt = findobj('Parent',fExportOriginalDB,'Tag','ExportTestDescrTXT');

% Copy the text over to the text menu
txt.String = str;

end