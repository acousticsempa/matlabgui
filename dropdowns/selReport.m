function selReport(src,~)
% Function is called when the user selects one of the elements in the
% Report dropdown menu

%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global dropLists

% Check if the SelectedIDs fields have been initialized
if ~isfield(dropLists.Report,'SelectedIDs')
    errordlg('Please select a Project first!','Selection Error','modal');
    return
end

%%%%%%%%%%%%%%%% RETRIEVE REPORT ID %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out which report has been selected
selectedReport = dropLists.Report.SelectedIDs(src.Value);

% Find the datasets that have as Report selectedReport
bool = (dropLists.Report.ID==selectedReport);

% Check that the selected report is unique
if sum(bool)~= 1
    errordlg('Multiple reports selected! Probable error in database!',...
             'Multiple Selection Error','modal');
    return
end

%%%%%%%%%%%%%%% RETRIEVE CORRESPONDING DATATYPE, PROJECT, %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% PRUFOBJ AND SHIELDING                     %%%%%%%%%%%%%%%%
% Datatype is not necessary to be retrieved, since the dropLists structure
% changes automatically when Datatype is changed
% Retrieve the associated Project
selectedProject = dropLists.Project.ID(bool);
% Retrieve the associated PrufObj
selectedPrufObj = dropLists.PrufObj.ID(bool);
% Retrieve the associated Shielding
selectedShielding = dropLists.Shielding.ID(bool);
% Find which datasets have as Project the selectedProject
boolProject = (dropLists.Project.ID == selectedProject);
% Find which datasets have as PrufObj the selectedPrufObj
boolPrufObj = (dropLists.PrufObj.ID == selectedPrufObj);

%%%%%%%%%%%%%%%%%% UPDATE THE Project DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the position at which the Project DropDown Menu has to be set
dd = findobj('Tag','ProjectDD');
dd.Value = selectedProject;

%%%%%%%%%%%%%%%%%% UPDATE THE PrufObj DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated PrufObjs IDs
indPrufObj = dropLists.PrufObj.ID(boolProject);
indPrufObj = unique(indPrufObj);

% Update the PrufObj dropdown with the list of unique PrufObjects that
% have selectedProject as Project
dd = findobj('Tag','PrufObjDD');
dd.String = dropLists.PrufObj.Unique(indPrufObj);

% Store this choice of IDs selected for the PrufObj DD in dropLists
dropLists.PrufObj.SelectedIDs = indPrufObj;

% Update the value of the PrufObj dropdown to reflect the selection
dd.Value = find(indPrufObj==selectedPrufObj);

%%%%%%%%%%%%%%%% UPDATE THE Shielding DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the associated Shielding IDs that have the selectedPrufObj as
% PrufObj and the selectedProject as Project
indShielding = dropLists.Shielding.ID(and(boolProject,boolPrufObj));
indShielding = unique(indShielding);

% Update the Shielding dropdown with the list of unique Shieldings that
% have selectedProject as Project and selectedPrufObj as PrufObj
dd = findobj('Tag','ShieldingDD');
dd.String = dropLists.Shielding.Unique(indShielding);

% Store this choice of IDs selected for the Shielding DD in dropLists
dropLists.Shielding.SelectedIDs = indShielding;

% Update the value of the Shielding dropdown to reflect the selection
dd.Value = find(indShielding==selectedShielding);

%%%%%%%%%%%%%%%% UPDATE THE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pr = findobj('Tag','TestDescriptionTXT');
pr.String = dropLists.Description(bool);
pr = findobj('Tag','TestObjectTXT');
pr.String = dropLists.Object(bool);
end