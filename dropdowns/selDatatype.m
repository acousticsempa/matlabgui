function selDatatype(src,callbackdata)

% Function is called when the user selects one of the elements in the
% Project dropdown menu

%%%%%%%%%%%%%%%% Reset the dropdowns below it %%%%%%%%%%%%%%%%%%%%%%%%%%%
dd = findobj('Tag','ProjectDD');
dd.Value = 1;
dd = findobj('Tag','PrufObjDD');
dd.Value = 1; dd.String = ' ';
dd = findobj('Tag','ShieldingDD');
dd.Value = 1; dd.String = ' ';
dd = findobj('Tag','ReportDD');
dd.Value = 1; dd.String = ' ';

%%%%%% Retrieve the value that has been selected in this dropdown menu %%%
datatyp = src.Value;

%%%%%%%%%%%%%%%%% RETRIEVE THE CORRECT DATASET BASED ON THE SELECTION %%%
if datatyp == 1
    % We have selected airborne, so retrieve this dataset
    popDropdowns('Airborne');
elseif datatyp == 2
    % We have selected 'Impact', so retrieve this dataset
    popDropdowns('Impact');
elseif datatyp == 3
    % We have selected 'Airborne Averaged', so retrieve this dataset
    popDropdowns('AirborneAverage');
else
    % Some error has occurred
    errordlg('Some error has occurred in the selection of the datatype!',...
             'Datatype Error','modal');
    return;
end

%%%%%%%%%%%%%%%%% RESET THE PREVIEW ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
pr = findobj('Tag','TestDescriptionTXT');
pr.String = ' ';
pr = findobj('Tag','TestObjectTXT');
pr.String = ' ';

end