function popDropdowns(str)
% Function populates the dropdown menus based on what has been selected in
% the dropdown menus
% 
% The following dropdown menus are available:
% dropdowns{1}:  Project-dropdown, allows the user to select the project
%                that he is looking for.
%                Tag: ProjectDropDown
% dropdowns{2}:  Prufobject-dropdown, allows the user to select the 
%                Prufobj from the prufobjects available under the selected
%                project
%                Tag: PrufObjDropDown
% dropdowns{3}:  Shielding-dropdown, allows the user to select the 
%                shielding conditions from the available shielding
%                conditions under the selected project and prufobj
%                Tag: ShieldingDropDown
% dropdowns{4}:  Berichte-dropdown, allows the user to select the 
%                Berichte from the available Berichte's under the selected
%                project, prufobj and shielding conditions

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global Data
global dropLists

%%%%%%%%%%%%%%%%%%% SELECT DATABASE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmpi(str,'Airborne')
    % Select the airborne data
    DB = Data.Air;
elseif strcmpi(str,'Impact')
    % Select the impact data
    DB = Data.Impact;
elseif strcmpi(str,'AirborneAverage')
    % Select teh airborne average data
    DB = Data.AirAVG;
end

% Check if the dropLists has a field 'Datatype' and which value it has %%%
% 'Airborne' or 'Impact' 
if isempty(dropLists) || ~isfield(dropLists,'Datatype') || ...
                         ~strcmpi(dropLists.Datatype,str)
    % IF: 
    % the dropLists structure is empty OR,
    % the dropLists structure has no 'Datatype' field OR,
    % the dropLists Datatype field is not equal to the input str
    % THEN we need to recompute the dropLists structure
   
    % Compute the unique lists for projects
    [dropLists.Project.ID, dropLists.Project.Unique] = getUniques(DB.Project);
    
    % Compute the unique lists for PrufObjs
    [dropLists.PrufObj.ID, dropLists.PrufObj.Unique] = getUniques(DB.PrufObj);
    
    % Compute the unique list for Shielding conditions
    [dropLists.Shielding.ID, dropLists.Shielding.Unique, ....
     dropLists.Shielding.UniqueSetting] = getUniqueShielding(DB.Shielding);
    
    % Compute the unique list for Reports
    [dropLists.Report.ID, dropLists.Report.Unique] = getUniques(DB.Report(:,1));
    
    % Attach to dropLists the test description and the test object
    dropLists.Description = DB.Test.Description;
    dropLists.Object = DB.Test.Object;

    % Add the datatype field
    dropLists.Datatype = str;
end

%%%%%%%%%%%%%%%%%%%%% POPULATE THE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the unique projects and update the string of the dropdown menu
dd = findobj('Tag','ProjectDD');
dd.String = dropLists.Project.Unique;
dd.Value = 1;

% Get the unique reports from all the report IDs and update the string of 
% the dropdown menu
dd = findobj('Tag','ReportDD');
indReport = unique(dropLists.Report.ID);
dd.String = dropLists.Report.Unique(indReport);
% Store this choice of indexes in the dropLists structure
dropLists.Report.SelectedIDs = indReport;


%%%%%%%%%%%%%%%% REMOVE THE SELECTED IDs FIELDS %%%%%%%%%%%%%%%%%%%%%%
% Check first if the field exists
subfields = {'PrufObj','Shielding'};
% Iterate over the subfields
for i=1:numel(subfields)
    % Retrieve the structure
    str = dropLists.(subfields{i});
    % Check if it has the SelectedIDs field
    if isfield(str,'SelectedIDs')
        str = rmfield(str,'SelectedIDs');
        dropLists.(subfields{i}) = str;
    end
end

end




