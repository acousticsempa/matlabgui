function ViewDatasetsExportUbertragung(src, ~)

% Function is responsible for transitioning between the figure fViewDatasets
% and the figure fExportUbertragung
% The function is only invoked on the ViewDatasets figure

%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fExportUbertragung

% Create shortcut
f = fExportUbertragung;
p = src.Parent;

% Center the Export Window
f.Position = withinScreen(centerOnBox(p.Position,f.Position));
% Feed the data into the figure
passed = fExportUbertragung_Populate(p);

% Show the Export figure only if we passed
if passed
    % Show the figure
    f.Visible = 'on';
end

end