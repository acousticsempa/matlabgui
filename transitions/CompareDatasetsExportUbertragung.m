function CompareDatasetsExportUbertragung(src,~)

% Function takes care of transitioning between the figure fCompareDatasets
% and the figure fExportUbertragung

% Global variables
global fCompareDatasets
global fExportUbertragung

% Center the fExportUbertragung window on the fCompareDatasets window
f = fCompareDatasets.Position;
p = fExportUbertragung.Position;
fExportUbertragung.Position = withinScreen(centerOnBox(f,p));

% Find the tabgroup
tabgr = findobj('Parent',fCompareDatasets,'Tag','topCompTAB');

% Check the tag of the source
if strcmpi(src.Tag,'ExportAvgUberBUT')
    % Call originate from Averaging Tab
    tab = findobj(tabgr.Children,'Tag','AveragingTAB');
elseif strcmpi(src.Tag,'ExportEnergyDataBUT')
    % Call originated from Energy Tab
    tab = findobj(tabgr.Children,'Tag','EnergyTAB');
end

% Populate the fExportUbertragung window
fExportUbertragung_Populate(tab);

% Show the ExportUbertragung window
fExportUbertragung.Visible = 'on';

end