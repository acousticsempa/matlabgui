function StartCreator(src,~)

% Function takes care on transitioning between the Start figure and the 
% CreateKnoten, CreateBauteil and CreateVorsatz figure, 
% the Start figure is always visible

% Global variables
global fCreateKnoten
global fCreateBauteil
global fCreateVorsatz

% Check which figure called the function and decide to hide/show the
% CreateKnoten, CreateBauteil, CreateVorsatz figure depending on the caller
switch src.Tag
    % Create Knoten Window
    case 'CreateKnotenBUT'
        fCreateKnoten_Populate(src);
        fCreateKnoten.Visible = 'on';
    
    % Create Bauteil Window
    case 'CreateBauteilBUT'
        fCreateBauteil.Visible = 'on';
        
    % Create Vorsatz window
    case 'CreateVorsatzBUT'
        fCreateVorsatz_Populate(src);
        fCreateVorsatz.Visible = 'on';
end

end