function CompareDatasetsExportOriginalDB(~,~)

% Function takes care of transitioning between the figure fCompareDatasets
% and the figure fExportOriginalDB

% Global variables
global fCompareDatasets
global fExportOriginalDB
global CompareDatasetsData

% Check that the averaged data exported to the original database is
% originating by averaging only two datasets
if size(CompareDatasetsData.Average.IDs,1)~=2
    % Throw error and return
    errordlg(['Exporting of averaged data to the original database',...
             ' is only possible when averaging only 2 datasets!'],...
             'Too many datasets for averaging selected','modal');
    return;
end

% Check if all the datasets are Airborne datasets
if any(CompareDatasetsData.Average.IDs(:,1)~=1)
    % Throw error
    errordlg(['Exporting of averaged data to the original database',...
              ' is only possible for Airborne Datasets!'],...
              'Non-Airborne datasets error','modal');
    return;
end    

% Center the fExportOriginalDB window on the fCompareDatasets window
f = fCompareDatasets.Position;
p = fExportOriginalDB.Position;
fExportOriginalDB.Position = withinScreen(centerOnBox(f,p));

% Populate the fExportOriginalDB window
fExportOriginalDB_Populate();

% Show the ExportOriginalDB window
fExportOriginalDB.Visible = 'on';

end