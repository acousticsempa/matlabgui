function StartViewer(src,~)

% Function takes care on transitioning between the Start figure and the 
% fParking figure with the different Knoten Panels, 
% the Start figure is always visible

% Figures and panels
global fStart
global fParking
global pKnoten
global pBauteil
global pUbertragung
global pVorsatz
global pVUbertragung

% First hide all panels
pKnoten.Visible = 'off';
pBauteil.Visible = 'off';
pVorsatz.Visible = 'off';
pUbertragung.Visible = 'off';
pVUbertragung.Visible = 'off';

% Get all the panel positions
pK = pKnoten.Position;
pB = pBauteil.Position;
pV = pVorsatz.Position;
pU = pUbertragung.Position;
pVU = pVUbertragung.Position;

% Set all the left / bottom to 1
pK(1:2) = 1;
pB(1:2) = 1;
pV(1:2) = 1;
pU(1:2) = 1;
pVU(1:2) = 1;

% Calculate some position shortcuts
pKB = [1,1,pK(3)+pB(3),max(pK(4),pB(4))];
pBV = [1,1,pB(3)+pV(3),max(pB(4),pV(4))];
pKBV = [1,1,max(pBV(3),pK(3)),pBV(4)+pK(4)];
pKBU = [1,1,max(pKB(3),pU(3)),pKB(4)+pU(4)];
pKBVVU = [1,1,max([pK(3),pBV(3),pVU(3)]),pK(4)+pBV(4)+pVU(4)];

% Get all the checkboxes in the figure
obj = findobj('Parent',fStart,'Style','checkbox');

% Check which button called the transition function and hide / show the
% correct panels in the correct position
% Define which panel to populate and populate it
switch src.Tag
    case 'ViewKnotenBUT'
        n = 'Viewer - Knoten';
        pKnoten.Visible = 'on';
        p = [1,1,pK(3),pK(4)];
        fParking.Position = p;
        pKnoten.Position = p;
        pKnoten_Populate(src);
        
    case 'ViewBauteilBUT'
        n = 'Viewer - Bauteil';
        % Get the value of the checkbox
        c = get(findobj(obj,'Tag','BauteilCHE'),'Value');
        if c
            pKnoten.Visible = 'on';
            pBauteil.Visible = 'on';
            fParking.Position = pKB;
            pKnoten.Position =  [      1, pKB(4)-pK(4)+1,  pK(3),pK(4)];
            pBauteil.Position = [1+pK(3), pKB(4)-pB(4)+1,  pB(3),pB(4)];
            pKnoten_Populate(src);
        else
            pBauteil.Visible = 'on';
            fParking.Position = pB;
            pBauteil.Position = pB;
            pBauteil_Populate(src);
        end
        
    case 'ViewVorsatzBUT'
        n = 'Viewer - Vorsatz';
        % Get the value of the checkbox
        c = get(findobj(obj,'Tag','VorsatzCHE'),'Value');
        if c
            pKnoten.Visible = 'on';
            pBauteil.Visible = 'on';
            pVorsatz.Visible = 'on';
            fParking.Position = pKBV;
            pKnoten.Position =  [      1, pKBV(4)-pK(4)+1,   pK(3),pK(4)];
            pBauteil.Position = [      1, pBV(4)-pB(4)+1,    pB(3),pB(4)];
            pVorsatz.Position = [1+pB(3), pBV(4)-pV(4)+1,    pV(3),pV(4)];
            pKnoten_Populate(src);
        else
            pVorsatz.Visible = 'on';
            fParking.Position = pV;
            pVorsatz.Position = pV;
            pVorsatz_Populate(src);
        end
        
    case 'ViewUbertragungBUT'
        n = 'Viewer - Ubertragung';
        % Get the value of the checkbox
        c = get(findobj(obj,'Tag','UbertragungCHE'),'Value');
        if c
            pKnoten.Visible = 'on';
            pBauteil.Visible = 'on';
            pUbertragung.Visible = 'on';
            fParking.Position = pKBU;
            pKnoten.Position = [       1, pKBU(4)-pK(4)+1,   pK(3),pK(4)];
            pBauteil.Position = [1+pK(3), pKBU(4)-pB(4)+1,   pB(3),pB(4)];
            pUbertragung.Position = pU;
            pKnoten_Populate(src);
        else
            pUbertragung.Visible = 'on';
            fParking.Position = pU;
            pUbertragung.Position = pU;
            pUbertragung_Populate(src);
        end
        
    case 'ViewVUbertragungBUT'
        n = 'Viewer - Vorsatz-Ubertragung';
        % Get the value of the checkbox
        c = get(findobj(obj,'Tag','VUbertragungCHE'),'Value');
        if c
            pKnoten.Visible = 'on';
            pBauteil.Visible = 'on';
            pVorsatz.Visible = 'on';
            pVUbertragung.Visible = 'on';
            fParking.Position = pKBVVU;
            pKnoten.Position = [1,pKBVVU(4)-pK(4)+1,pK(3),pK(4)];
            pBauteil.Position = [1,pKBVVU(4)-pK(4)-pB(4)+1,pB(3),pB(4)];
            pVorsatz.Position = [1+pB(3),pKBVVU(4)-pK(4)-pV(4)+1,pV(3),pV(4)];
            pVUbertragung.Position = pVU;
            pKnoten_Populate(src);
        else
            pVUbertragung.Visible = 'on';
            fParking.Position = pVU;
            pVUbertragung.Position = pVU;
            pVUbertragung_Populate(src);
        end
end

% Change the name of the figure
fParking.Name = n;

% Center figure on its current position
fParking.Position = centerOnBox(fStart.Position,fParking.Position);
% Check that we are within the screen
fParking.Position = withinScreen(fParking.Position);

% Make the figure and the panel visible
fParking.Visible = 'on';

end