function StartViewDatasets(src,~)

% Function takes care on transitioning between the Start figure and the 
% ViewDatasets figure, the Start figure is always visible

% Global variables
global fStart
global fViewDatasets

% Check which figure called the function and decide to hide/show the
% ViewDatasets figure depending on the caller
if src.Parent==fStart
    % Show the ViewDatasets figure
    fViewDatasets.Visible = 'on';
else
    fViewDatasets.Visible = 'off';
end

return; 
end