function CompareDatasetsExportVUbertragung(~,~)

% Function takes care of transitioning between the figure fCompareDatasets
% and the figure fExportVUbertragung

% Global variables
global fCompareDatasets
global fExportVUbertragung

% Center the fExportVUbertragung window on the fCompareDatasets window
f = fCompareDatasets.Position;
p = fExportVUbertragung.Position;
fExportVUbertragung.Position = withinScreen(centerOnBox(f,p));

% Populate the fExportVUbertragung window
fExportVUbertragung_Populate();

% Show the ExportVUbertragung window
fExportVUbertragung.Visible = 'on';

end