function StartCalcSituation(src,~)

% Function takes care on transitioning between the Start figure and the 
% CalcSituation figure, the Start figure is always visible

% Global variables
global fCalcVertSituation
global fCalcHorizSituation

% Check which figure called the function and decide to hide/show the
% CalcVertSituation figure depending on the caller
if strcmpi(src.Tag,'CalcVertSituationBUT')
    % Initialize figure
    fCalcVertSituation_Populate(src);
    % Show the CalcVertSituation figure
    fCalcVertSituation.Visible = 'on';
elseif strcmpi(src.Tag,'CalcHorizSituationBUT')
    % Initialize figure
    fCalcHorizSituation_Populate(src);
    % Show the CalcHorizSituation figure
    fCalcHorizSituation.Visible = 'on';
else
    % Hide both the Vert and Horiz situation calculations
    fCalcVertSituation.Visible = 'off';
    fCalcHorizSituation.Visible = 'off';
end

return; 
end