function ViewDatasetsCompareDatasets(src, ~)

% Function is responsible for transitioning between the figure fViewDatasets
% and the figure fCompareDatasets
% It automatically recognizes in which figure it has been called and
% transitions to the other one

%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fViewDatasets
global fCompareDatasets

% Data
global DatasetSelection

%%%%%%%%% Check which figure invoked the function %%%%%%%%%%%%%%%%%%%%%
% If the parent of the button is fViewDatasets, we switch to
% fCompareDatasets
if (src.Parent==fViewDatasets)
    % Get the position of the other figure and copy it to the new figure
    fCompareDatasets.OuterPosition = fViewDatasets.OuterPosition;
    
    % Show fCompareDatasets and hide fViewDatasets
    fCompareDatasets.Visible = 'on';
    
    % Retrieve the list of reports that has been selected
    lst = getReports(DatasetSelection);
    % Change the list in the list viewer
    lstH = findobj('Tag','SelDatasetsLST');
    if numel(lst)==0
        % Generate an empty listbox
        lstH.String = ' ';
        lstH.Max = 1;
        lstH.Value = 1;
    else
        % Place the string array, augment the maximum selection to the
        % maximum number and select the first value
        lstH.String = lst;
        lstH.Max = numel(lst);
        lstH.Value = 1;
    end
    % Hide the previous window
    fViewDatasets.Visible = 'off';
    
else
    % Get the position of the other figure and copy it to the new figure
    fViewDatasets.OuterPosition = fCompareDatasets.OuterPosition;
    
    % Show fViewDatasets and hide fComapareDatasets
    fViewDatasets.Visible = 'on';
    fCompareDatasets.Visible = 'off';
end

end