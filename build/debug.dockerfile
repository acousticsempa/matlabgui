# Docker Container for testing Bitbucket Pipelines
#
# ####################### BUILD IMAGE #############################
# Build this docker image by:
# cd into the directory
# docker build -t ImageName:Version -f file.dockerfile .
#
# ####################### RUN CONTAINER #############################
# Run the image by:
# docker run -it --name ContainerName ImageName:Version
#
# ####################### QUIT CONTAINER #########################
# Quit container by typing:
# exit
# in the shell

# Build on the alpine image
FROM alpine:latest

# Copy only the debug.sh script into the image
COPY /build/debug.sh /

# 1.) Install the bash shell
# 2.) Install the debugging tools
# 3.) Remove the script for installing the debugging tools
RUN apk add --update -f bash \
 && bash debug.sh \
 && rm -f debug.sh