#!/bin/bash
# Function takes care of what needs to be done in the build pipeline for the
# master branch
#
# Update the Alpine system curl and zip utilities
apk add --update --no-cache -f zip curl

# Run the zip command to zip the installer files
zip -j Installer-LeichtbauPrufstand.zip installer/* --exclude *.ico