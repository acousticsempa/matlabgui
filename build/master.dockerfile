# Docker Container for testing Bitbucket Pipeline for master branch
#
# ####################### BUILD IMAGE #############################
# Build this docker image by:
# cd into the directory
# docker build -t ImageName:Version -f file.dockerfile .
#
# ####################### RUN CONTAINER #############################
# Run the image by:
# docker run -it --name ContainerName ImageName:Version
#
# ####################### QUIT CONTAINER #########################
# Quit container by typing:
# exit
# in the shell

# Retrieve the correct docker image
FROM debug:latest

# Copy only the relevant the source code into the container
COPY /installer /installer
COPY /build /build

# 1) Remove the top directory copy of the .dockerfile
# 2) Run the build script for the master branch
RUN rm -f master.dockerfile \
 && bash build/master.sh

# Run the master pipeline script and create the shell entry point
ENTRYPOINT ["/bin/sh"]
