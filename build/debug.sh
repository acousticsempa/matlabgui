#!/bin/bash
# File takes care of downloading editors and other programs necessary for
# debugging and testing the application on the developers computer
# These operations will not be performed for the production container
#
# Download bash git curl and editors
apk add --update --no-cache -f git nano vim

# Retrieve the version of the vim editor
VIM_VERSION="$(ls /usr/share/vim/)"
VIM_COLORS="usr/share/vim/$VIM_VERSION/colors/"
GIT_FOLDER="VIMColorscheme"

# ########### Download and copy colorschemes colorschemes #####################
# Declare an associative array to store the colorschemes
declare -A URL
URL[cobalt]="https://github.com/gkjgh/cobalt.git"
#URL[afterglow]="https://github.com/danilo-augusto/vim-afterglow.git"
#URL[newproggie]="https://github.com/NewProggie/NewProggie-Color-Scheme.git"
#URL[magellan]="https://github.com/tokers/magellan.git"
#URL[monokai]="https://github.com/mom0tomo/dotfiles.git"
#URL[ayu]="https://github.com/ayu-theme/ayu-vim.git"
#URL[solarized]="https://github.com/altercation/vim-colors-solarized.git"
#URL[new-railscasts]="https://github.com/carakan/new-railscasts-theme.git"

# Loop over the possible colorschemes
for scheme in ${!URL[@]}
do
	# Create the directory
	mkdir $GIT_FOLDER
	# Download the repository
	git clone ${URL[$scheme]} $GIT_FOLDER
	# Copy the colorscheme
	cp "$GIT_FOLDER/colors/$scheme.vim" $VIM_COLORS
	# Remove the directory
	rm -r -f $GIT_FOLDER
done

# Create the configuration file for the vim editor
# Default options are: 
# Nice: 	desert, evening, industry, koehler, ron
# Eh: 		darkblue, delek, elflord, murphy, pablo, slate
# Not Nice: blue, default, morning, peachpuff, shine, torte
#
# Best: 	industry
echo -e "syntax on\nfiletype on\ncolorscheme industry" > ~/.vimrc