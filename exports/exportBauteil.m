function exportBauteil(src,~)

% Function where we control the input of the name and we store the data in
% the Excel database and we also update the internal representation of the
% excel database for Bauteil objects

%% %%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure and panels
global fCreateBauteil
global pBauteil
global pKnoten
% Configurations structure
global Configs
% Data structures
global Bauteile
global Knoten
global Vorsatz

%% %%%%%%%%%%%%%%%%%%% FLOW CONTROL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define variables used for flow control
newBaut = false;
modBaut = false;
delBaut = false;

% Define which graphical element is calling this function
if src.Parent == fCreateBauteil
    newBaut = true;
elseif src.Parent == pBauteil
    % Check if we are deleting or modifying the Bauteil
    if strcmpi(src.Tag,'DeleteBUT')
        delBaut = true;
    elseif strcmpi(src.Tag,'ModifyBUT')
        modBaut = true;
    end
end

%% %%%%%%%%%%%%%%%%%%% RETRIEVE OBJECTS IN FIGURE %%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the objects depending on the source of the call
obj = src.Parent.Children;

% Wand / Decke
if newBaut
    w = findobj(obj,'Tag','WandCHE');
    d = findobj(obj,'Tag','DeckeCHE');
else
    gr = findobj(obj,'Tag','BauteilTypRAD');
    w = findobj(gr.Children,'Tag','WandRAD');
    d = findobj(gr.Children,'Tag','DeckeRAD');
end

% Text fields
tN = findobj(obj,'Tag','BauteilNameTXT');
tK = findobj(obj,'Tag','KurzBeschreibungTXT');
tD = findobj(obj,'Tag','DetBeschreibungTXT');

% Retrieve the Bauteil ID of the Bauteil being modified or deleted
if modBaut || delBaut
    bID = findobj(obj,'Tag','BauteilDD');
    selID = pBauteil.Data.Bauteil(bID.Value);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% ASK CONFIRMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ask for confirmation for overwriting the data when the user if modifying
% a Bauteil
if newBaut
    proceed = askconfirmation('Bauteil','create');
elseif modBaut
    proceed = askconfirmation('Bauteil','modify');
elseif delBaut
    proceed = askconfirmation('Bauteil','delete');
end

% Check if we can proceed
if ~proceed
    return;
end

%% %%%%%%%%%%%%%%%%%%%%% CHECK BAUTEIL ID %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the ID of the selected Bauteil
if newBaut
    if w.Value && d.Value
        TypeID = [1,2];
        ID = max(Bauteile.ID) + [1,2];
    else
        TypeID = w.Value*1 + d.Value*2;
        ID = max(Bauteile.ID) + 1;
    end
elseif modBaut
    TypeID = w.Value*1 + d.Value*2;
    ID = selID;
elseif delBaut
    ID = selID;
end

if ~validID(ID,'Bauteil')
    return;
end


%% %%%%%%% CHECK NAME TO BE UNIQUE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that the name inputted by the user is unique and is not already
% existing in the database
Name{1,1} = tN.String;

% Initialize the passed variable
passed = true;

% Check for the different cases
if newBaut
    % Check if we have to modify the names
    if w.Value && d.Value
        Name = strcat(Name,{'_W';'_D'});
    end
    passed = checkName(Bauteile.Name,Name,'Bauteil');
elseif modBaut
    passed = checkName(Bauteile.Name(Bauteile.ID ~= selID),Name,'Bauteil');
end

% Check if we can continue
if ~passed
    return;
end

%% %%%%%%%%%%%%%%%%%%%% RETRIEVE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the TypeID and the ID for the different cases
% Retrieve the Kurzbeschreibung and DetailBeschreibung
KurzBeschr = tK.String;
DetBeschr = tD.String;

% Get the user performing the database write
user = getenv('USERNAME');
% Get the exact time as a string (to the second)
date = datestr(now,'dd-mmm-yyyy');
time = datestr(now,'HH:MM:SS');

%% %%%%%%%%%%%%%% CHECK IF BAUTEIL IS STILL USED %%%%%%%%%%%%%%%%%%%%%%%
% Check if the Bauteil is still used in Knoten and in Vorsatzschalen
% This is only needed when we delete a Bauteil
if delBaut
    % Find which Knoten use the Bauteil
    bK = any(Knoten.Bauteile == ID,2);
    % Find which Vorsatz use the Bauteil
    bV = any(Vorsatz.Bauteile == ID,2);
    % Define the string to display in the warning
    s = 'Cannot delete the Bauteil because it''s still used in:';
    tab = char([10,32*ones(1,4)]);
    % Retrieve the dropdown names for Knoten
    if any(bK)
        dK = Knoten.Dropdown(bK);
        sK = strjoin(dK,tab);
        s = [s,10,10,'Knoten:',tab,sK];
    end
    % Retrieve the dropdown names for Vorsatz
    if any(bV)
        dV = Vorsatz.Dropdown(bV);
        sV = strjoin(dV,tab);
        s = [s,10,10,'Vorsatz:',tab,sV];
    end
    % Show the error dialog only if we have at least a match
    if any(bK) || any(bV)
        % Show the error dialog
        errordlg(s,'Cannot delete Bauteil!','modal');
        return;
    end
end

%% %%%%%%%%%%%%%% EXPORT DATA TO EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-Server and the workbooks
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Bauteile,1);

% Check if we can continue
if ~success
    return;
end

% Extract the sheet of interest
exSheet = exSH{1,1};
% Extract the last row
lastrow = exSH{1,2};

% Calculate the writerow depending on the caller
if newBaut
    % If both
    if w.Value && d.Value
        r = lastrow + [1,2];
    else
        r = lastrow + 1;
    end
elseif modBaut || delBaut
    r = Bauteile.ExcelRow(Bauteile.ID == ID);
end

% Create function for creating a excel range string
excelcell = @(c,r) [c,num2str(r,'%.0f')];

% %%%%%%%%%%%%%%%%% WRITE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New data or modifying data
if newBaut || modBaut
    % Iterate over the writerows
    for i=1:numel(r)
        % ID
        exSheet.get('Range',excelcell('A',r(i))).Value = ID(i);
        % TypeID
        exSheet.get('Range',excelcell('B',r(i))).Value = TypeID(i);
        % Name
        exSheet.get('Range',excelcell('C',r(i))).Value = Name{i};
        % Kurzbeschreibung
        exSheet.get('Range',excelcell('D',r(i))).Value = KurzBeschr;
        % Detaillierte Beschreibung
        exSheet.get('Range',excelcell('E',r(i))).Value = DetBeschr;
        % Print username and date
        if newBaut
            exSheet.get('Range',excelcell('F',r(i))).Value = user;
            exSheet.get('Range',excelcell('G',r(i))).Value = date;
            exSheet.get('Range',excelcell('H',r(i))).Value = time;
        else
            exSheet.get('Range',excelcell('I',r(i))).Value = user;
            exSheet.get('Range',excelcell('J',r(i))).Value = date;
            exSheet.get('Range',excelcell('K',r(i))).Value = time;
        end
    end
else
    % Delete from the excel
    delBauteil(ID,'excel',exSheet);
end

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%% APPEND DATA TO BAUTEILE STRUCTURE %%%%%%%%%%%%%%%%%%%%
if newBaut
    % Retrieve size of arrays in the Bauteil Datastructure
    n = size(Bauteile.ID,1);
    
    % Bauteil ID
    Bauteile.ID = [Bauteile.ID; ID'];
    % TypeID
    Bauteile.TypeID = [Bauteile.TypeID; TypeID'];

    % Iterate over the selected Bauteile-TypeID
    for i=1:numel(Name)
        % Calculate the excel row
        r = lastrow + i;
        % ExcelRow
        Bauteile.ExcelRow = [Bauteile.ExcelRow; r];
        % Considered
        Bauteile.Considered = [Bauteile.Considered; true];
        % Name
        Bauteile.Name{n+i,1} = Name{i};
        % Kurzbeschreibung
        Bauteile.KurzBeschr{n+i,1} = KurzBeschr;
        % Detail Beschreibunhg
        Bauteile.DetBeschr{n+i,1} = DetBeschr;
    end
    
elseif modBaut
    % Find the index in the array
    indx = find(Bauteile.ID == ID);
    % Replace name, kurzbeschr, detbeschr,...
    Bauteile.Name{indx,1} = Name{1};
    Bauteile.KurzBeschr{indx,1} = KurzBeschr;
    Bauteile.DetBeschr{indx,1} = DetBeschr;
    
elseif delBaut
    % Delete the data from the internal representation
    delBauteil(ID,'structure');
    % Delete other objects related to Bauteil
    % --> Not necessary, since we block the user to remove the Bauteil from
    % all the references before be able to remove it
end

% Update the dropdown menus
Bauteile.Dropdown = dropdownNames(Bauteile,'bauteil');
    
%% Hide the originating window (the parent of the button)
if newBaut
    src.Parent.Visible = 'off';
else
    % Check if the Knoten panel is also visible
    if strcmpi(pKnoten.Visible,'on')
        % Update the Knoten panel (The propagation to the other panels is
        % taken care within the pKnoten_Populate function)
        pKnoten_Populate(pKnoten.Data.KnotenID);
    else
        % Populate the Bauteil Panel with the correct Bauteil
        pBauteil_Populate(ID);
    end
end

end