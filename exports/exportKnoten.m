function exportKnoten(src,~)

% Function where we control the input of the name and we store the data in
% the Excel database and we also update the internal representation of the
% excel database for Knoten objects
% The function takes care of creating, modifying and deleting Knotens
% through the program

%% %%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure and panels
global fCreateKnoten
global pKnoten
% Configurations structure
global Configs
% Data structures
global Knoten
global Ubertragung

%% %%%%%%%%%% RETRIEVE OBJECTS IN FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the objects depending on the source of the call
obj = src.Parent.Children;

% Kreuzstoss / TStoss / Anders
KnotenTyp = findobj(obj,'Tag','KnotenTypRAD');
k = findobj(KnotenTyp.Children,'Tag','KreuzstossRAD');
t = findobj(KnotenTyp.Children,'Tag','TStossRAD');
a = findobj(KnotenTyp.Children,'Tag','AndersRAD');
% Vertikal / Horizontal
Ausrichtung = findobj(obj,'Tag','AusrichtungRAD');
v = findobj(Ausrichtung.Children,'Tag','VertikalRAD');
h = findobj(Ausrichtung.Children,'Tag','HorizontalRAD');

% Text fields
tN = findobj(obj,'Tag','KnotenNameTXT');
tK = findobj(obj,'Tag','KurzBeschreibungTXT');
tD = findobj(obj,'Tag','DetBeschreibungTXT');

% Bauteil dropdowns
b1 = findobj(obj,'Tag','Bauteil1DD');
b2 = findobj(obj,'Tag','Bauteil2DD');
b3 = findobj(obj,'Tag','Bauteil3DD');
b4 = findobj(obj,'Tag','Bauteil4DD');
bDD = [b1, b2, b3, b4];
% Anisotropie dropdowns
a1 = findobj(obj,'Tag','Anisotropie1DD');
a2 = findobj(obj,'Tag','Anisotropie2DD');
a3 = findobj(obj,'Tag','Anisotropie3DD');
a4 = findobj(obj,'Tag','Anisotropie4DD');

%% %%%%%%%%%%%%%% FLOW CONTROL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define variables used for flow control
newKnot = false;
modKnot = false;
delKnot = false;

% Define which graphical element is calling this function
if src.Parent == fCreateKnoten
    newKnot = true;
elseif src.Parent == pKnoten
    % Check if we are deleting or modifying the Knoten
    if strcmpi(src.Tag,'DeleteBUT')
        delKnot = true;
    elseif strcmpi(src.Tag,'ModifyBUT')
        modKnot = true;
    end
end

% Retrieve the data structures
if newKnot
    data = fCreateKnoten.Data;
elseif modKnot || delKnot
    data = pKnoten.Data;
end

% Retrieve the Knoten ID of the Knoten being modified or deleted
if modKnot || delKnot
    kDD = findobj(obj,'Tag','KnotenDD');
    selID = data.Knoten(kDD.Value);
end

%% %%%%%%%%%%%%%%%%%%%% ASK CONFIRMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ask for confirmation for overwriting the data when the user is modifying
% a Knoten
if newKnot
    proceed = askconfirmation('Knoten','create');
elseif modKnot
    proceed = askconfirmation('Knoten','modify');
elseif delKnot
    proceed = askconfirmation('Knoten','delete');
end

% Check if we can proceed
if ~proceed
    return;
end

%% %%%%%%%%%%%%%%%%%% CHECK KNOTEN ID %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve Knoten ID
if newKnot
    ID = max(Knoten.ID) + 1;
elseif modKnot || delKnot
    ID = selID;
end

if ~validID(ID,'Knoten')
    return;
end

%% %%%%%%% CHECK NAME TO BE UNIQUE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that the name inputted by the user is unique and is not already
% existing in the database (unless we are modifying an already existing
% Knoten in which case the Name can be the same, because we are overwriting
% it
Name = tN.String;

% Initialize the passed variable
passed = true;

% Check the name to be unique
if newKnot
    passed = checkName(Knoten.Name,Name,'Knoten'); 
elseif modKnot
    passed = checkName(Knoten.Name(Knoten.ID ~= selID),Name,'Knoten');
end

% Check if we can continue
if ~passed
    return;
end

%% %%%%%%% Retrieve the data to be stored in the Knoten database %%%%%%%%%
% Calculate the TypeID
TypeID = k.Value*1 + t.Value*2 + a.Value*3;
% Calculate the Ausrichtung value
ausrichtung = v.Value*1 + h.Value*2;

% Retrieve the Kurzbeschreibung and DetailBeschreibung
KurzBeschr = tK.String;
DetBeschr = tD.String;

% Retrieve the BauteileID
bauteile = zeros(1,4);
if newKnot || modKnot
    % Retrieve the Bauteile IDs
    for i=1:4
        bauteile(i) = data.Bauteil{i}(bDD(i).Value);
    end
end

% Retrieve the Anisotropie ID
anisotropie = [a1.Value, a2.Value, a3.Value, a4.Value] - 1;

%% %%%%%%%%%%%%%% PERFORM CHECK FOR KNOTEN-TYPE %%%%%%%%%%%%%%%%%%%%%%%%%%
% Check only if we are not deleting the Knoten element
if newKnot || modKnot
    % Count the number of Bauteile being used
    nBau = sum(bauteile~=0);

    % Check that if the Knoten is specified as a Kreuzstoss it has 4 valid
    % bauteile
    if TypeID==1 && nBau~=4
        % Throw error and return
        errordlg('For a Kreuzstoss-Knoten all four Bauteile must be selected!',...
            'Knoten-Type inconsistency','modal');
        return;
    end
    
    % Check that if Knoten is specified as a TStoss it has 3 valid bauteile
    if TypeID==2 && nBau~=3
        % Throw error and return
        errordlg('For a TStoss-Knoten three valid Bauteile must be selected!',...
            'Knoten-Type inconsistency','modal');
        return;
    end
    
    % Check that if Knoten is specified as Anders it has 2 valid bauteile
    if TypeID==3 && nBau~=2
        % Throw error and return
        errordlg('For a Anders-Knoten two valid Bauteile must be selected!',...
            'Knoten-Type inconsistency','modal');
        return;
    end
end

%% %%%%%%%%%%% FIND OUT WHICH PARTS ARE BEING MODIFIED %%%%%%%%%%%%%%%%%%%
% Perform this operation only if we are modifying the Knoten
if modKnot
    % Retrieve the Knoten that we are modifying
    b = (Knoten.ID == ID);
    % Find which Bauteile and Anisotropie have been modified
    changeTeile = (Knoten.Bauteile(b,:) ~= bauteile);
    changeAniso = (Knoten.Anisotropie(b,:) ~= anisotropie);
    change = any([changeTeile;changeAniso],1);
    changeIndx = find(change);
    
    % Ask the user if he wants to keep the Ubertragungen that involve the
    % modified Bauteile (change of Bauteil or change of Anisotropie)
    % only if we have a change
    if ~isempty(changeIndx)
        % Find all the Ubertragungen associated with this Knoten
        bK = (Ubertragung.Knoten == ID);
        % Find all Ubertragungen that go through the modified Bauteile
        bB = any(ismember(Ubertragung.Bauteile,changeIndx),2);
        % Compound the boolean vector
        b = all([bK,bB],2);
        % If we have a match ask the user if he wants to keep the data or
        % remove it
        if any(b)
            q = questdlg(['Keep existing Ubertragungen related to the modified',...
                          ' Bauteile?'],...
                          'Keep Ubertragungen?',...
                          'No','Yes','Yes');
            switch lower(q)
                case 'no'
                    delUber = true;
            end
        end
    end
end

% Perform this part only if we are deleting the Knoten
if delKnot
    % Find all Ubertragungen associated with this Knoten
    b = (Ubertragung.Knoten == ID);
    % IF we have a match ask the user if he wants to keep the data or
    % remove it
    if any(b)
        q = questdlg(['Delete existing Ubertragungen related to the deleted',...
                      ' Knoten?'],...
                      'Delete Ubertragungen',...
                      'No','Yes','Yes');
        switch lower(q)
            case 'yes'
                delUber = true;
        end
    end
end    

% Perform the checks if we have to delete any Ubertragungen
if exist('delUber','var')==1 && delUber
   % Retrieve the IDs of the Ubertragungen to delete
   uID = Ubertragung.ID(b);
else
   delUber = false;
end

%% %%%%%%%%%%%%%%%%%%% USER AND DATETIME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the user performing the database write
user = getenv('USERNAME');
% Get the exact time as a string (to the second)
date = datestr(now,'dd-mmm-yyyy');
time = datestr(now,'HH:MM:SS');

%% %%%%%%%%%%%%%% EXPORT DATA TO EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Knoten
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Knoten,1);
% Open the COM-server and the workbooks for Ubertragung
if delUber
    [success2,ex2,exWB2,exSH2] = openExcelFile(Configs.Database.Ubertragung,1);
    % Compound success
    success = (success && success2);
end

% Check if we can continue
if ~success
    return;
end

% Extract the sheet of interest
exSheet = exSH{1,1};
% Extract the last row
lastrow = exSH{1,2};

% Calculate the writerow depending on the caller
if newKnot
    % Calculate the writerow 
    r = lastrow + 1;
elseif modKnot || delKnot
    r = Knoten.ExcelRow(Knoten.ID == ID);
end

% Create function for creating a excel range string
excelcell = @(c,r) [c,num2str(r,'%.0f')];
excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];

% %%%%%%%%%%%%%%%%% WRITE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if newKnot || modKnot
    % ID
    exSheet.get('Range',excelcell('A',r)).Value = ID;
    % TypeID
    exSheet.get('Range',excelcell('B',r)).Value = TypeID;
    % Name
    exSheet.get('Range',excelcell('C',r)).Value = Name;
    % Kurzbeschreibung
    exSheet.get('Range',excelcell('D',r)).Value = KurzBeschr; 
    % Ausrichtung
    exSheet.get('Range',excelcell('E',r)).Value = ausrichtung; 
    % Bauteile
    exSheet.get('Range',excelrange('F','I',r)).Value = bauteile; 
    % Anisotropie
    exSheet.get('Range',excelrange('J','M',r)).Value = anisotropie; 
    % Detaillierte Beschreibung
    exSheet.get('Range',excelcell('N',r)).Value = DetBeschr;
    % Print username and date
    if newKnot
        exSheet.get('Range',excelcell('O',r)).Value = user;
        exSheet.get('Range',excelcell('P',r)).Value = date;
        exSheet.get('Range',excelcell('Q',r)).Value = time;
    else
        exSheet.get('Range',excelcell('R',r)).Value = user;
        exSheet.get('Range',excelcell('S',r)).Value = date;
        exSheet.get('Range',excelcell('T',r)).Value = time;
    end
else
    % Empty the row
    delKnoten(ID,'excel',exSheet);
end

% Delete related Ubertragungen 
if delUber
    delUbertragung(uID,'excel',exSH2{1,1});
end

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);
if delUber
    closeExcel(ex2,exWB2);
end

%% %%%%%%%%%%%%%%%%%% APPEND DATA TO KNOTEN STRUCTURE %%%%%%%%%%%%%%%%%%%%
if newKnot
    % Retrieve size of arrays in the Knoten Datastructure
    n = size(Knoten.ID,1);
    
    % ExcelRow
    Knoten.ExcelRow = [Knoten.ExcelRow; r];
    % Considered
    Knoten.Considered = [Knoten.Considered; true];
    % Knoten ID, Type ID, Ausrichtung
    Knoten.ID = [Knoten.ID; ID];
    Knoten.TypeID = [Knoten.TypeID; TypeID];
    Knoten.Ausrichtung = [Knoten.Ausrichtung; ausrichtung];
    % Name, KurzuBeschr, DetBeschr
    Knoten.Name{n+1,1} = Name;
    Knoten.KurzBeschr{n+1,1} = KurzBeschr;
    Knoten.DetBeschr{n+1,1} = DetBeschr;
    % Bauteile / Anisotropie
    Knoten.Bauteile = [Knoten.Bauteile; bauteile];
    Knoten.Anisotropie = [Knoten.Anisotropie; anisotropie];
elseif modKnot
    % Find the index in the array
    indx = find(Knoten.ID == ID);
    % Replace name, kurzbeschr, detbeschr,...
    Knoten.Name{indx,1} = Name;
    Knoten.KurzBeschr{indx,1} = KurzBeschr;
    Knoten.DetBeschr{indx,1} = DetBeschr;
    % Replace Bauteile and Anisotropie
    Knoten.Bauteile(indx,:) = bauteile;
    Knoten.Anisotropie(indx,:) = anisotropie;
elseif delKnot
    % Delete the data from the internal representation
    delKnoten(ID,'structure');
end

% Delete related Ubertragungen
if delUber
    delUbertragung(uID,'structure');
end

% Update the dropdown names
Knoten.Dropdown = dropdownNames(Knoten,'knoten');
% We also need to update the names of the Ubertragungen
Ubertragung.Dropdown = dropdownNames(Ubertragung,'ubertragung');

%% Hide the originating window (the parent of the button)
if newKnot
    src.Parent.Visible = 'off';
else
    % Populate the Knoten Panel with the correct Knoten
    pKnoten_Populate(ID);
end

end