function exportOriginalDB(src,~)

% Function takes care of exporting averaged data to the original DB in the
% format it has been stored up until now (with a / separator)

%% %%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fExportOriginalDB
% Data
global Data
global walls
global CompareDatasetsData
% Configurations structure
global Configs

% Extract the relevant data from the global structures
data = CompareDatasetsData.Average;

%% %%%%%%%%%%%%%%%%%%%% FIND GRAPHICS OBJECTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = fExportOriginalDB.Children;

% Find the text fields
ReportName = findobj(obj,'Tag','ReportNameTXT');
TestDescr = findobj(obj,'Tag','ExportTestDescrTXT');
TestObj = findobj(obj,'Tag','ExportTestObjTXT');

%% %%%%%%%%%%%%%%%%%%%%%% CHECK INPUTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retreive the number of projects
n = size(data.IDs,1);

% Check again that the number of projects selected is only two, otherwise
% we cannot export
if n~=2
    % Throw error and return
    errordlg(['Exporting of averaged data to the original database',...
             ' is only possible when averaging only 2 datasets!'],...
             'Too many datasets for averaging selected','modal');
    return;
end

% Check again that the type of the datasets is 1
if any(data.IDs(:,1)~=1)
    errordlg(['Exporting of averaged data to the original database',...
              ' is only possible when averaging Airbone datasets!'],...
             'Wrong dataset types selected','modal');
    return;
end

%% %%%%%%%%%%%%%%% ASK CONFIRMATION %%%%%%%%%%%%%%%%%%%%%%%%%
% if we don't get confirmation return
if ~askconfirmation('Airborne-Averaged Dataset','create')
    return;
end

%% %%%%%%%%%%%%%%% RETRIEVE DATA TO WRITE TO DATABASE %%%%%%%%%%%%%%%%%%%%
% Get the DBType of the two datasets that have been used for the averaging
% operations
DB = Data.Air;
% Find the indexes of where these Datasets are stored
i = data.IDs(1,2);
j = data.IDs(2,2);

% Calculate new ID
ID = max(Data.AirAVG.ID) + 1;

% %%%%%%%%%%%%%%%%% REPORT %%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the report name from the figure
ReportName = ReportName.String;
% Check the name
if ~checkName(DB.Report(:,1),ReportName,'Airborne-Averaged Dataset')
    return;
end

% Retrieve the other reportdata from the database
Report = strcat(DB.Report(i,:),'/',DB.Report(j,:));
Report{1,1} = ReportName;
% Source room data
SR{1,1} = ['Room',32,DB.SR{i,1},'/Room',32,DB.SR{j,1}];
SR{1,2} = [DB.SR{i,2},'/',DB.SR{j,2}];
SR{1,3} = [num2str(DB.SR{i,3},'%.1f'),'/',num2str(DB.SR{j,3},'%.1f')];
% Receiver room data
RR{1,1} = ['Room',32,DB.RR{i,1},'/Room',32,DB.RR{j,1}];
RR{1,2} = [DB.RR{i,2},'/',DB.RR{j,2}];
RR{1,3} = [num2str(DB.RR{i,3},'%.1f'),'/',num2str(DB.RR{j,3},'%.1f')];
% Area
Area = [num2str(DB.Area(i,1),'%.2f'),'/',num2str(DB.Area(j,1),'%.2f')];
% Import Date
ImportDate = DB.ImportDate(i,1);
% QUESTION: ImportDate1 / ImportDate 2 OR ImportDate1
% ID1
ID1 = DB.ID(i,1);
ID2 = DB.ID(j,1);
% Datatype
DataType = strcat(DB.DataType(i,1),'/',DB.DataType(j,1));
% Check that Datatype is equal
if ~strcmpi(DB.DataType{i,1},DB.DataType{j,1})
    errordlg(['Measurement type is not identical for both the Airborne',...
              ' datasets selected.'],...
             'Measurement type not identical','modal');
    return;
end

% Flags
Flag1 = DB.Flag(i,:);
Flag2 = DB.Flag(j,:);
FlagsStr = {'','1'};
Flags = strcat(FlagsStr(Flag1+1),',',FlagsStr(Flag2+1));
% Retrieve the test description and test object
TestTitle = strcat(DB.Test.Title(i,1),'/',DB.Test.Title(j,1));
TestDescr = TestDescr.String;
TestObj = TestObj.String;
% SR / RR Data
SRData1 = cellfun(@strdec,num2cell(DB.Test.SR(i,:)),'UniformOutput',false);
SRData2 = cellfun(@strdec,num2cell(DB.Test.SR(j,:)),'UniformOutput',false);
SRData = strcat(SRData1,'/',SRData2);
RRData1 = cellfun(@strdec,num2cell(DB.Test.RR(i,:)),'UniformOutput',false);
RRData2 = cellfun(@strdec,num2cell(DB.Test.RR(j,:)),'UniformOutput',false);
RRData = strcat(RRData1,'/',RRData2);
% Wall types
W = DB.Walls(i,:);
W2 = DB.Walls(j,:);
% If the wall types are not equal we have a problem
if any(W~=W2)
    errordlg(['Wall types are not identical for both the Airborne',...
              ' datasets selected.'],...
             'Wall type not identical','modal');
    return;
end
% Map from ID to wall string for database
[~,indx] = ismember(W,walls.Encoding);
WString = walls.Types(indx);
% Shielding conditions
S = DB.Shielding(i,:);
S2 = DB.Shielding(j,:);
% If the shiedling conditions are not identical we have a problem
if any(S~=S2)
    errordlg(['Shielding conditions are not identical for both the',...
              ' Airborne datasets selected.'],...
             'Shielding conditions not identical','modal');
    return;
end
SType = {'FALSE','TRUE'};
SString = SType(S+1);
% TL values
TL1 = DB.TL(i,:);
TL2 = DB.TL(j,:);
% Project / Prufobj
Proj = DB.Project{i,1};
if ~strcmpi(Proj,DB.Project{j,1})
    errordlg(['Project attribute not identical for both the',...
              ' Airborne datasets selected.'],...
             'Project attribute not identical','modal');
    return;
end
PrufObj = DB.PrufObj{i,1};
if ~strcmpi(PrufObj,DB.PrufObj{j,1})
    errordlg(['Prufobject attribute not identical for both the',...
              ' Airborne datasets selected.'],...
             'Prufobject attribute not identical','modal');
    return;
end

%% %%%%%%%%%%%%%%%%%%% USER AND DATETIME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the user performing the database write
user = getenv('USERNAME');
% Get the exact time as a string (to the second)
date = datestr(now,'dd-mmm-yyyy');
time = datestr(now,'HH:MM:SS');

%% %%%%%%%%%%%%%% EXPORT DATA TO EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for AirborneAveraged
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Original,3);

% Check if we can continue
if ~success
    return;
end

% Open the excel sheet and the last row
exSheet = exSH{1,1};
lastrow = exSH{1,2};

% Calculate the writerow 
r = lastrow + 1;

% Create function for creating a excel range string
excelcell = @(c,r) [c,num2str(r,'%.0f')];
excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];

%% %%%%%%%%%%%%%%%%% WRITE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ID
exSheet.get('Range',excelcell('A',r)).Value = ID;
% Report name / date / Remarks / Client / Test Date
exSheet.get('Range',excelrange('B','F',r)).Value = Report;
% Source room / Receiver room
exSheet.get('Range',excelrange('G','I',r)).Value = SR;
exSheet.get('Range',excelrange('J','L',r)).Value = RR;
% Area
exSheet.get('Range',excelcell('M',r)).Value = Area;
% Import date
exSheet.get('Range',excelcell('N',r)).Value = ImportDate;
% IDs
exSheet.get('Range',excelcell('O',r)).Value = ID1;
exSheet.get('Range',excelcell('P',r)).Value = ID2;
% Datatype
exSheet.get('Range',excelcell('Q',r)).Value = DataType;
% SNR / C
exSheet.get('Range',excelcell('R',r)).Value = num2str(data.SNR,'%.0f');
exSheet.get('Range',excelrange('S','Z',r)).Value = data.C; 
% Averaged TL values
exSheet.get('Range',excelrange('AA','AU',r)).Value = data.Values; 
% Flags
exSheet.get('Range',excelrange('AV','BP',r)).Value = Flags;
% Sum-dev, max-dev, freq-max-dev
% TODO: Sum-dev, max-dev, freq-max-dev
% Test title / Description / Object
exSheet.get('Range',excelcell('BT',r)).Value = TestTitle;
exSheet.get('Range',excelcell('BU',r)).Value = TestDescr;
exSheet.get('Range',excelcell('BV',r)).Value = TestObj;
% SR data / RR Data
exSheet.get('Range',excelrange('BW','BY',r)).Value = SRData;
exSheet.get('Range',excelrange('BZ','CB',r)).Value = RRData;
% Walls types
exSheet.get('Range',excelrange('CC','CN',r)).Value = WString;
% Shielding conditions
exSheet.get('Range',excelrange('CO','DD',r)).Value = SString;
% SNR-dec, Sum-dev-dec, max-dev-dec, Freq-max-dev-dec
% TODO: SNR-dec, sum-dev-dec, max-dev-dec, freq-max-dev-dec
% TL values set 1 / 2
exSheet.get('Range',excelrange('DI','EC',r)).Value = TL1; 
exSheet.get('Range',excelrange('ED','EX',r)).Value = TL2;
% Project / Prufobject
exSheet.get('Range',excelcell('EY',r)).Value = Proj;
exSheet.get('Range',excelcell('EZ',r)).Value = PrufObj;
% Username and date-time
exSheet.get('Range',excelcell('FA',r)).Value = user;
exSheet.get('Range',excelcell('FB',r)).Value = date;
exSheet.get('Range',excelcell('FC',r)).Value = time;

% Save and close the Excel workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%% APPEND DATA TO Data.AirAVG STRUCTURE %%%%%%%%%%%%%%%%%%%%
% Retrieve size of arrays in the Data.AirAVG Datastructure
n = size(Data.AirAVG.ID,1);
k = n+1;

% ExcelRow / Considered / ID
Data.AirAVG.ExcelRow = [Data.AirAVG.ExcelRow; r];
Data.AirAVG.Considered = [Data.AirAVG.Considered; true];
Data.AirAVG.ID = [Data.AirAVG.ID; ID];
% Report / SR / RR
Data.AirAVG.Report(k,:) = Report;
Data.AirAVG.SR(k,:) = SR;
Data.AirAVG.RR(k,:) = RR;
% Area
Data.AirAVG.Area{k,1} = Area;
% Import Date
Data.AirAVG.ImportDate(k,1) = ImportDate;
% Links
Data.AirAVG.Link.Air = [Data.AirAVG.Link.Air; ID1];
Data.AirAVG.Link.Impact = [Data.AirAVG.Link.Impact; ID2];
% Datatype
Data.AirAVG.DataType{k,1} = DB.DataType{i,1};
% SNR value
Data.AirAVG.SNR = [Data.AirAVG.SNR; round_dec(data.SNR,0)];
% C Values
Data.AirAVG.C = [Data.AirAVG.C; data.C];
% TL Values
Data.AirAVG.TL = [Data.AirAVG.TL; data.Values];
% Flags
fl = zeros(1,numel(Flag1),2);
fl(:,:,1) = Flag1;
fl(:,:,2) = Flag2;
Data.AirAVG.Flag = [Data.AirAVG.Flag; fl];
% Deviations 
% TODO: Addition of deviations to internal Data.AirAVG database
% Test
Data.AirAVG.Test.Title{k,1} = TestTitle;
Data.AirAVG.Test.Description{k,1} = TestDescr;
Data.AirAVG.Test.Object{k,1} = TestObj;
Data.AirAVG.Test.SR(k,:) = SRData;
Data.AirAVG.Test.RR(k,:) = RRData;
% Walls and shielding
Data.AirAVG.Walls = [Data.AirAVG.Walls; W];
Data.AirAVG.Shielding = [Data.AirAVG.Shielding; S];
% Deviations-dec 
% TODO: Addition of deviations-dec to internal Data.AirAVG database
% TL1 / TL2
Data.AirAVG.TL1 = [Data.AirAVG.TL1; TL1];
Data.AirAVG.TL2 = [Data.AirAVG.TL2; TL2];
% Project and PrufObj
Data.AirAVG.Project{k,1} = Proj;
Data.AirAVG.PrufObj{k,1} = PrufObj;

%% Hide the originating window (the parent of the button)
src.Parent.Visible = 'off';
 
end