function delBauteil(IDs,type,exSheet)

% Function takes care of removing Bauteileen by their ID from:
% 1. Bauteile Excel Database
% 2. Global Bauteile Structure

% Global variables
global Bauteile

% Check the inputs
if ~any(strcmpi(type,{'excel','structure'}))
    error('Type argument must be either ''excel'' or ''structure''');
end

% Find the indexes of the IDs in the Bauteile.ID structure
[b,indx] = ismember(IDs,Bauteile.ID);

% Before deleting the data check that we can find all IDs
if ~all(b)
    error('Cannot find all IDs in the Bauteile.ID structure');
end

% Retrieve the excel rows
rows = Bauteile.ExcelRow(indx);

%% %%%%%%%%%%%%%%%% UPDATE THE EXCEL DATABASE %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the excel database (we are using the structure to determine the 
% rows in the excel file in which the Bauteileen are stored and
% therefore we ALWAYS have to delete FIRST the Excel database and SECOND
% the structure
if strcmpi(type,'excel')
    % Define function for excel range
    excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];
    
    % Iterate over the IDs and empty the rows
    for i=1:numel(IDs)
        exSheet.get('Range',excelrange('A','K',rows(i))).Value = '';
    end
    
    % Return
    return;
end

%% %%%%%%%%%%%%%%% UPDATE THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmpi(type,'structure')
    % Delete from the considered vector (row is offset by 1: Header)
    Bauteile.Considered = delBool(Bauteile.Considered,...
                                     rows-1);
    % Delete the ID, ExcelRow, TypeID vectors
    Bauteile.ID(indx) = [];
    Bauteile.ExcelRow(indx) = [];
    Bauteile.TypeID(indx) = [];
    % Delete the Name, KurzBeschr, DetBeschr and Dropdown
    Bauteile.Name = delRow(Bauteile.Name,indx);
    Bauteile.KurzBeschr = delRow(Bauteile.KurzBeschr,indx);
    Bauteile.DetBeschr = delRow(Bauteile.DetBeschr,indx);
    Bauteile.Dropdown = delRow(Bauteile.Dropdown,indx);
    
    % Return
    return;
end