function exportUbertragung(src,~)

% Function exports the data selected and inputted by the user to the
% Ubertragung database and the internal representation of the database

%% %%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure and panels
global fExportUbertragung
global pUbertragung
global pKnoten
% Configurations structure
global Configs
% Data
global Knoten
global Ubertragung

%% %%%%%%%%%%%%%%%%%% FLOW CONTROL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
newUber = false;
modUber = false;
delUber = false;

% Character array
space = char(32*ones(1,5));
tabs = char([10,space]);

% Define which graphical element is calling this function
if src.Parent == fExportUbertragung
    newUber = true;
elseif src.Parent == pUbertragung
    % Check if we are deleting or modifying the Bauteil
    switch lower(src.Tag)
        case 'deletebut'
            delUber = true;
        case 'modifybut'
            modUber = true;
    end
end

% Retrieve data
if newUber
    data = fExportUbertragung.Data;
else
    data = pUbertragung.Data;
end
    
%% %%%%%%%%%% RETRIEVE OBJECTS IN FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the objects depending on the source of the call
obj = src.Parent.Children;

% Retrieve Startraum / Endraum / Startbauteil / Endbauteil dropdowns
SR = findobj(obj,'Tag','StartraumDD');
ER = findobj(obj,'Tag','EndraumDD');
SB = findobj(obj,'Tag','StartbauteilDD');
EB = findobj(obj,'Tag','EndbauteilDD');

% Retrieve texts
fl = findobj(obj,'Tag','FlacheTXT');
la = findobj(obj,'Tag','LangeTXT');

% Window specific objects
if newUber
    kDD = findobj(obj,'Tag','KnotenDD');
else
    uDD = findobj(obj,'Tag','UbertragungDD');
end
    
%% %%%%%%%%%%%%%%%%% ASK CONFIRMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ask for confirmation for overwriting the data when the user if modifying
% an Ubertragung
if newUber
    proceed = askconfirmation('Ubertragung','create');
elseif modUber
    proceed = askconfirmation('Ubertragung','modify');
elseif delUber
    proceed = askconfirmation('Ubertragung','delete');
end

% Check if we can proceed
if ~proceed
    return;
end

%% %%% Retrieve the data to be stored in the Ubertragung database %%%%%%%
if newUber
    % Calculate the Ubertragung ID
    ID = max(Ubertragung.ID) + 1;
    % Retrieve the Knoten ID
    KnotenID = data.Knoten(kDD.Value);
    % Original Database ID
    OriginalDBID = data.OriginalDBID;
elseif modUber || delUber
    % Retrieve the Ubertragung ID
    ID = data.Ubertragung(uDD.Value);
    % Find the associated Knoten
    KnotenID = Ubertragung.Knoten(Ubertragung.ID == ID);
    % Original Database ID
    OriginalDBID = data.OriginalID;
end

% Retrieve the TypeID
TypeID = data.TypeID;

% Retrieve the Startraum and Endraum 
Startraum = SR.Value;
Endraum = ER.Value;

% Retrieve the Bauteile
Startbauteil = data.Startbauteil(SB.Value);
Endbauteil = data.Endbauteil(EB.Value);
% Retrieve the corresponding BauteileIDs
bK = (Knoten.ID == KnotenID);
StartbauteilID = Knoten.Bauteile(bK,Startbauteil);
EndbauteilID = Knoten.Bauteile(bK,Endbauteil);

% Retrieve transmission values
Values = data.Values;
% Round values to 0.1
Values = round_dec(Values,1);

% Retrieve area and length
Flache = str2double(fl.String);
Lange = str2double(la.String);

%% %%%%%%%%%%%%%%%%%%%%%%%% INVALIDY CHECKS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check for valid ID
if ~validID(ID,'Ubertragung')
    return;
end

% If it's zero throw error
if KnotenID == 0
    errordlg('No Knoten has been specified, please select a Knoten!',...
             'Knoten Selection Error','modal');
    return;
elseif ~any(bK)
    errordlg('Selected Knoten does not exist in the database!',...
             'Knoten Database Problem','modal');
    return;
end

% Check that the user has not selected the same Start and Endraum
if Startraum == Endraum
    errordlg('Startraum and Endraum are the same, please make new selection!',...
             'Same Raum error','modal');
    return;
end

% Check that the corresponding StartbauteilID is not zero
if StartbauteilID == 0
    errordlg('Selected Startbauteil is not part of the Knoten!',...
             'Startbauteil Selection Error',...
             'modal');
    return; 
end
% Check that corresponding EndbauteilID is not zero
if EndbauteilID == 0
    errordlg('Selected Endbauteil in not part of the Knoten!',...
             'Endbauteil Selection Error',...
             'modal');
    return;
end

% Check if there is a wall between the startraum and the endraum for the
% given Knoten
if ~wallPresent(KnotenID,Startraum,Endraum)
    errordlg('Startraum and Endraum are connected!',...
             'Raum Selection Problem','modal');
    return;
end

% Check for Trittschall if the startbauteil is nr. 1 or nr. 3
if TypeID == 2
    % Check that Startraum is either Raum 1 or Raum 2
    if ~any(Startraum==[1,2])
        errordlg(['For Trittschall-Data the Startraum needs to be either',...
                  ' Raum 1 or Raum 2!'],'Startbauteil Selection Error',...
                  'modal');
        return;
    end
    % Check that Startbauteil is either Bauteil 1 or Bauteil 3, depending
    % on the Raum being selected
    if (Startraum==1 && Startbauteil~=1) || (Startraum==2 && Startbauteil~=3)
        errordlg(['For Trittschall-Data the Startbauteil needs to be either',...
                  ' Bauteil 1 or Bauteil 3!'],'Startbauteil Selection Error',...
                  'modal');
        return;
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%% CHECK FOR UNIQUENESS %%%%%%%%%%%%%%%%%%%%%%%%%
% The Ubertragung database can only have unique combinations of the
% following tuples
% (KnotenID, TypeID, StartRaum, EndRaum, StartBauteil, EndBauteil)
% Therefore if we are storing a new Ubertragung (newUber) or we are
% modifying any of these properties for an existing one (modUber) we have
% to notify the user that he will be overwriting an existing Ubertragung
% and that the old one (modUber) will be deleted
if newUber || modUber
    % Create a matrix representing the configurations present in the
    % database
    ValidConfigurations = [Ubertragung.Knoten,Ubertragung.TypeID,...
                           Ubertragung.Raume,Ubertragung.Bauteile];

    % Retrieve the chosen configuration
    conf = [KnotenID,TypeID,Startraum,Endraum,Startbauteil,Endbauteil];
    
    % If we are dealing with Luftschall (TypeID==1) we need to check that
    % the inverse path is also not present, and we need to store only one
    % pathway for the direct and inverse Ubertragung
    if TypeID == 1
        conf = [conf;...
                KnotenID,TypeID,Endraum,Startraum,Endbauteil,Startbauteil];
    end

    % Check if it's already present in the database
    [b,indx] = ismember(conf,ValidConfigurations,'rows');
    
    % If it's present, ask the user for confirmation to overwrite the 
    % data
    if any(b)
        % Find out which one of the configurations is already present and
        % update the b and indx variables
        indx = indx(b);
        b = b(b);
        
        % If we have multiple matches throw an error
        if numel(b)>1
            dd = Ubertragung.Dropdown(indx);
            %Throw error
            errordlg(['Multiple Ubertragungen exist, but only ONE should be',...
                      ' present. Please choose one of the following',...
                      ' Ubertragungen:',10,tabs,strjoin(dd,tabs),10,10,...
                      'to be kept and delete the remaing ones!'],...
                     'Multiple Ubertragungen - Error','modal');
            return;
        end
        
        % If we are in modification mode, we need to check that the 
        % configuration has changed (we do this by comparing the
        % Ubertragung ID of the match in the database with the Ubertragung
        % ID of the selected Ubertragung in the figure, if they are
        % different it means it already exists in the database, so we store
        % the modification under that Ubertragung and we delete the
        % Ubertragung we were modifying
        if modUber && (Ubertragung.ID(indx)~=ID)
            % Store the ID of the selected Ubertragung to be deleted
            delID = ID;
        end
        
        % Find the correct ID to identify the combination
        ID = Ubertragung.ID(indx);
        
        % Ask the user if he wants to overwrite the existing Ubertragung
        % New Ubertragung Case
        if newUber
            q = questdlg(['The current combination of parameters for',...
                          ' Ubertragung is already present in the',...
                          ' database.',10,'Do you want to overwrite the',...
                          ' data of this existing Ubertragung?'],...
                         'Ubertragung already exists',...
                         'No','Yes','Yes');
            switch lower(q)
                case 'no'
                    return;
            end
        % Modify Ubertragung case (only if we are overwriting another
        % Ubertragung and deleting the current one)
        elseif exist('delID','var')==1
            q = questdlg(['The current combination of parameters for',...
                          ' Ubertragung is already present in the',...
                          ' database.',10,'Do you want to overwrite the',...
                          ' data of this existing Ubertragung and delete',...
                          ' the selected Ubertragung?'],...
                         'Ubertragung already exists',...
                         'No','Yes','Yes');
            switch lower(q)
                case 'no'
                    return;
            end
        end
            
        % If we are in new mode and we have a match, we need to switch to 
        % modification mode, otherwise we would not overwrite the correct
        % data
        if newUber
            newUber = false;
            modUber = true;
        end
    end
end

%% %%%%%%%%%%%%%%%%%%% USERNAME + DATE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the user performing the database write
user = getenv('USERNAME');
% Get the exact time as a string (to the second)
date = datestr(now,'dd-mmm-yyyy');
time = datestr(now,'HH:MM:SS');

%% %%%%%%%%%%%%%% EXPORT DATA TO EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-Server and the workbooks
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Ubertragung,1);

% Check if we can continue
if ~success
    return;
end

% Extract the sheet of interest
exSheet = exSH{1,1};
% Extract the last row
lastrow = exSH{1,2};

% Create function for creating a excel range string
excelcell = @(c,r) [c,num2str(r,'%.0f')];
excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];

% %%%%%%%%%%%%%%%%% WRITE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if newUber
    % Calculate the writerow 
    r = lastrow + 1;
    % ID, Knoten ID, TypeID, Startraum, Endraum, Startbauteil, Endbauteil
    exSheet.get('Range',excelcell('A',r)).Value = ID;
    exSheet.get('Range',excelcell('B',r)).Value = KnotenID;
    exSheet.get('Range',excelcell('C',r)).Value = TypeID;
    exSheet.get('Range',excelcell('D',r)).Value = Startraum;
    exSheet.get('Range',excelcell('E',r)).Value = Endraum;
    exSheet.get('Range',excelcell('F',r)).Value = Startbauteil;
    exSheet.get('Range',excelcell('G',r)).Value = Endbauteil;
    % Flache, Lange
    exSheet.get('Range',excelcell('H',r)).Value = Flache;
    exSheet.get('Range',excelcell('I',r)).Value = Lange;
    % Transmission values
    exSheet.get('Range',excelrange('K','AE',r)).Value = Values;
    % OriginalDB ID
    exSheet.get('Range',excelcell('J',r)).Value = OriginalDBID;
    % Print username and date
    exSheet.get('Range',excelcell('AF',r)).Value = user;
    exSheet.get('Range',excelcell('AG',r)).Value = date;
    exSheet.get('Range',excelcell('AH',r)).Value = time;
elseif modUber
    % Find the Excel row to modify
    r = Ubertragung.ExcelRow(Ubertragung.ID == ID);
    % Modify Startraum / Endraum / Startbauteil / Endbauteil
    exSheet.get('Range',excelcell('D',r)).Value = Startraum;
    exSheet.get('Range',excelcell('E',r)).Value = Endraum;
    exSheet.get('Range',excelcell('F',r)).Value = Startbauteil;
    exSheet.get('Range',excelcell('G',r)).Value = Endbauteil;
    % Modify Flache and Lange
    exSheet.get('Range',excelcell('H',r)).Value = Flache;
    exSheet.get('Range',excelcell('I',r)).Value = Lange;
    % Modify values
    exSheet.get('Range',excelrange('K','AE',r)).Value = Values;
    % OriginalDB ID
    exSheet.get('Range',excelcell('J',r)).Value = OriginalDBID;
    % Print username and date
    exSheet.get('Range',excelcell('AI',r)).Value = user;
    exSheet.get('Range',excelcell('AJ',r)).Value = date;
    exSheet.get('Range',excelcell('AK',r)).Value = time;
elseif delUber
    % Delete from the excel
    delUbertragung(ID,'excel',exSheet);
end

% If the delID variable exists, we need to remove that Ubertragung from the
% database
if exist('delID','var')==1
    % Remove that ID from the database
    delUbertragung(delID,'excel',exSheet);
end

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%% APPEND DATA TO KNOTEN STRUCTURE %%%%%%%%%%%%%%%%%%%%
if newUber
    % Get size of vectors
    n = size(Ubertragung.ID,1);

    % ExcelRow / Considered / ID / KnotenID / TypeID
    Ubertragung.ExcelRow = [Ubertragung.ExcelRow; r];
    Ubertragung.Considered = [Ubertragung.Considered; true];
    Ubertragung.ID = [Ubertragung.ID; ID];
    Ubertragung.Knoten = [Ubertragung.Knoten; KnotenID];
    Ubertragung.TypeID = [Ubertragung.TypeID; TypeID];
    
    % Startraum / Endraum / Startbauteil / Endbauteil
    Ubertragung.Raume = [Ubertragung.Raume; Startraum, Endraum];
    Ubertragung.Bauteile = [Ubertragung.Bauteile; Startbauteil, Endbauteil];
    % Flachen und Area
    Ubertragung.Area = [Ubertragung.Area; Flache];
    Ubertragung.Length = [Ubertragung.Length; Lange];
    % Values
    Ubertragung.Values = [Ubertragung.Values; Values];
    % OriginalDB-ID
    Ubertragung.OriginalID = addOriginalID(Ubertragung.OriginalID,OriginalDBID);
    % OriginalID-string
    Ubertragung.OriginalIDstr{n+1,1} = OriginalDBID;

elseif modUber
    % Find the index of the Ubertragung with the given ID
    indx = find(Ubertragung.ID == ID);
    
    % Replace Startraum / Endraum / Startbauteil / Endbauteil
    Ubertragung.Raume(indx,:) = [Startraum,Endraum];
    Ubertragung.Bauteile(indx,:) = [Startbauteil,Endbauteil];
    % Replace flachen and area
    Ubertragung.Area(indx) = Flache;
    Ubertragung.Length(indx) = Lange;
    % Replace the values
    Ubertragung.Values(indx,:) = Values;
    % OriginalDB-ID
    Ubertragung.OriginalID = addOriginalID(Ubertragung.OriginalID,...
                                           OriginalDBID,indx);
    % OriginalID-string
    Ubertragung.OriginalIDstr{indx,1} = OriginalDBID;
    
elseif delUber
    % Delete data from structure
    delUbertragung(ID,'structure');
    % Delete other objects associated with this Ubertragung
    % Not necessary since there are no object pointing to Ubertragung
end

% If the delID variable exists, we need to remove that Ubertragung from the
% database
if exist('delID','var')==1
    % Remove that ID from the database
    delUbertragung(delID,'structure');
end

% Update the dropdown names
Ubertragung.Dropdown = dropdownNames(Ubertragung,'ubertragung');

%% Hide the originating window (the parent of the button)
if strcmpi(fExportUbertragung.Visible,'on')
    % Hide the export window
    fExportUbertragung.Visible = 'off';
else
    if strcmpi(pKnoten.Visible,'on')
        % Populate the Knoten panle
        pKnoten_Populate(pKnoten.Data.KnotenID);
    else
        % Populate the Ubertragung panel with the correct Ubertragung
        pUbertragung_Populate(ID);
    end
end

end