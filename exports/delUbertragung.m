function delUbertragung(IDs,type,exSheet)

% Function takes care of removing Ubertragungen by their ID from:
% 1. Ubertragung Excel Database
% 2. Global Ubertragung Structure

% Global variables
global Ubertragung

% Check the inputs
if ~any(strcmpi(type,{'excel','structure'}))
    error('Type argument must be either ''excel'' or ''structure''');
end

% Find the indexes of the IDs in the Ubertragung.ID structure
[b,indx] = ismember(IDs,Ubertragung.ID);

% Before deleting the data check that we can find all IDs
if ~all(b)
    error('Cannot find all IDs in the Ubertragung.ID structure');
end

% Retrieve the excel rows
rows = Ubertragung.ExcelRow(indx);

%% %%%%%%%%%%%%%%%% UPDATE THE EXCEL DATABASE %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the excel database (we are using the structure to determine the 
% rows in the excel file in which the Ubertragungen are stored and
% therefore we ALWAYS have to delete FIRST the Excel database and SECOND
% the structure
if strcmpi(type,'excel')
    % Define function for excel range
    excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];
    
    % Iterate over the IDs and empty the rows
    for i=1:numel(IDs)
        exSheet.get('Range',excelrange('A','AK',rows(i))).Value = '';
    end
    
    % Return
    return;
end

%% %%%%%%%%%%%%%%% UPDATE THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmpi(type,'structure')
    % Delete the considered vector (row is offset by 1: Header)
    Ubertragung.Considered = delBool(Ubertragung.Considered,...
                                     rows-1);
    % Delete the ID, ExcelRow, TypeID vectors
    Ubertragung.ID(indx) = [];
    Ubertragung.ExcelRow(indx) = [];
    Ubertragung.TypeID(indx) = [];
    Ubertragung.Knoten(indx) = [];
    Ubertragung.Raume(indx,:) = [];
    Ubertragung.Bauteile(indx,:) = [];
    Ubertragung.Area(indx) = [];
    Ubertragung.Length(indx) = [];
    Ubertragung.OriginalID(indx,:,:) = [];
    Ubertragung.Values(indx,:) = [];
    % Delete cells
    Ubertragung.OriginalIDstr = delRow(Ubertragung.OriginalIDstr,indx);
    Ubertragung.Dropdown = delRow(Ubertragung.Dropdown,indx);
    
    % Return
    return;
end