function exportVUbertragung(src,~)

% Function exports the data selected and inputted by the user to the
% Vorsatz database and the internal representation of the database

%% %%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure
global fExportVUbertragung
global pVUbertragung
global pKnoten
% Configurations structure
global Configs
% Data
global VUbertragung

%% %%%%%%%%%%%%%%% FLOW CONTROL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define variables used for flow control
newVUber = false;
modVUber = false;
delVUber = false;

% Define which graphical element is calling this function
if src.Parent == fExportVUbertragung
    newVUber = true;
elseif src.Parent == pVUbertragung
    switch src.Tag
        case 'ModifyBUT'
            modVUber = true;
        case 'DeleteBUT'
            delVUber = true;
    end
end

%% %%%%%%%%%% RETRIEVE OBJECTS IN FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = src.Parent.Children;

% Radio
Agr = findobj(obj,'Tag','StrahlungRAD');
l = findobj(Agr.Children,'Tag','LuftRAD');
t = findobj(Agr.Children,'Tag','TrittRAD');

% Checkboxes
p = findobj(obj,'Tag','AnisoParallCHE');
s = findobj(obj,'Tag','AnisoSenkCHE');
d = findobj(obj,'Tag','AnisoDirCHE');

if newVUber
    % Dropdowns
    vDD = findobj(obj,'Tag','VorsatzDD');
    
elseif modVUber || delVUber
    % Radio buttons for Anisotropie
    pR = findobj(obj,'Tag','AnisoParallRAD');
    sR = findobj(obj,'Tag','AnisoSenkRAD');
    dR = findobj(obj,'Tag','AnisoDirRAD');
    % Dropdowns
    vuDD = findobj(obj,'Tag','VUbertragungDD');
end

% Retrieve the data
if newVUber
    data = fExportVUbertragung.Data;
    vID = data.Vorsatz(vDD.Value);
    
elseif modVUber || delVUber
    data = pVUbertragung.Data;
    vuID = data.VUbertragung(vuDD.Value);
    selAnisoID = pR.Value*1 + sR.Value*2 + dR.Value*3;
end

%% %%%%%%%%%%%%%%% ASK CONFIRMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ask confirmation for overwriting the data when the user is modifying a
% VUbertragung
if newVUber 
    proceed = askconfirmation('Vorsatz-Ubertragung','create');
elseif modVUber
    proceed = askconfirmation('Vorsatz-Ubertragung','modify');
elseif delVUber
    proceed = askconfirmation('Vorsatz-Ubertragung','delete');
end

% Check if we can proceed
if ~proceed
    return;
end

%% %%%%%%%%%%%%%%% DATA TO BE STORED IN VUBERTRAGUNG DB %%%%%%%%%%%%%%%%%%%
% Calculate the TypeID
TypeID = l.Value*1 + t.Value*2;

% Find out the Vorsatz-ID for the selected VUbertragung object
if modVUber || delVUber
    % Check if we have a match between the selected VUbertragung object and
    % the database
    b = (VUbertragung.ID == vuID);
    % Check if we have a match
    if any(b)
        % Find out the ID of the Vorsatz
        vID = (VUbertragung.Vorsatz(b));
    else
        % Throw error
        errordlg(['Vorsatz-Ubertragung selected in the dropdown is',...
                  ' not present in the VUbertraung database!',13,...
                  'Please select another Vorsatz-Ubertragung'],...
                 'Vorsatz-Ubertragung Database Error','modal');
        return;
    end
end

% Define an array containing the values selected for anisotropie in the
% anisotropie checkboxes
aniso = ([p.Value, s.Value, d.Value] == 1);
anisoID = [1,2,3];

% Check that we have selected at least one anisotropie type
if ~any(aniso)
    errordlg('Please select at least one type of Anisotropie!',...
             'Missing Anisotropie - Error','modal');
    return;
end

%% %%%%%%%%%%%%%%%%%%%%% CHECK UNIQUENESS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configurations which are valid for this vorsatzID (All configurations)
configs = zeros(numel(anisoID),3);
configs(:,1) = vID;
configs(:,2) = TypeID;
configs(:,3) = anisoID;

% Find if these configurations already exist
[b,indx] = ismember(configs,[VUbertragung.Vorsatz,...
                             VUbertragung.TypeID,...
                             VUbertragung.Anisotropie],'rows');

% Find out which anisotropie-ID have been selected, if the selected
% configuration exists and its eventual index in VUbertragung
anisoID = anisoID(aniso);
b = b(aniso);
indx = indx(aniso);

% If we are in modification mode, we only want to query the user if he is
% modifying OTHER VUbertragungen ADDITIONAL to the one selected
if newVUber || modVUber
    % Copy the vector indicating if the VUbertragung exists in the database
    % and the index vector
    b_check = b;
    indx_check = indx;

    if modVUber
        % Find at which position the radio-chosen configuration is in the 
        % set of all selected configurations
        [b_sel,indx_sel] = ismember([vID,TypeID,selAnisoID],...
                                    configs(aniso,:),'rows');
        % Remove the radio-selected configuration if it exist in the database,
        % because we already asked the user to confirm for that one
        if b_sel
            b_check(indx_sel) = [];
            indx_check(indx_sel) = [];
        end
    end
    
    % If we are modifiying existing VUbertragungen (other than the selected
    % one in the case of modification) we ask the user if he wants to
    % overwrite the existing data
    if any(b_check)
        s = ['The selected values for Anisotropie will cause an',...
             ' overwrite of the following Vorsatz-Ubertragungen:',10,10];
        % Retrieve the dropdown names of the VUbertragungen to be modified
        s = [s,strjoin(VUbertragung.Dropdown(indx_check(b_check)),char(10))];
        % Complete the question
        s = [s,10,10,'Do you wish to proceed anyway?'];
        % Throw the warning
        q = questdlg(s,'Overwrite existing Vorsatz-Ubertragungen',...
                       'No','Yes','Yes');
        switch lower(q)
            case 'no'
                return;
        end
    end
end

% Store negative b
nb = ~b;

% Calculate how many anisotropie-ID we are creating / modifying / deleting
% Calculate how many anisotropie-ID are new
n = numel(anisoID);
k = sum(nb);

%% %%%%%%%%%%%%%%%%%%%% DATA + USERNAMES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve correct IDs, existing VUbertragungen will be overwritten and new
% VUbertragungen get a new ID
ID = zeros(n,1);
ID(b) = VUbertragung.ID(indx(b));
if any(nb)
    ID(nb) = max(VUbertragung.ID) + (1:k);
end

% Retrieve correct indexes for new VUbertragungen
if any(nb)
    indx(nb) = numel(VUbertragung.ID) + (1:k);
end

% Original Database ID
OriginalDBID = data.OriginalDBID;

% Retrieve the transmission dR/dL values
Values = data.Values;
% Round values to 0.1
Values = round_dec(Values,1);

% Get the user performing the database write
user = getenv('USERNAME');
% Get the exact time as a string (to the second)
date = datestr(now,'dd-mmm-yyyy');
time = datestr(now,'HH:MM:SS');

%% %%%%%%%%%%%%%% EXPORT DATA TO EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.VUbertragung,1);

% Check if we can continue
if ~success
    return;
end

% Extract the sheet of interest
exSheet = exSH{1,1};
% Extract the last row
lastrow = exSH{1,2};

% Calculate the writerow, existing VUbertragungen are taken from the
% structure and new VUbertragungen get a new writerow
r = zeros(n,1);
r(b) = VUbertragung.ExcelRow(indx(b));
if any(nb)
    r(nb) = lastrow + (1:k);
end

% Create function for creating a excel range string
excelcell = @(c,r) [c,num2str(r,'%.0f')];
excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];

% %%%%%%%%%%%%%%%%% WRITE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if newVUber || modVUber
    for i=1:n
        % Find out the row
        row = r(i);
        % Write ID / VorsatzID / TypeID / AnisotropieID 
        exSheet.get('Range',excelcell('A',row)).Value = ID(i);
        exSheet.get('Range',excelcell('B',row)).Value = vID;
        exSheet.get('Range',excelcell('C',row)).Value = TypeID;
        exSheet.get('Range',excelcell('D',row)).Value = anisoID(i);
        % OriginalDB ID
        exSheet.get('Range',excelcell('E',row)).Value = OriginalDBID;
        % Transmission values
        exSheet.get('Range',excelrange('F','Z',row)).Value = Values;
        % Print username and date depending on whether its an update or a
        % new write
        if ~b(i)
            exSheet.get('Range',excelcell('AA',row)).Value = user;
            exSheet.get('Range',excelcell('AB',row)).Value = date;
            exSheet.get('Range',excelcell('AC',row)).Value = time;
        else
            exSheet.get('Range',excelcell('AD',row)).Value = user;
            exSheet.get('Range',excelcell('AE',row)).Value = date;
            exSheet.get('Range',excelcell('AF',row)).Value = time;
        end
    end
elseif delVUber
    % Be sure to limit the IDs to existing IDs
    b = ismember(ID,VUbertragung.ID);
    ID = ID(b);
    % Delete the excel rows only if ID is not empty
    if ~empty(ID)
        delVUbertragung(ID,'excel',exSheet);
    end
end   

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%% APPEND DATA TO VUBERTRAGUNG STRUCTURE %%%%%%%%%%%%%%%%%%
% Add / Modify data in the structure
if newVUber || modVUber
    % First start by resizing the structure to support additional data
    at = zeros(k,1);
    b = true(k,1);
    v = zeros(k,size(VUbertragung.Values,2));
    
    % Considered
    VUbertragung.Considered = [VUbertragung.Considered; b];
    % ExcelRow / ID / VorsatzID / TypeID / AnisotropieID
    VUbertragung.ExcelRow = [VUbertragung.ExcelRow; at];
    VUbertragung.ID = [VUbertragung.ID; at];
    VUbertragung.Vorsatz = [VUbertragung.Vorsatz; at];
    VUbertragung.TypeID = [VUbertragung.TypeID; at];
    % Values
    VUbertragung.Values = [VUbertragung.Values; v];
    
	% Iterate over the data to be written 
    for i=1:n
        % Retrieve the index
        ind = indx(i);
        % ExcelRow / ID / VorsatzID / TypeID / AnisotropieID
        VUbertragung.ExcelRow(ind) = r(i);
        VUbertragung.ID(ind) = ID(i);
        VUbertragung.Vorsatz(ind) = vID;
        VUbertragung.TypeID(ind) = TypeID;
        VUbertragung.Anisotropie(ind) = anisoID(i);
        % Values
        VUbertragung.Values(ind,:) = Values;
        % Original DB-ID string
        VUbertragung.OriginalIDstr{ind,1} = OriginalDBID;
        % Update the original DB-ID
        VUbertragung.OriginalID = addOriginalID(VUbertragung.OriginalID,...
                                                OriginalDBID,ind);
    end
    
elseif delVUber
    % Delete from structure
    if ~empty(ID)
        delVUbertragung(ID,'structure');
    end
    % Delete objects related to this VUbertragung
    % Not necessary since no objects point to VUbertragung
end

% Update the dropdown names
VUbertragung.Dropdown = dropdownNames(VUbertragung,'vubertragung');

%% Hide the originating window (the parent of the button)
if newVUber
    src.Parent.Visible = 'off';
else
    % Populate the VUbertragung panel with the correct VUbertragung
    if ~isempty(ID)
        ID = ID(1);
    else
        ID = 1;
    end
    if strcmpi(pKnoten.Visible,'on')
        % Update the Knoten panel
        pKnoten_Populate(pKnoten.Data.KnotenID);
    else
        pVUbertragung_Populate(ID);
    end
end

end