function exportVorsatz(src,~)

% Function exports the data selected and inputted by the user to the
% Vorsatz database and the internal representation of the database

%% %%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure and panels
global fCreateVorsatz
global pVorsatz
global pKnoten
% Configurations structure
global Configs
% Data
global Vorsatz
global Bauteile
global VUbertragung

%% %%%%%%%%%%%%%%% FLOW CONTROL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define variables used for flow control
newVor = false;
modVor = false;
delVor = false;

% Define which graphical element is calling this function
switch src.Parent
    case fCreateVorsatz
        newVor = true;
    case pVorsatz
        % Find out if we are modifying or deleting the Vorsatz
        switch src.Tag
            case 'ModifyBUT'
                modVor = true;
            case 'DeleteBUT'
                delVor = true;
        end
end

%% %%%%%%%%%% RETRIEVE OBJECTS IN FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the objects depending on the source of the call
obj = src.Parent.Children;

% Text objects
tN = findobj(obj,'Tag','VorsatzNameTXT');
tK = findobj(obj,'Tag','KurzBeschreibungTXT');
tD = findobj(obj,'Tag','DetBeschreibungTXT');

% Data structure
if newVor
    data = fCreateVorsatz.Data;
elseif modVor || delVor
    data = pVorsatz.Data;
end

% Vorsatz ID
if modVor || delVor
    vDD = findobj(obj,'Tag','VorsatzDD');
    selID = data.Vorsatz(vDD.Value);
end

%% %%%%%%%%%%%%%%%%%%%% ASK CONFIRMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ask for confirmation for overwriting the data when the user is modifying
% a Vorsatz
if newVor
    proceed = askconfirmation('Vorsatz','create');
elseif modVor
    proceed = askconfirmation('Vorsatz','modify');
elseif delVor
    proceed = askconfirmation('Vorsatz','delete');
end

% Check if we can proceed
if ~proceed
    return;
end

%% %%%%%%%%%%%%%%%%%%%%%%%% CHECK VALID ID %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve ID
if newVor
    ID = max(Vorsatz.ID) + 1;
elseif modVor || delVor
    ID = selID;
end

% Check that the ID is not 0
if ~validID(ID,'Vorsatz')
    return;
end

%% %%%%%%%%%%%%%%%%%%%%%%% CHECK NAME TO BE UNIQUE %%%%%%%%%%%%%%%%%%%%%%%%
% Check that the name inputted by the user is unique and is not already
% existing in the database (unless we are modifying an already existing 
% Vorsatz, in which case the Name can be the same, because we are
% overwriting it
Name = tN.String;

% Initialize the passed variable
passed = true;

% Check the name to be unique
if newVor
    passed = checkName(Vorsatz.Name,Name,'Vorsatz');
elseif modVor
    passed = checkName(Vorsatz.Name(Vorsatz.ID ~= selID),Name,'Vorsatz');
end

% Check if we can continue
if ~passed
    return;
end

%% %%%%%%%%%%%%%% Retrieve the data to be stored in the Vorsatz DB %%%%%%%%
% Retrieve the Kurzbeschreibung and Det. Beschreibung
KurzBeschr = tK.String;
DetBeschr = tD.String;

% Retrieve BauteileList, reshape it and sort it
bauIDs = data.BauteileList;
bauIDs = sort(bauIDs,'ascend');

% Check that the BauteileList contains only valid bauteile
if ~all(ismember(bauIDs,Bauteile.ID))
    % Throw error and return
    errordlg(['Some of the selected Bauteile are not present in the',...
             ' Bauteile-Database, please check again your entries!'],...
             'Bauteil not available Error','modal');
    return;
end  

% Concatenate the IDs to create a string
n = numel(bauIDs);
str = cell(1,n);
for i=1:n
    str{1,i} = num2str(bauIDs(i),'%.0f');
end
bauIDstr = strjoin(str,',');

%% %%%%%%%%%%%%% ASK IF WE WANT TO KEEP VUBERTRAGUNGEN %%%%%%%%%%%%%%%%%%%
% In case we are modifying the list of Bauteile
if modVor
    % Retrieve the Vorsatz that we are modifying
    b = (Vorsatz.ID == ID);
    % Find the previous list of Bauteile, remove NaN and sort
    teile = Vorsatz.Bauteile(b,:);
    teile = sort(teile(~isnan(teile)),'ascend');
    % Check if the Bauteile list has changed, in which case we still need
    % to check if there are VUbertragungen related to this Vorsatz to be
    % deleted
    if numel(teile)~=numel(bauIDs) || ~all(teile==bauIDs)
        % Find all VUbertragungen associated with this Vorsatz
        b = (VUbertragung.Vorsatz == ID);
        % If we have a match ask the user if he wants to keep the data or
        % not
        if any(b)
            q = questdlg(['Keep existing VUbertragungen even though the',...
                          ' Bauteile list has changed?'],...
                         'Keep VUbertragugen?',...
                         'No','Yes','Yes');
            switch lower(q)
                case 'no'
                    delVUber = true;
            end
        end
    end
end

% In case we are deleting the Vorsatz
if delVor
    % Find all VUbertragungen associated with this Vorsatz
    b = (VUbertragung.Vorsatz == ID);
    % IF we have a match ask the user if he wants to keep the
    % VUbertragungen or remove them
    if any(b)
        q = questdlg(['Delete existing VUbertragungen related to the',...
                      ' deleted Vorsatz?'],...
                     'Delete VUbertragungen?',...
                     'No','Yes','Yes');
        switch lower(q)
            case 'yes'
                delVUber = true;
        end
    end
end

% Perform checks if we have to delete any VUbertragungen
if exist('delVUber','var')==1 && delVUber
    % Retrieve the IDs of the VUbertragungen to delete
    vuID = VUbertragung.ID(b);
else
    delVUber = false;
end

%% %%%%%%%%%%%%%%%%%%% USER AND DATETIME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the user performing the database write
user = getenv('USERNAME');
% Get the exact time as a string (to the second)
date = datestr(now,'dd-mmm-yyyy');
time = datestr(now,'HH:MM:SS');

%% %%%%%%%%%%%%%% EXPORT DATA TO EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-Server and the workbooks
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Vorsatz,1);
% Open the COM-server and the workbooks for VUbertragung
if delVUber
    [success2,ex2,exWB2,exSH2] = openExcelFile(Configs.Database.VUbertragung,1);
    % compound the success
    success = (success && success2);
end

% Check if we can continue
if ~success
    return;
end

% Extract the sheet of interest
exSheet = exSH{1,1};
% Extract the last row
lastrow = exSH{1,2};

% Calculate teh writerow depending on the caller
if newVor
    r = lastrow + 1;
elseif modVor || delVor
    r = Vorsatz.ExcelRow(Vorsatz.ID == ID);
end

% Create function for creating a excel range string
excelcell = @(c,r) [c,num2str(r,'%.0f')];

% %%%%%%%%%%%%%%%%% WRITE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if newVor || modVor
    % ID / Name / Kurzbeschreibung / bauIDstr / DetBeschreibung
    exSheet.get('Range',excelcell('A',r)).Value = ID;
    exSheet.get('Range',excelcell('B',r)).Value = Name;
    exSheet.get('Range',excelcell('C',r)).Value = KurzBeschr;
    exSheet.get('Range',excelcell('D',r)).Value = bauIDstr;
    exSheet.get('Range',excelcell('E',r)).Value = DetBeschr;
    % Print username and date
    if newVor
        exSheet.get('Range',excelcell('F',r)).Value = user;
        exSheet.get('Range',excelcell('G',r)).Value = date;
        exSheet.get('Range',excelcell('H',r)).Value = time;
    else
        exSheet.get('Range',excelcell('I',r)).Value = user;
        exSheet.get('Range',excelcell('J',r)).Value = date;
        exSheet.get('Range',excelcell('K',r)).Value = time;
    end
else
    % Delete from the excel
    delVorsatz(ID,'excel',exSheet);
end

% Delete related VUbertragungen
if delVUber
    delVUbertragung(vuID,'excel',exSH2{1,1});
end

% Save and close the workbook and the COM-server
closeExcel(ex,exWB); 
if delVUber
    closeExcel(ex2,exWB2);
end

%% %%%%%%%%%%%%%%%%%% APPEND DATA TO KNOTEN STRUCTURE %%%%%%%%%%%%%%%%%%%%
if newVor
    % Get size of the vectors
    n = numel(Vorsatz.ID);

    % ExcelRow / Considered / ID
    Vorsatz.ExcelRow = [Vorsatz.ExcelRow; r];
    Vorsatz.Considered = [Vorsatz.Considered; true];
    Vorsatz.ID = [Vorsatz.ID; ID];
    % Name / KurzBeschr / DetBeschr
    Vorsatz.Name{n+1,1} = Name;
    Vorsatz.KurzBeschr{n+1,1} = KurzBeschr;
    Vorsatz.DetBeschr{n+1,1} = DetBeschr;
    % Bauteile
    Vorsatz.Bauteile = addBauteile(Vorsatz.Bauteile,bauIDs,n+1);
    
elseif modVor
    % Find the index in the array
    indx = find(Vorsatz.ID == ID);
    % Replace Name / KurzBeschr / DetBeschr
    Vorsatz.Name{indx,1} = Name;
    Vorsatz.KurzBeschr{indx,1} = KurzBeschr;
    Vorsatz.DetBeschr{indx,1} = DetBeschr;
    % Bauteile
    Vorsatz.Bauteile = addBauteile(Vorsatz.Bauteile,bauIDs,indx);
    
elseif delVor
    % Delete from structure
    delVorsatz(ID,'structure');
end

% Delete related VUbertragungen
if delVUber
    delVUbertragung(vuID,'structure');
end

% Update the dropdown cells
Vorsatz.Dropdown = dropdownNames(Vorsatz,'vorsatz');
% Update the associated VUbertragung dropdown names
VUbertragung.Dropdown = dropdownNames(VUbertragung,'vubertragung');
    
%% Hide the originating window (the parent of the button)
if newVor
    src.Parent.Visible = 'off';
else
    if strcmpi(pKnoten.Visible,'on')
        % Populate the Knoten panel
        pKnoten_Populate(pKnoten.Data.KnotenID);
    else
        % Populate the Vorsatz Panel with the correct Vorsatz
        pVorsatz_Populate(ID);
    end
end

end