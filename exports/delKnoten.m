function delKnoten(IDs,type,exSheet)

% Function takes care of removing Knoten by their ID from:
% 1. Knoten Excel Database
% 2. Global Knoten Structure

% Global variables
global Knoten

% Check the inputs
if ~any(strcmpi(type,{'excel','structure'}))
    error('Type argument must be either ''excel'' or ''structure''');
end

% Find the indexes of the IDs in the Knoten.ID structure
[b,indx] = ismember(IDs,Knoten.ID);

% Before deleting the data check that we can find all IDs
if ~all(b)
    error('Cannot find all IDs in the Knoten.ID structure');
end

% Retrieve the excel rows
rows = Knoten.ExcelRow(indx);

%% %%%%%%%%%%%%%%%% UPDATE THE EXCEL DATABASE %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the excel database (we are using the structure to determine the 
% rows in the excel file in which the Ubertragungen are stored and
% therefore we ALWAYS have to delete FIRST the Excel database and SECOND
% the structure
if strcmpi(type,'excel')
    % Define function for excel range
    excelrange = @(c1,c2,r) [c1,num2str(r,'%.0f'),':',c2,num2str(r,'%.0f')];
    
    % Iterate over the IDs and empty the rows
    for i=1:numel(IDs)
        exSheet.get('Range',excelrange('A','T',rows(i))).Value = '';
    end
    
    % Return
    return;
end

%% %%%%%%%%%%%%%%% UPDATE THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmpi(type,'structure')
    % Delete the considered vector (row is offset by 1: Header)
    Knoten.Considered = delBool(Knoten.Considered,...
                                rows-1);
    % Delete the ID, ExcelRow, TypeID and Ausrichtung vectors
    Knoten.ID(indx) = [];
    Knoten.ExcelRow(indx) = [];
    Knoten.TypeID(indx) = [];
    Knoten.Ausrichtung(indx) = [];
    % Delete the Bauteile  and Anisotropie
    Knoten.Bauteile(indx,:) = [];
    Knoten.Anisotropie(indx,:) = [];
    % Delete the Name, KurzBeschr, DetBeschr and Dropdown
    Knoten.Name = delRow(Knoten.Name,indx);
    Knoten.KurzBeschr = delRow(Knoten.KurzBeschr,indx);
    Knoten.DetBeschr = delRow(Knoten.DetBeschr,indx);
    Knoten.Dropdown = delRow(Knoten.Dropdown,indx);
    
    % Return
    return;
end