function resizeDataTab(src, callbackdata)
% Function takes care of placing the elements within the DataTab

%%%%%%%%%%%%%%%%%% RETRIEVE THE DATATAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DataTab = findobj('Tag','DatenTAB');

%%%%%%%%%%%%%%%%%% SPECIFY SKELETON OF UI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the number of horizontal elements and the number of vertical
% elements
nH = 8;
nV = 21;

%%%%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the tab border
% Format:       [left, bottom, right, top]
marg_tab = 5*ones(4,1);

% Spaces in between elements
intra = [5, 5];

% Retrieve the tab size and use relative positioning
tabpos = DataTab.Position;
tabpos(1:2) = 0;

%%%%%%%%%%%%%%%%%%%% GET ELEMENT POSITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate shares of each element of the GUI to the total share
shareH = ones(nH,1)/nH;
shareV = ones(nV,1)/nV;

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(tabpos, nH, nV, marg_tab, intra, shareH, shareV);

%%%%%%%%%%%% FIND ELEMENTS THAT HAVE DataTab AS PARENT %%%%%%%%%%%%%%%%%%%
obj = findobj('Parent',DataTab);

%%%%%%%%%%%%%%%%%%%% PLACE LABEL ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% REPORTS %%%%%%%%%%
i = 21;
lb = findobj(obj,'Tag','DTProjLB');
lb.Position = BoundingBox(boxes,1,i,nH); i = i-1;
lb = findobj(obj,'Tag','DTPrufObjLB');
lb.Position = BoundingBox(boxes,1,i,nH); i = i-1;
lb = findobj(obj,'Tag','DTReportClientLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportNumberLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportDateLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportRemarksLB');
lb.Position = BoundingBox(boxes,1,i,nH);
%%%%%%% DATATYPES AND LINKS %%%%%%%%%%
i = 14;
lb = findobj(obj,'Tag','DTDataTypeLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTImportDateLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTLinkAirLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTLinkImpactLB');
lb.Position = BoundingBox(boxes,1,i,nH);
%%%%%%% SOURCE ROOM %%%%%%%%%%
i = 9;
lb = findobj(obj,'Tag','DTSourceRoomLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTSRNotesLB');
lb.Position = BoundingBox(boxes,1,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTSRVolumeLB');
lb.Position = BoundingBox(boxes,1,i,nH);
lb = findobj(obj,'Tag','DTSRHumLB');
lb.Position = BoundingBox(boxes,3,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTSRTempLB');
lb.Position = BoundingBox(boxes,1,i,nH);
lb = findobj(obj,'Tag','DTSRPresLB');
lb.Position = BoundingBox(boxes,3,i,nH);
%%%%%%% RECEIVER ROOM %%%%%%%%%%
i = 9;
lb = findobj(obj,'Tag','DTReceiverRoomLB');
lb.Position = BoundingBox(boxes,5,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTRRNotesLB');
lb.Position = BoundingBox(boxes,5,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTRRVolumeLB');
lb.Position = BoundingBox(boxes,5,i,nH);
lb = findobj(obj,'Tag','DTRRHumLB');
lb.Position = BoundingBox(boxes,7,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTRRTempLB');
lb.Position = BoundingBox(boxes,5,i,nH);
lb = findobj(obj,'Tag','DTRRPresLB');
lb.Position = BoundingBox(boxes,7,i,nH);
%%%%%%% TEST OBJECT AND TEST DESCRIPTION %%%%%%%%%%
i = 21;
lb = findobj(obj,'Tag','DTTestTitleLB');
lb.Position = BoundingBox(boxes,5,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTTestDateLB');
lb.Position = BoundingBox(boxes,5,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTTestDescriptionLB');
lb.Position = BoundingBox(boxes,5,i,nH);  i = i-4;
lb = findobj(obj,'Tag','DTTestObjectLB');
lb.Position = BoundingBox(boxes,5,i,nH);
%%%%%%% MEASURED DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 4;
lb = findobj(obj,'Tag','DTCLB');
lb.Position = BoundingBox(boxes,1,i,nH);
lb = findobj(obj,'Tag','DTCtrLB');
lb.Position = BoundingBox(boxes,3,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTC3150LB');
lb.Position = BoundingBox(boxes,1,i,nH);
lb = findobj(obj,'Tag','DTCtr3150LB');
lb.Position = BoundingBox(boxes,3,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTC50LB');
lb.Position = BoundingBox(boxes,1,i,nH);
lb = findobj(obj,'Tag','DTCtr50LB');
lb.Position = BoundingBox(boxes,3,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTC100LB');
lb.Position = BoundingBox(boxes,1,i,nH);
lb = findobj(obj,'Tag','DTCtr100LB');
lb.Position = BoundingBox(boxes,3,i,nH);
i = 4;
lb = findobj(obj,'Tag','DTSNRLB');
lb.Position = BoundingBox(boxes,5,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTAreaLB');
lb.Position = BoundingBox(boxes,5,i,nH);

%%%%%%%%%%%%%%%%%%%% PLACE TEXT ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% REPORTS %%%%%%%%%%
i = 21;
lb = findobj(obj,'Tag','DTProjTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTPrufObjTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportClientTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportNumberTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportDateTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTReportRemarksTXT');
lb.Position = BoundingBox(boxes,[2,4],[i-1,i],nH);
%%%%%%% DATATYPES AND LINKS %%%%%%%%%%
i = 14;
lb = findobj(obj,'Tag','DTDataTypeTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTImportDateTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTLinkAirTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTLinkImpactTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);
%%%%%%% SOURCE ROOM %%%%%%%%%%
i = 9;
lb = findobj(obj,'Tag','DTSourceRoomTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTSRNotesTXT');
lb.Position = BoundingBox(boxes,[2,4],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTSRVolumeTXT');
lb.Position = BoundingBox(boxes,2,i,nH);
lb = findobj(obj,'Tag','DTSRHumTXT');
lb.Position = BoundingBox(boxes,4,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTSRTempTXT');
lb.Position = BoundingBox(boxes,2,i,nH);
lb = findobj(obj,'Tag','DTSRPresTXT');
lb.Position = BoundingBox(boxes,4,i,nH);
%%%%%%% RECEIVER ROOM %%%%%%%%%%
i = 9;
lb = findobj(obj,'Tag','DTReceiverRoomTXT');
lb.Position = BoundingBox(boxes,[6,8],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTRRNotesTXT');
lb.Position = BoundingBox(boxes,[6,8],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTRRVolumeTXT');
lb.Position = BoundingBox(boxes,6,i,nH);
lb = findobj(obj,'Tag','DTRRHumTXT');
lb.Position = BoundingBox(boxes,8,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTRRTempTXT');
lb.Position = BoundingBox(boxes,6,i,nH);
lb = findobj(obj,'Tag','DTRRPresTXT');
lb.Position = BoundingBox(boxes,8,i,nH);
%%%%%%% TEST OBJECT AND TEST DESCRIPTION %%%%%%%%%%
i = 21;
lb = findobj(obj,'Tag','DTTestTitleTXT');
lb.Position = BoundingBox(boxes,[6,8],i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTTestDateTXT');
lb.Position = BoundingBox(boxes,[6,8],i,nH);  i = i-2;
lb = findobj(obj,'Tag','DTTestDescriptionTXT');
lb.Position = BoundingBox(boxes,[5,8],[i-2,i],nH);  i = i-4;
lb = findobj(obj,'Tag','DTTestObjectTXT');
lb.Position = BoundingBox(boxes,[5,8],[i-3,i],nH);
%%%%%%% MEASURED DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 4;
lb = findobj(obj,'Tag','DTCTXT');
lb.Position = BoundingBox(boxes,2,i,nH);
lb = findobj(obj,'Tag','DTCtrTXT');
lb.Position = BoundingBox(boxes,4,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTC3150TXT');
lb.Position = BoundingBox(boxes,2,i,nH);
lb = findobj(obj,'Tag','DTCtr3150TXT');
lb.Position = BoundingBox(boxes,4,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTC50TXT');
lb.Position = BoundingBox(boxes,2,i,nH);
lb = findobj(obj,'Tag','DTCtr50TXT');
lb.Position = BoundingBox(boxes,4,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTC100TXT');
lb.Position = BoundingBox(boxes,2,i,nH);
lb = findobj(obj,'Tag','DTCtr100TXT');
lb.Position = BoundingBox(boxes,4,i,nH);
i = 4;
lb = findobj(obj,'Tag','DTSNRTXT');
lb.Position = BoundingBox(boxes,6,i,nH);  i = i-1;
lb = findobj(obj,'Tag','DTAreaTXT');
lb.Position = BoundingBox(boxes,6,i,nH);

end
