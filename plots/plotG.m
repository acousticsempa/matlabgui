function plotG(inputData)
% This function plots the 'Grundriss' view of the current 
% test
% The walls are displayed using a color encoding to show of which kind they
% are

%%%%%%%%%%%%%%%%%%% Create variable shortcuts %%%%%%%%%%%%%%%%%%%%%%%%%%%
l = inputData.WallLength; 
w = inputData.WallWidth;
s = inputData.Space;
C = inputData.WallColor;
SC = inputData.ShieldColor;
fW = inputData.WallFont;
fS = inputData.ShieldFont;
fR = inputData.RoomFont;

%% %%%%%%%%%%%%%%%%%% PLOTTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the AufbauTab for Grund
tab = findobj('Tag','GrundTAB');
% Find axes
ax = findobj(tab.Children,'Type','Axes');
if isempty(ax)
    % Initialize the axes
    ax = axes('Parent',tab,'Units','normalized',...
              'Position',[0,0,1,1]);
end
% Remove the children and the legend
delete(ax.Children(:));
delete(ax.Legend);

%% %%%%%%%%%%%%%%%%%% PLOT THE WALLS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start with the first 'Fixed walls'
wID = 3;
for i=1:2
    yb = (i-1)*(l+s);
    patch(ax,[-w,2*l,2*l,-w],yb+[-w,-w,0,0],C(wID,:));
    patch(ax,[-w,0,0,-w],yb+[-w,-w,l,l],C(wID,:));
end

% Then iteratively plot the other walls
wallsID = [2,4,6,8;...
           1,3,5,7];
wallRot = -[0,0,90,90];
wX = [0,l-0.5*w,l-0.5*w,0;...       % SWB, SWA
      l-0.5*w,2*l,2*l,l-0.5*w;...   % SWD, SWC
      l-w,l,l,l-w;...               % TWBD, TWAC
      2*l-w,2*l,2*l,2*l-w];         % AWD, AWC
wY = [l-w,l-w,l,l;...               % SWB, SWA
      l-w,l-w,l,l;...               % SWD, SWC
      0,0,l-w,l-w;...               % TWBD, TWAC
      0,0,l-w,l-w];                 % AWD, AWC 
for i=1:size(wallsID,1)
    yb = (i-1)*(l+s);
    for j=1:size(wallsID,2)
        wID = inputData.WallSpec(wallsID(i,j));
        if wID~=0
            % Plot the wall
            patch(ax,wX(j,:),yb + wY(j,:),C(wID,:));
            % Plot the name of the wall and it's type
            text(ax,0.5*(wX(j,1)+wX(j,3)),yb+0.5*(wY(j,1)+wY(j,3)),...
                 strcat(inputData.WallIdentifier{wallsID(i,j)},' (',...
                        inputData.WallTypes{wID},')'),...
                 'HorizontalAlignment','center',...
                 'Rotation',wallRot(j),...
                 'FontSize',fW);
        end
    end
end

%% %%%%%%%%%%%%%%%% PLOT THE SHIELDING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shieldID = [2,4,11,12,6;...
            1,3,9,10,5];
shieldRot = -[0,0,90,90,90];
wX = [0,l-w,l-w,0;...               % SWB_SB, SWA_SA
      l,2*l-w,2*l-w,l;...           % SWD_SD, SWC_SC
      l-2*w,l-w,l-w,l-2*w;...       % TWBD_SB, TWAC_SA
      l,l+w,l+w,l;...               % TWBD_SD, TWAC_SC
      2*l-2*w,2*l-w,2*l-w,2*l-2*w]; % AWD_SD, AWC_SC
wY = [l-2*w,l-2*w,l-w,l-w;...       % SWB_SB, SWA_SA
      l-2*w,l-2*w,l-w,l-w;...       % SWD_SD, SWC_SC
      0, 0, l-w, l-w;...            % TWBD_SB, TWAC_SA
      0, 0, l-w, l-w;...            % TWBD_SD, TWAC_SC
      0, 0, l-w, l-w];              % AWD_SD, AWC_SC
for i=1:size(shieldID,1)
    yb = (i-1)*(l+s);
    for j=1:size(shieldID,2)
        sID = inputData.ShieldSpec(shieldID(i,j));
        if sID
            % Plot the shielding
            patch(ax,wX(j,:),yb+wY(j,:),SC);
            % Plot the name of the shielding
            text(ax,0.5*(wX(j,1)+wX(j,3)),yb+0.5*(wY(j,1)+wY(j,3)),...
                 strcat(inputData.ShieldIdentifier{shieldID(i,j)},' (Shielding)'),...
                 'HorizontalAlignment','center',...
                 'Rotation',shieldRot(j),...
                 'FontSize',fS);
        end
    end
end

%% %%%%%%%%%%%%%%%%% PLOT THE ROOMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare the string with the sender room and receiver room to be added to
% the string that is going to be printed
emptyStr = {'','','',''};
rooms = {'A','B','C','D'};
% Find which is the sender room
SR = strcmpi(inputData.SR,rooms);
SRstr = emptyStr;
if sum(SR) > 0
    SRstr{SR} = '(S)';
end
% Find which is the receiver room
RR = strcmpi(inputData.RR,rooms);
RRstr = emptyStr;
if sum(RR) > 0
    RRstr{RR} = '(R)';
end

% Concatenate the string for printing
rooms = strcat(rooms,SRstr,RRstr);

% Print the room string to the graph
text(ax,0.5*l,s+1.5*l,rooms{1},'FontSize',fR,'HorizontalAlignment','center');
text(ax,0.5*l,0.5*l,rooms{2},'FontSize',fR,'HorizontalAlignment','center');
text(ax,1.5*l,s+1.5*l,rooms{3},'FontSize',fR,'HorizontalAlignment','center');
text(ax,1.5*l,0.5*l,rooms{4},'FontSize',fR,'HorizontalAlignment','center');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%% FORMAT THE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set limits for the graphs
ax.XLim = [-w,2*l];
ax.YLim = [-w,2*l+s];
ax.ZLim = [0,0.1*l];
axLim = [ax.XLim;ax.YLim;ax.ZLim];
dxyz = axLim(:,2) - axLim(:,1);
ax.PlotBoxAspectRatio = dxyz'/max(dxyz);
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';

% Remove the background color
ax.Color = 0.94*ones(1,3);

end

