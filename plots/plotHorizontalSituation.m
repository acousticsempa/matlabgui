function plotHorizontalSituation(ax,teile,anisotropie,vor,lx,lyFR,lySR,lz)

% Global variables
global Bauteile
global Vorsatz

% Delete all the axes children and the legend
delete(ax.Children(:));
delete(ax.Legend);

%% %%%%%%%%%%%%%%%%%%%% DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define certain geometric variables
w =  0.2;             % Wall width
wV = 0.2;             % Vorsatz width
d = 0.3;              % Ceiling / Floor depth
l = lyFR + w + lySR;  % Total length

% Retrieve the faces matrix
[~,fc] = vertices();

% Hold the axes and retrieve the current view
hold(ax,'on');
axView = ax.View;

%% %%%%%%%%%%%%%%%%%%%%%%%%%% COLORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%% Bauteile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out the unique elements in the teile array
[t_u,t_indxInMulti,t_indxInUnique] = unique(teile,'stable');
% Find out which teile_u are present in the Bauteile database
t_b = ismember(t_u,Bauteile.ID);
% Compound back to retrieve a vector of bauteile to plot
t_b_plot = t_b(t_indxInUnique);
% Create a map for each teile to the corresponding color
t_u_plot = t_u(t_b);
[~,t_colorIndx] = ismember(teile,t_u_plot);
% Calculate number of distinctive bauteile
n_teile = numel(t_u_plot);

% %%%%%%%%%%%%%%%%%%%% VORSATZSCHALEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out the unique elements in the vorsatz (ID) array
[v_u,v_indxInMulti,v_indxInUnique] = unique(vor,'stable');
% Find out which vor_u are present in the Vorsatz database
v_b = ismember(v_u,Vorsatz.ID);
% Compound back to retrieve a vector of vorsatze to plot
v_b_plot = v_b(v_indxInUnique);
% Create a map for each vor to the corresponding color
v_u_plot = v_u(v_b);
[~,v_colorIndx] = ismember(vor,v_u_plot); 
% Offset due to the printing of the Bauteile first
v_colorIndx = v_colorIndx + n_teile;
% Number of vorsatzschalen
n_vor = numel(v_u_plot);

% %%%%%%%%%%%%%%%%%%%% COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of colors needed
n_colors = n_teile + n_vor;

% Retrieve the color palette
colors = getColors(n_colors);
                
%% %%%%%%%%%%%%%%%%%% PLOT THE BAUTEILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The teile vector should be in the following format
% 1: Bauteil in first room on the floor
% 2: Bauteil in first room on the y-axis
% 3: Bauteil in first room on the ceiling
% 4: Bauteil in first room parallel to y-axis
% 5: Bauteil separating first room from second room
% 6: Bauteil in second room on the floor
% 7: Bauteil in second room on the y-axis
% 8: Bauteil in second room on the ceiling
% 9: Bauteil in second room parallel to the y-axis

% Define the positions and volumes, so that we can loop
pos = [-w,      0, -d;...   % First room Bauteile
       -w,      0,  0;...
       -w,      0, lz;...
       lx,      0,  0;...   
       -w,   lyFR, -d;...   % Separation wall
       -w, lyFR+w, -d;...   % Second room Bauteile
       -w, lyFR+w,  0;...
       -w, lyFR+w, lz;...
       lx, lyFR+w,  0];
vol = [lx+2*w, lyFR,      d;...     % First room Bauteile
            w, lyFR,     lz;...
       lx+2*w, lyFR,      d;...
            w, lyFR,     lz;...
       lx+2*w,    w, lz+2*d;...     % Separation wall
       lx+2*w, lySR,      d;...     % Second room Bauteile
            w, lySR,     lz;...
       lx+2*w, lySR,      d;...
            w, lySR,     lz];
    
% Initialize the legend
leg_h = gobjects(9,1);
leg_s = cell(9,1);

% Loop over the Bauteile
for i=1:9
    % Plot the Bauteil only if it is valid
    if t_b_plot(i)
        ha = plotBauteil3D(ax,fc,vertices(pos(i,:),vol(i,:)),...
                              colors(t_colorIndx(i),:),...
                              anisotropie(i));
        % Legend
        leg_h(i) = ha;
        leg_s{i,1} = Bauteile.Dropdown{Bauteile.ID==teile(i),1};
    end
end

% Reduce the legend to the unique elements
forLeg = t_indxInMulti(t_b);
leg_h = leg_h(forLeg);
leg_s = leg_s(forLeg,1);

%% %%%%%%%%%%%%%%%%%% PLOT THE VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The vor vector should be in the following format
% 1: Vorsatz in first room on the floor
% 2: Vorsatz in first room on the y-axis
% 3: Vorsatz in first room on the ceiling
% 4: Vorsatz in first room parallel to y-axis
% 5: Vorsatz in first room on wall separating first / second room
% 6: Vorsatz in second room on wall separating first / second room
% 7: Vorsatz in second room on the floor
% 8: Vorsatz in second room on the y-axis
% 9: Vorsatz in second room on the ceiling
% 10: Vorsatz in second room parallel to the y-axis

% Define positions and volumes, so we can loop
pos = [    0,       0,     0;...   % First room
           0,       0,     0;...
           0,       0, lz-wV;...
       lx-wV,       0,     0;...
           0, lyFR-wV,     0;...   % Separating vorsatz
           0,  lyFR+w,     0;...
           0,  lyFR+w,     0;...   % Second room
           0,  lyFR+w,     0;...
           0,  lyFR+w, lz-wV;...
       lx-wV,  lyFR+w,     0];
vol = [lx, lyFR, wV;...     % First room
       wV, lyFR, lz;...
       lx, lyFR, wV;...
       wV, lyFR, lz;...
       lx,   wV, lz;...     % Separatin vorsatz
       lx,   wV, lz;...
       lx, lySR, wV;...     % Second room
       wV, lySR, lz;...
       lx, lySR, wV;...
       wV, lySR, lz];
% Specify in which coordinate (x=+-1, y=+-2, z=+-3) the cube should be 
% formed into a pyramid
pyr = {'z+','x+','z-','x-',...  % First room
       'y-','y+',...        % Separating vorsatz
       'z+','x+','z-','x-'};
    
% Initialize legend handle
leg_hV = gobjects(10,1);
leg_sV = cell(10,1);

% Loop over the Vorsatzschalen
for i=1:10
    % Plot the vorsatz only if it is valid
    if v_b_plot(i)
        ha = patch(ax,'Faces',fc,...
                      'Vertices',vertices(pos(i,:),vol(i,:),pyr{i},wV),...
                      'FaceColor',colors(v_colorIndx(i),:));
        % Legend
        leg_hV(i) = ha;
        leg_sV{i,1} = Vorsatz.Dropdown{Vorsatz.ID == vor(i),1};
    end
end

% Reduce the legend to unique elements
forLeg = v_indxInMulti(v_b);
leg_hV = leg_hV(forLeg);
leg_sV = leg_sV(forLeg,1);

%% %%%%%%%%%%%%%%%% PLOT COORDINATE SYSTEM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Coordinate system arrow length
aL = 1;
plotCoordinateSystem(ax,[-w,0,-d],aL);

% Arrow hald width
aW = aL/6;

%% %%%%%%%%%%%%%%%%% FORMAT THE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hide the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';
% Enable perspective
ax.Projection = 'perspective';

% Remove the background color
ax.Color = 0.94*ones(1,3);

% Set the view to the previous one
ax.View = axView;

% Use correct ratio of the axes
axRatio = [lx + 2*w, l, lz + 2*d] + aW;
ax.PlotBoxAspectRatio = axRatio./max(axRatio);

% Update the limits of the axes
ax.XLim = [-aW-w, lx+w];
ax.YLim = [  -aW,    l];
ax.ZLim = [-aW-d, lz+d];

% Enable 3D-rotate
rotate3d(ax,'on');

%% %%%%%%%%%%%%%%%%%%%% PLOT KNOTEN NAMES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fT = 12;
dw = 2*aW;
% Choose color of the text depending on the color of the central element
col = [0,0,0];

% Print the text
text(ax,lx/2,lyFR+w/2,-d-dw,'Knoten 1','HorizontalAlignment','center',...
                          'FontSize',fT,'Color',col,'FontWeight','bold');
text(ax,-w-dw,lyFR+w/2,lz/2,'Knoten 2','HorizontalAlignment','center',...
                          'FontSize',fT,'Color',col,'FontWeight','bold');
text(ax,lx/2,lyFR+w/2,lz+d+dw,'Knoten 3','HorizontalAlignment','center',...
                            'FontSize',fT,'Color',col,'FontWeight','bold');
text(ax,lx+w+dw,lyFR+w/2,lz/2,'Knoten 4','HorizontalAlignment','center',...
                            'FontSize',fT,'Color',col,'FontWeight','bold');

%% %%%%%%%%%%%%%% PLOT THE LEGEND %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Concateneate the legend
leg_h = [leg_h; leg_hV];
leg_s = [leg_s; leg_sV];

% Check if leg_h is empty in which case we don't print
if ~isempty(leg_h)
    % Plot the legend
    legend(ax,leg_h,leg_s,'Location','eastoutside',...
                          'Units','pixels',...
                          'Interpreter','none',...
                          'Color',0.94*ones(1,3),...
                          'Box','off',...
                          'Tag','SituationLEG');
end

end