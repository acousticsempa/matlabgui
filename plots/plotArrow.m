function plotArrow(ax,startpoint,endpoint,ArrowColor)

% Function plots an arrow (3D) into the given axes and displays the text
% passed in at the tip of the arrow

% Block the axes
hold(ax,'on');

% Calculate the distance between the start and the end point
dxyz = endpoint - startpoint;
l = sqrt(sum(dxyz.^2));

% Calculate the width of the arrow and arrow tip
w = l/10;
wA = l/3;

% Define at which point the arrowhead begins
bAH = l-wA;

%% %%%%%%%%%%%%%%%%%%% FACES AND VERTICES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the arrow body
[v,f] = vertices([0,-w/2,-w/2],[bAH,w,w]);

% Define the arrow part
vA = [bAH, -wA/2, -wA/2;... % 1
      bAH,  wA/2, -wA/2;... % 2
      bAH, -wA/2,  wA/2;... % 3
      bAH,  wA/2,  wA/2;... % 4
        l,     0,     0];   % 5
fA1 = [1,2,4,3];  % Bottom of pyramid
fA2 = [1,5,2;...  % Pyramid sides
       1,5,3;...
       2,5,4;...
       3,5,4];
   
%% %%%%%%%%%%%%%%%%%%%% COORDINATE ROTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the rotation matrix
R = getRotationMatrix([1,0,0],dxyz);

% Apply the rotation on the vertices coordinates
v = transpose(R*transpose(v));
vA = transpose(R*transpose(vA));

% Shift so that the origin lies at the startpoint
v = v + repmat(reshape(startpoint,[1,3]),[size(v,1),1]);
vA = vA + repmat(reshape(startpoint,[1,3]),[size(vA,1),1]);

%% %%%%%%%%%%%%%%%%%%%%% PLOT OF THE ARROW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
patch(ax,'Faces',f,'Vertices',v,'FaceColor',ArrowColor);
patch(ax,'Faces',fA1,'Vertices',vA,'FaceColor',ArrowColor);
patch(ax,'Faces',fA2,'Vertices',vA,'FaceColor',ArrowColor);

end