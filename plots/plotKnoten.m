function plotKnoten(ax,teile,anis,vorsatz)

% Function prints the Knoten situation based on the ID being passed in and 
% in case the plotVorsatz is set to true it also plots the Vorsatz as they
% are selected from the dropdown menus in the fCalcWithKnoten window

% Check the input to see if the user also wants to print the Vorsatz
if nargin < 4
    vorsatz = zeros(1,8);
end

%% %%%%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure and panels
global pKnoten
global fCalcWithKnoten
global fCreateKnoten
global fExportUbertragung
global fViewUbertragung
% Data variables
global Bauteile
global Vorsatz

% Delete all the children axes and the legend
delete(ax.Children(:));
delete(ax.Legend);

%% Define properties of the display
% Wall thickness
th = 1;
% Vorsatz thickness
vth = 0.7;
% Wall length
l = 7;
% Font Size
fB = 10;    % Bauteile
fV = 8;     % Vorsatz
fR = 12;    % Room

%% %%%%%%%%%%%%%%%%%%%%%%% AXES LIMITS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ax.XLim = [-l,l];
ax.YLim = [-l,l];
ax.ZLim = [0,0.1*l];
axLim = [ax.XLim;ax.YLim;ax.ZLim];
dxyz = axLim(:,2) - axLim(:,1);
% Define the plotbox ratio (to maintain a correct ratio)
ax.PlotBoxAspectRatio = [dxyz(1),dxyz(2),dxyz(3)]/max(dxyz);
% Hide the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';

%% %%%%%%%%%%%%%%%%%%% DEFINE COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Worst case scenario we need 12 colors, so we prepare a colors array with
% already 12 colors
colors = [lines(7); prism(6)];

%% %%%%%%%%%%%%%%%%%%% RETRIEVE BAUTEIL PROPERTIES %%%%%%%%%%%%%%%%%%%%%%%
% Find out the unique elements in the teile array
[teile_u,indxInMulti,indxInUnique] = unique(teile,'stable');
% Find out which teile_u are present in the Bauteile database
b = ismember(teile_u,Bauteile.ID);
% Compund back to retrieve a vector of the bauteile to plot
b_plot = b(indxInUnique);
% Create a map for each teile to the corresponding color
teile_u_plot = teile_u(b);
[~,colorIndx] = ismember(teile,teile_u_plot);

%% %%%%%%%%%%%%%%%%%%% PLOT THE KNOTEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a legend handle
leg_h = gobjects(4,1);
leg_s = cell(4,1);
% Define proxies for coordinates
t = th/2;
k = l/2+th/4;
% Define coordinates for the 4 Bauteile
x = [-l, -t, -t, -l;...
     -t,  t,  t, -t;...
      t,  l,  l,  t;...
     -t,  t,  t, -t];
y = [-t, -t,  t,  t;...
      t,  t,  l,  l;...
     -t, -t,  t,  t;...
     -l, -l, -t, -t];
z = [-k,0;  0,k;  k,0;  0,-k];
% Define text rotation
r = [0,90,0,90];
% Iterate over the bauteile
for i=1:4
    % Create the i_str
    i_str = num2str(i,'%.0f');
    % Plot the Bauteil i
    if b_plot(i)
        % Plot the bauteil
        h = plotBauteilAnisotropie(ax,x(i,:),y(i,:),...
                                   colors(colorIndx(i),:),...
                                   anis(i));
        % Plot the name of the bauteil
        text(ax,z(i,1),z(i,2),['Bauteil ',i_str],...
                'HorizontalAlignment','center',...
                'Rotation',r(i),'FontSize',fB);
        % Prepare the legend
        leg_h(i) = h;
        leg_s{i,1} = Bauteile.Name{Bauteile.ID==teile(i),1};
    end
end
% Plot the central piece
patch(ax,[-1,1,1,-1]*t,[-1,-1,1,1]*t,[0,0,0]);

% Reduce the legend to the unique elements 
forLeg = indxInMulti(b);
% Reduce the legend handles
leg_h = leg_h(forLeg);
% Reduce the legend text
leg_s = leg_s(forLeg,1);
% Retrieve the number of elements in the legend at this point
n = numel(leg_h);

%% %%%%%%%%%%%%%%%%%%%%%%%% PLOT VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot only if the user wants it to be plotted
if any(vorsatz ~= 0)
    % vorsatz has the form 1x8 
    
    %%%%%%%%%%%%%% CALCULATE UNIQUE VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Find out the unique elements in the vorsatz (ID) array
    [vor_u,indxInMulti] = unique(vorsatz,'stable');
    % Find out which vor_u are present in the Vorsatz database
    b = ismember(vor_u,Vorsatz.ID);
    % Create a map for each vorsatz to the corresponding color
    vor_u_plot = vor_u(b);
    [~,colorIndx] = ismember(vorsatz,vor_u_plot);
    % Update color index to take into account the n-Bauteile plotted
    colorIndx = colorIndx + n;
    
    %%%%%%%%%%%%%%%% DEFINE COORDINATES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define the coordinates of where to plot the Vorsatze, starting by
    % Vorsatz1RechtsUnten and turning clockwise
    % Define proxies for coordinates
    t = th/2;
    p = th/2+vth;
    k = th/2+vth/2;
    w = l/2+th/4;
    x = [-l, -t, -p, -l;...
         -l, -p, -t, -l;...
         -p, -t, -t, -p;...
          t,  p,  p,  t;...
          t,  l,  l,  p;...
          p,  l,  l,  t;...
         -p, -t, -t, -p;...
          t,  p,  p,  t];
    y = [ t,  t,  p,  p;...
         -p, -p, -t, -t;...
          p,  t,  l,  l;...
          t,  p,  l,  l;...
          t,  t,  p,  p;...
         -p, -p, -t, -t;...
         -l, -l, -t, -p;...
         -l ,-l, -p, -t];
    z = [ -w,k; -w,-k;  -k,w;  k,w;...
           w,k;  w,-k; -k,-w; k,-w;];
    % Define text rotation
    r = [0,0,90,90,0,0,90,90];
    % Define text appendix
    txt = {'Ob','Un','Li','Re','Ob','Un','Li','Re'};
    % Initialize legend handle
    leg_hV = gobjects(8,1);
    leg_sV = cell(8,1);
    % Plot the Vorsatzschalen
    for i=1:8
        % Plot the Vorsatzschale if it has an ID different than 0
        if vorsatz(i)
            % Retrieve the colorIndx
            c = colorIndx(i);
            % Calculate the j-str
            j = floor(i/2) + mod(i,2);
            j_str = num2str(j,'%.0f');
            % Plot the surface
            h = patch(ax,x(i,:),y(i,:),colors(c,:),...
                           'EdgeColor',colors(c,:));
            % Plot the name of the bauteil
            text(ax,z(i,1),z(i,2),['Vorsatz ',j_str,txt{1,i}],...
                        'HorizontalAlignment','center',...
                        'Rotation',r(i),'FontSize',fV);
            % Prepare the legend
            leg_hV(i) = h;
            leg_sV{i,1} = Vorsatz.Name{Vorsatz.ID == vorsatz(i),1};
        end
    end
    % Trim the legend entries
    forLeg = indxInMulti(b);
    % Reduce the legend handles
    leg_hV = leg_hV(forLeg);
    % Reduce the legend text
    leg_sV = leg_sV(forLeg,1);
    
    % Concatenate the legend for the Vorsatze with the legend for the Bauteile
    leg_h = [leg_h; leg_hV];
    leg_s = [leg_s; leg_sV];
end

%% %%%%%%%%%%%%%%%%%%%%% PLOT THE R�UME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the position where to plot the text
rpos = 2*l/3 + th/4;
% Plot the text
text(ax,-rpos,rpos,'Raum 1','HorizontalAlignment','center',...
                            'FontSize',fR);
text(ax,rpos,rpos,'Raum 2','HorizontalAlignment','center',...
                           'FontSize',fR);
text(ax,rpos,-rpos,'Raum 3','HorizontalAlignment','center',...
                            'FontSize',fR);
text(ax,-rpos,-rpos,'Raum 4','HorizontalAlignment','center',...
                             'FontSize',fR);
                                 
%% %%%%%%%%%%%%%%%%%%%% PLOT THE LEGEND %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change the location depending of the parent figure of the axes
if any(ax.Parent==[pKnoten,fCreateKnoten,fExportUbertragung])
    loc = 'southoutside';
elseif any(ax.Parent==[fCalcWithKnoten,fViewUbertragung])
    loc = 'eastoutside';
else
    loc = 'eastoutside';
end
% Check if leg_h is not empty, in which case we print the legend
if ~isempty(leg_h)
    % Plot the legend
    legend(ax,leg_h,leg_s,'Location',loc,...
                          'Interpreter','none',...
                          'Color',0.94*ones(1,3),...
                          'Box','off',...
                          'Tag','KnotenLEG');
end

end