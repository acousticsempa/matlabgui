function plotCoordinateSystem(ax,origin,length)

% Function takes care of plotting a coordinate system into the axes given
% the origin and the arrow length

% Put the axes on hold
hold(ax,'on');

% Calculate the end vectors
xend = origin + [1,0,0]*length;
yend = origin + [0,1,0]*length;
zend = origin + [0,0,1]*length;

%% Plot the arrows
% ANSYS: x: r, y: g, z: b
% AutoCad: x: r, y: g, z: b
plotArrow(ax,origin,xend,'r');
plotArrow(ax,origin,yend,'g');
plotArrow(ax,origin,zend,'b');

%% Plot the texts
% Unitary prolongation of the array
s = 0.1;
% Text size
fT = 15;

% Find the coordinates where to plot the text
x = origin + (1+s)*(xend - origin);
y = origin + (1+s)*(yend - origin);
z = origin + (1+s)*(zend - origin);

% Plot the texts
text(ax,x(1),x(2),x(3),'X','HorizontalAlignment','center',...
                           'FontSize',fT,...
                           'Color','r',...
                           'FontWeight','bold');
text(ax,y(1),y(2),y(3),'Y','HorizontalAlignment','center',...
                           'FontSize',fT,...
                           'Color','g',...
                           'FontWeight','bold');
text(ax,z(1),z(2),z(3),'Z','HorizontalAlignment','center',...
                           'FontSize',fT,...
                           'Color','b',...
                           'FontWeight','bold');
end