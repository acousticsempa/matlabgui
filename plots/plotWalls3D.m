function plotWalls3D(inputData)

% Function plots the 3D-view of the currently selected dataset test
% The walls are displayed using a color encoding to show which kind of wall
% they are

%% %%%%%%%%%%%%%%%%%%% Create variable shortcuts %%%%%%%%%%%%%%%%%%%%%%%%%
l = inputData.WallLength;
h = inputData.WallHeight;
w = inputData.WallWidth;
d = w;
wS = 0.5*w;
C = inputData.WallColor;
SC = inputData.ShieldColor;
fR = inputData.RoomFont;

% Retrieve the faces matrix
[~,fc] = vertices();

%% Initialize the axes
% Find the AufbauTag for 3D
tab = findobj('Tag','3DTAB');
% Find axes
ax = findobj(tab.Children,'Type','Axes');
if isempty(ax)
    % Initialize the axes aelement
    ax = axes('Parent',tab,'Units','normalized',...
              'Position',[0,0,1,1]);
    % Initialize a correct view
    ax.View = [-9,10];
end
% Remove the legend and the children
delete(ax.Children(:));
delete(ax.Legend);

% Hold the axes and retrieve the view
hold(ax,'on');
axView = ax.View;

% Enable 3D mode
rotate3d(ax,'on');

%% %%%%%%%%%%%%%%%%%% PLOT THE SKELETON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wID = 3;
% Define the positions and volumes, so that we can loop over
pos = [ -w, 0, -d;...
        -w, 0,  0];
vol = [2*l+3*w, l,       d;...
             w, l, 2*h+2*d];
% Plot the skeleton
for i=1:2
    gh = plotBauteil3D(ax,fc,vertices(pos(i,:),vol(i,:)),....
                         C(wID,:));
end

% Add the legend
leg_h(1) = gh;
leg_s{1} = 'Fixed Walls';

%% %%%%%%%%%%%%%%%%%% PLOT THE BAUTEILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The walls vector should be in the following format
%  1: SWA:   Seitenwand A
%  2: SWB:   Seitenwand B
%  3: SWC:   Seitenwand C
%  4: SWD:   Seitenwand D
%  5: TWAC:  Trennwand AC
%  6: TWBD:  Trennwand BD
%  7: AWC:   Aussenwand C
%  8: AWD:   Aussenwand D
%  9: TDAB:  Trenndecke AB
% 10: TDCD:  Trenndecke CD
% 11: DAA:   Decke Aussen A
% 12: DAC:   Decke Aussen C

% Define the positions and volumes, so that we can loop
pos = [      0, l-w,   h+d;...   % Seitenwande
             0, l-w,     0;...
           l+w, l-w,   h+d;...
           l+w, l-w,     0;...
             l,   0,   h+d;...   % Trennwande
             l,   0,     0;...
         2*l+w,   0,   h+d;...   % Aussenwande
         2*l+w,   0,     0;...
             0,   0,     h;...   % Trenndecken
       l+0.5*w,   0,     h;...
             0,   0, 2*h+d;...   % Decken aussen
       l+0.5*w,   0, 2*h+d];
       
vol = [      l, w, h;...     % Seitenwande
             l, w, h;...
             l, w, h;...
             l, w, h;...
             w, l, h;...     % Trennwnade
             w, l, h;...
             w, l, h;...     % Aussenwande
             w, l, h;...
       l+0.5*w, l, d;...     % Trenndecken
       l+1.5*w, l, d;...
       l+0.5*w, l, d;...     % Decken Aussen
       l+1.5*w, l, d];
% Number of walls to print
n = size(pos,1);
% Initialize legend object
leg_hW = gobjects(n,1);
leg_sW = cell(n,1);
% Loop over the Wande
for i=1:n
    % Plot wand only if it exists
    wID = inputData.WallSpec(i);
    if wID ~= 0
        % Plot the wall
        gh = plotBauteil3D(ax,fc,vertices(pos(i,:),vol(i,:)),C(wID,:));
        leg_hW(i) = gh;
        leg_sW{i} = [inputData.WallTypes{wID},32,'walls'];
    end
end     

% Check which unique elements of the legend to keep
[w_u,indxInMulti] = unique(inputData.WallSpec,'stable');
% Find out valid walls
b = (w_u ~= 0);
% Retrieve valid incides
forLeg = indxInMulti(b);
% Reduce the legend handles and strings
leg_h = [leg_h; leg_hW(forLeg)];
leg_s = [leg_s; leg_sW(forLeg)];

%% %%%%%%%%%%%%%%%%%% PLOT THE VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The shielding vector should be in the following format
%  1: SWA_SA:   Seitenwand A - Seite A
%  2: SWB_SB:   Seitenwand B - Seite B
%  3: SWC_SC:   Seitenwand C - Seite C
%  4: SWD_SC:   Seitenwand D - Seite D
%  5: AWC_SC:   Aussenwand C - Seite C
%  6: AWD_SD:   Aussenwand D - Seite D
%  7: DAA_SA:   Decke Aussen A - Seite A
%  8: DAC_SC:   Decke Aussen C - Seite C
%  9: TWAC_SA:  Trennwand AC - Seite A
% 10: TWAC_SC:  Trennwand AC - Seite C
% 11: TWBD_SB:  Trennwand BD - Seite B
% 12: TWBD_SD:  Trennwand BD - Seite D
% 13: TDAB_SA:  Trenndecke AB - Seite A
% 14: TDAB_SB:  Trenndecke AB - Seite B
% 15: TDCD_SC:  Trenndecke CD - Seite C
% 16: TDCD_SD:  Trenndecke CD - Seite D

% Define the positions and volumes, so that we can loop
pos = [       0, l-w-wS,      h+d;...     % Seitenwand
              0, l-w-wS,        0;...
            l+w, l-w-wS,      h+d;...
            l+w, l-w-wS,        0;...
       2*l+w-wS,      0,      h+d;...     % Aussenwande
       2*l+w-wS,      0,        0;...
              0,      0, 2*h+d-wS;...     % Decke Aussen
            l+w,      0, 2*h+d-wS;...
           l-wS,      0,      h+d;...     % Trennwand
            l+w,      0,      h+d;...
           l-wS,      0,        0;...
            l+w,      0,        0;...
              0,      0,      h+d;...     % Trenndecke
              0,      0,     h-wS;...
            l+w,      0,      h+d;...
            l+w,      0,     h-wS];
vol = [ l, wS,  h;...   % Seitenwande
        l, wS,  h;...   
        l, wS,  h;...
        l, wS,  h;...
       wS,  l,  h;...   % Aussenwande
       wS,  l,  h;...
        l,  l, wS;...   % Decke Aussen
        l,  l, wS;...
       wS,  l,  h;...   % Trennwand
       wS,  l,  h;...
       wS,  l,  h;...
       wS,  l,  h;...
        l,  l, wS;...   % Trenndecke
        l , l, wS;...
        l,  l, wS;...
        l,  l, wS];
% Specify in which coordinate (x=+-1, y=+-2, z=+-3) the cube should be 
% formed into a pyramid
pyr = {'y-','y-','y-','y-',...  % Seitenwand
       'x-','x-',...            % Aussenwand
       'z-','z-',...            % Decke Aussen
       'x-','x+','x-','x+',...  % Trennwand
       'z+','z-','z+','z-'};
% Number of shields to print
n = size(pos,1);
% Initialize legend object
leg_hS = gobjects(n,1);
leg_sS = cell(n,1);
% Loop over the Shielding
for i=1:n
    % Plot shielding only if it exists
    sID = inputData.ShieldSpec(i);
    if sID
        % Plot the shiedling
        gh = plotBauteil3D(ax,fc,vertices(pos(i,:),vol(i,:),pyr{i},wS),SC);
        leg_hS(i) = gh;
        leg_sS{i} = 'Shielding shell';
    end
end 

% Check which unique elements of the legend to keep
[s_u,indxInMulti] = unique(inputData.ShieldSpec,'stable');
% Find out valid shielding
b = (s_u ~= 0);
% Retrieve valid incides
forLeg = indxInMulti(b);
% Reduce the legend handles and strings
leg_h = [leg_h; leg_hS(forLeg)];
leg_s = [leg_s; leg_sS(forLeg)];

%% %%%%%%%%%%%%%%%%% PLOT THE LEGEND %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isempty(leg_h)
    legend(ax,leg_h,leg_s,'Location','eastoutside',...
                          'Interpreter','none',...
                          'Color',0.94*ones(1,3),...
                          'Box','off');
end

%% %%%%%%%%%%%%%%%%% PLOT THE ROOMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare the string with the sender room and receiver room to be added to
% the string that is going to be printed
emptyStr = {'','','',''};
rooms = {'A','B','C','D'};
% Find which is the sender room
SR = strcmpi(inputData.SR,rooms);
SRstr = emptyStr;
if sum(SR) > 0
    SRstr{SR} = '(S)';
end
% Find which is the receiver room
RR = strcmpi(inputData.RR,rooms);
RRstr = emptyStr;
if sum(RR) > 0
    RRstr{RR} = '(R)';
end

% Concatenate the string for printing
rooms = strcat(rooms,SRstr,RRstr);

% Print the room string to the graph
text(ax,0.5*l,l/2,1.5*h+d,rooms{1},'FontSize',fR,'HorizontalAlignment','center');
text(ax,0.5*l,l/2,0.5*h,rooms{2},'FontSize',fR,'HorizontalAlignment','center');
text(ax,1.5*l+w,l/2,1.5*h+d,rooms{3},'FontSize',fR,'HorizontalAlignment','center');
text(ax,1.5*l+w,l/2,0.5*h,rooms{4},'FontSize',fR,'HorizontalAlignment','center');

%% %%%%%%%%%%%%%%%%% FORMAT THE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hide the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';
% Enable perspective
ax.Projection = 'perspective';

% Remove the background color
ax.Color = 0.94*ones(1,3);

% Specify the limits
ax.XLim = [-w, 2*l+2*w];
ax.YLim = [0,l];
ax.ZLim = [-d, 2*h+2*d];

% Set the view to the previous one
ax.View = axView;

% Use correct ratio of the axes
axRatio = [2*l+3*w, l, 2*h+3*d];
ax.PlotBoxAspectRatio = axRatio./max(axRatio);
