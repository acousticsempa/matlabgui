function [vert,faces] = vertices(Position,Volume,Pyramid,dp)

% Function returns the vertices set for a block with its (0,0,0) point at
% position and scaled in [x,y,z] dimension to the scale (dV = [dx,dy,dz])
% specified

%% Define basis cube
% Define the basis cube
vert = [0,0,0;...  % 1
        1,0,0;...  % 2
        0,1,0;...  % 3
        1,1,0;...  % 4
        0,0,1;...  % 5
        1,0,1;...  % 6
        0,1,1;...  % 7
        1,1,1];    % 8
 
% Define the corresponding faces
faces = [1,2,4,3;...      % xy plane
         1,2,6,5;...      % xz plane
         1,3,7,5;...      % yz plane
         5,6,8,7;...      % xy plane shifted
         3,4,8,7;...      % xz plane shifted
         2,4,8,6];        % yz plane shifted

%% In case we don't have any input return the standard variables
if nargin < 1
   return;
end

%% Check the input for correct size and correct number of inputs
if ~any(nargin==[2,4])
    error('Function requires either 2 or 4 arguments!');
end
if numel(Position)~=3
    error('The argument Position must be a vector with 3 components!');
end
if numel(Volume)~=3
    error('The argument Volume must be a vector with 3 components!');
end

%% Scale the vector to the desired scale and shift to position
vert(:,1) = Position(1) + vert(:,1)*Volume(1);
vert(:,2) = Position(2) + vert(:,2)*Volume(2);
vert(:,3) = Position(3) + vert(:,3)*Volume(3);

% Return if we have only two input parameters
if nargin==2
    return;
end

%% Prepare the pyramid 
% First read in the pyramid direction
% Check if we have a valid string
if ~ischar(Pyramid) && any(strcmpi(Pyramid,{'x+','x-','y+','y-','z+','z-'}))
    error(['The argument pyramid must be a string.\nValid strings are:\n',...
           '''x+'',''x-'',''y+'',''y-'',''z+'',''z-''']);
end
% Find out vertices for + cases
switch lower(Pyramid(1))
    case 'x'
        c = [2,3];
        pF = [0,1,0,1,0,1,0,1];
    case 'y'
        c = [1,3];
        pF = [0,0,1,1,0,0,1,1];
    case 'z'
        c = [1,2];
        pF = [0,0,0,0,1,1,1,1];
end
% Convert to boolean
pF = (pF' == 1);
% Find out vertices for - cases
if strcmpi(Pyramid(2),'-')
    pF = ~pF;
end

% Prepare vector for changing coordinates
d1 = [1,-1,1,-1]'*dp;
d2 = [1,1,-1,-1]'*dp;

% Update the coordinates
vert(pF,c(1)) = vert(pF,c(1)) + d1;
vert(pF,c(2)) = vert(pF,c(2)) + d2;

end