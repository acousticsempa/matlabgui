function h = plotBauteilAnisotropie(ax,x,y,color,anisotropie)

% Function prints the patch for the anisotropie using an encoding into
% checkered Bauteile
% The function returns only a single handle to an original color patch

% Define how we plot different anisotropie-IDs
s = 5;  % Anisotropie 1
t = 5;  % Anisotropie 2
% Define alpha-value for lower shade
alpha = 0.6;

% Check which type of anisotropie and plot the Bauteil in different ways
% based on that

%% %%%%%%%%%%%%%%%%%%% ANISOTROPIE-ID == 0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First we plot the case where we have no anisotropie, the corresponding
% AnisotropieID is 0
if anisotropie==0
    % Plot the Bauteil using solid color as specified above
    h = patch(ax,x,y,color,'FaceAlpha',1,...
                           'EdgeColor',color,'EdgeAlpha',1);
    return;
end

%% %%%%%%%%%%%%%%%%%%% ANISOTROPIE-ID == 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Second we plot the case where we have anisotropie parallel zur
% Knotenrichtung, the corresponding Anisotropie-ID is 1
if anisotropie==1
    % Plot the Bauteil using multiple patches, first we need to understand
    % in which direction lies the Bauteil, if xrange > yrange than the
    % Bauteil is horizontal, else is vertical
    dx = max(x) - min(x);
    dy = max(y) - min(y);
    
    % Store original coordinates
    xorig = x;
    yorig = y;
    
    % Create the new positions array
    if dx > dy
        % Horizontal lying Bauteil 
        y = repmat(y,[s,1]);
        % Create proxy vector
        z = (1:s)';
        % Create the new x-Positions vector
        x = repmat(x(1)*ones(1,4),[s,1]) + [z-1,z,z,z-1]/s*dx;
    else
        % Vertical lying Bauteil
        x = repmat(x,[s,1]);
        % Create proxy vector
        z = (1:s)';
        % Create the new y-Positions vector
        y = repmat(y(1)*ones(1,4),[s,1]) + [z-1,z-1,z,z]/s*dy;
    end
    % Create a handler 
    h = gobjects(s,1);
    % Iterate over the elements to plot and plot them
    for i=1:s
        % Calculate a value (full color for uneven, alpha for even)
        a = mod(i,2) + (1-mod(i,2))*alpha;
        % Plot the piece
        h(i) = patch(ax,x(i,:),y(i,:),color,'FaceAlpha',a,...
                                     'EdgeColor','none');
    end
    % Retain only the first handle
    h = h(1);
    % Plot a box on top with the correct edgecolor and fully transparent
    patch(ax,xorig,yorig,color,'FaceAlpha',0,...
                               'EdgeColor',color,'EdgeAlpha',1);
    % Return
    return;
end

%% %%%%%%%%%%%%%%%%%%% ANISOTROPIE-ID == 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Third we plot the case where we have anisotropie senkrecht zur
% Knotenrichtung, the corresponding Anisotropie-ID is 2
if anisotropie==2
    % Plot the Bauteil using multiple patches, first we need to understand
    % in which direction lies the Bauteil, if xrange > yrange than the
    % Bauteil is horizontal, else is vertical
    dx = max(x) - min(x);
    dy = max(y) - min(y);
    
    % Store original coordinates
    xorig = x;
    yorig = y;
    
    % Create the new positions array
    if dx > dy
        % Horizontal lying Bauteil 
        x = repmat(x,[t,1]);
        % Create proxy vector
        z = (1:t)';
        % Create the new x-Positions vector
        y = repmat(y(1)*ones(1,4),[t,1]) + [z-1,z-1,z,z]/t*dy;
    else
        % Vertical lying Bauteil
        y = repmat(y,[t,1]);
        % Create proxy vector
        z = (1:t)';
        % Create the new y-Positions vector
        x = repmat(x(1)*ones(1,4),[t,1]) + [z-1,z,z,z-1]/t*dx;
    end
    % Create a handler 
    h = gobjects(t,1);
    % Iterate over the elements to plot and plot them
    for i=1:t
        % Calculate a value (full color for uneven, alpha for even)
        a = mod(i,2) + (1-mod(i,2))*alpha;
        % Plot the piece
        h(i) = patch(ax,x(i,:),y(i,:),color,'FaceAlpha',a,...
                                     'EdgeColor','none');
    end
    % Retain only the first handle
    h = h(1);
    % Plot a box on top with the correct edgecolor and fully transparent
    patch(ax,xorig,yorig,color,'FaceAlpha',0,...
                               'EdgeColor',color,'EdgeAlpha',1);
    % Return
    return;
end

% If we arrive at this point we have an error, throw error
error('Incorrect Anisotropie-ID passed in, must be either 0,1,2!');

end          