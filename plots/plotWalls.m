function plotWalls(indx,dataType)
% Function initializes important parameters for plotting the walls
% and calls the functions which are specialized in plotting the walls

%% %%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure
global fViewDatasets
global Data
global walls

%% %%%%%%%%%%%%%%%%%%%%%%%%% COLORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the lines colormap
cmap = lines(7);

% Initialize arrays
PlotData.WallColor = zeros(3,3);
PlotData.ShieldColor = zeros(1,3);

% Pick the colors
PlotData.WallColor(1,:) = cmap(1,:);      % Blue for 'Default' walls
PlotData.WallColor(2,:) = cmap(2,:);      % Orange for 'Specimen' walls
PlotData.WallColor(3,:) = 0.5;            % Dark grey for Fixed walls
PlotData.ShieldColor(1,:) = cmap(5,:);    % Green for 'Shielding' = True

%% %%%%%%%%%%%%%%%%%%%%%% GEOMETRY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PlotData.WallLength = 10;
PlotData.WallHeight = 10;
PlotData.WallWidth = 0.8;
PlotData.Space = 3;

%% %%%%%%%%%%%%%%%%%%%%%% TEXT TO DISPLAY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PlotData.WallIdentifier = {'SWA','SWB','SWC','SWD',...
                            'TWAC','TWBD','AWC','AWD',...
                            'TDAB','TDCD','DAA','DAC'};
PlotData.ShieldIdentifier = {'SWA-SA','SWB-SB','SWC-SC','SWD-SD',...
                             'AWC-SC','AWD-SD','DAA-SA','DAC-SC',...
                             'TWAC-SA','TWAC-SC','TWBD-SB','TWBD-SD',...
                             'TDAB-SA','TDAB-SB','TDCD-SC','TDCD-SD'};
PlotData.WallTypes = walls.Types(2:end);

%% %%%%%%%%%%%%%%%%%%%%%% DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the field where the data can be found
if dataType == 1
    % Airborne Data
    field = 'Air';
elseif dataType == 2
    % Impact Data
    field = 'Impact';
elseif dataType == 3
    % Airborne Averaged data
    field = 'AirAVG';
end

% Retrieve the data
PlotData.WallSpec = Data.(field).Walls(indx,:);
PlotData.ShieldSpec = Data.(field).Shielding(indx,:);
PlotData.SR = Data.(field).SR{indx,1};
PlotData.RR = Data.(field).RR{indx,1};

%% %%%%%%%%%%%%%%%%%%%%% FONTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PlotData.WallFont = 9;
PlotData.ShieldFont = 9;
PlotData.RoomFont = 45;

%% %%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out which tab is select and plot that first
parTab = findobj(fViewDatasets.Children,'Tag','topTAB');
parTab = findobj(parTab.Children,'Tag','AufbauTAB');

% Plot the 3D / Grundriss / Längsschnitt / Querschnitt views
switch lower(parTab.Children.SelectedTab.Tag)
    case '3dtab'
        plotWalls3D(PlotData);
        i = 1;
    case 'grundtab'
        plotG(PlotData);
        i = 2;
    case 'langstab'
        plotL(PlotData);
        i = 3;
    case 'quertab'
        plotQ(PlotData);
        i = 4;
end

% Plot the rest of the graphs
if i~=1
    plotWalls3D(PlotData);
end
if i~=2
    plotG(PlotData);
end
if i~=3 
    plotL(PlotData);
end
if i~=4  
    plotQ(PlotData); 
end