function plotGraphs(indx, dataType)
% Function plots various graphs associated with the dataset
% Airborne:     L2, L2sb, L2b, T, TL 
%               Flag, L1 vs. MeasFreq
% Impact:       L2, L2sb, L2b, T,
%               Flag, NISPL vs. MeasFreq

%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global Data

%%%%%%%%%%%%%%%% RETRIEVE THE CORRECT DATASET %%%%%%%%%%%%%%%%%%%%%%%
if dataType == 1
    DB = Data.Air;
    x = DB.MeasFreq;
elseif dataType == 2
    DB = Data.Impact;
    x = DB.MeasFreq;
elseif dataType == 3
    DB = Data.AirAVG;
    x = DB.MeasFreq;
end

%%%%%%%%%%%%%%%% GET THE TAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab = findobj('Tag','GraphenTAB');
% Find all axes that have as parent tab and delete them and their children
oldAxes = findobj('Type','Axes','Parent',tab);
for i=1:numel(oldAxes)
    % Get the children
    ch = oldAxes(i).Children;
    % Delete all children
    for j=1:numel(ch)
        delete(ch(j));
    end
    % Delete the axes
    delete(oldAxes(i));
end
% Delete the legends
oldLegend = findobj('Type','Legend','Parent',tab);
for i=1:numel(oldLegend)
    % Delete the legend
    delete(oldLegend(i));
end

%%%%%%%%%%%%%%% CREATE THE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if dataType == 1 || dataType == 2
    % Create all three axes
    axL = axes('Parent',tab,'Units','normalized',...
               'OuterPosition',[0,0,0.5,1]);
    axSpec = axes('Parent',tab,'Units','normalized',...
               'OuterPosition',[0.5,0.5,0.5,0.5]);
    axT = axes('Parent',tab,'Units','normalized',...
               'OuterPosition',[0.5,0,0.5,0.5]);
    % Create an array of handles
    axHand = [axL, axT, axSpec];
elseif dataType == 3
    % Create only the Spec axes and let them fill the whole space
    axSpec = axes('Parent',tab,'Units','normalized',...
                  'OuterPosition',[0,0,1,1]);
    % Create an array of handles
    axHand = [axSpec];
end

% Hold all axes
for i=1:numel(axHand)
    % Hold the axes
    hold(axHand(i),'on');
end
          
%%%%%%%%%%%%%%%% PLOT OF L MEASURES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if dataType == 1 || dataType == 2
    % Which are:    L1, L2, L2sb, Lb and the flag
    hLb = plot(axL,x,DB.Lb(indx,:),'Color','g','LineStyle','-');
    hL2sb = plot(axL,x,DB.L2sb(indx,:),'Color','r','LineStyle','-');
    hL2 = plot(axL,x,DB.L2(indx,:),'Color','b','LineStyle','-');
    % Plot the flag
    hFlagL = plot(axL,x,DB.Flag(indx,:)-1,'LineStyle','none','Marker','o',...
                                          'MarkerEdgeColor','r',...
                                          'MarkerFaceColor','none');
    if dataType==1
        hL1 = plot(axL,x,DB.L1(indx,:),'Color','m','LineStyle','-');
        legend(axL,[hL1,hL2,hL2sb,hLb,hFlagL],{'L1','L2','L2sb','Lb','Flag'});
    else
        legend(axL,[hL2,hL2sb,hLb,hFlagL],{'L2','L2sb','Lb','Flag'});
    end
    % Set the ticks to a space of ten
    axL.YTick = getTicks([0,axL.YLim(2)],10);
    % Reset the ylim to 0 at the bottom
    axL.YLim = [0,axL.YLim(2)];
    axL.YLabel.String = 'Pegel [dB]';
end

%%%%%%%%%%%%%%%% PLOT OF Spec Measures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Airborne datatype
if dataType == 1
    % Plot TL
    hSpec = plot(axSpec,x,DB.TL(indx,:),'Color','b','LineStyle','-');
    % Plot the flag at teh border of the y-axis minimum value
    ymin = axSpec.YLim(1);
    hFlagSpec = plot(axSpec,x,DB.Flag(indx,:)-1+ymin,...
                     'LineStyle','none','Marker','o',...
                     'MarkerEdgeColor','r','MarkerFaceColor','none');
    % Reset axis limits
    axSpec.YLim = [ymin,axSpec.YLim(2)];
    % Plot legend
    legend(axSpec,[hSpec,hFlagSpec],{['TL (',DB.DataType{indx,:},')'],...
                                     'Flag'},'Location','northwest');
    % Change y-label
    axSpec.YLabel.String = 'Schalldammmass TL [dB]';
% Impact datatype
elseif dataType == 2
    % Plot NISPL
    hSpec = plot(axSpec,x,DB.NISPL(indx,:),'Color','b','LineStyle','-');
    % Plot flag at the border of the y-axis minimum value
    ymin = axSpec.YLim(1);
    hFlagSpec = plot(axSpec,x,DB.Flag(indx,:)-1+ymin,...
                     'LineStyle','none','Marker','o',...
                     'MarkerEdgeColor','r','MarkerFaceColor','none');
    % Reset axis limits
    axSpec.YLim = [ymin,axSpec.YLim(2)];
    % Plot legend
    legend(axSpec,[hSpec,hFlagSpec],{['NISPL (',DB.DataType{indx,1},')'],...
                                     'Flag'},'Location','northeast');
    % Change y-label
    axSpec.YLabel.String = 'Trittschallpegel NISPL [dB]';
elseif dataType == 3
    % Plot the TL values
    hSpec1 = plot(axSpec,x,DB.TL1(indx,:),'Color','b','LineStyle','-');
    hSpec2 = plot(axSpec,x,DB.TL2(indx,:),'Color','r','LineStyle','-');
    hSpecA = plot(axSpec,x,DB.TL(indx,:),'Color','k','LineStyle','--',...
                                         'LineWidth',1.5);
    % Plot the flags at the border of the y-axis minimum value
    ymin = axSpec.YLim(1);
    ymax = axSpec.YLim(2);
    hFlagSpec1 = plot(axSpec,x,DB.Flag(indx,:,1)-1+ymin,...
                      'LineStyle','none','LineWidth',2,...
                      'Marker','o','MarkerSize',6,...
                      'MarkerEdgeColor','b','MarkerFaceColor','none');
    hFlagSpec2 = plot(axSpec,x,DB.Flag(indx,:,2)-1+ymin,...
                      'LineStyle','none','LineWidth',0.5,...
                      'Marker','o','MarkerSize',4,...
                      'MarkerEdgeColor','r','MarkerFaceColor','r');
    % Reset axis limits
    axSpec.YLim = [ymin,ymax];
    % Plot legend
    legend(axSpec,[hSpec1,hSpec2,hSpecA,hFlagSpec1,hFlagSpec2],...
                  {'TL 1st set','TL 2nd set',...
                  ['TL Avg (',DB.DataType{indx,1},')'],...
                  'Flag 1st set','Flag 2nd set'},...
                  'Location','northwest');
    % Change y-label
    axSpec.YLabel.String = 'Schalldammmass TL [dB]';
end

%%%%%%%%%%%%%%%%%% PLOT OF T Measures %%%%%%%%%%%%%%%%%%%%%%%%%%
if dataType == 1 || dataType == 2
    hT = plot(axT,x,DB.T(indx,:),'Color','b','LineStyle','-');
    axT.YLabel.String = 'Nachhallzeit T [s]';
end

%%%%%%%%%%%%%%%%% SPECIFY THE SCALES, RANGES AND LABELS %%%%%%%%%%%%%%%%%
for i=1:numel(axHand)
    % Enable the gridlines
    axHand(i).XGrid = 'on';
    axHand(i).YGrid = 'on';
    % Specify the scale
    axHand(i).XScale = 'log';
    % Specify the limits of the x-axis
    axHand(i).XLim = [min(x), max(x)];
    % Specify the x-ticks
    axHand(i).XTick = x(2:3:end);
    % Specify the x-label
    axHand(i).XLabel.String = 'Frequenz f [Hz]';
    % Unhold the axes
    hold(axHand(i),'off');
end

%%%%%%%%%%%%%%%% SCALING OF LEFT FULL HEIGHT AXES %%%%%%%%%%%%%%%%%%%%%%%
if dataType == 1 || dataType == 2
    % Shift the axes to have a nice fitting dashboard
    axSpecPos = axSpec.Position;
    axTPos = axT.Position;
    % Update the position of the L-axes to be aligned with the other ones
    axL.Position = [axTPos(1)-0.5,axTPos(2),axTPos(3),...
                    (axSpecPos(2)-axTPos(2))+axSpecPos(4)];
end

end

