function plotQ(inputData)
% This function plots the 'Querschnitt' view of the current 
% test
% The walls are displayed using a color encoding to show of which kind they
% are

%% %%%%%%%%%%%%%%%%% Create variable shortcuts %%%%%%%%%%%%%%%%%%%%%%%%%%%
l = inputData.WallLength;
h = inputData.WallHeight;
w = inputData.WallWidth;
s = inputData.Space;
C = inputData.WallColor;
SC = inputData.ShieldColor;
fW = inputData.WallFont;
fS = inputData.ShieldFont;
fR = inputData.RoomFont;

%% %%%%%%%%%%%%%%%%%% PLOTTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the AufbauTab for Quer
tab = findobj('Tag','QuerTAB');
% Find axes
ax = findobj(tab.Children,'Type','Axes');
if isempty(ax)
    % Initialize the axes
    ax = axes('Parent',tab,'Units','normalized',...
              'Position',[0,0,1,1]);
end
% Remove the children and the legend
delete(ax.Children(:));
delete(ax.Legend);

%% %%%%%%%%%%%%%%%%%% PLOT THE WALLS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start with the first 'Fixed walls'
wID = 3;
for i=1:2
    xb = (i-1)*(l+s);
    patch(ax,xb+[-w,l,l,-w],[-w,-w,0,0],C(wID,:));
    patch(ax,xb+[-w,0,0,-w],[-w,-w,2*h,2*h],C(wID,:));
end

% Then iteratively plot the other walls
wallsID = [1,2,9,11;...
           3,4,10,12];
wallRot = -[90,90,0,0];
wX = [l-w,l,l,l-w;...               % SWA, SWC
      l-w,l,l,l-w;...               % SWB, SWD
      0,l-0.5*w,l-0.5*w,0;...       % TDAB, TDCD
      0,l-0.5*w,l-0.5*w,0];         % DAA, DAC
wY = [h, h, 2*h-w, 2*h-w;...        % SWA, SWC
      0, 0, h-w, h-w;...            % SWB, SWD
      h-w, h-w, h, h;...            % TDAB, TDCD
      2*h-w, 2*h-w, 2*h, 2*h];      % DAA, DAC
for i=1:size(wallsID,1)
    xb = (i-1)*(l+s);
    for j=1:size(wallsID,2)
        wID = inputData.WallSpec(wallsID(i,j));
        if wID~=0
            % Plot the wall
            patch(ax,xb+wX(j,:),wY(j,:),C(wID,:));
            % Plot the name of the wall and it's type
            text(ax,xb+0.5*(wX(j,1)+wX(j,3)),0.5*(wY(j,1)+wY(j,3)),...
                 strcat(inputData.WallIdentifier{wallsID(i,j)},' (',...
                        inputData.WallTypes{wID},')'),...
                 'HorizontalAlignment','center',...
                 'Rotation',wallRot(j),...
                 'FontSize',fW);
        end
    end
end

%% %%%%%%%%%%%%%%%% PLOT THE SHIELDING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shieldID = [7, 13, 14, 1, 2;...
            8, 15, 16, 3, 4];
shieldRot = -[0,0,0,90,90];
wX = [0,l-w,l-w,0;...                   % DAA_SA, DAC_SC
      0,l-w,l-w,0;...                   % TDAB_SA, TDCD_SC
      0,l-w,l-w,0;...                   % TDAB_SB, TDCD_SD
      l-2*w,l-w,l-w,l-2*w;...           % SWA_SA, SWC_SC
      l-2*w,l-w,l-w,l-2*w];             % SWB_SB, SWD_SD
wY = [2*h-2*w, 2*h-2*w,2*h-w,2*h-w;...  % DAA_SA, DAC_SC
      h, h, h+w, h+w;...                % TDAB_SA, TDCD_SC
      h-2*w, h-2*w, h-w, h-w;...        % TDAB_SB, TDCD_SD
      h, h, 2*h-w, 2*h-w;...            % SWA_SA, SWC_SC
      0,0,h-w,h-w];                     % SWB_SB, SWD_SD
for i=1:size(shieldID,1)
    xb = (i-1)*(l+s);
    for j=1:size(shieldID,2)
        sID = inputData.ShieldSpec(shieldID(i,j));
        if sID
            % Plot the shielding
            patch(ax,xb+wX(j,:),wY(j,:),SC);
            % Plot the name of the shielding
            text(ax,xb+0.5*(wX(j,1)+wX(j,3)),0.5*(wY(j,1)+wY(j,3)),...
                 strcat(inputData.ShieldIdentifier{shieldID(i,j)},' (Shielding)'),...
                 'HorizontalAlignment','center',...
                 'Rotation',shieldRot(j),...
                 'FontSize',fS);
        end
    end
end

%% %%%%%%%%%%%%%%%%% PLOT THE ROOMS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare the string with the sender room and receiver room to be added to
% the string that is going to be printed
emptyStr = {'','','',''};
rooms = {'A','B','C','D'};
% Find which is the sender room
SR = strcmpi(inputData.SR,rooms);
SRstr = emptyStr;
if sum(SR) > 0
    SRstr{SR} = '(S)';
end
% Find which is the receiver room
RR = strcmpi(inputData.RR,rooms);
RRstr = emptyStr;
if sum(RR) > 0
    RRstr{RR} = '(R)';
end

% Concatenate the string for printing
rooms = strcat(rooms,SRstr,RRstr);

% Print the room string to the graph
text(ax,0.5*l,1.5*h,rooms{1},'FontSize',fR,'HorizontalAlignment','center');
text(ax,0.5*l,0.5*h,rooms{2},'FontSize',fR,'HorizontalAlignment','center');
text(ax,1.5*l+s,1.5*h,rooms{3},'FontSize',fR,'HorizontalAlignment','center');
text(ax,1.5*l+s,0.5*h,rooms{4},'FontSize',fR,'HorizontalAlignment','center');

%% %%%%%%%%%%%%%%%%%%%%%%%%%% FORMAT AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set limits for the graphs
ax.XLim = [-w,2*l+s];
ax.YLim = [-w,2*h];
ax.ZLim = [0,0.1*l];
axLim = [ax.XLim;ax.YLim;ax.ZLim];
dxyz = axLim(:,2) - axLim(:,1);
ax.PlotBoxAspectRatio = [dxyz(1),dxyz(2),dxyz(3)]/max(dxyz);
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';

% Remove the background color
ax.Color = 0.94*ones(1,3);

end

