function plotVerticalSituation(ax,teile,anisotropie,vor,lx,ly,lzLR,lzUR)

% Global variables
global Bauteile
global Vorsatz

% Delete all the axes children and the legend
delete(ax.Children(:));
delete(ax.Legend);

%% %%%%%%%%%%%%%%%%%%%% DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define certain geometric variables
w =  0.2;              % Wall width
wV = 0.2;              % Vorsatz width
d = 0.3;               % Ceiling / Floor depth
h = lzLR + d + lzUR;   % Total height

% Retrieve the faces matrix
[~,fc] = vertices();

% Hold the axes and retrieve the current view
hold(ax,'on');
axView = ax.View;

%% %%%%%%%%%%%%%%%%%%%%%%%%%% COLORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%% Bauteile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out the unique elements in the teile array
[t_u,t_indxInMulti,t_indxInUnique] = unique(teile,'stable');
% Find out which teile_u are present in the Bauteile database
t_b = ismember(t_u,Bauteile.ID);
% Compound back to retrieve a vector of bauteile to plot
t_b_plot = t_b(t_indxInUnique);
% Create a map for each teile to the corresponding color
t_u_plot = t_u(t_b);
[~,t_colorIndx] = ismember(teile,t_u_plot);
% Calculate number of distinctive bauteile
n_teile = numel(t_u_plot);

% %%%%%%%%%%%%%%%%%%%% VORSATZSCHALEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out the unique elements in the vorsatz (ID) array
[v_u,v_indxInMulti,v_indxInUnique] = unique(vor,'stable');
% Find out which vor_u are present in the Vorsatz database
v_b = ismember(v_u,Vorsatz.ID);
% Compound back to retrieve a vector of vorsatze to plot
v_b_plot = v_b(v_indxInUnique);
% Create a map for each vor to the corresponding color
v_u_plot = v_u(v_b);
[~,v_colorIndx] = ismember(vor,v_u_plot); 
% Offset due to the printing of the Bauteile first
v_colorIndx = v_colorIndx + n_teile;
% Number of vorsatzschalen
n_vor = numel(v_u_plot);

% %%%%%%%%%%%%%%%%%%%% COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of colors needed
n_colors = n_teile + n_vor;

% Retrieve the color palette
colors = getColors(n_colors);
                
%% %%%%%%%%%%%%%%%%%% PLOT THE BAUTEILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The teile vector should be in the following format
% 1: Bauteil in bottom room on the x-axis
% 2: Bauteil in bottom room on the y-axis
% 3: Bauteil in bottom room parallel to x-axis
% 4: Bauteil in bottom room parallel to y-axis
% 5: Bauteil ceiling of bottom room / floor of top room
% 6: Bauteil in top room on the x-axis
% 7: Bauteil in top room on the y-axis
% 8: Bauteil in top room parallel to the x-axis
% 9: Bauteil in top room parallel to the y-axis

% Define the positions and volumes, so that we can loop
pos = [-w, -w,      0;...   % Bottom bauteile
       -w,  0,      0;...
       -w, ly,      0;...
       lx,  0,      0;...   
       -w, -w,   lzLR;...   % Ceiling / Floor
       -w, -w, lzLR+d;...   % Top Bauteile
       -w,  0, lzLR+d;...
       -w, ly, lzLR+d;...
       lx,  0, lzLR+d];

vol = [lx+2*w,      w, lzLR;...     % Bottom bauteile
            w,     ly, lzLR;...
       lx+2*w,      w, lzLR;...
            w,     ly, lzLR;...
       lx+2*w, ly+2*w,    d;...     % Ceiling / Floor
       lx+2*w,      w, lzUR;...     % Top Bauteile
            w,     ly, lzUR;...
       lx+2*w,      w, lzUR;...
            w,     ly, lzUR];
    
% Initialize the legend
leg_h = gobjects(9,1);
leg_s = cell(9,1);

% Loop over the Bauteile
for i=1:9
    % Plot the Bauteil only if it is valid
    if t_b_plot(i)
        ha = plotBauteil3D(ax,fc,vertices(pos(i,:),vol(i,:)),...
                              colors(t_colorIndx(i),:),...
                              anisotropie(i));
        % Legend
        leg_h(i) = ha;
        leg_s{i,1} = Bauteile.Dropdown{Bauteile.ID==teile(i),1};
    end
end

% Reduce the legend to the unique elements
forLeg = t_indxInMulti(t_b);
leg_h = leg_h(forLeg);
leg_s = leg_s(forLeg,1);

%% %%%%%%%%%%%%%%%%%% PLOT THE VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The vor vector should be in the following format
% 1: Vorsatz in bottom room on the x-axis
% 2: Vorsatz in bottom room on the y-axis
% 3: Vorsatz in bottom room parallel to x-axis
% 4: Vorsatz in bottom room parallel to y-axis
% 5: Vorsatz ceiling of bottom room
% 6: Vorsatz floor of top room
% 7: Vorsatz in top room on the x-axis
% 8: Vorsatz in top room on the y-axis
% 9: Vorsatz in top room parallel to the x-axis
% 10: Vorsatz in top room parallel to the y-axis

% Define positions and volumes, so we can loop
pos = [    0,     0,       0;...    % Bottom room
           0,     0,       0;...
           0, ly-wV,       0;...
       lx-wV,     0,       0;...
           0,     0, lzLR-wV;...    % Separating vorsatz
           0,     0,  lzLR+d;...
           0,     0,  lzLR+d;...    % Top room
           0,     0,  lzLR+d;...
           0, ly-wV,  lzLR+d;...
       lx-wV,     0,  lzLR+d];
vol = [lx, wV, lzLR;... % Bottom room
       wV, ly, lzLR;...
       lx, wV, lzLR;...
       wV, ly, lzLR;...
       lx, ly,   wV;... % Separating vorsatz
       lx, ly,   wV;...
       lx, wV, lzUR;... % Top room
       wV, ly, lzUR;...
       lx, wV, lzUR;...
       wV, ly, lzUR];
% Specify in which coordinate (x+-, y+-, z+-) the cube should be formed
% into a pyramid
pyr = {'y+','x+','y-','x-',...  % Bottom room
       'z-','z+',...            % Separating vorsatz
       'y+','x+','y-','x-'};    % Top room
   
% Initialize legend handle
leg_hV = gobjects(10,1);
leg_sV = cell(10,1);

% Loop over the Vorsatzschalen
for i=1:10
    % Plot the vorsatz only if it is valid
    if v_b_plot(i)
        ha = patch(ax,'Faces',fc,...
                      'Vertices',vertices(pos(i,:),vol(i,:),pyr{i},wV),...
                      'FaceColor',colors(v_colorIndx(i),:));
        % Legend
        leg_hV(i) = ha;
        leg_sV{i,1} = Vorsatz.Dropdown{Vorsatz.ID == vor(i),1};
    end
end

% Reduce the legend to unique elements
forLeg = v_indxInMulti(v_b);
leg_hV = leg_hV(forLeg);
leg_sV = leg_sV(forLeg,1);

%% %%%%%%%%%%%%%%%% PLOT COORDINATE SYSTEM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Coordinate system arrow length
aL = 1;
plotCoordinateSystem(ax,[-w,-w,0],aL);

% Arrow half width
aW = aL/6;

%% %%%%%%%%%%%%%%%%% FORMAT THE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hide the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';
% Enable perspective
ax.Projection = 'perspective';

% Remove the background color
ax.Color = 0.94*ones(1,3);

% Set the view to the previous one
ax.View = axView;

% Use correct ratio of the axes
axRatio = [lx + 2*w, ly + 2*w, h] + aW;
ax.PlotBoxAspectRatio = axRatio./max(axRatio);

% Update the limits of the axes
ax.XLim = [-aW-w, lx+w];
ax.YLim = [-aW-w, ly+w];
ax.ZLim = [-aW,      h];

% Enable 3D-rotate
rotate3d(ax,'on');

%% %%%%%%%%%%%%%% PLOT KNOTEN NAMES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fT = 12;
dw = 2*aW;
% Choose color of the text depending on the color of the central element
col = [0,0,0];

% Print the text
text(ax,lx/2,-w-dw,lzLR+d/2,'Knoten 1','HorizontalAlignment','center',...
                            'FontSize',fT,'Color',col,'FontWeight','bold');
text(ax,-w-dw,ly/2,lzLR+d/2,'Knoten 2','HorizontalAlignment','center',...
                            'FontSize',fT','Color',col,'FontWeight','bold');
text(ax,lx/2,ly+w+dw,lzLR+d/2,'Knoten 3','HorizontalAlignment','center',...
                            'FontSize',fT,'Color',col,'FontWeight','bold');
text(ax,lx+w+dw,ly/2,lzLR+d/2,'Knoten 4','HorizontalAlignment','center',...
                             'FontSize',fT,'Color',col,'FontWeight','bold');

%% %%%%%%%%%%%%%% PLOT THE LEGEND %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Concateneate the legend
leg_h = [leg_h; leg_hV];
leg_s = [leg_s; leg_sV];

% Check if leg_h is empty in which case we don't print
if ~isempty(leg_h)
    % Plot the legend
    legend(ax,leg_h,leg_s,'Location','eastoutside',...
                          'Units','pixels',...
                          'Interpreter','none',...
                          'Color',0.94*ones(1,3),...
                          'Box','off',...
                          'Tag','SituationLEG');
end

end