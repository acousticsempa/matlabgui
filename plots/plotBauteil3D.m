function h = plotBauteil3D(ax,f,v,color,anisotropie)

% Function takes care of printing the cube with the anisotropie properties 
% which can be either:
% 0:    no anisotropie
% 1:    anisotropie in x-direction
% 2:    anisotropie in y-direction
% 3:    anisotropie in z-direction

% Define in how many stripes we want to devide the cube in subcubes of
% different shades
sx = 11;
sy = 11;
sz = 5;

% Define the transparency of the elements with reduced transparency
alpha = 0.7;
alphaEdge = 0.3;
% Define a grey edge color
colorEdge = 'k';

% Hold the axes
hold(ax,'on');

% If anysotropie does not exist we replace it with 0
if ~exist('anisotropie','var')
    anisotropie = 0;
end

%% %%%%%%%%%%%%%%%%% ANISOTROPIE = 0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First we plot Bauteile which have no anisotropie
if anisotropie == 0
    % Plot the Bauteil using solid color as specified by the user
    h = patch(ax,'Faces',f,'Vertices',v,...
                 'FaceColor',color,...
                 'FaceAlpha',1);
    return;
end

%% %%%%%%%%%%%%%%%% ANISOTROPIE == 1,2,3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Divide the box to be plotted in multiple subboxes along the xyz-direction
% Anisotropie == 1:     subdivision on x-axis (j = 1)
% Anisotropie == 2:     subdivision on y-axis (j = 2)
% Anisotropie == 3:     subdivision on z-axis (j = 3)

if any(anisotropie == [1,2,3])
    % Define a box subdivision
    switch anisotropie
        case 1
            s = sx;
        case 2
            s = sy;
        case 3
            s = sz;
    end
    % Check that the subdivision is uneven
    if mod(s,2)==0
        s = s+1;
    end
    
    % Find out which coordinate we are modifying
    j = anisotropie;
    
    % Get the dxyz of the cube
    dj = max(v(:,j)) - min(v(:,j));
    % Divide to obtain the new cube dxyz-size
    dj = dj/s;
    
    % Define a mapping for start and endpoints
    switch anisotropie
        case 1
            sF = [1,0,1,0,1,0,1,0];
        case 2
            sF = [1,1,0,0,1,1,0,0];
        case 3
            sF = [1,1,1,1,0,0,0,0];
    end
    sF = (sF' == 1);
    eF = ~sF;
    
    % Create graphical objects array
    h = gobjects(s,1);
    % Iterate over the elememts to plot
    for i=1:s
        % Copy the whole vertices as passed in
        vj = v;
        % Calculate the new position for the 'start' face
        vj(sF,j) = v(sF,j) + (i-1)*dj;
        % Calculate the new position for the 'end' face
        vj(eF,j) = vj(sF,j) + dj;
        
        % Calculate the alpha value
        a = mod(i,2) + (1-mod(i,2))*alpha;
        % Plot the box
        h = patch(ax,'Faces',f,'Vertices',vj,...
                     'FaceColor',color,...
                     'FaceAlpha',a,...
                     'EdgeColor',colorEdge,...
                     'EdgeAlpha',alphaEdge);
    end
    % Retain only the first handle
    h = h(1);
    % Plot the edges around it
    patch(ax,'Faces',f,'Vertices',v,...
             'FaceColor',color,...
             'FaceAlpha',0);
    % Return
    return;    
end