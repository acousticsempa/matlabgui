function printData(indx,dataType)
% Function prints the data about the selected dataset to a figure or tab
% The data being printed is:
%   C-Values, Datatypes, ImportDate, Links, Project, PrufObj, 
%   Report, RR, SR, SNR, Test
% For Airborne we also have
%   Area

%%%%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global Data

%%%%%%%%%%%%%%%%%%%%%%%% DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Airborne Data
if dataType == 1
    DB = Data.Air;
% Impact data
elseif dataType == 2
    DB = Data.Impact;
% Airborne Average data
elseif dataType == 3
    DB = Data.AirAVG;
end

%%%%%%%%%%%%%%%%%%%%%% PLACE THE INFORMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First retrieve the parent tab
DataTab = findobj('Tag','DatenTAB');

% Retrieve all objects that have as Parent DataTab
obj = findobj('Parent',DataTab);
       
% Place the Project info
txt = findobj(obj,'Tag','DTProjTXT');
txt.String = DB.Project{indx,1};
txt = findobj(obj,'Tag','DTPrufObjTXT');
txt.String = DB.PrufObj{indx,1};
txt = findobj(obj,'Tag','DTReportNumberTXT');
%txt.String = DB.Report{indx,1};
txt.String = strcat('(',num2str(DB.ID(indx),'%.0f'),')',32,DB.Report{indx,1});
txt = findobj(obj,'Tag','DTReportDateTXT');
txt.String = DB.Report{indx,2};
txt = findobj(obj,'Tag','DTReportRemarksTXT');
txt.String = DB.Report{indx,3};
txt = findobj(obj,'Tag','DTReportClientTXT');
txt.String = DB.Report{indx,4};

% Place further information on datatypes and import dates and links between
% datasets
txt = findobj(obj,'Tag','DTDataTypeTXT');
txt.String = DB.DataType{indx,1};
txt = findobj(obj,'Tag','DTImportDateTXT');
txt.String = DB.ImportDate{indx,1};

% Place the linking data
% Retrieve the link handles
lbl_air = findobj(obj,'Tag','DTLinkAirLB');
lbl_imp = findobj(obj,'Tag','DTLinkImpactLB');
txt_air = findobj(obj,'Tag','DTLinkAirTXT');
txt_imp = findobj(obj,'Tag','DTLinkImpactTXT');

% Airborne and Impact cases
if dataType == 1 || dataType == 2
    % Change the label text to Link Airborne and Link Impact
    lbl_air.String = 'Link Airborne';
    lbl_imp.String = 'Link Impact';
    
    % Retrieve link to an Airborne dataset
    AirLinkID = DB.Link.Air(indx);
    if AirLinkID ~= 0
        rep = Data.Air.Report{Data.Air.ID==AirLinkID,1};
        txt_air.String = strcat('(',num2str(AirLinkID,'%.0f'),')',32,rep);
    else
        txt_air.String = '-';
    end
    % The link to an Impact dataset
    ImpactLinkID = DB.Link.Impact(indx);
    if ImpactLinkID ~= 0
        rep = Data.Impact.Report{Data.Impact.ID==ImpactLinkID,1};
        txt_imp.String = strcat('(',num2str(ImpactLinkID,'%.0f'),')',32,rep);
    else
        txt_imp.String = '-';
    end
% Airborne average case
else
    % Change the label text to ID first dataset, ID second dataset
    lbl_air.String = 'ID 1st dataset';
    lbl_imp.String = 'ID 2nd dataset';
    
    % Retrieve the link to the first Airborne dataset
    AirLinkID = DB.Link.Air(indx);
    % ID cannot be zero, if it we need to throw an error
    if AirLinkID ~= 0
        rep = Data.Air.Report{Data.Air.ID==AirLinkID};
        txt_air.String = strcat('(',num2str(AirLinkID,'%.0f'),')',32,rep);
    else
        errordlg('Missing 1st dataset for averaged airborne data!',...
                 'Error','modal');
        return;
    end
    % Retrieve the link to the second Airborne dataset
    AirLinkID = DB.Link.Impact(indx);
    % ID cannot be zero, if it we need to throw an error
    if AirLinkID ~= 0
        rep = Data.Air.Report{Data.Air.ID==AirLinkID};
        txt_imp.String = strcat('(',num2str(AirLinkID,'%.0f'),')',32,rep);
    else
        errordlg('Missing 2nd dataset for averaged airborne data!',...
                 'Error','modal');
        return;
    end
end

% Place information about the test
txt = findobj(obj,'Tag','DTTestTitleTXT');
txt.String = DB.Test.Title{indx,1};
txt = findobj(obj,'Tag','DTTestDateTXT');
txt.String = DB.Report{indx,5};
txt = findobj(obj,'Tag','DTTestDescriptionTXT');
txt.String = DB.Test.Description{indx,1};
txt = findobj(obj,'Tag','DTTestObjectTXT');
txt.String = DB.Test.Object{indx,1};

% Place information about the source room
txt = findobj(obj,'Tag','DTSourceRoomTXT');
txt.String = strcat('Room',32,DB.SR{indx,1});
txt = findobj(obj,'Tag','DTSRNotesTXT');
txt.String = DB.SR{indx,2};
% Airborne and Impact Data
if dataType == 1 || dataType == 2
    SRVol = num2str(DB.SR{indx,3},'%.1f');
    SRTemp = num2str(DB.Test.SR(indx,1),'%.0f');
    SRHum = num2str(DB.Test.SR(indx,2),'%.0f');
    SRPres = num2str(DB.Test.SR(indx,3),'%.1f');
% Airborne average data
else
    SRVol = DB.SR{indx,3};
    SRTemp = DB.Test.SR{indx,1};
    SRHum = DB.Test.SR{indx,2};
    SRPres = DB.Test.SR{indx,3};
end
txt = findobj(obj,'Tag','DTSRVolumeTXT');
txt.String = strcat(SRVol,' m3');
txt = findobj(obj,'Tag','DTSRTempTXT');
txt.String = strcat(SRTemp,' �C');
txt = findobj(obj,'Tag','DTSRHumTXT');
txt.String = strcat(SRHum,' %');
txt = findobj(obj,'Tag','DTSRPresTXT');
txt.String = strcat(SRPres,' kPa');
    
% Place information about the receiver room
txt = findobj(obj,'Tag','DTReceiverRoomTXT');
txt.String = strcat('Room',32,DB.RR{indx,1});
txt = findobj(obj,'Tag','DTRRNotesTXT');
txt.String = DB.RR{indx,2};
% Airborne and Impact Data
if dataType == 1 || dataType == 2
    RRVol = num2str(DB.RR{indx,3},'%.1f');
    RRTemp = num2str(DB.Test.RR(indx,1),'%.0f');
    RRHum = num2str(DB.Test.RR(indx,2),'%.0f');
    RRPres = num2str(DB.Test.RR(indx,3),'%.1f');
% Airborne average data
else
    RRVol = DB.RR{indx,3};
    RRTemp = DB.Test.RR{indx,1};
    RRHum = DB.Test.RR{indx,2};
    RRPres = DB.Test.RR{indx,3};
end
txt = findobj(obj,'Tag','DTRRVolumeTXT');
txt.String = strcat(RRVol,' m3');
txt = findobj(obj,'Tag','DTRRTempTXT');
txt.String = strcat(RRTemp,' �C');
txt = findobj(obj,'Tag','DTRRHumTXT');
txt.String = strcat(RRHum,' %');
txt = findobj(obj,'Tag','DTRRPresTXT');
txt.String = strcat(RRPres,' kPa');

% Place information about the measurement values
% Here we have to be careful to hide certain other features depending on
% the selected Airborne or Impact data

% Define the tag strings
tagsOrig = {'C','Ctr','C3150','Ctr3150','C50','Ctr50','C100','Ctr100'};

if dataType == 1 || dataType == 3
    %%%%%%%%%%%%%% PREPARE THE TXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define the tag strings
    tags = strcat('DT',tagsOrig,'TXT');
    % Iterate over the tags
    for i=1:numel(tags)
        % Retrieve the element
        txt = findobj(obj,'Tag',tags{i});
        % Make it visible
        txt.Visible = 'on';
        % Put the value there
        txt.String = num2str(DB.C(indx,i),'%.0f');
    end
    % Output the SNR and the area
    txt = findobj(obj,'Tag','DTSNRTXT');
    txt.String = num2str(DB.SNR(indx,1),'%.1f');
    txt = findobj(obj,'Tag','DTAreaTXT');
    txt.Visible = 'on';
    if dataType == 1
        Area = num2str(DB.Area(indx,1),'%.2f');
    else
        Area = DB.Area{indx,1};
    end
    txt.String = [Area,' m2'];
    
    %%%%%%%%%%%%%% PREPARE THE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define the tag strings
    tags = strcat('DT',tagsOrig,'LB');
    % Iterate over the tags
    for i=1:numel(tags)
        % Retrieve the element
        txt = findobj(obj,'Tag',tags{i});
        % Make it visible
        txt.Visible = 'on';
    end
    % Correct the labels for the first two elements
    txt = findobj(obj,'Tag','DTCLB');
    txt.String = 'C';
    txt = findobj(obj,'Tag','DTCtrLB');
    txt.String = 'C_tr';
    % Render the area label
    txt = findobj(obj,'Tag','DTAreaLB');
    txt.Visible = 'on';
elseif dataType == 2
    %%%%%%%%%%%%%% HIDE THE TXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define the tag strings
    tags = strcat('DT',tagsOrig,'TXT');
    % Iterate over the tags
    for i=1:numel(tags)
        % Retrieve the element
        txt = findobj(obj,'Tag',tags{i});
        % Make it visible
        txt.Visible = 'off';
        % Put a zero value in 
        txt.String = '0';
    end
    % Output the C_I and C_I_50_2500
    txt = findobj(obj,'Tag','DTCTXT');
    txt.Visible = 'on';
    txt.String = num2str(DB.C(indx,1),'%.0f');
    txt = findobj(obj,'Tag','DTCtrTXT');
    txt.Visible = 'on';
    txt.String = num2str(DB.C(indx,2),'%.0f');
    
    % Output the SNR and the area
    txt = findobj(obj,'Tag','DTSNRTXT');
    txt.String = num2str(DB.SNR(indx,1),'%.1f');
    txt = findobj(obj,'Tag','DTAreaTXT');
    txt.Visible = 'off';
    txt.String = '0 m2';
    
    %%%%%%%%%%%%%% PREPARE THE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Define the tag strings
    tags = strcat('DT',tagsOrig,'LB');
    % Iterate over the tags
    for i=1:numel(tags)
        % Retrieve the element
        txt = findobj(obj,'Tag',tags{i});
        % Make it visible
        txt.Visible = 'off';
    end
    % Correct the labels for the first two elements
    txt = findobj(obj,'Tag','DTCLB');
    txt.Visible = 'on';
    txt.String = 'C_I';
    txt = findobj(obj,'Tag','DTCtrLB');
    txt.Visible = 'on';
    txt.String = 'C_I 50-2500';
    
    % Hide the area label
    txt = findobj(obj,'Tag','DTAreaLB');
    txt.Visible = 'off';
end

end

