function [newData] = levelSubtraction(inputDatasets)
% This function takes care of averaging the datasets passed in, the
% function returns a structure containing the relevant information
% %%% INPUT %%%
% inputDatasets:    [Datatype,indx] a 2x2 array indicating which
%                   projects are being considered for comparison
%
% %%% OUTPUT %%%
% newData:          Structure containing the averaged data
%   .Type           Airborne or Impact
%   .TL             Transmission loss (Airborne)
%   .NISPL          something (Impact)

%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
global Data

% Retrieve the number of datasets we are averaging
n = size(inputDatasets,1);

% If the number is not two return an error
if n~=2
    % Throw error dialog
    errordlg(['Number of passed in datasets for level subtraction is',...
              ' different than 2!'],'Number of datasets error','modal');
    return;
end

% Retrieve the datatype
selDBType = inputDatasets(1,1);

% Define the DBType
DBType = {'Air','Impact','AirAVG'};

% Retrieve the project indexes
projIndx = inputDatasets(:,2);

%% %%%%%%%%%%% Retrieve the relevant information %%%%%%%%%%%%%%%%%%%%%%%%%%
% Airborne data
if selDBType == 1 || selDBType == 3
    % Retrieve the measurement frequencies
    freq = Data.(DBType{selDBType}).MeasFreq;
    % Retrieve datatype
    meastype = Data.(DBType{selDBType}).DataType{projIndx(1),1};
    
    % Check if all datatypes are either 1 or 3, in this case we can do
    % the TL retrieval directly, otherwise we need to iterate
    % through each dataset
    % Retrieve the datatypes
    dTypes = unique(inputDatasets(:,1));
    % Check which type it is
    if numel(dTypes) == 1
        % Retrieve Transmission Loss
        TL = Data.(DBType{dTypes}).TL(projIndx,:);
    else
        % Initialize the TL Matrix
        TL = zeros(n,numel(freq));
        % Retrieve the TL row by row
        for i=1:n
            TL(i,:) = Data.(DBType{inputDatasets(i,1)}).TL(projIndx(i),:);
        end
    end
    
    % Subtract the data
    deltaTL = TL(1,:) - TL(2,:);
    % Round to one decimal place
    deltaTL = fix(10*deltaTL + 0.5)/10;
    
    % Store data in the structure
    newData.Type = 'Airborne';
    newData.Values = deltaTL;
    newData.DataType = meastype;
% Impact data   
elseif selDBType == 2
    % Retrieve Trittschallpegel
    NISPL = Data.Impact.NISPL(projIndx,:);
    % Retrieve datatype
    meastype = Data.Impact.DataType{projIndx(1),1};
    
    % Subtract the data
    deltaNISPL = NISPL(1,:)-NISPL(2,:);
    % Round to one decimal place
    deltaNISPL = fix(10*deltaNISPL + 0.5)/10;
    
    % Store data in the structure
    newData.Type = 'Impact';
    newData.Values = deltaNISPL;
    newData.DataType = meastype;
end
end