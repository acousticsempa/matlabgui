function [passed] = checkParentProperties(inputDatasets,varargin)

% Function takes care of comparison the datasets passed by checking that
% the project selected all have the same Datatype (Air/Impact), Project, 
% Prufobj, Datatype (R',L',D',...) and Shielding conditions
% %% INPUT %%
% inputDatasets:    An nx2 array identifying the datasets uniquely
% toBeChecked:      an mx1 cell array, containing the properties names that
%                   should be checked
%                   Valid properties are:   Project, PrufObj, Shielding
%                                           All

%% Declare global variables 
global Data

% Initialize return value 
passed = false;

% Create list of datatypes
DBType = {'Air','Impact','AirAVG'};

% Retrieve which test should be done
testProject = any(strcmpi(varargin,'Project'));
testPrufObj = any(strcmpi(varargin,'PrufObj'));
testShielding = any(strcmpi(varargin,'Shielding'));

% Check if the user has inputted 'all' as a parameter
testAll = any(strcmpi(varargin,'All'));
if testAll
    % Set all the boolean variables to true
    testProject = true; 
    testPrufObj = true; 
    testShielding = true; 
    testMeasType = true; 
end

%% Retrieve the properties of the first report
selDatatype = inputDatasets(1,1);

% Check that the selected datatype is one of the allowed ones
if ~(selDatatype >= 1 && selDatatype <=3)
    % Throw an error
    errordlg('Error in specification of project datatype!','Error','modal');
    return;
end

% Retrieve field for first project
dbType = DBType{selDatatype};

% Retrieve the data for the first project
indx = inputDatasets(1,2);
selProject = Data.(dbType).Project{indx,1};
selMeasType = Data.(dbType).DataType{indx,1};
selPrufObj = Data.(dbType).PrufObj{indx,1};
selShielding = Data.(dbType).Shielding(indx,:);

%% Compare all other projects with the first project and return errors in 
%  case they do not share the same parent properties
% Retrieve the number of datasets to analyze
n = size(inputDatasets,1);

% Initialize datatype error
datatypeError = false;

% Iterate
for i=2:n
    % Retrieve the datatype for the specific project
    selDtype = inputDatasets(i,1);
    dbType = DBType{selDtype};
    % Retrieve the project index
    indx = inputDatasets(i,2);
    
    % If the Datatype is not equal we do not do averaging
    % (Air and AirAVG can be averaged together!!)
    if sum(selDatatype == [1,3]) && prod(selDtype ~= [1,3])
        % Datatype error
        datatypeError = true;
    elseif (selDatatype == 2) && (selDtype ~= 2)
        % Datatype error
        datatypeError = true;
    end
    if datatypeError
        % Throw datatype error
        errordlg(['All for comparison selected datasets should have',...
                  ' the same datatype (Airborne and Airborne Average',...
                  ' or Impact)!'],...
                 'Datatype error','modal');
        return;
    end
    
    % Compare project names
    if ~strcmpi(selProject,Data.(dbType).Project{indx,1}) && testProject
        % Throw project error
        errordlg(['All for comparison selected datasets should be from',...
                  ' the same project!'],...
                 'Project error','modal');
        return;
    end
    
    % Compare Prufobjects
    if ~strcmpi(selPrufObj,Data.(dbType).PrufObj{indx,1}) && testPrufObj
        % Throw prufobj error
        errordlg(['All for comparison selected datasets should have the',...
                  ' same Prufobject!'],...
                 'Prufobject error','modal');
        return;
    end
    
    % Compare measuremt type
    if ~strcmpi(selMeasType,Data.(dbType).DataType{indx,1})
        % Throw datatype error
        errordlg(['All for comparison selected datasets should have the',...
                  ' same Measurement Datatype (R'',D,L'')!'],...
                 'Measurement Datatype Error','modal');
        return;
    end
    
    % Compare shielding conditions
    if ~prod(selShielding==Data.(dbType).Shielding(indx,:)) && testShielding
        % Throw shielding error
        errordlg(['All for comparision selected datasets should have',...
                  ' the same Shielding conditions!'],...
                 'Shielding error','modal');
        return;
    end
end

% If all the test are passed return a true boolean variable
passed = true; 
return;
end