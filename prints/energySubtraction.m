function [newData] = energySubtraction(inputDatasets)
% This function takes care of averaging the datasets passed in, the
% function returns a structure containing the relevant information
% %%% INPUT %%%
% inputDatasets:    [Datatype,indx] a 2x2 array indicating which
%                   projects are being considered for comparison
%
% %%% OUTPUT %%%
% newData:          Structure containing the averaged data
%   .Type           Airborne or Impact
%   .TL             Transmission loss (Airborne)
%   .NISPL          something (Impact)
%   .SNR            Single Number Rating for the averaged set
%   .DataType       Measurement type (R',D,L'...)
%   .C              Spectrum correction values
%                   Airborne:   C,Ctr,C_50_....
%                   Impact:     C_I, C_I_50...

%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
global Data

% Read the threshold from the sliders
sli = findobj('Tag','EnergyThresholdSLI');
threshold = sli.Value;

% Retrieve the number of datasets we are averaging
n = size(inputDatasets,1);

% If the number is not two return an error
if n~=2
    % Throw error dialog
    errordlg(['Number of passed in datasets for energy subtraction is',...
              ' different than 2!'],'Number of datasets error','modal');
    return;
end

% Retrieve the datatype
selDBType = inputDatasets(1,1);

% Define the DBType
DBType = {'Air','Impact','AirAVG'};

% Retrieve the project indexes
projIndx = inputDatasets(:,2);

%%%%%%%%%%%%%% Retrieve the relevant information %%%%%%%%%%%%%%%%%%%%%%%%%%
if any(selDBType == [1,3])
    %% Airborne data
    % Retrieve the measurement frequencies
    freq = Data.(DBType{selDBType}).MeasFreq;
    % Retrieve datatype
    meastype = Data.(DBType{selDBType}).DataType{projIndx(1),1};
    
    % Check if all datatypes are either 1 or 3, in this case we can do
    % the TL retrieval directly, otherwise we need to iterate
    % through each dataset
    % Retrieve the datatypes
    dTypes = unique(inputDatasets(:,1));
    % Check which type it is
    if numel(dTypes) == 1
        % Retrieve Transmission Loss
        TL = Data.(DBType{dTypes}).TL(projIndx,:);
    else
        % Initialize the TL Matrix
        TL = zeros(n,numel(freq));
        % Retrieve the TL row by row
        for i=1:n
            TL(i,:) = Data.(DBType{inputDatasets(i,1)}).TL(projIndx(i),:);
        end
    end
    
    % Retrieve the areas, iterate for each selected dataset, taking care of
    % differentiating between Airborne and AirborneAveraged different
    % formatting
    n = numel(projIndx);
    Area = zeros(n,1);
    for i=1:n
        if inputDatasets(i,1)==1
            Area(i) = Data.Air.Area(projIndx(i),1);
        else
            % Retrieve the string and split it by /
            str = strsplit(Data.AirAVG.Area{projIndx(i),1},'/');
            % Evaluate the string to an array, average and store
            Area(i) = mean([eval(str{1,1}),eval(str{1,2})]);
        end
    end
    
    % Check the threshold
    thresholdNotPassed = ((TL(2,:)-TL(1,:)) < threshold);
    % Subtract the data
    subTL = -10*log10(10.^(-TL(1,:)/10)-10.^(-TL(2,:)/10));
    % Convert complex values (coming from negative logarithm arguments
    % to NaN
    for i=1:numel(subTL)
        % Check if this index is real or comples
        if ~isreal(subTL(i))
            % Change to NaN
            subTL(i) = NaN;
        end
    end
    % Update the subtracted values with the threshold control
    subTL(thresholdNotPassed) = NaN;
   
    % Calculate SNR and C-Values
    SNRC = SNRCValues(freq,subTL,1);
    
    % Store data in the structure
    newData.Type = 'Airborne';
    newData.Freq = freq;
    newData.Values = subTL;
    newData.SNR = SNRC.SNR;
    newData.DataType = meastype;
    newData.C = SNRC.C;
    newData.Area = mean(Area);
    newData.Length = 0;
     
elseif selDBType == 2
    %% Impact data  
    % Retrieve Trittschallpegel
    NISPL = Data.Impact.NISPL(projIndx,:);
    % Retrieve datatype
    meastype = Data.Impact.DataType{projIndx(1),1};
    % Retrieve the measurement frequencies
    freq = Data.Impact.MeasFreq;
    
    % Check the threshold
    thresholdNotPassed = ((NISPL(1,:)-NISPL(2,:)) < threshold);
    % Average the data
    subNISPL = 10*log10(10.^(NISPL(1,:)/10)-10.^(NISPL(2,:)/10));
    % Convert complex values (coming from negative logarithm arguments
    % to NaN
    for i=1:numel(subNISPL)
        % Check if this index is real or comples
        if ~isreal(subNISPL(i))
            % Change to NaN
            subNISPL(i) = NaN;
        end
    end
    % Update the subtracted values with the threshold control
    subNISPL(thresholdNotPassed) = NaN;
    
    % Calculate SNR and C-Values
    SNRC = SNRCValues(freq,subNISPL,2);
    
    % Store data in the structure
    newData.Type = 'Impact';
    newData.Freq = freq;
    newData.Values = subNISPL;
    newData.DataType = meastype;
    newData.SNR = SNRC.SNR;
    newData.C = SNRC.C;
    newData.Area = 0;
    newData.Length = 0;
end

end