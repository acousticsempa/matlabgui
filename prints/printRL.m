function printRL(sheet,position,vals,typeID)

% Function takes care of printing the SNR + C values plus the values
% depending on the typeID passed in

% Declare global variables
global Data

% Calculate the SNR + C values
if typeID==1
    specs = SNRCValues(Data.Air.MeasFreq,vals,typeID);
else
    specs = SNRCValues(Data.Impact.MeasFreq,vals,typeID);
end

% Transpose the data
vals = transpose(vals);
specs.C = transpose(specs.C);

% Print the data to the spreadsheet
if typeID==1
    setRange(sheet,position,2,1,specs.SNR);
    setRange(sheet,position+[2,0],8,1,specs.C);
    setRange(sheet,position+[11,0],21,1,vals);
else
    setRange(sheet,position,2,1,specs.SNR);
    setRange(sheet,position+[2,0],2,1,specs.C);
    setRange(sheet,position+[10,0],21,1,vals);
end

end