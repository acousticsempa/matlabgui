function [newData] = avgDatasets(inputDatasets)
% This function takes care of averaging the datasets passed in, the
% function returns a structure containing the relevant information
% %%% INPUT %%%
% inputDatasets:    [Datatype,indx] a nx2 array indicating which 
%                   projects are being considered for comparison
%
% %%% OUTPUT %%%
% newData:          Structure containing the averaged data
%   .TL             Transmission loss (Airborne)
%   .NISPL          something (Impact)
%   .SNR            Single Number Rating for the averaged set

%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
global Data

% Retrieve the number of datasets we are averaging
n = size(inputDatasets,1);

% Retrieve the datatype
selDBType = inputDatasets(1,1);
% Define the DBType
DBType = {'Air','Impact','AirAVG'};

% Retrieve the project indexes
projIndx = inputDatasets(:,2);

%% Retrieve the relevant information
if any(selDBType==[1,3])
    %% Airborne data
     % Retrieve the measurement frequencies
    avgFreq = Data.(DBType{selDBType}).MeasFreq;
    % Retrieve datatype
    meastype = Data.(DBType{selDBType}).DataType{projIndx(1),1};
    
    % Check if all datatypes are either 1 or 3, in this case we can do 
    % the TL retrieval directly, otherwise we need to iterate
    % through each dataset
    % Retrieve the datatypes
    dTypes = unique(inputDatasets(:,1));
    % Check which type it is
    if numel(dTypes) == 1
        % Retrieve Transmission Loss
        TL = Data.(DBType{dTypes}).TL(projIndx,:);
    else
        % Initialize the TL Matrix
        TL = zeros(n,numel(avgFreq));
        % Retrieve the TL row by row
        for i=1:n
            TL(i,:) = Data.(DBType{inputDatasets(i,1)}).TL(projIndx(i),:);
        end
    end
    
    % Retrieve the areas, iterate for each selected dataset, taking care of
    % differentiating between Airborne and AirborneAveraged different
    % formatting
    n = numel(projIndx);
    Area = zeros(n,1);
    for i=1:n
        if inputDatasets(i,1)==1
            Area(i) = Data.Air.Area(projIndx(i),1);
        else
            % Retrieve the string and split it by /
            str = strsplit(Data.AirAVG.Area{projIndx(i),1},'/');
            % Evaluate the string to an array, average and store
            Area(i) = mean([eval(str{1,1}),eval(str{1,2})]);
        end
    end

    % Average the data
    avgTL = -10*log10(1/n*sum(10.^(-TL/10),1));
    Area = mean(Area);
    % Round the data
    avgTL = round_dec(avgTL,1);
    
    % Calculate new SNR and C-values
    SNRC = SNRCValues(avgFreq,avgTL,1);
    
    % Store data in the structure
    newData.Type = 'Airborne';
    newData.Freq = avgFreq;
    newData.Values = avgTL;
    newData.SNR = SNRC.SNR;
    newData.DataType = meastype;
    newData.C = SNRC.C;
    newData.Area = Area;
    newData.Length = 0;
    
elseif selDBType == 2
    %% Impact data 
    % Retrieve Trittschallpegel
    NISPL = Data.Impact.NISPL(projIndx,:);
    % Retrieve datatype
    meastype = Data.Impact.DataType{projIndx(1),1};
    % Retrieve the measurement frequencies
    avgFreq = Data.Impact.MeasFreq;
    
    % Average the data
    avgNISPL = 10*log10(1/n*sum(10.^(NISPL/10),1));
    % Round the data
    avgNISPL = round_dec(avgNISPL,1);

    % Calculate new SNR and C-values
    SNRC = SNRCValues(avgFreq,avgNISPL,2);
    
    % Store data in the structure
    newData.Type = 'Impact';
    newData.Freq = avgFreq;
    newData.Values = avgNISPL;
    newData.DataType = meastype;
    newData.SNR = SNRC.SNR;
    newData.C = SNRC.C;
    newData.Area = 0;
    newData.Length = 0;
end

end