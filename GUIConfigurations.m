function GUIConfigurations()

% Function is a configuration file, where variables critical to the GUI
% behaviour are define and made accessible for the rest of the GUI
% functions

%% %%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configurations structure
global Configs

%% %%%%%%%%%%%%%%%%%%% DEFINE XML NAME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
XMLconfig = 'configs.xml';

%% %%%%%%%%%%%%%%%%%%% GET USERNAME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the user starting this program
Configs.User = getenv('USERNAME');

%% %%%%%%%%%%%%%%%%%%% TESTING CONDITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TESTING: Here we specify a boolean variable to be used throughout the
%          program for pieces of the scripts that we only want to execute
%          in testing and for blocking scripts that we want to execute only
%          in production
% Specify if we are testing or if we are in production

% 'mabe should lead to testing conditions
if strcmpi(Configs.User,'mabe')
    Configs.Testing = true;
else
    Configs.Testing = false;
end

%% %%%%%%%%%%%%%%%%%%%%% DEFINE DEFAULT VALUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%% EXCEL FILES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify path to the excel database
if Configs.Testing
    DBPath = 'C:\Users\mabe\Documents\Projects\MatlabGUI\tmp\database\';
else
    DBPath = 'G:\Limit\LK106\DATABASE\';
end

% Specify the name of the original database
DBName = 'LK106_DATABASE';
% Full paths to the original and converted databases
Configs.Database.Original = [DBPath,DBName,'.xlsx'];
Configs.Database.Converted = [DBPath,DBName,'_R.xlsx'];

% %%%%%%%%%%%%%%%%%% EXPORT DATABASES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File for storage of Knoten / Bauteile / Vorsatz / Ubertragung /
% VUbertragung
Configs.Database.Knoten = [DBPath,'Knoten.xlsx'];
Configs.Database.Bauteile = [DBPath,'Bauteile.xlsx'];
Configs.Database.Vorsatz = [DBPath,'Vorsatz.xlsx'];
Configs.Database.Ubertragung = [DBPath,'Ubertragung.xlsx'];
Configs.Database.VUbertragung = [DBPath,'VUbertragung.xlsx'];

% %%%%%%%%%%%%%%%%% DIARY FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File for storage of command line commands and outputs
Configs.Diary = [DBPath,'Diary.txt'];
    
% %%%%%%%%%%%%%%%%% HASHING ALGORITHM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the hashing algorithm to be used
Configs.Hashing = 'SHA512';

% %%%%%%%%%%%%%%%%%% TEMPLATES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Configs.Template.Situation.Vertical = [pwd(),'\templates\VerticalSituation.xlsx'];
Configs.Template.Situation.Horizontal = [pwd(),'\templates\HorizontalSituation.xlsx'];


%% %%%%%%%%%%%%%%%%%%%%% WRITE XML FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write the XML configuration file only if it does not yet exist
if exist(XMLconfig,'file')~=2
   % Write the XML configuration file
   writeXMLconfig(XMLconfig);
end
    
%% %%%%%%%%%%%%%%%%%%%%%%%% READ THE XML %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
updateXML = readXMLconfig(XMLconfig);

%% %%%%%%%%%%%%%%%%%%%%%% UPDATE THE XML %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We update the XML file only if necessary
if updateXML 
    % Write the XML configuration file
    writeXMLconfig(XMLconfig);
end

%% %%%%%%%%%%%%%%%%%%%%%% RETRIEVE APP VERSION %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read in the XML file containing information on the application version
readXMLversion();

end