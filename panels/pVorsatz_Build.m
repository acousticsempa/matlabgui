function pVorsatz_Build()
% Function defines the elements in the pVorsatz window 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and panels
global fParking
global pVorsatz

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 10*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
drop = [150,20];
text = [100,20];
textKur = [200,35];
textDet = [200,60];
list = [150,60];
button = [150,20];
but = [60,20];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = label(1) + ... 
            max([drop(1)+intra(1),...
                 text(1)+intra(1),...
                 textKur(1)+intra(1),...
                 textDet(1)+intra(1),...
                 drop(1)+but(1)+2*intra(1),...
                 list(1)+intra(1),...
                 2*button(1)+2*intra(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = max([6*label(2),...
                  list(2)+max(drop(2),but(2))+...
                  +textDet(2)+textKur(2)+text(2)+drop(2)]) + ...
             + 6*intra(2) + button(2) + marg(2) + marg(4);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the panel position 
pos = [1,1,min_width,min_height];

% Create a new full screen figure
pVorsatz = uipanel('Parent',fParking,...
                   'Units','pixels',...'BorderType','none',...
                   'Position',pos,...
                   'Visible','off');

% Add a data structure to the panel
addprop(pVorsatz,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Vorsatz-Name','Name','Kurzbeschreibung',...
           'Det. Beschreibung','Bauteile'};
tags = {'VorsatzName','Name','KurzBeschr',...
        'DetBeschr','Bauteile'};
lbl_tag = strcat(tags,'LBL');
for i=1:numel(lbl_txt)
    uicontrol('Parent',pVorsatz,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% EDIT TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the editable text boxes
tags = {'VorsatzName','KurzBeschreibung','DetBeschreibung'};
txt_tag = strcat(tags,'TXT');
txt_max = [1,2,2];
for i=1:numel(txt_tag)
    uicontrol('Parent',pVorsatz,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Min',0,'Max',txt_max(i),...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%% LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to display in the lists
tags = {'Bauteile'};
list_tag = strcat(tags,'LST');
for i=1:numel(list_tag)
    uicontrol('Parent',pVorsatz,'Style','listbox','Min',0,'Max',1,...
              'Visible','on','Units','pixels',...
              'KeyPressFcn',@pVorsatz_Populate,...
              'String',' ','Tag',list_tag{i});
end
                    
%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the dropdowns
tags = {'Vorsatz','Bauteil'};
dd_tag = strcat(tags,'DD');
uicontrol('Parent',pVorsatz,'Style','popupmenu',...
          'Visible','on','Units','pixels',...
          'Callback',@pVorsatz_Populate,...
          'String',' ','Tag',dd_tag{1});
uicontrol('Parent',pVorsatz,'Style','popupmenu',...
          'Visible','on','Units','pixels',...
          'KeyPressFcn',@pVorsatz_Populate,...
          'String',' ','Tag',dd_tag{2});


%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
but_txt = {'Delete','Save','Add'};
tags = {'Delete','Modify','AddBauteil'};
but_tag = strcat(tags,'BUT');
but_fcn = {'exportVorsatz','exportVorsatz','pVorsatz_Populate'}; 
but_fcn = strcat('@',but_fcn);
for i=1:numel(but_txt)
    uicontrol('Parent',pVorsatz,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 5; 
nV = 7;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = label(1);
h2 = round(max([(drop(1)-3*intra(1))/4,...
                (text(1)-3*intra(1))/4,...
                (textKur(1) -3*intra(1))/4,...
                (textDet(1)-3*intra(1))/4,...
                (drop(1)-2*intra(1))/3,...
                 but(1),...
                (list(1)-3*intra(1))/4,...
                (button(1)-intra(1))/2]));
shareH = [h1, h2, h2, h2, h2]/(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));
    
d = max(label(2),drop(2));
t = max(label(2),text(2));
tK = max(label(2),textKur(2));
tD = max(label(2),textDet(2));
l = max(label(2),list(2));
b = max(d,but(2));
shareV = [button(2), l, b, tD, tK, t, d]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have pVorsatz as Parent
obj = findobj('Parent',pVorsatz);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV+1;
for j=1:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
for j=1:3
    txt = findobj(obj,'Tag',txt_tag{j});
    txt.Position = BoundingBox(boxes,[2,nH],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%%% PLACE LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 2;
lst = findobj(obj,'Tag',list_tag{1});
lst.Position = BoundingBox(boxes,[2,nH],i,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
dd = findobj(obj,'Tag',dd_tag{1});
dd.Position = BoundingBox(boxes,[2,nH],i,nH);
dd = findobj(obj,'Tag',dd_tag{2});
dd.Position = BoundingBox(boxes,[2,nH-1],3,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag',but_tag{1});
but.Position = BoundingBox(boxes,[2,3],1,nH);
but = findobj(obj,'Tag',but_tag{2});
but.Position = BoundingBox(boxes,[4,5],1,nH);
but = findobj(obj,'Tag',but_tag{3});
but.Position = BoundingBox(boxes,nH,3,nH);
