function pKnoten_Populate(src,~)

% Function takes care of populating the Knoten panel with the correct data

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Panels
global pKnoten
% Data
global Knoten
global Bauteile

%% %%%%%% RETRIEVE THE OBJECTS IN THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the objects which have as parent the panel
obj = pKnoten.Children;

% Knoten TypeID: Kreuzstoss, TStoss, Anders
gr = findobj(obj,'Tag','KnotenTypRAD');
k = findobj(gr.Children,'Tag','KreuzstossRAD');
t = findobj(gr.Children,'Tag','TStossRAD');
a = findobj(gr.Children,'Tag','AndersRAD');

% Knoten Ausrichtung: Vertikal, Horizontal
gr = findobj(obj,'Tag','AusrichtungRAD');
v = findobj(gr.Children,'Tag','VertikalRAD');
h = findobj(gr.Children,'Tag','HorizontalRAD');

% Knoten Dropdown menu
kDD = findobj(obj,'Tag','KnotenDD');

% Text Boxes
tN = findobj(obj,'Tag','KnotenNameTXT');
tK = findobj(obj,'Tag','KurzBeschreibungTXT');
tD = findobj(obj,'Tag','DetBeschreibungTXT');

% Bauteile Dropdowns
b1 = findobj(obj,'Tag','Bauteil1DD');
b2 = findobj(obj,'Tag','Bauteil2DD');
b3 = findobj(obj,'Tag','Bauteil3DD');
b4 = findobj(obj,'Tag','Bauteil4DD');
bDD = [b1,b2,b3,b4];
% Anisotropie Dropdowns
a1 = findobj(obj,'Tag','Anisotropie1DD');
a2 = findobj(obj,'Tag','Anisotropie2DD');
a3 = findobj(obj,'Tag','Anisotropie3DD');
a4 = findobj(obj,'Tag','Anisotropie4DD');
aDD = [a1,a2,a3,a4];

% Retrieve axes
ax = findobj(obj,'Tag','KnotenPreviewAX');

%% %%%%%%%%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
elem =  [k,t,a, v,h, kDD,     bDD,     aDD];
upLev = [1,1,1, 2,2,   3, 4,4,4,4, 5,5,5,5];

% Find which element called this function
b = (src==elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

% Check if the src is actually a number
if isnumeric(src)
    % Specify the ID of the Knoten to be shown
    showID = src;
else
    % Show the first Knoten
    showID = 0;
end

%% %%%%%%%%%%%%%%%%% UPDATE RADIO BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source passed in to the function is actually a number, we need to
% show that KnotenID, which means we need to retrieve the characteristics
% of the Knoten
if (updateLevel == 0) && (showID ~= 0)
    % Find the Knoten
    b = (Knoten.ID == showID);
    % Retrieve values only if we have a match
    if any(b)
        % Retrieve the TypeID and Ausrichtung
        TypeID = Knoten.TypeID(b);
        Ausrichtung = Knoten.Ausrichtung(b);
        
        % Set the radio buttons
        switch TypeID
            case 1
                k.Value = 1;
            case 2
                t.Value = 1;
            case 3
                a.Value = 1;
        end
        switch Ausrichtung
            case 1
                v.Value = 1;
            case 2 
                h.Value = 1;
        end
    end
end

% Calculate the Knoten-TypeID
TypeID = k.Value*1 + t.Value*2 + a.Value*3;
% Calculate the Knoten-Ausrichtung
Ausrichtung = v.Value*1 + h.Value*2;

%% %%%%%%%%%%%%%%%%% DROPDOWN FOR KNOTEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Knoten Dropdown menu only if the updateLevel is lower than 3
if updateLevel < 3
    % Find Knoten that have the same TypeID as selected
    b_TypeID = (Knoten.TypeID == TypeID);
    % Find Knoten that have the same Ausrichtung as selected
    b_Ausrichtung = (Knoten.Ausrichtung == Ausrichtung);
    % Create the compounded boolean vector
    b = all([b_TypeID, b_Ausrichtung],2);
    
    % Check if we have a match or not
    if any(b)
        % Retrieve the Knoten that respect the selected properties
        dd = Knoten.Dropdown(b);
        % Retrieve the indexes of the Knoten shown in the dropdown
        pKnoten.Data.Knoten = Knoten.ID(b);
    else
        [dd,pKnoten.Data.Knoten] = addNone();
    end
    
    % Update the dropdown and update the value
    kDD.String = dd;
    kDD.Value = 1;
    % Choose the Value of the dropdown so that we see the Knoten, which ID
    % we may have passed in
    if showID~=0
        % Check if we have a match
        b = (pKnoten.Data.Knoten == showID);
        % If we have a match
        if any(b)
            kDD.Value = find(b);
        end
    end
end

% Retrieve the selected Knoten ID
pKnoten.Data.KnotenID = pKnoten.Data.Knoten(kDD.Value);

%% %%%%%%%%%%%%%%%%%%%%%%% KNOTEN PROPERTIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve Knoten properties if the updateLevel is lower than 4
if updateLevel < 4
    % Find out which Knoten is selected in the dropdown menu
    ID = pKnoten.Data.KnotenID;
    % Match Knoten ID in the database
    b = (Knoten.ID==ID);
    % Check if we have a match
    bMatch = any(b);
    
    % If we have a match retrieve Knoten properties
    if bMatch
        % Retrieve the Bauteile and Anisotropie in the Knoten
        teile = Knoten.Bauteile(b,:);
        aniso = Knoten.Anisotropie(b,:);
    else
        % Zero Bauteile-ID and Zero Anisotropie-ID
        teile = zeros(1,4);
        aniso = zeros(1,4);
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%% UPDATE TEXTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the texts only if the updateLevel is lower than 4
if updateLevel < 4
    % Check if we have a match
    if bMatch
        % Retrieve the name, kurzbeschr and detBeschr and place it in the 
        % text-boxes
        tN.String = Knoten.Name{b,1};
        tK.String = Knoten.KurzBeschr{b,1};
        tD.String = Knoten.DetBeschr{b,1};
    else
        % Empty the text-boxes
        tN.String = '';
        tK.String = '';
        tD.String = '';
    end
end
       
%% %%%%%%%%%%%%%%%%%%%% UPDATE BAUTEILE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%
% Update the Bauteile dropdown only if the updateLevel is lower than 4
if updateLevel < 4
   % Find out which Ausrichtung the Knoten has and depending on that we 
   % we need to retrieve Bauteile which are allowed in that position
   % If Ausrichtung == 1 (Vertikal), Bauteile 1/3 are Decken (2)
   %                                 Bauteile 2/4 are Wande (1)
   % If Ausrichtung == 2 (Horizon),  Bauteile 1/3 are Wande (1)
   %                                 Bauteile 2/4 are Wande (1)
   % The Knoten ID and the matching have already been done when updating 
   % the text fields, we can reuse it
   
   % Check if we have a match
   if bMatch
        % Retrieve wande und decke
        bW = (Bauteile.TypeID == 1);
        bD = (Bauteile.TypeID == 2);
        
        % Retrieve dropdown text and corresponding IDs
        ddW = Bauteile.Dropdown(bW);
        ddD = Bauteile.Dropdown(bD);
        idW = Bauteile.ID(bW);
        idD = Bauteile.ID(bD);
        % Update the dropdowns with none
        [ddW,idW] = addNone(ddW,idW);
        [ddD,idD] = addNone(ddD,idD);
        
        % Construct cell arrays with data
        dd = {ddW, ddD};
        id = {idW, idD};
        indx = ones(1,4);
        if Ausrichtung == 1
            indx([1,3]) = 2;
        end
        
        % Update the dropdowns for Bauteile 1/2/3/4
        for i=1:4
            % Update the dropdown string
            bDD(i).String = dd{indx(i)};
            % Update the global variable
            pKnoten.Data.Bauteil{i} = id{indx(i)};
            % Find the correct position of the dropdown
            bDD(i).Value = updateValue(id{indx(i)},teile(i));
        end
   else
       % Update all dropdowns to Nones
       for i=1:4
           [bDD(i).String,pKnoten.Data.Bauteil{i}] = addNone();
           bDD(i).Value = 1;
       end
   end
end

%% %%%%%%%%%%%%%%%%% UPDATE ANISOTROPIE DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%
% Update the anisotropie dropdowns only if the updateLevel is lower than 4
if updateLevel < 4
    % Update using the aniso vector (which is initialized to zeros if we
    % don't have a Knoten match)
    for i=1:4
        aDD(i).Value = aniso(i) + 1;
    end
end

%% %%%%%%%%%%%%%%%%%%%% UPDATE GRAPH %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the teile IDs and anisotropie IDs from the dropdowns
teile = zeros(1,4);
for i=1:4
    teile(i) = pKnoten.Data.Bauteil{i}(bDD(i).Value);
end

% Retrieve the correct anisotropieID
aniso = [a1.Value, a2.Value, a3.Value, a4.Value] - 1;

% Update the graph in all cases
plotKnoten(ax,teile,aniso);

%% %%%%%%%%%%%%%%%%%%%%%%% PROPAGATE DOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the src has an updateLevel smaller than 4 we propagate the changes
% down the chain
if updateLevel < 4
    % Update the other panels
    updatePanels('Knoten');
end