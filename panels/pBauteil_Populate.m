function pBauteil_Populate(src,~)

% Function takes care of populating the Bauteil panel with the correct data

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Panels
global pBauteil
global pKnoten
% Data
global Knoten
global Bauteile
global Vorsatz

%% %%%%%% RETRIEVE THE OBJECTS IN THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the objects which have as parent the panel
obj = pBauteil.Children;

% Bauteil TypeID: Wand/Decke
gr = findobj(obj,'Tag','BauteilTypRAD');
w = findobj(gr.Children,'Tag','WandRAD');
d = findobj(gr.Children,'Tag','DeckeRAD');

% Bauteil Dropdown menu
bDD = findobj(obj,'Tag','BauteilDD');

% Text Boxes
tN = findobj(obj,'Tag','BauteilNameTXT');
tK = findobj(obj,'Tag','KurzBeschreibungTXT');
tD = findobj(obj,'Tag','DetBeschreibungTXT');

% Lists
kLst = findobj(obj,'Tag','KnotenLST');
vLst = findobj(obj,'Tag','VorsatzLST');

%% %%%%%%%%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
elem =  [w,d, bDD];
upLev = [1,1,   2];

% Find which element called this function
b = (src==elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

% Check if the src is actually a number
if isnumeric(src)
    % Specify the ID of the Bauteil to be shown
    showID = src;
else
    % Show the first Bauteil
    showID = 0;
end

%% %%%%%%%%%%%%%%%%% UPDATE RADIO BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source passed in to the function is actually a number, we need to
% show that BauteilID, which means we need to retrieve the characteristics
% of the Bauteil
if (updateLevel == 0) && (showID ~= 0)
    % Find the Bauteil
    b = (Bauteile.ID == showID);
    % Retrieve values only if we have a match
    if any(b)
        % Retrieve the TypeID and Ausrichtung
        TypeID = Bauteile.TypeID(b);
        
        % Set the radio buttons
        switch TypeID
            case 1
                w.Value = 1;
            case 2
                d.Value = 1;
        end
    end
end

% Calculate the Bauteil-TypeID
TypeID = w.Value*1 + d.Value*2;

%% %%%%%%%%%%%%%%%%% DROPDOWN FOR BAUTEIL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Bauteil Dropdown menu only if the updateLevel is lower than 2
if updateLevel < 2
    % Find Bauteil that have the same TypeID as selected
    b = (Bauteile.TypeID == TypeID);
    
    % Check if Knoten is visible, in which case, we need to find which 
    % Bauteile are used in the selected Knoten and restrict the choice of
    % Bauteile to those ones
    if strcmpi(pKnoten.Visible,'on')
        % Find if we have a match with the selected Knoten
        bK = (Knoten.ID == pKnoten.Data.KnotenID);
        if any(bK)
            % Find Bauteile which are member of the Knoten-Bauteile
            bB = ismember(Bauteile.ID,Knoten.Bauteile(bK,:));
        else
            bB = false(size(b));
        end
        % Compound the boolean vectors
        b = all([b,bB],2);
    end            
    
    % Check if we have a match or not
    if any(b)
        % Retrieve the Bauteil that respect the selected properties
        dd = Bauteile.Dropdown(b);
        % Retrieve the indexes of the Bauteil shown in the dropdown
        pBauteil.Data.Bauteil = Bauteile.ID(b);
    else
        [dd,pBauteil.Data.Bauteil] = addNone();
    end
    
    % Update the dropdown and update the value
    bDD.String = dd;
    bDD.Value = 1;
    % Choose the Value of the dropdown so that we see the Bauteil, which ID
    % we may have passed in
    if showID~=0
        % Check if we have a match
        b = (pBauteil.Data.Bauteil == showID);
        % If we have a match
        if any(b)
            bDD.Value = find(b);
        end
    end
end

% Retrieve the selected Bauteil ID
pBauteil.Data.BauteilID = pBauteil.Data.Bauteil(bDD.Value);

%% %%%%%%%%%%%%%%%%%%%%%%% BAUTEIL PROPERTIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve Bauteil properties if the updateLevel is lower than 3
if updateLevel < 3
    % Find out which Bauteil is selected in the dropdown menu
    ID = pBauteil.Data.BauteilID;
    % Match Bauteil ID in the database
    b = (Bauteile.ID == ID);
    % Check if we have a match
    bMatch = any(b);
end

%% %%%%%%%%%%%%%%%%%%%%%%% UPDATE TEXTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the texts only if the updateLevel is lower than 3
if updateLevel < 3
    % Check if we have a match
    if bMatch
        % Retrieve the name, kurzbeschr and detBeschr and place it in the 
        % text-boxes
        tN.String = Bauteile.Name{b,1};
        tK.String = Bauteile.KurzBeschr{b,1};
        tD.String = Bauteile.DetBeschr{b,1};
    else
        % Empty the text-boxes
        tN.String = '';
        tK.String = '';
        tD.String = '';
    end
end
       
%% %%%%%%%%%%%%%%%%%%%% UPDATE BAUTEILE LISTS %%%%%%%%%%%%%%%%%%%%%%%%
% Update the Bauteile lists only if the updateLevel is lower than 3
if updateLevel < 3
   % Find out which Knoten have the Bauteil in their build-up and 
   % find out which Vorsatz are connected to the Bauteil
   
   % Find Knoten
   bK = any(Knoten.Bauteile == ID,2);
   % Find Vorsatz
   bV = any(Vorsatz.Bauteile == ID,2);
   
   % Check if we have a match for Knoten
   if any(bK)
       kLst.String = Knoten.Dropdown(bK,1);
   else
       kLst.String = ' ';
   end
   kLst.Value = 1;
   
   % Check if we have a match for Vorsatz
   if any(bV)
       vLst.String = Vorsatz.Dropdown(bV,1);
   else
       vLst.String = ' ';
   end
   vLst.Value = 1;
end

%% %%%%%%%%%%%%%%%%%%% PROPAGATE DOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the src has an updateLevel smaller than 3 we propagate the changes
% down the chain
if updateLevel < 3
    % Update the other panels
    updatePanels('Bauteil');
end

end