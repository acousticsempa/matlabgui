function pUbertragung_Populate(src,callbackdata)

% Function takes care of populating the Ubertragung panel with the correct
% data

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Panels
global pUbertragung
global pKnoten
global pBauteil
% Data
global Knoten
global Ubertragung

%% %%%%%% RETRIEVE THE OBJECTS IN THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the objects which have as parent the panel
obj = pUbertragung.Children;

% UbertragungTyp: Luftschall / Trittschall
gr = findobj(obj,'Tag','UbertragungTypRAD');
l = findobj(gr.Children,'Tag','LuftschallRAD');
t = findobj(gr.Children,'Tag','TrittschallRAD');

% Dropdown menus
ddU = findobj(obj,'Tag','UbertragungDD');
ddSR = findobj(obj,'Tag','StartraumDD');
ddER = findobj(obj,'Tag','EndraumDD');
ddSB = findobj(obj,'Tag','StartbauteilDD');
ddEB = findobj(obj,'Tag','EndbauteilDD');

% Get the text-fields
fl = findobj(obj,'Tag','FlacheTXT');
la = findobj(obj,'Tag','LangeTXT');
oDB = findobj(obj,'Tag','OriginalDBTXT');

% Get the table
tb = findobj(obj,'Tag','RLWerteTABLE');

% Get the axes
ax = findobj(obj,'Tag','RLWerteAX');

%% %%%%%%%%%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
elem =  [l,t, ddU, tb, ddSR,ddER];
upLev = [1,1,   2,  3,    4,   4];

% Find which element called this function
b = (src == elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

% Check if the src is actually a number
if isnumeric(src)
    % Specify the ID of the Ubertragung to be shown
    showID = src;
else
    % Show the first Ubertragung
    showID = 0;
end

%% %%%%%%%%%%%%%%%%%%% UPDATE RADIO BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source passed in to the function is actually a number, we need to
% show that UbertragungID, which means we need to retrieve the
% characteristics of the Ubertragung
if (updateLevel == 0) && (showID ~= 0)
    % Find the Ubertragung
    b = (Ubertragung.ID == showID);
    % Retrieve values only if we have a match
    if any(b)
        % Retrieve the Ubertragung-Typ
        switch Ubertragung.TypeID(b)
            case 1
                l.Value = 1;
            case 2
                t.Value = 1;
        end
    end
end

% Calculate the Ubertragung-TypeID
TypeID = l.Value*1 + t.Value*2;
pUbertragung.Data.TypeID = TypeID;

%% %%%%%%%%%%%%%%%% UPDATE THE UBERTRAGUNG DROPDOWN %%%%%%%%%%%%%%%%%%%%%%
% Update the UbertragungDD only if the source of the call has an updateLevel
% smaller than 2
if updateLevel < 2
    % Find Ubertragung that have the same TypeID as selected
    b = (Ubertragung.TypeID == TypeID);
    
    % Check if Knoten is visible, in which case we need to find which
    % Ubertragung are linked to the selected Knoten and restrict the choice
    % of Ubertragung to those ones
    if strcmpi(pKnoten.Visible,'on')
        % Find the Ubertragungen that are linked to the current Knoten
        bU = (Ubertragung.Knoten == pKnoten.Data.KnotenID);
        % Compound the boolean vector
        b = all([b,bU],2);
    end
    
    % Check if Bauteil is visible, in which case we need to find which
    % Ubertragung are linked to the selected Bauteil and restrict the
    % choice of Ubertragung to those ones
    if strcmpi(pBauteil.Visible,'on')
        % Find which Knoten have the current Bauteil in them
        bK = any(Knoten.Bauteile == pBauteil.Data.BauteilID,2);
        % Limit valid Ubertragungen to those which have a valid Knoten
        bU = ismember(Ubertragung.Knoten,Knoten.ID(bK));
        % Compound with the other boolean vector to further reduce the
        % list of valid Ubertragungen
        b = all([b,bU],2);
        % Continue sorting through the valid Ubertragungen which contain
        % the Bauteil in the path
        indx = find(b);
        [~,pos] = ismember(Ubertragung.Knoten(b),Knoten.ID);
        % Go through the valid Ubertragungen to check if their path really
        % contains the selected Bauteil
        for i=1:numel(indx)
            % Find the bauteile (1,2,3,4) in question
            loc = (Ubertragung.Bauteile(indx(i),:));
            % Find the corresponding Bauteile ID
            bauID = Knoten.Bauteile(pos(i),loc);
            % Find if it is a match with the selected BauteilID
            if ~any(bauID == pBauteil.Data.BauteilID)
               b(indx(i)) = false;
            end
        end
    end 
    
    % Check if we have found Ubertragungen that respect the properties
    if any(b)
        % Retrieve the Ubertragungen that respect the selected properties
        dd = Ubertragung.Dropdown(b);
        % Retrieve the indexes of the Ubertragungen shown in the dropdown menu
        pUbertragung.Data.Ubertragung = Ubertragung.ID(b);
    else
        % None dropdown
        [dd,pUbertragung.Data.Ubertragung] = addNone();
    end
    
    % Update the dropdown menu
    ddU.String = dd;
    % Reset the value anyway
    ddU.Value = 1;
    % Choose the Value of the dropdown so that we see the Ubertragung,
    % which ID we may have passed in
    if showID~=0
        % Check if we have a match
        b = (pUbertragung.Data.Ubertragung == showID);
        if any(b)
            ddU.Value = find(b);
        end
    end
end

% Find Ubertragung ID selected in the dropdown menu
ID = pUbertragung.Data.Ubertragung(ddU.Value);
pUbertragung.Data.UbertragungID = ID;

% Find a match
b = (Ubertragung.ID == ID);
bMatch = any(b);

%% %%%%%%%%%%%%%%%% UPDATE THE RAUME DD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the RaumeDD if the source has an updateLvel smaller than 3
if updateLevel < 3
    % Dropdown list
    dd = {'Raum 1','Raum 2','Raum 3','Raum 4'};
    if bMatch
        % Raume IDs
        rID = Ubertragung.Raume(b,:);
    else
        rID = [1,2];
    end
    
    % Update the dropdowns
    ddSR.String = dd;
    ddER.String = dd;
    ddSR.Value = rID(1);
    ddER.Value = rID(2);
end

%% %%%%%%%%%%%%%%%%%%%%% CHECK THE RAUME DD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that when the user selects a raum, it automatically selects a
% different raum for the other room
if updateLevel == 4
    % Check that start and endraum are not equal
    if ddSR.Value == ddER.Value
        % Initialize the vector of different possible values
        v = [1,2,3,4];
        
        % We have to find a new value for either the Endraum or the
        % Startraum
        if src == ddSR
            % Find new value for the Endraum
            v = v(v ~= ddSR.Value);
            ddER.Value = v(1);
        
        elseif src == ddER
            % Find new value for the Startraum
            v = v(v ~= ddER.Value);
            ddSR.Value = v(1);
        end
    end
end

%% %%%%%%%%%%%%%%%% UPDATE THE BAUTEILE DD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the BauteileDD if the source has an updateLevel is different than
% 3
if updateLevel ~= 3 
    % Initialize BauteilePresence and the Bauteile Position ID
    baut = true(1,4);
    bID = [1,1];
    % Find the corresponding Knoten and the Bauteile in the Knoten
    if bMatch
        % Find the Bauteile Position ID
        bID = Ubertragung.Bauteile(b,:);
        % Find the associated Knoten
        bK = (Knoten.ID == Ubertragung.Knoten(b));
        if any(bK)
            baut = (Knoten.Bauteile(bK,:) > 0);
        end
    end
    
    % Prepare possible dropdowns
    dd = {'Bauteil 1','Bauteil 2','Bauteil 3','Bauteil 4'};
    % Mapping of room to possible Bauteil
    RtoB = [1,2; 2,3; 3,4; 1,4];
    
    % Find out which bauteile to display
    sbID = RtoB(ddSR.Value,:);
    ebID = RtoB(ddER.Value,:);
    sbID = sbID(baut(sbID));
    ebID = ebID(baut(ebID));
    
    % Update the Startbauteil Dropdown
    if ~isempty(sbID)
        % Actualize the string and the global variable
        ddSB.String = dd(sbID);
        pUbertragung.Data.Startbauteil = sbID;
        % Actualize the value
        bv = find(sbID == bID(1));
        if ~isempty(bv)
            ddSB.Value = bv;
        else
            ddSB.Value = 1;
        end
    else
        % None string
        [ddSB.String,pUbertragung.Data.Startbauteil] = addNone();
    end
    
    % Update the Endbauteil Dropdown
    if ~isempty(ebID)
        % Actualize the string and the global variable
        ddEB.String = dd(ebID);
        pUbertragung.Data.Endbauteil = ebID;
        % Actualize the value
        bv = find(ebID == bID(2));
        if ~isempty(bv)
            ddEB.Value = bv;
        else
            ddEB.Value = 1;
        end
    else
        % None string
        [ddEB.String,pUbertragung.Data.Endbauteil] = addNone();
    end
end

%% %%%%%%%%%%%% CREATE THE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define x-values for table and for plotting
x = [50,63,80,100,125,160,200,250,315,400,500,630,...
     800,1000,1250,1600,2000,2500,3150,4000,5000];
% Create the table only if the updateLevel is smaller than 3
if updateLevel < 3
    % Retrieve the y-values
    y = zeros(size(x));
    if bMatch
        y = Ubertragung.Values(b,:);
    end
    
    % Prepare the data for the table
    tblData = cell(numel(x),2);
    % Prepare the label column
    for i=1:numel(x)
        tblData{i,1} = x(i);
        tblData{i,2} = y(i);
    end
    
    % Prepare a global variable for storage of y-values
    pUbertragung.Data.Values = y;

    % Plot the table
    if TypeID == 1
        tb.ColumnName = {'Freq|[Hz]','R|[dB]'};
    else
        tb.ColumnName = {'Freq|[Hz]','L|[dB]'};
    end
    tb.ColumnWidth = {50, 80};
    tb.Data = tblData;
    tb.ColumnEditable = [false, true];
end

%% %%%%%%%%%%%%%% UPDATE INFORMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the information only if the updateLevel is lower than 3
if updateLevel < 3
    if any(b)
        % Retrieve the Flache
        a = Ubertragung.Area(b);
        pUbertragung.Data.Area = a;
        fl.String = num2str(a,'%.2f');
        % Retrieve the Length
        l = Ubertragung.Length(b);
        pUbertragung.Data.Length = l;
        la.String = num2str(l,'%.2f');
        % Original Database ID
        oDB.String = Ubertragung.OriginalIDstr{b,1};
        pUbertragung.Data.OriginalID = oDB.String;
    else
        pUbertragung.Data.Area = 0;
        fl.String = '-';
        pUbertragung.Data.Length = 0;
        la.String = '-';
        pUbertragung.Data.OriginalID = '-';
        oDB.String = '-';
    end
end

%% %%%%%%%%%%%%%% EDIT THE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if updateLevel == 3
    % Find out which cells have been modified
    pos = callbackdata.Indices;
    % Get the inputted value
    val = callbackdata.NewData;
    % Check that the value is numeric
    if isnumeric(val)
        src.Data{pos(1),pos(2)} = val;
    else
        % Keep old data in the table
        src.Data{pos(1),pos(2)} = callbackdata.PreviousData;
        % Throw error
        errordlg('Please input a valid number in the cell',...
                 'Input error','modal');
        return;
    end
    
    % Modify the value in the global structure
    pUbertragung.Data.Values(pos(1)) = val;
end

%% %%%%%%%%%%%% UPDATE THE UBERTRAGUNG PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Ubertragung plot only if the source has an update level
% lower than 4
if updateLevel < 4
    % First delete the children
    delete(ax.Children(:));
    % Hold the axes
    hold(ax,'on');
    plot(ax,x,pUbertragung.Data.Values,'LineStyle','-','Color','b');
    % Update ticks and labels
    ax.XTick = x(2:3:end);
    ax.XLim = [min(x),max(x)];
    if TypeID==1
        ax.YLabel.String = 'R [dB]';
    else
        ax.YLabel.String = 'L [dB]';
    end
    % Put the grid and the correct scale
    ax.XGrid = 'on';
    ax.YGrid = 'on';
    ax.XScale = 'log';
    hold(ax,'off');

    % Update the position
    op = pUbertragung.Data.AxesPosition;
    ti = ax.TightInset;
    ax.Position = [op(1)+ti(1),op(2)+ti(2),...
                   op(3)-ti(1)-ti(3),op(4)-ti(2)-ti(4)];
end

end