function pBauteil_Build()
% Function defines the elements in the pBauteil window 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and panels
global fParking
global pBauteil

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 10*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
radio = [100,20];
drop = [150,20];
text = [200,20];
textKur = [200,35];
textDet = [200,60];
list = [150,60];
button = [150,20];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = label(1) + ... 
            max([2*radio(1)+2*intra(1),...
                 drop(1)+intra(1),...
                 text(1)+intra(1),...
                 textKur(1)+intra(1),...
                 textDet(1)+intra(1),...
                 list(1)+intra(1),...
                 2*button(1)+2*intra(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = max([7*label(2),...
                  2*list(2)+textDet(2)+textKur(2)+text(2)+drop(2)+radio(2)])+...
                  7*intra(2) + button(2)+ marg(2) + marg(4);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the panel position
pos = [1,1,min_width,min_height];

% Create the new panel (in the parking figure)
pBauteil = uipanel('Parent',fParking,...
                   'Units','pixels',...'BorderType','none',...
                   'Position',pos,...
                   'Visible','off');
               
% Add a data structure to the panel
addprop(pBauteil,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Bauteil-Typ','Bauteil-Name','Name','Kurzbeschreibung',...
           'Det. Beschreibung','Knoten','Vorsatzschalen'};
tags = {'BauteilTyp','BauteilName','Name','KurzBeschr',...
        'DetBeschr','Knoten','Vorsatz'};
lbl_tag = strcat(tags,'LBL');
% Create the label elements iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',pBauteil,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%%%%%% EDIT TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define text fields with modifiable content
tags = {'BauteilName','KurzBeschreibung','DetBeschreibung'};
txt_tag = strcat(tags,'TXT');
txt_max = [1,2,2];
for i=1:numel(txt_tag)
    uicontrol('Parent',pBauteil,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Min',0,'Max',txt_max(i),...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the first group of radio buttons
rad_txt = {'Wand','Decke'};
tags = {'Wand','Decke'};
rad_tag = strcat(tags,'RAD');
% Create the radio button group
bauteilTyp = uibuttongroup('Parent',pBauteil,'Units','pixels',...
                            'Visible','on','BorderType','none',...
                            'Tag','BauteilTypRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_tag)
    uicontrol('Parent',bauteilTyp,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@pBauteil_Populate,...
              'Tag',rad_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%% LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to display in the lists
tags = {'Knoten','Vorsatz'};
lst_tag = strcat(tags,'LST');
for i=1:numel(lst_tag)
    uicontrol('Parent',pBauteil,'Style','listbox','Min',0,'Max',1,...
              'Visible','on','Units','pixels',...
              'String',' ','Tag',lst_tag{i});
end
              
%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the dropdowns
tags = {'Bauteil'};
dd_tag = strcat(tags,'DD');
for i=1:numel(dd_tag)
    uicontrol('Parent',pBauteil,'Style','popupmenu',...
               'Visible','on','Units','pixels',...
               'Callback',@pBauteil_Populate,...
               'String',' ','Tag',dd_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display
but_txt = {'Delete','Save'};
tags = {'Delete','Modify'};
but_tag = strcat(tags,'BUT');
but_fcn = {'exportBauteil','exportBauteil'}; 
but_fcn = strcat('@',but_fcn);
for i=1:numel(but_tag)
    uicontrol('Parent',pBauteil,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 3; 
nV = 8;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = label(1);
h2 = round(max([radio(1),...
               (drop(1)-intra(1))/2,...
               (text(1)-intra(1))/2,...
               (textKur(1)-intra(1))/2,...
               (textDet(1)-intra(1))/2,...
               (list(1)-intra(1))/2,...
                button(1)]));
shareH = [h1, h2, h2]/(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));
    
d = max(label(2),drop(2));
t = max(label(2),text(2));
tK = max(label(2),textKur(2));
tD = max(label(2),textDet(2));
l = max(label(2),list(2));
r = max(label(2),radio(2));
shareV = [button(2), l, l, tD, tK, t, d, r]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have pBauteil as Parent
obj = pBauteil.Children;

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
for j=0:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j+1});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 6;
for j=0:2
    txt = findobj(obj,'Tag',txt_tag{j+1});
    txt.Position = BoundingBox(boxes,[2,nH],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%%% PLACE LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 3;
for j=0:1
    lst = findobj(obj,'Tag',lst_tag{j+1});
    lst.Position = BoundingBox(boxes,[2,nH],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV-1;
for j=0:0
    dd = findobj(obj,'Tag',dd_tag{j+1});
    dd.Position = BoundingBox(boxes,[2,nH],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Division of radio groups
rSub = 2;

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,nH],nV,nH);
% Place the group
bauteilTyp.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
for j=0:1
    % Retrieve the radios and place it correctly
    rad = findobj(bauteilTyp.Children,'Tag',rad_tag{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,rSub);
end

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:2
    but = findobj(obj,'Tag',but_tag{j});
    but.Position = BoundingBox(boxes,j+1,1,nH);
end