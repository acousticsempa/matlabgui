function pVUbertragung_Build()

% Function defines the elements in the pVUbertragung panel

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and panels
global fParking
global pVUbertragung

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the panel border
marg = 10*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [135,20];
radio = [80,20];
check = [80,20];
drop = [100,20];
text = [200,20];
button = [150,20];
table = [150,0];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = label(1) + ... 
            max([2*radio(1)+2*intra(1),...
                 3*radio(1)+3*intra(1),...
                 drop(1)+intra(1),...
                 3*check(1)+3*intra(1),...
                 text(1)+intra(1),...
                 2*button(1)+2*intra(1)]) + ...
             + table(1) + intra(1) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = max([5*label(2),...
                  text(2)+check(2)+drop(2)+2*radio(2)])+...
                  6*intra(2) +button(2)+ marg(2) + marg(4);
          
% Account for the axes to be displayed at the end
WidthToHeight = 1.4;
% Retrieve axes dimensions through proportions
axesDim = [WidthToHeight,1]*(min_height-marg(2)-marg(4));
% Augment the width with the corresponding image width
min_width = min_width + axesDim(1) + intra(1);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the panel position
pos = [1,1,min_width,min_height];

% Create a new full screen panel
pVUbertragung = uipanel('Parent',fParking,...
                        'Units','pixels',...'BorderType','none',...
                        'Position',pos,...
                        'Visible','off');
                    
% Add property to panel for storage of dropdowns, values, etc
addprop(pVUbertragung,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Strahlungstyp','Anisotropie','Vorsatz-Ubertragung',...
           'Apply to Anisotropie','Original DB-ID'};
tags = {'Stahlungstyp','Anisotropie','VUbertragung',...
        'ApplyAnisotropie','OriginalDBID'};
lbl_tag = strcat(tags,'LBL');
for i=1:numel(lbl_txt)
    uicontrol('Parent',pVUbertragung,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the Strahlungtyp group of radio buttons
rad_txt = {'Luft','Tritt'};
tags = {'Luft','Tritt'};
tip = {sprintf('Luftschall-Anregung\nLuftschall-Abstrahlung\nTrittschall-Abstrahlung'),...
               'Trittschall-Anregung'};
rad_tag1 = strcat(tags,'RAD');
strahlung = uibuttongroup('Parent',pVUbertragung,'Units','pixels',...
                          'Visible','on','BorderType','none',...
                          'Tag','StrahlungRAD');
for i=1:numel(rad_tag1)
    uicontrol('Parent',strahlung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'TooltipString',tip{i},...
              'Callback',@pVUbertragung_Populate,...
              'Tag',rad_tag1{i});
end

% Define the text to be displayed in the Anisotropietyp group of radio buttons
rad_txt = {'Parallel','Senkrecht','Direkt'};
tags = {'AnisoParall','AnisoSenk','AnisoDir'};
tip = {sprintf('G�ltig f�r Weg parallel\nzur Anisotropierichtung'),...
       sprintf('G�ltig f�r Weg senkrecht\nzur Anisotropierichtung'),...
               'G�ltig f�r Direktdurchgang'};
rad_tag2 = strcat(tags,'RAD');
anisotropie = uibuttongroup('Parent',pVUbertragung,'Units','pixels',...
                            'Visible','on','BorderType','none',...
                            'Tag','AnisotropieRAD');
for i=1:numel(rad_tag2)
    uicontrol('Parent',anisotropie,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'TooltipString',tip{i},...
              'Callback',@pVUbertragung_Populate,...
              'Tag',rad_tag2{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the check buttons
che_txt = {'Parallel','Senkrecht','Direkt'};
tags = {'AnisoParall','AnisoSenk','AnisoDir'};
tip = {sprintf('G�ltig f�r Weg parallel\nzur Anisotropierichtung'),...
       sprintf('G�ltig f�r Weg senkrecht\nzur Anisotropierichtung'),...
               'G�ltig f�r Direktdurchgang'};
che_tag = strcat(tags,'CHE');
for i=1:numel(che_tag)
    uicontrol('Parent',pVUbertragung,'Visible','on','Units','pixels',...
              'Style','checkbox','String',che_txt{i},...
              'TooltipString',tip{i},...
              'Tag',che_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the dropdown
tags = {'VUbertragung'};
dd_tag = strcat(tags,'DD');
for i=1:numel(dd_tag)
    uicontrol('Parent',pVUbertragung,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@pVUbertragung_Populate,...
              'String',' ','Tag',dd_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed
tags = {'OriginalDB'};
txt_tag = strcat(tags,'TXT');
for i=1:numel(txt_tag)
    uicontrol('Parent',pVUbertragung,'Style','text',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb = uitable('Parent',pVUbertragung,'Visible','on',...
             'RowName',[],'ColumnName',[],'RowStriping','on',...
             'CellEditCallback',@pVUbertragung_Populate,...
             'Units','pixels','Tag','RLWerteTABLE');

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display
but_txt = {'Delete','Save'};
tags = {'Delete','Modify'};
but_tag = strcat(tags,'BUT');
but_fcn = {'exportVUbertragung','exportVUbertragung'};
but_fcn = strcat('@',but_fcn);
for i=1:numel(but_txt)
    uicontrol('Parent',pVUbertragung,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% AXES FOR PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the axes for plot
ax = axes('Parent',pVUbertragung,'Units','pixels','Visible','on',...
          'Tag','RLWerteAX');
% Define the label (at least x-axis)
ax.XLabel.String = 'Frequenz f [Hz]';

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 9; 
nV = 6;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = label(1);
h2 = round(max([(radio(1)-2*intra(1))/3,...
                (radio(1)-intra(1))/2,...
                (drop(1)-5*intra(1))/6,...
                (check(1)-intra(1))/2,...
                (text(1)-5*intra(1))/6,...
                (button(1)-2*intra(1))/3]));
shareH = [h1, h2*ones(1,6), table(1), axesDim(1)]...
            /(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));

l = label(2);
t = max(l,text(2));
d = max(l,drop(2));
c = max(l,check(2));
r = max(l,radio(2));
shareV = [button(2), t, c, d, r, r]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have pVUbertragung as Parent
obj = findobj('Parent',pVUbertragung);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV+1;
for j=1:nV-1
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the subdivision of the radio groups
rSub = 3;

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,nH-2],nV,nH);
% Place the group
strahlung.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
for j=1:2
    % Retrieve the radios and place it correctly
    rad = findobj(strahlung.Children,'Tag',rad_tag1{j});
    rad.Position = BoundingBox(subboxes,j,1,rSub);
end

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,nH-2],nV-1,nH);
% Place the group
anisotropie.Position = box;
for j=1:rSub
    % Retrieve the radios and place it correctly
    rad = findobj(anisotropie.Children,'Tag',rad_tag2{j});
    rad.Position = BoundingBox(subboxes,j,1,rSub);
end

%% %%%%%%%%%%%%%%%%%%% PLACE CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV + 1;
for j=1:3
    % Retrieve the checkboxes and place tehm correctly
    che = findobj(obj,'Tag',che_tag{j});
    che.Position = BoundingBox(boxes,[2,3]+(j-1)*2,i-4,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV + 1;
% Place Vorsatz DD
dd = findobj(obj,'Tag','VUbertragungDD');
dd.Position = BoundingBox(boxes,[2,nH-2],i-3,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 2;
for j=1:1
    % Retrieve the text element and place it
    txt = findobj(obj,'Tag',txt_tag{j});
    txt.Position = BoundingBox(boxes,[2,nH-2],i,nH);
end

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','DeleteBUT');
but.Position = BoundingBox(boxes,[2,4],1,nH);
but = findobj(obj,'Tag','ModifyBUT');
but.Position = BoundingBox(boxes,[5,7],1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb.Position = BoundingBox(boxes,nH-1,[1,nV],nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define outerposition of axes
AxesPosition = BoundingBox(boxes,nH,[1,nV],nH);
ax.OuterPosition = AxesPosition;

% Store axes position to the data structure
pVUbertragung.Data.AxesPosition = AxesPosition;