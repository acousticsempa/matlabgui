function pVUbertragung_Populate(src,callbackdata)

% Function takes care of populating the VUbertragung panel with the correct
% data

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Panels
global pVUbertragung
global pVorsatz
% Data
global VUbertragung

%% %%%%%% RETRIEVE THE OBJECTS IN THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the objects which have as parent the panel
obj = pVUbertragung.Children;

% StrahlungTyp: Luft / Tritt
gr = findobj(obj,'Tag','StrahlungRAD');
l = findobj(gr.Children,'Tag','LuftRAD');
t = findobj(gr.Children,'Tag','TrittRAD');

% Anisotropie Typ: Parallel, Senkrecht, direkt
gr = findobj(obj,'Tag','AnisotropieRAD');
p = findobj(gr.Children,'Tag','AnisoParallRAD');
s = findobj(gr.Children,'Tag','AnisoSenkRAD');
d = findobj(gr.Children,'Tag','AnisoDirRAD');

% Check-boxes
pC = findobj(obj,'Tag','AnisoParallCHE');
sC = findobj(obj,'Tag','AnisoSenkCHE');
dC = findobj(obj,'Tag','AnisoDirCHE');

% Dropdown menus
ddVU = findobj(obj,'Tag','VUbertragungDD');

% Text fields
tODB = findobj(obj,'Tag','OriginalDBTXT');

% Table
tb = findobj(obj,'Tag','RLWerteTABLE');

% Axes
ax = findobj(obj,'Tag','RLWerteAX');

%% %%%%%%%%%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
elem =  [l,t,p,s,d, ddVU, tb];
upLev = [1,1,1,1,1,    2,  3];

% Find which element called this function
b = (src == elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

% Check if the src is actually a number
if isnumeric(src)
    % Specify the ID of the VUbertragung to be shown
    showID = src;
else
    % Show the first VUbertragung
    showID = 0;
end

%% %%%%%%%%%%%%%%%%%%% UPDATE RADIO BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source passed in to the function is actually a number, we need to
% show that VUbertragungID, which means we need to retrieve the
% characteristics of the VUbertragung
if (updateLevel == 0) && (showID ~= 0)
    % Find the VUbertragung
    b = (VUbertragung.ID == showID);
    % Retrieve values only if we have a match
    if any(b)
        % Retrieve the Strahlung-Typ
        switch VUbertragung.TypeID(b)
            case 1
                l.Value = 1;
            case 2
                t.Value = 1;
        end
        % Retrieve the Anisotropie-Typ
        switch VUbertragung.Anisotropie(b)
            case 1
                p.Value = 1;
            case 2
                s.Value = 1;
            case 3
                d.Value = 1;
        end
    end
end

% Calculate the Ubertragung-TypeID
TypeID = l.Value*1 + t.Value*2;
Aniso = p.Value*1 + s.Value*2 + d.Value*3;

% Store to panel data structure
pVUbertragung.Data.TypeID = TypeID;
pVUbertragung.Data.Anisotropie = Aniso;

%% %%%%%%%%%%%%%%%% UPDATE THE VUBERTRAGUNG DROPDOWN %%%%%%%%%%%%%%%%%%%%%%
% Update the VUbertragungDD only if the source of the call has an updateLevel
% smaller than 2
if updateLevel < 2
    % Find VUbertragung that have the same TypeID and Anisotropie as selected
    bT = (VUbertragung.TypeID == TypeID);
    bA = (VUbertragung.Anisotropie == Aniso);
    % Compound the boolean vector
    b = all([bT,bA],2);
    
    % Check if Vorsatz is visible, in which case we need to find which 
    % VUbertragungen are linked to the selected Vorsatz and restrict the 
    % choice of VUbertragungen to those ones
    if strcmpi(pVorsatz.Visible,'on')
        % Find VUbertragungen that have as Vorsatz the selected Vorsatz
        bVU = (VUbertragung.Vorsatz == pVorsatz.Data.VorsatzID);
        % Compound the boolean vector
        b = all([b,bVU],2);
    end
        
    % Check if we have found VUbertragungen that respect the properties
    if any(b)
        % Retrieve the VUbertragungen that respect the selected properties
        dd = VUbertragung.Dropdown(b);
        % Retrieve the indexes of the VUbertragungen shown in the dropdown menu
        pVUbertragung.Data.VUbertragung = VUbertragung.ID(b);
    else
        % None dropdown
        [dd,pVUbertragung.Data.VUbertragung] = addNone();
    end
    
    % Update the dropdown menu
    ddVU.String = dd;
    % Reset the value anyway
    ddVU.Value = 1;
    % Choose the Value of the dropdown so that we see the VUbertragung,
    % which ID we may have passed in
    if showID~=0
        % Check if we have a match
        b = (pVUbertragung.Data.VUbertragung == showID);
        if any(b)
            ddVU.Value = find(b);
        end
    end
end

% Find VUbertragung ID selected in the dropdown menu
ID = pVUbertragung.Data.VUbertragung(ddVU.Value);
pVUbertragung.Data.VUbertragungID = ID;

% Find a match
b = (VUbertragung.ID == ID);
bMatch = any(b);

%% %%%%%%%%%%%%%%%% UPDATE THE CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the checkboxes if the updateLevel is smaller than 3
if updateLevel < 3
    % Set all the checkboxes to empty
    pC.Value = 0;
    sC.Value = 0;
    dC.Value = 0;
    
    % Check if we have a valid match
    if bMatch
        % Switch the cases
        switch Aniso
            case 1
                pC.Value = 1;
            case 2
                sC.Value = 1;
            case 3
                dC.Value = 1;
        end
    end
end

%% %%%%%%%%%%%%%%%%%% CREATE THE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define x-values for table and for plotting
x = [50,63,80,100,125,160,200,250,315,400,500,630,...
     800,1000,1250,1600,2000,2500,3150,4000,5000];
 
% Create the table only if the updateLevel is smaller than 3
if updateLevel < 3
    % Retrieve the y-values
    y = zeros(size(x));
    if bMatch
        y = VUbertragung.Values(b,:);
    end
    
    % Prepare the data for the table
    tblData = cell(numel(x),2);
    % Prepare the label column
    for i=1:numel(x)
        tblData{i,1} = x(i);
        tblData{i,2} = y(i);
    end
    
    % Prepare a global variable for storage of y-values
    pVUbertragung.Data.Values = y;

    % Plot the table
    if TypeID == 1
        tb.ColumnName = {'Freq|[Hz]','dR|[dB]'};
    else
        tb.ColumnName = {'Freq|[Hz]','dL|[dB]'};
    end
    tb.ColumnWidth = {50, 80};
    tb.Data = tblData;
    tb.ColumnEditable = [false, true];
end

%% %%%%%%%%%%%%%% UPDATE INFORMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the information only if the updateLevel is lower than 3
if updateLevel < 3
    if bMatch
        % Retrieve the original DB-ID
        str = VUbertragung.OriginalIDstr{b,1};
        tODB.String = str;
        pVUbertragung.Data.OriginalDBID = str;
    else
        tODB.String = '-';
    end
end

%% %%%%%%%%%%%%%% EDIT THE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if updateLevel == 3
    % Find out which cells have been modified
    pos = callbackdata.Indices;
    % Get the inputted value
    val = callbackdata.NewData;
    % Check that the value is numeric
    if isnumeric(val)
        src.Data{pos(1),pos(2)} = val;
    else
        % Keep old data in the table
        src.Data{pos(1),pos(2)} = callbackdata.PreviousData;
        % Throw error
        errordlg('Please input a valid number in the cell',...
                 'Input error','modal');
        return;
    end
    
    % Modify the value in the global structure
    pVUbertragung.Data.Values(pos(1)) = val;
end

%% %%%%%%%%%%%% UPDATE THE VUBERTRAGUNG PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the VUbertragung plot only if the source has an update level
% lower than 4
if updateLevel < 4
    % First delete the children
    delete(ax.Children(:));
    % Hold the axes
    hold(ax,'on');
    plot(ax,x,pVUbertragung.Data.Values,'LineStyle','-','Color','b');
    % Update ticks and labels
    ax.XTick = x(2:3:end);
    ax.XLim = [min(x),max(x)];
    if TypeID==1
        ax.YLabel.String = 'dR [dB]';
    else
        ax.YLabel.String = 'dL [dB]';
    end
    % Put the grid and the correct scale
    ax.XGrid = 'on';
    ax.YGrid = 'on';
    ax.XScale = 'log';
    hold(ax,'off');

    % Update the position
    op = pVUbertragung.Data.AxesPosition;
    ti = ax.TightInset;
    ax.Position = [op(1)+ti(1),op(2)+ti(2),...
                   op(3)-ti(1)-ti(3),op(4)-ti(2)-ti(4)];
end

end