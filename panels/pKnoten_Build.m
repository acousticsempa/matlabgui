function pKnoten_Build()

% Function creates the panel for the viewing, modifying or deleting a
% Knoten element, which will then be put in the figure for viewing multiple
% things at once (Knoten, Bauteile, Ubertragungen, Vorsatzschalen and
% Vorsatzubertragungen)

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and panels
global fParking
global pKnoten
% Graphics arrays
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 10*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
radio = [100,20];
drop = [150,20];
text = [200,20];
textKur = [200,35];
textDet = [200,60];
button = [150,20];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = max(label(1),button(1)) + ... 
            max([3*radio(1)+3*intra(1),...
                 text(1)+intra(1),...
                 textKur(1)+intra(1),...
                 textDet(1)+intra(1),...
                 2*drop(1)+2*intra(1),...
                 2*button(1)+intra(1)]) + ...
            + marg(1) + marg(3);
% Minimum height
d = max(label(2),drop(2));
t = max(label(2),text(2));
tK = max(label(2),textKur(2));
tD = max(label(2),textDet(2));
r = max(label(2),radio(2));
min_height = 5*d + t + tK + tD + 2*r + button(2) +...
             10*intra(2) + marg(2) + marg(4);
              
% Account for the axes to be displayed at the end
WidthToHeight = 0.8;
% Retrieve axes dimensions through proportions
axesDim = [WidthToHeight,1]*(min_height-marg(2)-marg(4));
% Augment the width with the corresponding axes width
min_width = min_width + axesDim(1) + intra(1);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize a figure for keeping panels
fParking = figure('Name','Viewer','NumberTitle','off',...
                  'ToolBar','none','MenuBar','none',...
                  'Units','pixels','Resize','off',...
                  'Position',[1,1,100,100],...
                  'CloseRequestFcn',@closeFigure,...
                  'Visible','off');
% Add this figure to the figure handle array
h_fig = [h_fig,fParking];
   
% Calculate the panel position
pos = [1,1,min_width,min_height];

% Create the new panel (in the parking figure)
pKnoten = uipanel('Parent',fParking,...
                  'Units','pixels',...'BorderType','none',...
                  'Position',pos,...
                  'Visible','off');

% Add a data strucutre to the panel
addprop(pKnoten,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Knoten-Typ','Ausrichtung','Knoten-Namen','Name',...
           'Kurzbeschreibung','Det. Beschreibung',...
           'Bauteil 1','Bauteil 2','Bauteil 3','Bauteil 4'};
tags = {'KnotenTyp','Ausrichtung','KnotenNamen','Name',...
        'KurzBeschreibung','DetBeschreibung',...
        'Bauteil1','Bauteil2','Bauteil3','Bauteil4'};
lbl_tag = strcat(tags,'LBL');
% Create the label elements iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',pKnoten,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% EDIT TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define text field with modifiable content
tags = {'KnotenName','KurzBeschreibung','DetBeschreibung'};
txt_tag = strcat(tags,'TXT');
txt_max = [1,2,2];
for i=1:numel(txt_tag)
    uicontrol('Parent',pKnoten,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Min',0,'Max',txt_max(i),...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the first group of radio buttons
rad_txt = {'Kreuzstoss','T-Stoss','Anders'};
tags = {'Kreuzstoss','TStoss','Anders'};
rad_tag1 = strcat(tags,'RAD');
% Create the radio button group
knotenTyp = uibuttongroup('Parent',pKnoten,'Units','pixels',...
                           'Visible','on','BorderType','none',...
                           'Tag','KnotenTypRAD');
for i=1:numel(rad_tag1)
    uicontrol('Parent',knotenTyp,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@pKnoten_Populate,...
              'Tag',rad_tag1{i});
end

% Define the text to be displayed in the second group of radio buttons
rad_txt = {'Vertikal','Horizontal'};
tags = {'Vertikal','Horizontal'};
rad_tag2 = strcat(tags,'RAD');
% Create the radio button group
Ausrichtung = uibuttongroup('Parent',pKnoten,'Units','pixels',...
                            'Visible','on','BorderType','none',...
                            'Tag','AusrichtungRAD');
for i=1:numel(rad_tag2)
    uicontrol('Parent',Ausrichtung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@pKnoten_Populate,...
              'Tag',rad_tag2{i});
end
                    
%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the first dropdown
tags = 'Knoten';
dd_tag1 = strcat(tags,'DD');
uicontrol('Parent',pKnoten,'Style','popupmenu',...
          'Visible','on','Units','pixels',...
          'Callback',@pKnoten_Populate,...
          'String',' ','Tag',dd_tag1);
      
% Define which text needs to be displayed in the second set of dropdowns
tags = {'Bauteil1','Bauteil2','Bauteil3','Bauteil4'};
dd_tag2 = strcat(tags,'DD');
for i=1:numel(dd_tag2)
    uicontrol('Parent',pKnoten,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@pKnoten_Populate,...
              'String',' ','Tag',dd_tag2{i});
end

% Define which text needs to be displayed in the third set of dropdowns
txt = {'Keine Anisotropie','Anisotropie parallel zur Knotenrichtung',...
       'Anisotropie senkrecht zur Knotenrichtung'};
tags = {'Anisotropie1','Anisotropie2','Anisotropie3','Anisotropie4'};
dd_tag3 = strcat(tags,'DD');
for i=1:numel(dd_tag3)
    uicontrol('Parent',pKnoten,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@pKnoten_Populate,...
              'String',txt,'Tag',dd_tag3{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
but_txt = {'Delete','Save'};
tags = {'Delete','Modify'};
but_tag = strcat(tags,'BUT');
but_fcn = {'exportKnoten','exportKnoten'}; 
but_fcn = strcat('@',but_fcn);
for i=1:numel(but_txt)
    uicontrol('Parent',pKnoten,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% AXES FOR PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the axes for plot
ax = axes('Parent',pKnoten,'Units','pixels','Visible','on',...
          'Tag','KnotenPreviewAX',...
          'Color',0.94*ones(1,3));
% Disable the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 8; 
nV = 11;

% Calculate pos relative to window
pos(1:2) = 0;

% Calculate shares of each element of the GUI to the total share
h1 = max(label(1),button(1));
h2 = round(max([(radio(1)-intra(1))/2,...
                (drop(1)-2*intra(1))/3,...
                (text(1)-5*intra(1))/6,...
                (textKur(1) -5*intra(1))/6,...
                (textDet(1)-5*intra(1))/6,...
                (button(1)-2*intra(1))/3]));
shareH = [h1, h2*ones(1,6), axesDim(1)]...
            /(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));
shareV = [button(2), d, d, d, d, tD, tK, t, d, r, r]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have pKnoten as Parent
obj = pKnoten.Children;

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
for j=0:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j+1});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE EDIT TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 8;
for j=0:2
    txt = findobj(obj,'Tag',txt_tag{j+1});
    txt.Position = BoundingBox(boxes,[2,7],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Place first dropdown
i = nV-2;
dd = findobj(obj,'Tag',dd_tag1);
dd.Position = BoundingBox(boxes,[2,7],i,nH);

% Place second and third group of dropdowns
i = 5;
for j=0:3
    dd = findobj(obj,'Tag',dd_tag2{j+1});
    dd.Position = BoundingBox(boxes,[2,4],i-j,nH);
    dd = findobj(obj,'Tag',dd_tag3{j+1});
    dd.Position = BoundingBox(boxes,[5,7],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subdivision in groups
rSub = 3;

% Get the box where the radio button will be placed and place the group
box = BoundingBox(boxes,[2,7],nV,nH);
knotenTyp.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
for j=0:2
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',knotenTyp,'Tag',rad_tag1{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,rSub);
end

% Get the box where the radio button will be placed and place the group
box = BoundingBox(boxes,[2,7],nV-1,nH);
Ausrichtung.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
for j=0:1
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',Ausrichtung,'Tag',rad_tag2{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,rSub);
end

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag',but_tag{1});
but.Position = BoundingBox(boxes,[2,4],1,nH);
but = findobj(obj,'Tag',but_tag{2});
but.Position = BoundingBox(boxes,[5,7],1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Position the axes
ax.Position = BoundingBox(boxes,nH,[1,nV],nH);

end