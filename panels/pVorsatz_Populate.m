function pVorsatz_Populate(src,callbackdata)

% Function populates the text fields in the view vorsatz window

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global pVorsatz
global pBauteil
% Data
global Bauteile
global Vorsatz

%% %%%%%%%%%%%%%%% FIND OBJECTS IN FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = pVorsatz.Children;

% Get the dropdown
vDD = findobj(obj,'Tag','VorsatzDD');
bDD = findobj(obj,'Tag','BauteilDD');

% Get the texts
tN = findobj(obj,'Tag','VorsatzNameTXT');
tK = findobj(obj,'Tag','KurzBeschreibungTXT');
tD = findobj(obj,'Tag','DetBeschreibungTXT');

% Get lists
lst = findobj(obj,'Tag','BauteileLST');

% Get button
but = findobj(obj,'Tag','AddBauteilBUT');

%% %%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
elem = [vDD, bDD, but, lst];
upLev = [ 1,   2,   3,   4];

% Find which element called this function
b = (src == elem);
% Retrieve the updateLevel 
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    updateLevel = 0;
end

% Check if the src is actually a number
if isnumeric(src)
    % Specify the ID of the Vorsatz to be shown
    showID = src;
else
    % Show the first Vorsatz
    showID = 0;
end

%% %%%%%%%%%%%%%%%%% VORSATZ DROPDOWN UPDATE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The vorsatz dropdown gets updated when the caller of the function has an
% updateLevel smaller than 1
if updateLevel < 1
    % Find valid vorsatze
    b = (Vorsatz.ID ~= 0);
    
    % Check if Bauteil is visible, in which case we need to find which 
    % Vorsatz are linked to the selected Bauteil and restrict the 
    % choice of Vorsatz to those ones
    if strcmpi(pBauteil.Visible,'on')
        % Find Vorsatz that are applicable to the selected Bauteil
        bV = any(Vorsatz.Bauteile == pBauteil.Data.BauteilID,2);
        % Compound the boolean vector
        b = all([b,bV],2);
    end
    
    % Check if we have a match
    if any(b)
        % Retrieve a list of the Vorsatz
        dd = Vorsatz.Dropdown(b);
        % Retrieve the IDs of the Vorsatz shown
        pVorsatz.Data.Vorsatz = Vorsatz.ID(b);
    else
        [dd,pVorsatz.Data.Vorsatz] = addNone();
    end
    
    % Update the dropdown and update the value
    vDD.String = dd;
    vDD.Value = 1;
    % Choose the value of the dropdown so that we see the Vorsatz, which ID
    % we may have passed in the function
    if showID~=0
        % Check if we have a match
        b = (pVorsatz.Data.Vorsatz == showID);
        % If we have a match
        if any(b)
            vDD.Value = find(b);
        end
    end
end

% Find Vorsatz ID selected in the dropdown menu
pVorsatz.Data.VorsatzID = pVorsatz.Data.Vorsatz(vDD.Value);
ID = pVorsatz.Data.VorsatzID;

%% %%%%%%%%%%%%%%%%%%%% BAUTEILE DROPDOWN UPDATE %%%%%%%%%%%%%%%%%%%%%%%%%
% The bauteile dropdown gets updated when the caller of the function has an
% updateLevel smaller than 1
if updateLevel < 1
    % Find all Bauteile
    b = (Bauteile.ID ~= 0);
    % Check if we have a match
    if any(b)
        % Retrieve a list of Bauteile
        dd = Bauteile.Dropdown(b);
        % Retrieve the IDs of the Bauteile shown
        pVorsatz.Data.Bauteile = Bauteile.ID(b);
    else
        [dd, pVorsatz.Data.Bauteile] = addNone();
    end

    % Update the dropdown and update the value
    bDD.String = dd;
    bDD.Value = min(numel(dd),bDD.Value);
end

%% %%%%%%%%%%%%%%%%%%%% VORSATZ PROPERTIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve Vorsatz properties if the updateLevel is smaller than 2
if updateLevel < 2
    % Match Vorsatz ID in the database
    b = (Vorsatz.ID == ID);
    % Check if we have a match
    bMatch = any(b);
end

%% %%%%%%%%%%%%%%%%%%%% UPDATE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the text fields when the caller to the function has an updateLevel
% smaller than 2
if updateLevel < 2
    % Check if we have a match
    if bMatch
        % Retrieve the name, kurzbeschr and detbeschr and place it in the
        % text-boxes
        tN.String = Vorsatz.Name{b,1};
        tK.String = Vorsatz.KurzBeschr{b,1};
        tD.String = Vorsatz.DetBeschr{b,1};
    else
        % Empty the text-boxes
        tN.String = '';
        tK.String = '';
        tD.String = '';
    end
end

%% %%%%%%%%%%%%%%%%%%% UPDATE BAUTEILE LIST %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the list when the updateLevel is smaller than 2 (initialize) or
% equal to 2 (delete Bauteil)

% Initialize the list
if updateLevel < 2
    % Retrieve the Bauteile List
    if bMatch
        lstID = Vorsatz.Bauteile(b,:);
    else
        lstID = [];
    end
    % Update the data structure
    pVorsatz.Data.BauteileList = lstID;
end

% Get the Bauteile List
lstID = pVorsatz.Data.BauteileList;

% Add Bauteile
if ((updateLevel == 2) && checkKey(callbackdata.Key,'enter')) || ...
    (updateLevel == 3)
    % Find out which Bauteil is selected
    bID = pVorsatz.Data.Bauteile(bDD.Value);
    % Add this Bauteil to the Bauteile_List
    lstID = [lstID, bID];
    % Keep only unique values
    lstID = unique(lstID,'stable');
end    

% Delete Bauteile
if updateLevel == 4
    % Check that the key being pressed is a delete key
    if checkKey(callbackdata.Key,'delete')
        % Delete the selected elements from the list
        vals = lst.Value;
        % Update the list of IDs
        lstID(vals) = [];
    end
end

%%%%%%%%%%%%%%%% PRINT THE LIST %%%%%%%%%%%%%%%%%%
% Check that only existing IDs are being used
[b,pos] = ismember(lstID,Bauteile.ID);

% Check that we have at least a match
if any(b)
    % Get the list names
    dd = Bauteile.Dropdown(pos(b),1);
    % Update the list variable
    lstID = lstID(b);
else
    % None list
    [dd,lstID] = addNone();
end

% Update the list and the value if needed
lst.String = dd;
lst.Max = numel(dd);
lst.Value = unique(min(lst.Value,numel(dd)),'sorted');

% Update the data variable
pVorsatz.Data.BauteileList = lstID;

%% %%%%%%%%%%%%%%%%%%% PROPAGATE DOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the src has an updateLevel smaller than 2 we propagate the changes
% down the chain
if updateLevel < 2
    % Update the other panels
    updatePanels('Vorsatz');
end

end