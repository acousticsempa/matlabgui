function pUbertragung_Build()

% Function creates the panel for viewing, modifying or deleting an 
% Ubertragung object, which will then be put in the figure for viewing
% multiple things at once (Knoten, Bauteile, Ubertragungen, Vorsatzschalen
% and Vorsatzubertragungen)

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and panels
global fParking
global pUbertragung

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 10*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
radio = [100,20];
drop = [100,20];
text = [150,20];
button = [100,20];
table = [150,0];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = max(label(1),button(1)) + ... 
            max([2*radio(1)+2*intra(1),...
                 2*drop(1)+2*intra(1),...
                 text(1)+intra(1),...
                 2*button(1)+2*intra(1)]) + ...
            + table(1) + intra(1) + ...
            + marg(1) + marg(3);
% Minimum height
min_height = max([7*label(2),...
                  3*text(2)+3*drop(2)+radio(2)])+...
             + button(2) + 7*intra(2) + ...
             + marg(2) + marg(4);
          
% Account for the axes to be displayed at the end
WidthToHeight = 1.2;
% Retrieve axes dimensions through proportions
axesDim = [WidthToHeight,1]*(min_height-marg(2)-marg(4));
% Augment the width with the corresponding image width
min_width = min_width + axesDim(1) + intra(1);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the panel position
pos = [1,1,min_width,min_height];

% Create a new full screen figure
pUbertragung = uipanel('Parent',fParking,...
                       'Units','pixels',...'BorderType','none',...
                       'Position',pos,...
                       'Visible','off');
                   
% Add a data structure to the panel
addprop(pUbertragung,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Ubertragung-Typ','Ubertragung-Name',...
           'Startraum/-bauteil','Endraum/-bauteil',...
           'Bauteilfl�che [m2]','L�nge Stossstelle [m]',...
           'Original-DB-IDs'};
tags = {'UbertragungTyp','UbertragungName',...
        'StartEndraum','StartEndbauteil',...
        'Flache','Lange',...
        'OriginalDB'};
lbl_tag = strcat(tags,'LBL');
% Create the label elements iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',pUbertragung,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the first group of radio buttons
rad_txt = {'Luftschall','Trittschall'};
tags = {'Luftschall','Trittschall'};
rad_tag = strcat(tags,'RAD');
% Create the radio button group
UbertragungTyp = uibuttongroup('Parent',pUbertragung,'Units','pixels',...
                               'Visible','on','BorderType','none',...
                               'Tag','UbertragungTypRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_tag)
    uicontrol('Parent',UbertragungTyp,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@pUbertragung_Populate,...
              'Tag',rad_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the first set of dropdowns
txt = ' ';
tags = {'Ubertragung','Startraum','Endraum'};
dd_tag1 = strcat(tags,'DD');
for i=1:numel(dd_tag1)
    uicontrol('Parent',pUbertragung,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@pUbertragung_Populate,...
              'String',txt,'Tag',dd_tag1{i});
end

% Define which text needs to be displayed in the second set of dropdowns
tags = {'Startbauteil','Endbauteil'};
dd_tag2 = strcat(tags,'DD');
for i=1:numel(dd_tag2)
    uicontrol('Parent',pUbertragung,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'String',txt,'Tag',dd_tag2{i});
end

%% %%%%%%%%%%%%%%%%%%%% NON-EDIT TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the texts fields
txt = ' ';
tags = {'OriginalDB'};
txt_tag = strcat(tags,'TXT');
for i=1:numel(txt_tag)
    uicontrol('Parent',pUbertragung,'Style','text',...
              'Visible','on','Units','pixels',...
              'String',txt,'HorizontalAlignment','left',...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%% EDITABLE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed
txt = ' ';
tags = {'Flache','Lange'};
etxt_tag = strcat(tags,'TXT');
% Create the dropdown menus iteratively
for i=1:numel(etxt_tag)
    uicontrol('Parent',pUbertragung,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String',txt,'HorizontalAlignment','left',...
              'Min',0,'Max',1,...
              'Tag',etxt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb = uitable('Parent',pUbertragung,'Visible','on',...
             'RowName',[],'ColumnName',[],'RowStriping','on',...
             'CellEditCallback',@pUbertragung_Populate,...
             'Units','pixels','Tag','RLWerteTABLE');
                    
%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'Delete','Save'};
tags = {'Delete','Modify'};
button_tag = strcat(tags,'BUT');
button_fcn = {'exportUbertragung','exportUbertragung'}; 
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',pUbertragung,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% AXES FOR PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the axes for plot
ax = axes('Parent',pUbertragung,'Units','pixels','Visible','on',...
          'Tag','RLWerteAX');
% Define the label (at least x-axis)
ax.XLabel.String = 'Frequenz f [Hz]';

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 5; 
nV = 8;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = max(label(1),button(1));
h2 = max([radio(1),drop(1),(text(1)-intra(1))/2,button(1)]);
shareH = [h1, h2, h2, table(1), axesDim(1)]...
            /(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));
    
t = max(label(2),text(2));
d = max(label(2),drop(2));
r = max(label(2),radio(2));
shareV = [button(2), t, t, t, d, d, d, r]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have pUbertragung as Parent
obj = findobj('Parent',pUbertragung);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
for j=0:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j+1});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define subdivision of groups of radios 
rSub = 2;

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,3],nV,nH);
% Place the group
UbertragungTyp.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
for j=1:2
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',UbertragungTyp,'Tag',rad_tag{j});
    rad.Position = BoundingBox(subboxes,j,1,rSub);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i=nV-1;
dd = findobj(obj,'Tag','UbertragungDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);        i=i-1;   
dd = findobj(obj,'Tag','StartraumDD');
dd.Position = BoundingBox(boxes,2,i,nH);
dd = findobj(obj,'Tag','StartbauteilDD');
dd.Position = BoundingBox(boxes,3,i,nH);            i=i-1;
dd = findobj(obj,'Tag','EndraumDD');
dd.Position = BoundingBox(boxes,2,i,nH);
dd = findobj(obj,'Tag','EndbauteilDD');
dd.Position = BoundingBox(boxes,3,i,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i=4;
txt = findobj(obj,'Tag','FlacheTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);   i=i-1;
txt = findobj(obj,'Tag','LangeTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);   i=i-1;
txt = findobj(obj,'Tag','OriginalDBTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb.Position = BoundingBox(boxes,4,[1,nV],nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','DeleteBUT');
but.Position = BoundingBox(boxes,2,1,nH);
but = findobj(obj,'Tag','ModifyBUT');
but.Position = BoundingBox(boxes,3,1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define outerposition of axes
AxesPosition = BoundingBox(boxes,nH,[1,nV],nH);
ax.OuterPosition = AxesPosition;
% Store variable for later
pUbertragung.Data.AxesPosition = AxesPosition;