function editCell(src,callbackdata)

% Function is called when the user has edited a cell in the energy / level 
% subtraction tab, the function replots the data if the user modifies any
% of the values (TL/NISPL)

% Global variables
global CompareDatasetsData

% Find the parent tab
tab = src.Parent;

% Check which table has called this function and retrieve data and
% graphical objects accordingly
if strcmpi(src.Tag,'AveragingTABLE')
    % Select the correct axes
    ax = findobj('Parent',tab,'Tag','AveragingAXES');
    % Specify the field in which the data is stored
    dataField = 'Average';
    % Specify the sourceType
    srcType = 1;
elseif strcmpi(src.Tag,'EnergyTABLE')
    % Select the correct axes
    ax = findobj('Parent',tab,'Tag','EnergyAXES');
    % Specify the field in which the data is stored
    dataField = 'EnergySubtraction';
    % Read in the flag handle
    f = ax.Children(2);
    % Specify the sourceType
    srcType = 2;
elseif strcmpi(src.Tag,'LevelTABLE')
    % Select the correct axes
    ax = findobj('Parent',tab,'Tag','LevelAXES');
    % Select the right axis for security
    yyaxis(ax,'right');
    % Specify the field in which the data is stored
    dataField = 'LevelSubtraction';
    % Specify source type
    srcType = 3;
end
    

% Read out the first of the children of the axes, since the energy data and
% the level data are put on top of the stack
l = ax.Children(1);     % Line data

% Retrieve the size of the data
n = numel(l.XData);
% Find out size of source table
[nt,~] = size(src.Data);

% Find out which cells have been modified
pos = callbackdata.Indices;

% Check the type of dataset we are using here and create a mapping between
% the table position and the data field to modify
TypeID = CompareDatasetsData.(dataField).TypeID;

% Differentiate between Average, EnergySubtraction and LevelSubtraction
% tables and their different format
% AverageTAB
if srcType==1
    if TypeID==1            % Luftschall
        field = ['SNR',repmat({'C'},1,8),repmat({'Values'},1,n)];
        indx = [1,1:8,1:n];
    elseif TypeID==2        % Trittschall
        field = ['SNR',repmat({'C'},1,2),repmat({'Values'},1,n)];
        indx = [1,1:2,1:n];
    end   
% EnergyTABLE
elseif srcType==2
    if TypeID==1            % Luftschall
        field = ['SNR',repmat({'C'},1,8),repmat({'Values'},1,n)];
        indx = [1,1:8,1:n];
    elseif TypeID==2        % Trittschall
        field = ['SNR',repmat({'C'},1,2),repmat({'Values'},1,n)];
        indx = [1,1:2,1:n];
    end
% LevelTABLE
elseif srcType==3
    field = repmat({'Values'},1,n);
    indx = 1:n;
end
% Cell indexes
cellPos = 4:nt;

% Find the boolean vector indicating if the modified position is one we are
% actually tracking
b = (pos(1)==cellPos);

%% Modify something only if we are tracking the value
if any(b)
    % Get the inputted string
    str = callbackdata.NewData;
    % Keep only mathematical relevant characters
    str = mathStr(str);
    % Try to evaluate the string and if does not work tell the user
    try val = eval(str);
        % Store the new value in the cell at the end of the function, in
        % order to maintain the selection on that cell!!
    catch
        % Keep old data in the table
        src.Data{pos(1),pos(2)} = callbackdata.PreviousData;
        % Throw error
        errordlg('Please input a valid string in the cell',...
                 'Input error','modal');
        return;
    end
    
    % Store the value in the correct field in the global variable
    b = (pos(1)==cellPos);
    CompareDatasetsData.(dataField).(field{1,b})(1,indx(1,b)) = val;
    
    %% %%%%%%%%%%%%%%%%%% UPDATE SNR AND C-Values %%%%%%%%%%%%%%%%%%%%%%%%
    % Check if we need to update the SNR and C-Values (if field Values has
    % been modified), this is only done if the user has modified values in
    % the AverageTABLE or the EnergyTABLE
    if strcmpi(field{1,b},'Values') && any(srcType==[1,2])
        % Perform the SNR and C-values calculation again for the new data
        % as inputted by the user
        % Retrieve the frequencies
        freq = CompareDatasetsData.(dataField).Freq;
        values = CompareDatasetsData.(dataField).Values;
        % Calculate the new SNR and C-values
        SNRC = SNRCValues(freq,values,TypeID);
        % Extract the data
        SNR = SNRC.SNR;
        C = SNRC.C;
        
        % Update the global variables 
        CompareDatasetsData.(dataField).SNR = SNR;
        CompareDatasetsData.(dataField).C = C;
        
        % Update the table with this new data (only the modified column)
        SNRindx = cellPos(strcmpi(field,'SNR'));
        Cindx = cellPos(strcmpi(field,'C'));
        src.Data{SNRindx,3} = num2str(SNR,'%.1f');
        for i=1:numel(Cindx)
            src.Data{Cindx(i),3} = num2str(C(i),'%.0f');
        end
    end
    
    %% %%%%%%%%%%%%%%%%%% UPDATE PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check if we need to update the plot (if field Values has been
    % modified
    if strcmpi(field{1,b},'Values')
        % Update the line plot data
        l.YData = CompareDatasetsData.(dataField).Values;
        
        % For Energy Plot we need to plot also the flags, and update the
        % axes limits correctly
        if srcType==2
            % Retrieve the min and max values of each of the three lines
            % plotted in the graph
            mav = -Inf;
            miv = Inf;
            indx = [1,3,4];
            for i=1:numel(indx)
                % Retrieve y-values
                d = ax.Children(indx(i)).YData;
                % Filter data
                d = d(and(~isinf(d),~isnan(d)));
                % Calculate new max and new min
                mav = max([mav,d]);
                miv = min([miv,d]);
            end
            % Calculate the new axes limits
            ylim = [miv,mav];
            % Get the ticks
            ti = getTicks(ylim,10);
            % Retrieve the minimum value
            ymin = min(ti);
            % Update the flags
            flag = isnan(l.YData)-1+ymin;
            f.YData = flag;
            % Replace the limits
            ax.YLim = [min(ti),max(ti)];
            ax.YTick = ti;
        end
    end
    
    %% %%%%%%%%%%%%%%%%% UPDATE CELL VALUE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the string of the table with the inputted value
    src.Data{pos(1),pos(2)} = str;
end

end