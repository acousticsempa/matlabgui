# Leichtbau Prufstand - Analysis Tool #

This is the repository for the Matlab GUI application for working with the Leichtbau Prufstand datasets and perform acoustics calculations.

## How to use the application ##
The application allows the user the to:

* Create Objects
* View / Modify Objects
* Update Databases
* Calculate Vertical / Horizontal Situations

### Create Objects ###
The application allows the user to create / modify the following objects and define the properties:

* Bauteile

    * Type-ID: Wand, Decke
    * Namen
    * Kurzbeschreibung
    * Detaillierte Beschreibung

* Vorsatzschalen

    * Namen
    * Kurzbeschreibung
    * Anwendbarkeit auf Bauteile
    * Detaillierte Beschreibung

* Knoten

    * Namen
    * Kurzbeschreibung
    * Detaillierte Beschreibung
    * Type-ID: Kreuzstoss, T-Stoss, Anders
    * Ausrichtung: Vertikal, Horizontal
    * Bauteile:

         * Bauteil 1: Links
         * Bauteil 2: Oben
         * Bauteil 3: Rechts
         * Bauteil 4: Unten

    * Anisotropie für jeden Bauteil:

         * 0: **Keine** Anisotropie
         * 1: Anisotropie **parallel** zur Knotenrichtung
         * 2: Anisotropie **senkrecht** zur Knotenrichtung

* Ubertragungen

     * Knoten
     * Type-ID: Luftschall, Trittschall
     * Startraum / Endraum:

         * Raum 1: Links Oben
         * Raum 2: Rechts Oben
         * Raum 3: Rechts Unten
         * Raum 4: Links Unten

     * Startbauteil / Endbauteil
     * Trennbauteilfläche
     * Länge Stossstelle
     * Original Database IDs
     * Transmissionswerte (R/L)

* Vorsatzschalen-Ubertragungen

     * Vorsatzschale
     * Type-ID: Luftschall, Trittschall
     * Anisotropie:

         * 1: Gültig für Weg **parallel** zur Anisotropierichtung
         * 2: Gültig für Weg **senkrecht** zur Anisotropierichtung und für Bauteile **ohne** Anisotropie
         * 3: Gültig für Direktdurchgang

     * Original Database IDs
     * Delta Transmissionswerte (dR/dL)

## Calculate Vertical / Horizontal Situations ##
### Vertical Situation ###
The user can calculate the transmission values for two room one above the other. For the construction of the structure the user can choose from the Knoten database for Knotens with the desired properties. Furthermore the user can select which Vorsatzschalen to use on each of the Bauteile in the structure and specify which pathways should be considered.
The geometry of the structure can also be modified by the user.

### Horizontal Situation ###
TODO

## How to install the application ###
Head to the following link [Installer-LeichtbauPrufstand](https://bitbucket.org/matteoempa/matlabgui/downloads/Installer-LeichtbauPrufstand.zip) and download the zip file. Once the file is downloaded unzip the files and double-click on the Installer-LeichtbauPrufstand.bat file.

The installer is going to prompt you for Administrator rights in order to install the application. Throughout the installation process, you will be able to see where the application is being installed and how to start the application.

The installation will create a shortcut on the desktop for quick access.

## Contact ##
For question about the repository and the application please write to:

* matteo.berchier at empa.ch
