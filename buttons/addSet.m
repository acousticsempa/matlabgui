function addSet(src, callbackdata)
% Function adds the selected dataset to the For Comparison Selected
% Datasets

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global DatasetSelection
global CompDatasetSelection

% Check if the DatasetSelection variable is empty
if isempty(DatasetSelection)
    % Return
    return;
end

% Retrieve the value of the list box, highlighting which elements are
% currently selected
lstDSS = findobj('Tag','SelDatasetsLST');
lstCompDSS = findobj('Tag','ComparisonSelDatasetsLST');

% Retrieve the selected elements in the 'Selected Datasets' listbox
% and add them to the end of the listbox 'For Comparison Selected Datasets'
selDS = lstDSS.Value;

% Retrieve the identifiers of the selected Datasets
selDS_ID = DatasetSelection(selDS,:);

% Add this identifiers to the end of queue in the 'For Comparison Selected
% Datasets'
CompSelDS = [CompDatasetSelection; selDS_ID];
% Retrieve the unique elements
CompSelDS = unique(CompSelDS,'rows','stable');

% Actualize the global variable
CompDatasetSelection = CompSelDS;

% Actualize the listbox
lst = getReports(CompDatasetSelection);

% Change the list in the list viewer
if numel(lst)==0
    % Generate an empty listbox
    lstCompDSS.String = ' ';
    lstCompDSS.Max = 1;
    lstCompDSS.Value = 1;
else
    % Place the string array, augment the maximum selection to the
    % maximum number and select the first value
    lstCompDSS.String = lst;
    lstCompDSS.Max = numel(lst);
end

end

