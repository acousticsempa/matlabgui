function viewData(~, ~)

% Function is responsible for populating the GUI with the correct data
% once the user has selected a new dataset to visualize

%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global selectedIndex
global selectedCase

%%%%%%%%%%%%%%%% CHECK THAT DROPDOWNS ARE CORRECTLY SELECTED %%%%%%%%%%%%
% Use the function to check that the dropdowns are correctly selected
[passed, indx, selectedDatatype] = checkSelection();

% If the test is not passed, we return
if ~passed
    return; 
end

% If we do not have an error, we can perform the rendering of the data
% Retrieve the previously plotted report and compare to the one we want to
% print now to avoid reprinting the same stuff
indxOld = selectedIndex;
caseOld = selectedCase;

% Check if the old index is equal to the new one and the case are equal as
% well
if ~isempty(indxOld) && ~isempty(caseOld) && (indxOld==indx) && ...
                        (caseOld == selectedDatatype)
    % We are plotting the same stuff as already rendered, therefore we can
    % exit
    return;
else
    selectedIndex = indx;
    selectedCase = selectedDatatype;
end

% See which tab is selected and plot that first
topTab = findobj('Tag','topTAB');

% Switch statement between the tabs to define which tab to plot first
switch lower(topTab.SelectedTab.Title)
    case 'aufbau'
        plotWalls(indx,selectedDatatype);
        i=1;
    case 'daten'
        printData(indx,selectedDatatype);
        i=2;
    case 'graphen'
        plotGraphs(indx,selectedDatatype);
        i=3;
end

% Plot the rest of the tabs
if i~=1
    plotWalls(indx,selectedDatatype);
end
if i~=2
    printData(indx,selectedDatatype);
end
if i~=3
    plotGraphs(indx,selectedDatatype);
end

end