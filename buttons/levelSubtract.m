function levelSubtract(~,~)
% Function performs the calculation necessary for doing a level
% subtraction of two acoustic datasets

%% %%%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%
global Data
global CompDatasetSelection
global CompareDatasetsData

%% %%%%%%%%%%%%%%%%%%%%%% DECLARE HANDLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab = findobj('Tag','LevelTAB');
tbl = findobj('Parent',tab,'Tag','LevelTABLE');
ax = findobj('Parent',tab,'Tag','LevelAXES');
but = findobj('Parent',tab,'Tag','ExportLevelDataBUT');
ch = findobj('Parent',ax,'Type','Line');
leg = findobj('Type','Legend','Tag','LevelLEG');

% Clear the children in any case and the legend
delete(ch(:));
delete(leg);

% Retrieve the number of datasets
n = size(CompDatasetSelection,1);

% Hide every graphical object (table, axes and button
tbl.Visible = 'off';
ax.Visible = 'off';
but.Visible = 'off';

% Avoid performing any action if any number of elements except for two
% have been selected
if n~=2
    % Throw error dialog
    errordlg(['Number of passed in datasets for energy subtraction is',...
              ' different than 2!'],'Number of datasets error','modal');
    return;
end

%% %%%%%%%%%%%%%%%%%%% CHECK FOR EQUAL PARENT CONDITIONS %%%%%%%%%%%%%%%%%
passed = checkParentProperties(CompDatasetSelection);
% If the test is passed we continue, otherwise we return
if ~passed
    return;
end

% Retrieve the initial dbType
dbType = CompDatasetSelection(1,1);
% Initialize the DBType array
DBType = {'Air','Impact','AirAVG'};


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a colormap and retrieve the colors to be used for plotting
colorsRGB = lines(n);
colorLevelRGB = [0,0,0];

% Get the equivalent hex value
colorsHEX = rgb2hex(colorsRGB);
colorLevelHEX = rgb2hex(colorLevelRGB);

% Define a color generation function for HTML
colorCell = @(text,color) ['<html><table border=0 width=1000',...
                           ' margin=0 padding=0 bgcolor=',color,...
                           '><tr><td>',text,'</td></tr></table></html>'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a table generation function for placing the content in the middle
% of the table
alignCell = @(text,width) ['<html><table border=0 width=',width,...
                           ' margin=0 padding=0><tr><td align="center"',...
                           ' valign="middle">',text,'</td></tr>',...
                           '</table></html>'];
% Retrieve the frequency dependant values
freq = Data.(DBType{dbType}).MeasFreq;
m = numel(freq);
freq_head = cell(1,m);

% Define values to print for Airborne and Airborne Average data
row_head = {'Plot Color','Plot ID','Datatype'};
% Initialize the variables to be used for the table
if any(dbType==[1,3])
    % Add the frequency dependant values, repeat for each frequency
    for i=1:m
        % Retrieve and print to frequency data
        freq_head{1,i} = strcat('TL(',num2str(freq(i),'%.0f'),'Hz)');
    end
else
    % Add the frequency dependant values, repeat for each frequency
    for i=1:m
        % Retrieve and print to frequency data
        freq_head{1,i} = strcat('NISPL(',num2str(freq(i),'%.0f'),'Hz)');
    end
end
% Concatenate the values
row_head = [row_head,freq_head];
rows = numel(row_head);
% Define delta i
di = 2+1;
col_head = cell(n+di,1);
tblData = cell(rows,n+di);

% Specify the column widths
col_width = cell(1,n+di);
col_width{1} = 75;
for i=2:n+di
    col_width{i} = 80;
end

% Prepare the first column with the row heads MANUALLY
for j=1:rows
    % Input the row head
    tblData{j,1} = row_head{j};
end

%% %%%%%%%%%%%%% Print column with subtracted data %%%%%%%%%%%%%%%%%%%%%%%%
% Initialize row count and column count
j=1; i=2;

% Retrieve the subtracted data
levelData = levelSubtraction(CompDatasetSelection);

% Retrieve the column width and set the column head
w = num2str(col_width{1,i},'%.0f');
col_head{i,1} = 'Subtracted | Values';

% Print the data to the table
tblData{j,i} = colorCell('',colorLevelHEX);         j=j+1;
tblData{j,i} = alignCell('0',w);                    j=j+1;
tblData{j,i} = alignCell(levelData.DataType,w);
for g=1:m
    tblData{j+g,i} = alignCell(num2str(levelData.Values(g),'%.1f'),w);
end

% Print data for editing (without HTML shit) (in numbers)
j=1; i=3;
col_head{i,1} = 'Edited | Subtracted | Values';
tblData{j,i} = colorCell('',colorLevelHEX);         j=j+1;
tblData{j,i} = '0';                                 j=j+1;
tblData{j,i} = levelData.DataType;
for g=1:m
    tblData{j+g,i} = num2str(levelData.Values(g),'%.1f');
end

%% %%%%%%%%%%%%%%%%%%%%% Print columns with original data %%%%%%%%%%%%%%%%
% Go through the original data
for i=1+di:n+di
    % Retrieve the datatype
    dbType = CompDatasetSelection(i-di,1);
    % Specify the DB
    DB = Data.(DBType{dbType});
    
    % Retrieve the indx
    indx = CompDatasetSelection(i-di,2);
    % Retrieve a string with the column width
    w = num2str(col_width{1,i},'%.0f');
    % Retrieve the report name and split it for better rendering
    col_head{i,1} = strSplit(DB.Report{indx,1},13,'|');
    
    % Initialize row count
    j = 1;
    % Define the plot color
    tblData{j,i} = colorCell('',colorsHEX(i-di,:));     j=j+1;
    % Define a plot ID
    tblData{j,i} = alignCell(num2str(i-di,'%.0f'),w);   j=j+1;
    % Retrieve the data type
    tblData{j,i} = alignCell(DB.DataType{indx,1},w);
    % Retrieve the TL / NISPL values
    if dbType == 1 || dbType == 3
        TL = DB.TL(indx,:);
        % Print the TL values to the table
        for k=1:m
            tblData{j+k,i} = alignCell(num2str(TL(k),'%.1f'),w);
        end
    else
        NISPL = DB.NISPL(indx,:);
        % Print the NISPL values to the table
        for k=1:m
            tblData{j+k,i} = alignCell(num2str(NISPL(k),'%.1f'),w);
        end
    end
end

% Put data in the table
tbl.ColumnName = col_head;
tbl.ColumnWidth = col_width;
tbl.RowName = [];
tbl.Data = tblData;
% Make the third column available for editing
col_Edit = false(1,n+di);
col_Edit(3) = true;
tbl.ColumnEditable = col_Edit;

% Set the table to visible
tbl.Visible = 'on';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hold the axes
hold(ax,'on');
% Initialize delta i
di = 1;
% Initialize the legend handle and string
leg_h = gobjects(2*n+di,1);
leg_s = cell(2*n+di,1);
leg_b = false(2*n+di,1);

%% Plot the subtracted data
% Select the correct database from where to draw the data
if any(dbType==[1,3])
   % Airborne Data
   yDataField = 'TL';
   yLabelLeft = 'Schalldammmass TL [dB]';
   yLabelRight = 'Einfügedämmung \Delta TL [dB]';
   lgn_pos = 'northwest';
elseif dbType==2
   % Impact Data
   yDataField = 'NISPL';
   yLabelLeft = 'Trittschallpegel NISPL [dB]';
   yLabelRight = 'Pegeländerung \Delta NISPL [dB]';
   lgn_pos = 'northeast';
end
% Retrieve the x-values and y-values
x = DB.MeasFreq;
y_lev = levelData.Values;
% Activate the right y-axis
yyaxis(ax,'right');
% Plot the data
h_lev = plot(ax,x,y_lev,'Color',colorLevelRGB,...
                        'LineStyle','--','LineWidth',1.5,...
                        'Marker','x',...
                        'MarkerFaceColor','none',...
                        'MarkerEdgeColor',colorLevelRGB);
% Prepare the legend
leg_h(1) = h_lev;
leg_s{1,1} = ['0 (',levelData.DataType,')'];
leg_b(1) = true;

%% Plot the original data
% Retrieve the important informations to be displayed in the plot
% Activate the left y-axis
yyaxis(ax,'left');
% Go throught the original data
for i=1+di:n+di
    % Calculate new iterator
    j = i-di;
    % Retrieve the dbType 
    dbType = CompDatasetSelection(j,1);
    % Retrieve the correct database
    DB = Data.(DBType{dbType});
    % Retrieve the indx
    indx = CompDatasetSelection(j,2);
    % Retrieve the color
    color = colorsRGB(j,:);
    % Retrieve the y-values 
    y = DB.(yDataField)(indx,:);
    % Retrieve the flags
    flag = any(DB.Flag(indx,:,:),3);
    % Plot the data
    h = plot(ax,x,y,'Color',color,...
                    'LineStyle','-','Marker','none');
    % Prepare the legend
    leg_h(2*j) = h;
    leg_s{2*j,1} = [num2str(j,'%.0f'),' (',DB.DataType{indx,1},')'];
    leg_b(2*j) = true;
    
    % Plot the flags if any are present
    if any(flag)
        h = plot(ax,x(flag),y(flag),'MarkerEdgeColor',color,...
                                    'MarkerFaceColor',color,...
                                    'Marker','x',...
                                    'LineStyle','none');
        leg_h(2*j+1) = h;
        leg_s{2*j+1,1} = [num2str(j,'%.0f'),' (Flag)'];
        leg_b(2*j+1) = true;
    end
end

% Place the legend
legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
                                    'Tag','LevelLEG',...
                                    'Location',lgn_pos);

% Left axis modifications
yyaxis(ax,'left');
% Enable the gridlines
ax.YGrid = 'on';
% Specify the y-label
ax.YLabel.String = yLabelLeft;
% Specify the color
ax.YColor = colorsRGB(1,:);

% Right axis modifications
yyaxis(ax,'right');
% Enable the gridlines
ax.YGrid = 'on';
% Specify the y-label
ax.YLabel.String = yLabelRight;
% Specify the color
ax.YColor = colorLevelRGB;

% Enable the gridlines
ax.XGrid = 'on';
% Specify the scale
ax.XScale = 'log';
% Specify the limits of the x-axis
ax.XLim = [min(x), max(x)];
% Specify the x-ticks
ax.XTick = x(2:3:end);
% Specify the x-label and y-label
ax.XLabel.String = 'Frequenz f [Hz]';
% Unhold the axes and set them to visible
hold(ax,'off');
ax.Visible = 'on';

% Set the zoom property
z = zoom(tab.Parent.Parent);
setAllowAxesZoom(z,ax,true);

% Show the export button
but.Visible = 'on';

%% Set the focus on the selected tab to averaging
% Find the tab group
tabParent = findobj('Tag','topCompTAB');
% Set the selected value to the averaging tab
tabParent.SelectedTab = tab; 

%% %%%%%%%%%%%% STORE DATA TO GLOBAL VARIABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The global variable CompareDatasetsData has three fields, one for averaged
% one for energy subtraction data and one for level subtraction data
% Store the level subtraction data under the LevelSubtraction field

% Copy the avgData to the data
data = levelData;

% Add the IDs of the datasets used
data.IDs = CompDatasetSelection;
% Store the Messungstype Type-ID
if any(dbType==[1,3])
    data.TypeID = 1;    % Luftschall
else
    data.TypeID = 2;    % Trittschall
end
% Store the OriginalDB-ID
data.OriginalDBID = getOriginalDBID(CompDatasetSelection,'L');

% Store the data under the global variable
CompareDatasetsData.LevelSubtraction = data;

end

