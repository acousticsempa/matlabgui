function compareSets(~,~)
% Function takes care of placing the important data within the 'Comparison'
% tab for the 'For Comparison Selected Datasets'

%%%%%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%
global Data
global CompDatasetSelection

%%%%%%%%%%%%%%%%%%%%%%%% DECLARE HANDLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab = findobj('Tag','ComparisonTAB');
tbl = findobj('Parent',tab,'Tag','ComparisonTABLE');
ax = findobj('Parent',tab,'Tag','ComparisonAXES');
ch = findobj('Parent',ax,'Type','Line');
leg = findobj('Type','Legend','Tag','ComparisonLEG');

% Clear the children in any case and the legend
delete(ch(:));
delete(leg);

% Retrieve the number of datasets
n = size(CompDatasetSelection,1);

% Hide every graphical object (table, axes)
tbl.Visible = 'off';
ax.Visible = 'off';

% Avoid performing any action if no elements are selected
if n==0 || n>39
    if n>39
        % Throw error
        errordlg('Too many datasets selected, maximum is 39 datasets!',...
                 'Max. number of datasets exceeded!','modal');
    end
    return;
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a colormap and retrieve the colors to be used for plotting
colors = repmat([lines(7); prism(6)],[3,1]);
colorsRGB = colors(1:n,:);

% Get the equivalent hex value
colorsHEX = rgb2hex(colorsRGB);

% Define a color generation function for HTML
colorCell = @(text,color) ['<html><table border=0 width=1000',...
                           ' margin=0 padding=0 bgcolor=',color,...
                           '><tr><td>',text,'</td></tr></table></html>'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a table generation function for placing the content in the middle
% of the table
alignCell = @(text,width) ['<html><table border=0 width=',width,...
                           ' margin=0 padding=0><tr><td align="center"',...
                           ' valign="middle">',text,'</td></tr>',...
                           '</table></html>'];
                  
% Initialize the variables to be used for the table
row_head = {'Plot Color','Plot ID','Datatype','SNR','C','C_tr',...
            'C_50-3150','C_tr_50-3150','C_50-5000','C_tr_50-5000',...
            'C_100-5000','C_tr_100-5000','C_I','C_I_50-2500'};
% Define delta i
di = 1;
% Define cells for storing column heads and table data
rows = numel(row_head);
col_head = cell(n+di,1);
tblData = cell(rows,n+di);

% Specify the column widths
col_width = cell(1,n+1);
col_width{1} = 75;
for i=2:n+di
    col_width{i} = 80;
end

% Prepare the first column with the row heads MANUALLY
for j=1:rows
    % Input the row head
    tblData{j,1} = row_head{j};
end

% Retrieve the important informations to be displayed in the table
for i=1+di:n+di
    % Retrieve the DBType
    DBType = CompDatasetSelection(i-di,1);
    
    % Select the correct database from where to draw the data
    if DBType == 1
        % Airborne Data
        DB = Data.Air;
    elseif DBType == 2
        % Impact Data
        DB = Data.Impact;
    elseif DBType == 3
        % Airborne Averaged Data
        DB = Data.AirAVG;
    else
        % Some error
        errordlg('Dataset Type selection problem!','Dataset Type Problem',...
                 'modal');
        return;
    end
    % Retrieve the indx
    indx = CompDatasetSelection(i-di,2);
    % Retrieve a string with the column width
    w = num2str(col_width{1,i},'%.0f');
    
    %%%%%%%%%%%%%%%%% Retrieve the actual data %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Retrieve the report name and split it for better rendering
    col_head{i,1} = strSplit(DB.Report{indx,1},13,'|');
    % Initialize row count
    j = 1;
    
    % Define the plot color
    tblData{j,i} = colorCell('',colorsHEX(i-di,:));             j=j+1;
    % Define a plot ID
    tblData{j,i} = alignCell(num2str(i-1,'%.0f'),w);            j=j+1;
    % Retrieve the data type
    tblData{j,i} = alignCell(DB.DataType{indx,1},w);            j=j+1;
    % Retrieve the SNR
    tblData{j,i} = alignCell(num2str(DB.SNR(indx),'%.1f'),w);                     
    % Retrieve the C values
    if any(DBType==[1,3])
        % Airborne Data and Airborne Average Data
        for k=1:8
            tblData{j+k,i} = alignCell(num2str(DB.C(indx,k),'%.0f'),w);
        end
        % Update step variable
        j = j+8;
        % Fill cells that are not relevant for 'Airborne' data
        for k=1:2
            tblData{j+k,i} = alignCell('-',w);
        end
    elseif DBType == 2
        % Impact Data
        % Fill cells that are not relevant for 'Impact' data
        for k=1:8
            tblData{j+k,i} = alignCell('-',w);
        end
        % Update step variable
        j = j+8;
        % Retrieve the C-values
        for k=1:2
            tblData{j+k,i} = alignCell(num2str(DB.C(indx,k),'%.0f'),w);
        end
    end
end

% Put data in the table
tbl.ColumnName = col_head;
tbl.ColumnWidth = col_width;
tbl.RowName = [];
tbl.Data = tblData;

% Set the table to visible
tbl.Visible = 'on';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hold the axes
hold(ax,'on');
% Initialize the legend handle and string
leg_h = gobjects(2*n,1);
leg_s = cell(2*n,1);
leg_b = false(2*n,1);

% Initialize y-axis labels
AirSelected = false;
ImpSelected = false;
% Define after how many plots the linestyle changes
PlotCycle = 13;
LineStyle = {'-','--','-.'};

% Retrieve the important informations to be displayed in the plot
for i=1:n
     % Retrieve the DBType
    DBType = CompDatasetSelection(i,1);
    
    % Select the correct database from where to draw the data
    if DBType == 1
        % Airborne Data
        DB = Data.Air;
        yDataField = 'TL';
        AirSelected = true;
    elseif DBType==2
        % Impact Data
        DB = Data.Impact;
        yDataField = 'NISPL';
        ImpSelected = true;
    elseif DBType == 3
        % Airborne Average
        DB = Data.AirAVG;
        yDataField = 'TL';
        AirSelected = true;
    end
    % Retrieve the color
    color = colorsRGB(i,:);
    % Retrieve the indx
    indx = CompDatasetSelection(i,2);
    % Retrieve the x-values
    x = DB.MeasFreq;
    % Retrieve the y-values 
    y = DB.(yDataField)(indx,:);
    % Retrieve the flag boolean
    flag = any(DB.Flag(indx,:,:),3);
    % Get the linestyle index
    lineIndx = (mod(i,PlotCycle)>0) + floor(i/PlotCycle);
    % Plot the data (x-y-values)
    h = plot(ax,x,y,'Color',color,'LineStyle',LineStyle{lineIndx});
    % Prepare the legend
    leg_h(i*2-1) = h;
    leg_s{i*2-1,1} = [num2str(i,'%.0f'),' (',DB.DataType{indx,1},')'];
    leg_b(i*2-1) = true;
    % Plot the flags if any are present
    if any(flag)
        h = plot(ax,x(flag),y(flag),'MarkerEdgeColor',color,...
                                    'MarkerFaceColor',color,...
                                    'Marker','x',...
                                    'LineStyle','none');
        leg_h(i*2) = h;
        leg_s{i*2,1} = [num2str(i,'%.0f'),' (Flag)'];
        leg_b(i*2) = true;
    end
end

% Prepare the legend and the y-label according to the selected datasets
if AirSelected && ~ImpSelected
    % Airborne data selected, but on Impact data selected
    lgn_loc = 'northwest';
    yLbl = 'Schalldammmass TL [dB]';
elseif ImpSelected && ~AirSelected
    % Impact data selected, but on Airborne data selected
    lgn_loc = 'northeast';
    yLbl = 'Trittschallpegel NISPL [dB]';
else
    % Both data has been selected
    lgn_loc = 'north';
    yLbl = 'Schalldammmass TL [dB] / Trittschallpegel NISPL [dB]';
end

% Place the legend
legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
                                    'Tag','ComparisonLEG',...
                                    'Location',lgn_loc);
                    
% Enable the gridlines
ax.XGrid = 'on';
ax.YGrid = 'on';
% Specify the scale
ax.XScale = 'log';
% Specify the limits of the x-axis
ax.XLim = [min(x), max(x)];
% Specify the x-ticks
ax.XTick = x(2:3:end);
% Specify the x-label
ax.XLabel.String = 'Frequenz f [Hz]';
% Specify the y-label
ax.YLabel.String = yLbl;
% Unhold the axes and set them to visible
hold(ax,'off');
ax.Visible = 'on';

% Set the zoom property
z = zoom(tab.Parent.Parent);
setAllowAxesZoom(z,ax,true);

%% Set the focus on the selected tab to comparison
% Find the tab group
tabParent = findobj('Tag','topCompTAB');

% Set the selected value to the averaging tab
tabParent.SelectedTab = tab;

end

