function updateDatabase(src,~)

% Function takes care of telling the user that the database has been 
% updated or that there was a problem while updating the database

% Find the type of object being updated
switch src.Tag
    case 'UpdateKnotenBUT'
        success = ImportKnoten();
        obj = 'Knoten';
        
    case 'UpdateBauteilBUT'
        success = ImportBauteile();
        obj = 'Bauteile';
        
    case 'UpdateVorsatzBUT'
        success = ImportVorsatz();
        obj = 'Vorsatzschalen';
        
    case 'UpdateUbertragungBUT'
        success = ImportUbertragung();
        obj = 'Ubertragungen';
        
    case 'UpdateVUbertragungBUT'
        success = ImportVUbertragung();
        obj = 'Vorsatzschalen-Ubertragungen';
end

% If the import has not worked throw a warning, otherwise notify the user
% that the database update could be performed without errors
if success
    msgbox(['Update of',32,obj,' Database successfully completed!'],...
            'Database Update Completed');
    return;

else
    errordlg(['Update of',32,obj,' Database was unsuccessful!',10,...
              'Please check that the Excel file is closed and that there',...
              ' are no errors in the database!'],...
             'Database Update - Error','modal');
    return;
end