function selectDataset(src, callbackdata)

% Function is responsible for selecting the currently selected project, 
% it also throws an error if no report is being selected

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global DatasetSelection

%%%%%%%%%%%%%%%% CHECK THAT DROPDOWNS ARE CORRECTLY SELECTED %%%%%%%%%%%%
% Use the function to check that the dropdowns are correctly selected
[passed, indx, selectedDatatype] = checkSelection();

% If the test is not passed, we return
if ~passed
    return; 
end

%%%% Add the dataset to the global variable DatasetSelection %%%%%%%%%%%%%
tmp = [DatasetSelection; selectedDatatype, indx];

% Keep only unique combinations
DatasetSelection = unique(tmp,'rows','stable');

end