function energySubtract(~,~)
% Function performs the calculation necessary for doing an energy
% subtraction of two acoustic datasets

%% %%%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%
% Data
global Data
global CompDatasetSelection
global CompareDatasetsData

%% %%%%%%%%%%%%%%%%%%%%%% DECLARE HANDLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab = findobj('Tag','EnergyTAB');
tbl = findobj('Parent',tab,'Tag','EnergyTABLE');
ax = findobj('Parent',tab,'Tag','EnergyAXES');
but = findobj('Parent',tab,'Tag','ExportEnergyDataBUT');
ch = findobj('Parent',ax,'Type','Line');
leg = findobj('Type','Legend','Tag','EnergyLEG');

% Clear the children in any case and the legend
delete(ch(:));
delete(leg);

% Retrieve the number of datasets
n = size(CompDatasetSelection,1);

% Hide every graphical object (table, axes and button
tbl.Visible = 'off';
ax.Visible = 'off';
but.Visible = 'off';

% Avoid performing any action if any number of elements except for two
% have been selected
if n~=2
    % Throw error
    errordlg(['Number of passed in datasets for energy subtraction is',...
              ' different than 2!'],'Number of datasets error','modal');
    return; 
end

%% %%%%%%%%%%%%%%%%%%% CHECK FOR EQUAL PARENT CONDITIONS %%%%%%%%%%%%%%%%%
passed = checkParentProperties(CompDatasetSelection,'Project','MeasType');
% If the test is passed we continue, otherwise we return
if ~passed
    return;
end

% Retrieve the initial dbType
dbType = CompDatasetSelection(1,1);
% Initialize the DBType array
DBType = {'Air','Impact','AirAVG'};

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a colormap and retrieve the colors to be used for plotting
colorsRGB = lines(n);
colorEnergyRGB = [0,0,0];

% Get the equivalent hex value
colorsHEX = rgb2hex(colorsRGB);
colorEnergyHEX = rgb2hex(colorEnergyRGB);

% Define a color generation function for HTML
colorCell = @(text,color) ['<html><table border=0 width=1000',...
                           ' margin=0 padding=0 bgcolor=',color,...
                           '><tr><td>',text,'</td></tr></table></html>'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a table generation function for placing the content in the middle
% of the table
alignCell = @(text,width) ['<html><table border=0 width=',width,...
                           ' margin=0 padding=0><tr><td align="center"',...
                           ' valign="middle">',text,'</td></tr>',...
                           '</table></html>'];
% Retrieve the frequency dependant values
freq = Data.(DBType{dbType}).MeasFreq;
m = numel(freq);
freq_head = cell(1,m);

% Initialize the variables to be used for the table
if any(dbType==[1,3])
    % Define values to print for Airborne and Airborne Average data
    row_head = {'Plot Color','Plot ID','Datatype','SNR','C','C_tr',...
                'C_50-3150','C_tr_50-3150','C_50-5000','C_tr_50-5000',...
                'C_100-5000','C_tr_100-5000'};
    % Add the frequency dependant values, repeat for each frequency
    for i=1:m
        % Retrieve and print to frequency data
        freq_head{1,i} = strcat('TL(',num2str(freq(i),'%.0f'),'Hz)');
    end
else
    % Define values to print for impact data
    row_head = {'Plot Color','Plot ID','Datatype','SNR','C_I','C_I_50-2500'};
    % Add the frequency dependant values, repeat for each frequency
    for i=1:m
        % Retrieve and print to frequency data
        freq_head{1,i} = strcat('NISPL(',num2str(freq(i),'%.0f'),'Hz)');
    end
end
% Concatenate the values
row_head = [row_head,freq_head];
rows = numel(row_head);
% Define delta i
di = 2+1;
col_head = cell(n+di,1);
tblData = cell(rows,n+di);

% Specify the column widths
col_width = cell(1,n+di);
col_width{1} = 75;
for i=2:n+di
    col_width{i} = 80;
end

% Prepare the first column with the row heads MANUALLY
for j=1:rows
    % Input the row head
    tblData{j,1} = row_head{j};
end

%% %%%%%%%%%%%%% Print column with subtracted data %%%%%%%%%%%%%%%%%%%%%%%%
% Initialize row count and column count
j=1; i=2;

% Retrieve the subtracted data
energyData = energySubtraction(CompDatasetSelection);

% Retrieve the column width and set the column head
w = num2str(col_width{1,i},'%.0f');
col_head{i,1} = 'Subtracted | Values';

% Print the data to the table
tblData{j,i} = colorCell('',colorEnergyHEX);                     j=j+1;
tblData{j,i} = alignCell('0',w);                                 j=j+1;
tblData{j,i} = alignCell(energyData.DataType,w);                 j=j+1;
tblData{j,i} = alignCell(num2str(energyData.SNR,'%.1f'),w);
% Check the case for printing C-values to the table
if any(dbType==[1,3])
    for g=1:8
        tblData{j+g,i} = alignCell(num2str(energyData.C(g),'%.0f'),w);
    end
else
    for g=1:2
        tblData{j+g,i} = alignCell(num2str(energyData.C(g),'%.0f'),w);
    end
end
% Update the j value
j = j + g;
% Print the frequency dependant data
for g=1:m
    tblData{j+g,i} = alignCell(num2str(energyData.Values(g),'%.1f'),w);
end

% Print data for editing (without HTML SHIT) (in numbers)
j=1; i=3;
col_head{i,1} = 'Edited | Subtracted | Values';
tblData{j,i} = colorCell('',colorEnergyHEX);        j=j+1;
tblData{j,i} = '0';                                 j=j+1;
tblData{j,i} = energyData.DataType;                 j=j+1;
tblData{j,i} = num2str(energyData.SNR,'%.1f');
% Check the case for printing C-values to the table
if any(dbType==[1,3])
    for g=1:8
        tblData{j+g,i} = num2str(energyData.C(g),'%.0f');
    end
else
    for g=1:2
        tblData{j+g,i} = num2str(energyData.C(g),'%.0f');
    end
end
% Update the j value
j = j + g;
% Print the frequency dependant data
for g=1:m
    tblData{j+g,i} = num2str(energyData.Values(g),'%.1f');
end

%% %%%%%%%%%%%%%%%%%%%%% Print columns with original data %%%%%%%%%%%%%%%%
% Go through the original data
for i=1+di:n+di
    % Retrieve the datatype
    dbType = CompDatasetSelection(i-di,1);
    % Specify the DB
    DB = Data.(DBType{dbType});
    
    % Retrieve the indx
    indx = CompDatasetSelection(i-di,2);
    % Retrieve a string with the column width
    w = num2str(col_width{1,i},'%.0f');
    % Retrieve the report name and split it for better rendering
    col_head{i,1} = strSplit(DB.Report{indx,1},13,'|');
    
    % Initialize row count
    j = 1;
    % Define the plot color
    tblData{j,i} = colorCell('',colorsHEX(i-di,:));              j=j+1;
    % Define a plot ID
    tblData{j,i} = alignCell(num2str(i-di,'%.0f'),w);            j=j+1;
    % Retrieve the data type
    tblData{j,i} = alignCell(DB.DataType{indx,1},w);             j=j+1;
    % Retrieve the SNR
    tblData{j,i} = alignCell(num2str(DB.SNR(indx),'%.1f'),w);        
    % Retrieve the C values
    if any(dbType==[1,3])
        for k=1:8
            tblData{j+k,i} = alignCell(num2str(DB.C(indx,k),'%.0f'),w);
        end
    else
        for k=1:2
            tblData{j+k,i} = alignCell(num2str(DB.C(indx,k),'%.0f'),w);
        end
    end
    % Update the j-value
    j = j+k;
    % Retrieve the TL / NISPL values
    if any(dbType==[1,3])
        TL = DB.TL(indx,:);
        % Print the TL values to the table
        for k=1:m
            tblData{j+k,i} = alignCell(num2str(TL(k),'%.1f'),w);
        end
    else
        NISPL = DB.NISPL(indx,:);
        % Print the NISPL values to the table
        for k=1:m
            tblData{j+k,i} = alignCell(num2str(NISPL(k),'%.1f'),w);
        end
    end
end

% Put data in the table
tbl.ColumnName = col_head;
tbl.ColumnWidth = col_width;
tbl.RowName = [];
tbl.Data = tblData;
% Make the third column available for editing
col_Edit = false(1,n+di);
col_Edit(3) = true;
tbl.ColumnEditable = col_Edit;

% Set the table to visible
tbl.Visible = 'on';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hold the axes
hold(ax,'on');
% Initialize the legend handle and string
leg_h = gobjects(2*(n+1),1);
leg_s = cell(2*(n+1),1);
leg_b = false(2*(n+1),1);

%% Plot the subtracted data
% Select the correct database from where to draw the data
if any(dbType==[1,3])
   % Airborne Data
   yDataField = 'TL';
   yLabel = 'Schalldammmass TL [dB]';
   lgn_pos = 'northwest';
elseif dbType==2
   % Impact Data
   yDataField = 'NISPL';
   yLabel = 'Trittschallpegel NISPL [dB]';
   lgn_pos = 'northeast';
end
% Retrieve the x-values and y-values
x = DB.MeasFreq;
y_ene = energyData.Values;
% Plot the data
h_ene = plot(ax,x,y_ene,'Color',colorEnergyRGB,...
                        'LineStyle','--','LineWidth',1.5,...
                        'Marker','x',...
                        'MarkerFaceColor','none',...
                        'MarkerEdgeColor',colorEnergyRGB);
% Prepare the legend
leg_h(1) = h_ene;
leg_s{1,1} = ['0 (',energyData.DataType,')'];
leg_b(1) = true;

%% Plot the original data
% Retrieve the important informations to be displayed in the plot
% Define the delta in indexing
di = 2;
% Go throught the original data
for i=1+di:n+di
    % Create new index
    j = i-di;
    % Retrieve the dbType 
    dbType = CompDatasetSelection(j,1);
    % Retrieve the correct database
    DB = Data.(DBType{dbType});
    % Retrieve the indx
    indx = CompDatasetSelection(j,2);
    % REtrieve hte color
    color = colorsRGB(j,:);
    % Retrieve the y-values 
    y = DB.(yDataField)(indx,:);
    % Retrieve the flag boolean
    flag = any(DB.Flag(indx,:,:),3);
    % Plot the data
    h = plot(ax,x,y,'Color',color,...
                    'LineStyle','-','Marker','none');
    % Prepare the legend
    leg_h(2*j+1) = h;
    leg_s{2*j+1,1} = [num2str(j,'%.0f'),' (',DB.DataType{indx,1},')'];
    leg_b(2*j+1) = true;
    
    % Plot the flags if necessary
    if any(flag)
        h = plot(ax,x(flag),y(flag),'MarkerEdgeColor',color,...
                                    'MarkerFaceColor',color,...
                                    'Marker','x',...
                                    'LineStyle','none');
        leg_h(2*j+2) = h;
        leg_s{2*j+2,1} = [num2str(j,'%.0f'),' (Flag)'];
        leg_b(2*j+2) = true;
    end
end

% Set automatic axes limits for y-axis
ax.YLimMode = 'auto';
ti = getTicks(ax.YLim,10);
ax.YTick = ti;
ax.YLim = [min(ti),max(ti)];

% Calculate the flags for the energy subtracted data and place them on the
% bottom of the y-axis
ymin = min(ti);
flag = or(isnan(y_ene),isinf(y_ene));
if any(flag)
    % Calculate where to plot the flag
    flag = flag-1+ymin;
    % Plot the flags
    h_ene_flag = plot(ax,x,flag,'Color',colorEnergyRGB,...
                            	'LineStyle','none','Marker','o',...
                                'MarkerFaceColor','none',...
                                'MarkerEdgeColor',colorEnergyRGB);
    % Prepare the legend
    leg_h(2) = h_ene_flag;
    leg_s{2,1} = '0 (Flag)';
    leg_b(2) = true;
end

% Place the legend
legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
                                    'Tag','EnergyLEG',...
                                    'Location',lgn_pos);
                    
% Enable the gridlines
ax.XGrid = 'on';
ax.YGrid = 'on';
% Specify the scale
ax.XScale = 'log';
% Specify the limits of the x-axis
ax.XLim = [min(x), max(x)];
% Specify the x-ticks
ax.XTick = x(2:3:end);
% Specify the x-label and y-label
ax.XLabel.String = 'Frequenz f [Hz]';
ax.YLabel.String = yLabel;
% Unhold the axes and set them to visible
hold(ax,'off');
ax.Visible = 'on';
% Put the subtraction plot on top
uistack(h_ene,'top');

% Set the zoom property
z = zoom(tab.Parent.Parent);
setAllowAxesZoom(z,ax,true);

% Show the export button
but.Visible = 'on';

%% Set the focus on the selected tab to averaging
% Find the tab group
tabParent = findobj('Tag','topCompTAB');
% Set the selected value to the averaging tab
tabParent.SelectedTab = tab; 

%% %%%%%%%%%%%% STORE DATA TO GLOBAL VARIABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The global variable CompareDatasetsData has three fields, one for averaged
% one for energy subtraction data and one for level subtraction data
% Store the energy subtraction data under the EnergySubtraction field

% Copy the avgData to the data
data = energyData;

% Add the IDs of the datasets used
data.IDs = CompDatasetSelection;
% Store the Messungstype Type-ID
if any(dbType==[1,3])
    data.TypeID = 1;    % Luftschall
else
    data.TypeID = 2;    % Trittschall
end
% Store the OriginalDB-ID
data.OriginalDBID = getOriginalDBID(CompDatasetSelection,'E');

% Store the data under the global variable
CompareDatasetsData.EnergySubtraction = data;

end
