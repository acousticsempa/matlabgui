function calcHorizSituation(src,~)

% Function is responsible for populating the fSituationPlots graphs with
% the correct data and storing the data for an eventual export to Excel
% 

%% %%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fCalcHorizSituation
global fSituationPlots
% Data
global Ubertragung
global VUbertragung
global Data
global ExportSituation

%% %%%%%%%%%%%%%%%%%%%%%%%% RETRIEVE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find all objects in the figure
obj = fCalcHorizSituation.Children;

% Some data inputted in the fCalcHorizSituation figure is readily available
% under a special structure: 
% Bauteile:     .Bauteile:      1x9 vector identifying the Bauteile 
%                               used in the construction of the vertical
%                               situation
% Anisotropie:  .Anisotropie:   1x9 vector identifying the Anisotropie of
%                               the Bauteile used in the construction of
%                               the vertical situation
% Vorsatz:      .Vorsatz:       1x10 vector identifying the Vorsatzschalen 
%                               used in the construction of the vertical
%                               situation
% Knoten:       .KnotenID:      1x4 vector identifying the Knotens used in
%                               the construction of the vertical situation
% Partition:    .KnotenBauteilID:   1x6 vector identifying the orientation 
%                               of the Knoten in the horizontal situation, 
%                               useful when looking for Ubertragungen and 
%                               VUbertragungen
data = fCalcHorizSituation.Data;

% Retrieve geometry
lx = findobj(obj,'Tag','LxTXT');
lz = findobj(obj,'Tag','LzTXT');
FRLy = findobj(obj,'Tag','FRLyTXT');
SRLy = findobj(obj,'Tag','SRLyTXT');
lx = str2double(lx.String);
lz = str2double(lz.String);
FRLy = str2double(FRLy.String);
SRLy = str2double(SRLy.String);

% Calculate the area of the partition
A = lx*lz;

% Character array
space = char(32*ones(1,5));
tabs = char([10,space]);

%% %%%%%%%%%%%%%%%%%%%%%%%%%% LS / TS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check if the caller is the Luftschall or Trittschall button and define 
% certain variables depending on that
if strcmpi(src.Tag,'LuftschallBUT')
    % Define the maximum number of Knoten
    kmax = 4;
    % Maximum number of pathways is 4: Dd, Df, Fd, Ff
    pmin = 1;
    pmax = 4;
    % Type ID in Ubertragung is 1 and in VUbertragung is 1 for Start and 1
    % for End side
    TypeID(1:2) = [1,1];
    % Sign of operation when considering Vorsatzschalen
    VS = 1;
    % Define boolean for flow control
    Luftschall = true;
    % Case string
    CaseString = 'Luftschall';
    % Define the cell size
    cS = 4;
else
    % Define maximum number of Knoten 
    kmax = 1;
    % Maximum number of pathways is 2: Fd, Ff (and fD, fF)
    pmin = 2;
    pmax = 5;
    % Type ID in Ubertragung is 2 and in VUbertragung is 2 for Start and 1
    % for End side
    TypeID(1:2) = [2,1];
    % Sign of operation when considering Vorsatzschalen
    VS = -1;
    % Define boolean for flow control
    Luftschall = false;
    % Case string
    CaseString = 'Trittschall';
    % Define the cell size
    cS = 5;
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% PATHWAYS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The matrix represent for each Knoten (on rows) the following pathways:
%   Dd:     Direkubertragung Trenndecke
%   Df:     Flankenübertragung Decke-Wand-Pfad
%           --> Becomes fD in case of Trittschall
%   Fd:     Flankenübertragung Wand-Decken-Pfad
%   Ff:     Flankenübertragung Wand-Wand-Pfad
%           
%   fF:     Only for Trittschall

% Initialize pathways
pathways = zeros(4,cS);

% Retrieve the Dd - Ubertragung
pathways(:,1) = get(findobj(obj,'Tag','DdCHE'),'Value');

% Retrieve the Df, Fd, Ff - Ubertragungen for all Knoten 
for i=1:4
    str = ['Knoten',num2str(i,'%.0f')];
    pathways(i,2) = get(findobj(obj,'Tag',[str,'DfCHE']),'Value');
    pathways(i,3) = get(findobj(obj,'Tag',[str,'FdCHE']),'Value');
    pathways(i,4) = get(findobj(obj,'Tag',[str,'FfCHE']),'Value');
end

% Transform it into a logical matrix
pathways = (pathways == 1);

% Check if the Bauteile for the selected Pathways are present, if they are
% not present, the pathways is to be considered as not to be considered
% Trennwand
if data.Bauteile(5)==0
    pathways(:,[1,2,3]) = false;  % Dd,Df,Fd - Ubertragungen are impossible
end
% First Room Wande / Ceiling / Floor
b = (data.Bauteile(1:4) == 0);
if any(b)
    pathways(b,[3,4]) = false;    % Fd,Ff - Ubertragungen are impossible
end
% Second Room Wande / Ceiling / Floor
b = (data.Bauteile(6:9) == 0);
if any(b)
    pathways(b,[2,4]) = false;    % Df,Ff - Ubertragungen are impossible
end
% Knoten 
b = (data.KnotenID == 0); 
if any(b)
    pathways(b,:) = false;      % Dd,Df,Fd,Ff - Ubertragungen are 
                                % impossible if no Knoten has been selected
end

% Logic to be performed when calculating Trittschall
if ~Luftschall
    % Block the Dd all Knoten because not possible
    pathways(:,1) = false;
    % Block the pathways for Knotens 2-4, because not feasible
    pathways(2:4,:) = false;
    % Add the fF - pathway
    pathways(1,5) = pathways(1,4);
end

% Check if we have any pathway to calculate, if not just return
PW = pathways(:,pmin:pmax);
if ~any(PW(:))
    errordlg(['No',32,CaseString,' pathways selected for calculation!',10,...
              'Please select at least one',32,CaseString,' pathway to proceed.'],...
             ['No',32,CaseString,' pathways selected - Error'],'modal');
    return;
end

% Find out which VUpathways are requested
% The matrix represent for each Knoten (on rows) the following pathways:
%   Dd:     Direkubertragung Trenndecke
%   Df:     Flankenübertragung Decke-Wand-Pfad
%   Fd:     Flankenübertragung Wand-Decken-Pfad
%   Ff:     Flankenübertragung Wand-Wand-Pfad
% Furthermore on the 3-dimension we see store the start and end
% VorsatzUbertragung for a given pathway
VUpathways(:,:,1) = pathways;
VUpathways(:,:,2) = pathways;

% Vorsatzschalen Wände First Room
b = (data.Vorsatz(1:4) == 0);
if any(b)
    VUpathways(b,[3,4],1) = false;  % Fd,Ff don't have a Start Vorsatz
end
% Vorsatzschale Trennwand First Room
b = (data.Vorsatz(5)==0);
if b
    VUpathways(:,[1,2],1) = false;  % Dd,Df don't have a Start Vorsatz
end
% Vorsatzschale Trennwand Second Room
b = (data.Vorsatz(6)==0);
if b
    VUpathways(:,[1,3],2) = false;  % Dd, Fd don't have an End Vorsatz
end
% Vorsatzschalen Wände Second Room
b = (data.Vorsatz(7:10)==0);
if any(b)
    VUpathways(b,[2,4],2) = false;  % Df,Ff don't have an End Vorsatz
end

% Trittschall dependant logic for the fD + fF pathways
if ~Luftschall
    x = VUpathways(:,2,1);
    VUpathways(:,2,1) = VUpathways(:,2,2);
    VUpathways(:,2,2) = x;
    VUpathways(:,5,1) = VUpathways(:,4,2);
    VUpathways(:,5,2) = VUpathways(:,4,1);
end

%% %%%%%%%%%%%%%%%%%%%%%% FIND UBERTRAGUNGEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that all the necessary Ubertragungen are present in the database
% and throw a warning to the user if they are not present

% Mapping of Bauteil selection to Ubertragungen in the database for the
% different Knoten
% Configuration:    [Startraum, Endraum, Startbauteil, Endbauteil]
keys = cell(4,1);
Configurations = cell(4,cS);
keys([1,3]) = {[1;3]};
keys([2,4]) = {[1,2; 2,1;    2,3; 3,2;    3,4; 4,3;    1,4; 4,1]};
% Knoten 1 - Configurations
Configurations{1,1} = [1,2,2,2; 2,1,2,2];    % Dd - Configurations
Configurations{1,2} = [1,2,2,3; 2,1,2,1];    % Df - Configurations
Configurations{1,3} = [1,2,1,2; 2,1,3,2];    % Fd - Configurations
Configurations{1,4} = [1,2,1,3; 2,1,3,1];    % Ff - Configurations
% Knoten 3 - Configurations              
Configurations{3,1} = [4,3,4,4; 3,4,4,4];   % Dd - Configuration
Configurations{3,2} = [4,3,4,3; 3,4,4,1];   % Df - Configuration
Configurations{3,3} = [4,3,1,4; 3,4,3,4];   % Fd - Configuration
Configurations{3,4} = [4,3,1,3; 3,4,3,1];   % Ff - Configuration
% Knoten 2 /4 - Configurations
D = [1,1; 1,2;    2,2; 2,3;    3,3; 3,4;    4,1; 4,4];
d = [4,1; 2,2;    1,2; 3,3;    2,3; 4,4;    1,1; 3,4];
F = [1,2; 1,1;    2,3; 2,2;    3,4; 3,3;    4,4; 4,1];
f = [4,4; 2,3;    1,1; 3,4;    2,2; 4,1;    1,2; 3,3];
Dr = D(:,1);    Db = D(:,2);
dr = d(:,1);    db = d(:,2);
Fr = F(:,1);    Fb = F(:,2);
fr = f(:,1);    fb = f(:,2);
Configurations([2,4],1) = {[Dr,dr,Db,db]};     % Dd - Configuration
Configurations([2,4],2) = {[Dr,fr,Db,fb]};     % Df - Configuration
Configurations([2,4],3) = {[Fr,dr,Fb,db]};     % Fd - Configuration
Configurations([2,4],4) = {[Fr,fr,Fb,fb]};     % Ff - Configuration

% Trittschall - Knoten 1
% Invert:   Df - configuration
% Store:    fF - configuration additionally
if ~Luftschall
    conf = Configurations{1,2};
    Configurations{1,2} = [conf(:,2),conf(:,1),conf(:,4),conf(:,3)];
    conf = Configurations{1,4};
    Configurations{1,5} = [conf(:,2),conf(:,1),conf(:,4),conf(:,3)];
end

% Define Lange value for the possible pathways
pathwayLength = zeros(4,cS);
pathwayLength([1,3],2:4) = lx;
pathwayLength([2,4],2:4) = lz;
if ~Luftschall
    pathwayLength(1,5) = lx;
end

% We should retrieve all the Ubertragungen configurations from the database
% Initialize cell-arrays to store the Ubertragunge-Values and the
% Ubertragung-ID
Uvalues = cell(4,cS);
uIDs = zeros(4,cS);
Uarea = zeros(4,cS);
Ulength = zeros(4,cS);
missing = [];
% Loop over the Knoten
for k=1:kmax
    % Check if we have selected the Knoten
    if data.KnotenID(k)~=0
        % Find the KnotenID, the Bauteil configuration
        kID = data.KnotenID(k);
        if mod(k,2)==1
            bId = data.KnotenBauteilID(k);
        else
            bId = [data.KnotenBauteilID(k), data.KnotenBauteilID(k+1+mod(k,3))];
        end
        % Repeat for all the possible pathways
        for p=pmin:pmax
            % Search for the configuration only if necessary
            if pathways(k,p)
                % Calculate boolean array for indexing configurations
                b = ismember(keys{k},bId,'rows');
                % Calculate the configuration
                config = [kID,TypeID(1),Configurations{k,p}(b,:)];
                % Find the Ubertragung ID 
                % DirectionIndipendent:     For Luftschall:     True
                %                           For Trittschall:    False
                uID = getUbertragung(config,Luftschall);
                % Check if we have a sole match
                bU = (Ubertragung.ID == uID);
                if sum(bU)==1
                    Uvalues{k,p} = Ubertragung.Values(bU,:);
                    Uarea(k,p) = Ubertragung.Area(bU);
                    Ulength(k,p) = Ubertragung.Length(bU);
                    uIDs(k,p) = uID;
                else
                    missing = [missing; config];
                end
            end
        end
    end
end
        
% Check if we have any missing configuration
if ~isempty(missing)
    % Reduced not found configurations to the unique ones
    missing = unique(missing,'rows','stable');
    % Format the data for the dropdown-Names function
    uber.ID = zeros(size(missing,1),1);
    uber.Knoten = missing(:,1);
    uber.TypeID = missing(:,2);
    uber.Raume = missing(:,[3,4]);
    uber.Bauteile = missing(:,[5,6]);
    dd = dropdownNames(uber,'Ubertragung');
    % Throw the error to the user: 
    errordlg(['The following Ubertragungen are missing from the database',...
              ' in order to perform the desired calculations:',10,tabs,...
              strjoin(dd,tabs),10,10,...
              'Please add these Ubertragungen to the database to proceed.'],...
              'Missing Ubertragungen - Error');
    return;
end

% Check that all the values for the Dd-Ubertragung are equal, We don't need 
% to control for missing Ubertragungen since we already have done this
Dd_values = Uvalues(pathways(:,1),1);
Dd_values = cell2mat(Dd_values);
if ~isempty(Dd_values)
    % Find the unique rows in the Dd_vals array
    [Dd_unique,~,indxInUnique] = unique(Dd_values,'rows','stable');
    % Throw error if we have more than a unique Dd-Ubertragung
    if size(Dd_unique,1)~=1
        % Retrieve the dropdown names
        uID = uIDs(pathways(:,1),1);
        [~,pos] = ismember(uID,Ubertragung.ID);
        % Iterate over the groups
        str = '';
        % Retrieve the corresponding dropdown names and group teh names if
        % they have equal values, so that we know how the error is
        % distributed
        for i=1:max(indxInUnique)
            b = (indxInUnique == i);
            posX = unique(pos(b),'stable');
            str = [str,space,strjoin(Ubertragung.Dropdown(posX),tabs),10,10];
        end
        % Throw the error
        errordlg(['Dd-Ubertragungen for the selected Knotens are',...
                  ' not equal. There are a total of',32,...
                  num2str(size(Dd_unique,1),'%.0f'),' different sets',...
                  ' of Ubertragung Values. The Ubertragungen are grouped',...
                  ' as follows:',10,10,str,...
                  'Please correct the values, so that all the above',...
                  ' mentioned Ubertragungen Values are identical.'],...
                  'Ubertragungen not identical - Error','modal');
        return;
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%% FIND VUBERTRAGUNGEN %%%%%%%%%%%%%%%%%%%%%%%%%%
% Here we need to find the VUbertragungen associated with the different
% Bauteile used in the Vert-Situation and which on the Anisotropie-ID is
% valid for used in this case

% Create a matrix mapping which Bauteil is needed to retrieve the Bauteil 
% anisotropie for every possibly needed VUbertragung
% Dd, Df, Fd, Ff
bauteilVUbertragung = zeros(4,cS,2);
bauteilVUbertragung(:,1:4,1) = [5, 5, 1, 1;...
                                5, 5, 2, 2;...
                                5, 5, 3, 3;...
                                5, 5, 4, 4];
bauteilVUbertragung(:,1:4,2) = [5, 6, 5, 6;...
                                5, 7, 5, 7;...
                                5, 8, 5, 8;...
                                5, 9, 5, 9];
% Create matrix mapping which Vorsatz is needed in the pathway
vorsatzVUbertragung = bauteilVUbertragung;
vorsatzVUbertragung(:,:,2) = vorsatzVUbertragung(:,:,2) + 1;

% Modify the bauteilVUbertraung and vorsatzVUbertragung for Trittschall
% to add the fF pathway and modify the Df pathway
if ~Luftschall
   x = bauteilVUbertragung(:,2,1);
   bauteilVUbertragung(:,2,1) = bauteilVUbertragung(:,2,2);
   bauteilVUbertragung(:,2,2) = x;
   bauteilVUbertragung(:,5,1) = [6,7,8,9]';
   bauteilVUbertragung(:,5,2) = [1,2,3,4]';
   
   x = vorsatzVUbertragung(:,2,1);
   vorsatzVUbertragung(:,2,1) = vorsatzVUbertragung(:,2,2);
   vorsatzVUbertragung(:,2,2) = x;
   vorsatzVUbertragung(:,5,1) = [7,8,9,10]';
   vorsatzVUbertragung(:,5,2) = [1,2,3,4]';
end

% Create mapping from Bauteil Anisotropie to Anisotropie-ID in VUbertragung
% for each possibly needed VUbertragung
d = 2;      % Default Anisotropie-ID when Bauteil has Anisotropie = 0

% Initialize mapping matrix
mapping = cell(4,cS,2,2);

% Knoten 1/3 - Start side / End side
k=[1,3]; s=1;
mapping(k,1,s,1) = {[0,1,3]};       % Trennwand FR - Dd path
mapping(k,1,s,2) = {[3,3,3]};
mapping(k,2,s,1) = {[0,1,3]};       % Trennwand FR - Df path
mapping(k,2,s,2) = {[d,2,1]};
mapping(k,[3,4],s,1) = {[0,1,2]};   % Boden/Decke FR - Fd + Ff paths
mapping(k,[3,4],s,2) = {[d,2,1]};
s=2;
mapping(k,1,s,1) = {[0,1,3]};       % Trennwand SR - Dd path
mapping(k,1,s,2) = {[3,3,3]};
mapping(k,3,s,1) = {[0,1,3]};       % Trennwand SR - Fd path
mapping(k,3,s,2) = {[d,2,1]};
mapping(k,[2,4],s,1) = {[0,1,2]};   % Boden/Decke SR - Df + Ff paths
mapping(k,[2,4],s,2) = {[d,2,1]};

% Knoten 2/4 - Start side / End side
k=[2,4]; s=1;
mapping(k,1,s,1) = {[0,1,3]};       % Trennwand FR - Dd path
mapping(k,1,s,2) = {[3,3,3]};
mapping(k,2,s,1) = {[0,1,3]};       % Trennwand FR - Df path
mapping(k,2,s,2) = {[d,1,2]};
mapping(k,[3,4],s,1) = {[0,2,3]};   % Wand 2/4 FR - Fd + Ff paths
mapping(k,[3,4],s,2) = {[d,1,2]};
s=2;
mapping(k,1,s,1) = {[0,1,3]};       % Trennwand SR - Dd path
mapping(k,1,s,2) = {[3,3,3]};
mapping(k,3,s,1) = {[0,1,3]};       % Trennwand SR - Fd path
mapping(k,3,s,2) = {[d,1,2]};
mapping(k,[2,4],s,1) = {[0,2,3]};   % Wand 2/4 SR - Df + Ff paths
mapping(k,[2,4],s,2) = {[d,1,2]};

% Trittschall special case
if ~Luftschall
    % Switch the cases for the Knoten 1 Df - pathway
    x = mapping(1,2,1,:);
    y = mapping(1,2,2,:);
    mapping(1,2,1,:) = y;
    mapping(1,2,2,:) = x;
    % Define mapping for the fF pathway
    mapping(1,5,[1,2],1) = {[0,1,2]};
    mapping(1,5,[1,2],2) = {[d,2,1]};
end

% Initialize storage of VUbertragung values and storage of IDs
VUvalues = cell(4,cS,2);
vuIDs = zeros(4,cS,2);

% Prepare the valid VUbertragung configuration 
ValidVUbertragung = [VUbertragung.Vorsatz, VUbertragung.TypeID,...
                     VUbertragung.Anisotropie];
% Initialize missing configurations
missing = [];

% Go through all the VUbertragungen and retrieve the values
% Iterate over Knoten
for k=1:kmax
    % Iterate over Pathways
    for p=pmin:pmax
        % Iterate over Start / End side
        for s=1:2
            % Check if we need this VUbertragung
            if VUpathways(k,p,s)
                % Retrieve the Bauteil Anisotropie
                aniso = data.Anisotropie(bauteilVUbertragung(k,p,s));
                % Find the Anisotropie-ID of the VUbertragung through the 
                % mapping
                b = (aniso == mapping{k,p,s,1});
                anisoID = mapping{k,p,s,2}(b);
                % Retrieve Vorsatz ID
                vID = data.Vorsatz(vorsatzVUbertragung(k,p,s));
                % Look for this VorsatzUbertragung configuration
                % in the database
                config = [vID,TypeID(s),anisoID];
                [b,pos] = ismember(config,ValidVUbertragung,'rows');
                % Check if we have a unique match
                if b
                    VUvalues{k,p,s} = VUbertragung.Values(pos,:);
                    vuIDs(k,p,s) = VUbertragung.ID(pos);
                else
                    missing = [missing; config];
                end
            end
        end
    end
end

% Check if we have any missing
if ~isempty(missing)
    % Reduce the missing configurations to the unique ones
    missing = unique(missing,'rows','stable');
    % Format the data for the dropdown-Names function
    vuber.ID = zeros(size(missing,1),1);
    vuber.Vorsatz = missing(:,1);
    vuber.TypeID = missing(:,2);
    vuber.Anisotropie = missing(:,3);
    dd = dropdownNames(vuber,'VUbertragung');
    % Throw the error to the user: 
    errordlg(['The following VUbertragungen are missing from the database',...
              ' in order to perform the desired calculations:',10,tabs,...
              strjoin(dd,tabs),10,10,...
              'Please add these VUbertragungen to the database to proceed.'],...
              'Missing VUbertragungen - Error');
    return;
end

%% %%%%%%%%%%%%%%%%%%%%%% CALCULATE VALUES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize the cell array for storing transmission values for all the
% pathways
values = cell(4,cS+1);

% Calculate Ubertragung values for each selected pathway (considering the
% Vorsatzschalen)
% Iterate over Knoten
for k=1:kmax
    % Iterate over pathways
    for p=pmin:pmax
        % Check if the pathway is being considered
        if pathways(k,p)
            % Retrieve the values for the Ubertragung
            uber = Uvalues{k,p};
            % Retrieve the values for the Vorsatzschale Start
            if VUpathways(k,p,1)
                uber = uber + VS*VUvalues{k,p,1};
            end
            % Retrieve the values for the Vorsatzschale End
            if VUpathways(k,p,2)
                uber = uber + VS*VUvalues{k,p,2};
            end
            % Perform the length and area scaling only for pathways over
            % the Flanken!!
            if p>1
                % Correct for length and area
                % CHECK: Control that length and area scaling are
                %        giving correct values
                uber = uber + VS*10*log10(Ulength(k,p)/pathwayLength(k,p)) +...
                            + VS*10*log10(A/Uarea(k,p));
            end
            % Store the values 
            values{k,p} = uber;
        end
    end
end

% Calculate the overall transmission values of a Knoten by summing up the 
% single transmission values
if Luftschall
    for k=1:kmax
        % First check if we have any values to calculate the transmission
        % TODO: Only Df, Fd, Ff are considered for calculation of Knoten
        %       transmission values of Luftschall
        %       Only Df are considered for calculation of Knoten
        %       transmission values of Trittschall
        b = pathways(k,2:pmax);
        if any(b)
            Kvalues = [];
            % Retrieve the values
            for i=1:numel(b)
                if b(i)
                    Kvalues = [Kvalues; values{k,i+1}];
                end
            end
            % Calculate the summed up transmission for the Knoten
            values{k,5} = -VS*10*log10(sum(10.^(-VS*0.1*Kvalues),1));
        end
    end
    
    % Calculate the sum of the transmission over the Flanken
    p = pathways(:,2:pmax);
    Fvalues = [];
    if any(p(:))
        for i=2:pmax
            if any(pathways(:,i))
                Fvalues = [Fvalues; cell2mat(values(:,i))];
            end
        end
        Fvalues = -VS*10*log10(sum(10.^(-VS*0.1*Fvalues),1));
    end
    
% Trittschall case
else
    % Check if we have the FX - transmission
    indx = [3,4];
    b = pathways(1,indx);
    if any(b)
        % Initialize LF_values
        LF_values = [];
        for i=1:numel(b)
            if b(i)
                LF_values = [LF_values; values{1,indx(i)}];
            end
        end
        % Calculate the summed up transmission for the FX-tranmissions
        LF_values = 10*log10(sum(10.^(0.1*LF_values),1));
    end
    % Check if we have a fX - transmission
    indx = [2,5];
    b = pathways(1,indx);
    if any(b)
        % Initialize Lf_values
        Lf_values = [];
        for i=1:numel(b)
            if b(i)
                Lf_values = [Lf_values; values{1,indx(i)}];
            end
        end
        % Calculate the summed up transmission for the FX-tranmissions
        Lf_values = 10*log10(sum(10.^(0.1*Lf_values),1));
    end
end

% Calculate the overall Ubertragung, by summing up single Knoten
% Ubertraugungen + the Dd
Svalues = [];
if ~isempty(Dd_values)
    Svalues = [Svalues; Dd_values];
end
Svalues = [Svalues; cell2mat(values(:,5))];
OverallUbertragung = -VS*10*log10(sum(10.^(-VS*0.1*Svalues),1));
      
%% %%%%%%%%%%%%%%%%%%%%%%%% PLOT AXES DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the children
obj = fSituationPlots.Children;

% Request the color map
colors = lines(7);

% Define the x-values
x = Data.Air.MeasFreq;

%%%%%%%%%%%%%%%%%%%%%%%%%%% LUFTSCHALL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Luftschall
    % Plot only the requested Knotens and the requested Ubertragungen
    for k=1:4
        % Find out what to print in the legend
        kstr = num2str(k,'%.0f');
        leg_s = {'R_{Dd}',['R_{',kstr,',Df}'],...
                ['R_{',kstr,',Fd}'],['R_{',kstr,',Ff}'],...
                ['R_',kstr,'''']};
        % Find the correct axes
        ax = findobj(obj,'Tag',['Knoten',kstr,'AX']);
        % Clear the axes
        delete(ax.Children(:));
        delete(ax.Legend);
        % Hold the axes
        hold(ax,'on');
        % Plot the different pathways
        leg_h = gobjects(1,5);
        leg_b = false(1,5);
        for p=1:4
            % Check if we want to consider this pathway
            if pathways(k,p)
                % Get the values and print them
                leg_h(p) = plot(ax,x,values{k,p},...
                                    'LineStyle','-',...
                                    'LineWidth',1,...
                                    'Color',colors(p,:));
                leg_b(p) = true;
            end
        end
        % Plot the overall pathway
        if any(pathways(k,2:pmax))
            leg_h(5) = plot(ax,x,values{k,5},'LineStyle','-',...
                                             'LineWidth',1,...
                                             'Color',colors(5,:));
            leg_b(5) = true;
        end
    
        % Cut the legend short of data we don't want
        legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
                'Box','off','Interpreter','tex');
    end

    % Plot the summary values
    ax = findobj(obj,'Tag','SummaryAX');
    delete(ax.Children(:));
    delete(ax.Legend);
    hold(ax,'on');
    leg_h = gobjects(1,7);
    leg_s = {'R_{Dd}','R_1''','R_2''','R_3''','R_4''','R_{Flank}','R'''};
    leg_b = false(1,7);
    % Plot the Dd-Ubertragung
    if any(pathways(:,1))
        indx = find(pathways(:,1),1);
        leg_h(1) = plot(ax,x,values{indx,1},'LineStyle','-',...
                                            'LineWidth',1,...
                                            'Color',colors(1,:));
        leg_b(1) = true;
    end
    % Plot the Knoten (Df, Fd, Ff - Ubertragungen)
    for k=1:4
        % Check if we have a value
        if any(pathways(k,2:pmax))
            leg_h(k+1) = plot(ax,x,values{k,5},'LineStyle','-',...
                                               'LineWidth',1,...
                                               'Color',colors(k+1,:));
            leg_b(k+1) = true;
        end
    end
    if ~isempty(Fvalues)
        leg_h(6) = plot(ax,x,Fvalues,'LineStyle','-',...
                                     'LineWidth',1,...
                                     'Color',colors(6,:));
        leg_b(6) = true;
    end
    % Plot the overall Ubertragung
    leg_h(7) = plot(ax,x,OverallUbertragung,'LineStyle','-',...
                                            'LineWidth',1,...
                                            'Color',colors(7,:));
    leg_b(7) = true;

    % Plot the legend
    legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
            'Box','off','Interpreter','tex');

    % In case the data in the fCalcHorizSituation has changed we need to
    % erase the Trittschall Graphs 
    if fCalcHorizSituation.Data.Changed
        ax = findobj(obj,'Tag','TrittschallAX');
        delete(ax.Children(:));
        delete(ax.Legend);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TRITTSCHALL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~Luftschall
    % Retrieve the axes where we need to plot the data
    ax = findobj(obj,'Tag','TrittschallAX');
    delete(ax.Children(:));
    delete(ax.Legend);
    hold(ax,'on');
    leg_h = gobjects(1,6);
    leg_s = {'L_{Fd}','L_{Ff}','L_{F}''',...
             'L_{fD}','L_{fF}','L_{f}'''};
    leg_b = false(1,6);
    % Plot the Fd and Ff-Ubertragung
    indx = [3,4];
    for i=1:2
        if pathways(1,indx(i))
            leg_h(i) = plot(ax,x,values{1,indx(i)},'LineStyle','-',...
                                                   'LineWidth',1,...
                                                   'Color',colors(i,:));
            leg_b(i) = true;
        end
    end
    % Plot the FX-Ubertragungen
    if exist('LF_values','var')==1
        leg_h(3) = plot(ax,x,LF_values,'LineStyle','-',...
                                       'LineWidth',1,...
                                       'Color',colors(3,:));
        leg_b(3) = true;
    end
    % Plot the fD and fF-Ubertragung
    indx = [2,5];
    di = 3;
    for i=1:2
        if pathways(1,indx(i))
            leg_h(i+di) = plot(ax,x,values{1,indx(i)},'LineStyle','-',...
                                                      'LineWidth',1,...
                                                      'Color',colors(i+di,:));
            leg_b(i+di) = true;
        end
    end
    % Plot the fX-Ubertragungen
    if exist('Lf_values','var')==1
        leg_h(6) = plot(ax,x,Lf_values,'LineStyle','-',...
                                       'LineWidth',1,...
                                       'Color',colors(6,:));
        leg_b(6) = true;
    end
    
    % Plot the legend
    legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
            'Box','off','Interpreter','tex');

    % In case the data in the fCalcHorizSituation has changed we need to
    % erase the Luftschall Graphs
    if fCalcHorizSituation.Data.Changed
        for k=1:4
            ax = findobj(obj,'Tag',['Knoten',num2str(k,'%.0f'),'AX']);
            delete(ax.Children(:));
            delete(ax.Legend);
        end
        ax = findobj(obj,'Tag','SummaryAX');
        delete(ax.Children(:));
        delete(ax.Legend);
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%% STORE GLOBAL DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Clean the global structure if something has changed and recreate the
% structure structure
if fCalcHorizSituation.Data.Changed
    ExportSituation = [];
    ExportSituation.Structure.Knoten = data.KnotenID;
    ExportSituation.Structure.KnotenBauteil = data.KnotenBauteilID;
    ExportSituation.Structure.Bauteile = data.Bauteile;
    ExportSituation.Structure.Vorsatz = data.Vorsatz;
    ExportSituation.Structure.Anisotropie = data.Anisotropie;
    ExportSituation.Geometry.Lx = lx;
    ExportSituation.Geometry.Ly = [FRLy,SRLy];
    ExportSituation.Geometry.Lz = lz;
    ExportSituation.Pathways = pathways;
end

% Collect data shared among Airborne and Impact
% Ubertragung data
expo.Ubertragung.Pathways = pathways;
expo.Ubertragung.ID = uIDs;
expo.Ubertragung.Values = Uvalues;
expo.Ubertragung.Area = Uarea;
expo.Ubertragung.Length = Ulength;
% VUbertragung data
expo.VUbertragung.Pathways = VUpathways;
expo.VUbertragung.ID = vuIDs;
expo.VUbertragung.Values = VUvalues;
% Calculated data
expo.Transmission.Pathways = pathways;
expo.Transmission.Values = values;
expo.Transmission.Total = OverallUbertragung;
if exist('Fvalues','var')
    expo.Transmission.Flanking = Fvalues;
end
if exist('LF_values','var')
    expo.Transmission.Flanking.Fx = LF_values;
end
if exist('Lf_values','var')
    expo.Transmission.Flanking.fX = Lf_values;
end

% Store under global variable
if Luftschall
    ExportSituation.Airborne = expo;
else
    ExportSituation.Impact = expo;
end
ExportSituation.Situation = 'Horizontal';

%% %%%%%%%%%%%%%%%%%%%%%%%% SHOW THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change the figure name
fSituationPlots.Name = 'Horizontal Situation - Luftschall + Trittschall';

% Change the labels
obj = fSituationPlots.Children;
lbl = findobj(obj,'Tag','TitleLBL');
lbl.String = 'Horizontal Situation - Luftschall + Trittschall';

% Show the figure
fSituationPlots.Visible = 'on';

%% %%%%%%%%%%%%%%%%%%%%%%%% SAVE SITUATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store the Changed as false, so that if the user does not modify anything
% we can keep both graphs (luftschall and trittschall)
fCalcHorizSituation.Data.Changed = false;

end
