function clearDatasets(src, callbackdata)
% Function removes all the datasets from the Selected Datasets list

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global DatasetSelection

% Check if the CompDatasetSelection variable is empty
if isempty(DatasetSelection)
    % Return
    return;
end

% Retrieve the value of the list box, highlighting which elements are
% currently selected
lstDSS = findobj('Tag','SelDatasetsLST');

% Update the global variable
DatasetSelection = [];

% Generate an empty listbox
lstDSS.String = ' ';
lstDSS.Max = 1;
lstDSS.Value = 1;

end

