function removeSet(src, callbackdata)
% Function removes the selected datasets from the For Comparison Selected
% Datasets

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global CompDatasetSelection

% Check if the CompDatasetSelection variable is empty
if isempty(CompDatasetSelection)
    % Return
    return;
end

% Retrieve the value of the list box, highlighting which elements are
% currently selected
lstCompDSS = findobj('Tag','ComparisonSelDatasetsLST');
selDS = lstCompDSS.Value; 

% Update the global variable
CompDatasetSelection(selDS,:) = [];

% Actualize the listbox
lst = getReports(CompDatasetSelection);

% Change the list in the list viewer
if numel(lst)==0
    % Generate an empty listbox
    lstCompDSS.String = ' ';
    lstCompDSS.Max = 1;
    lstCompDSS.Value = 1;
else
    % Place the string array, augment the maximum selection to the
    % maximum number and select the first value
    lstCompDSS.String = lst;
    lstCompDSS.Max = numel(lst);
    lstCompDSS.Value = 1;
end

end

