function shiftDownSet(src, callbackdata)
% Function shift the selected datasets in the 'For comparison selected
% datasets' down by 1 in the list

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global CompDatasetSelection

% Check if the CompDatasetSelection variable is empty
if isempty(CompDatasetSelection)
    % Return
    return;
end

%%%%%%%%%%%%%%%%% GET LIST PROPERTIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the value of the list box, highlighting which elements are
% currently selected
lstCompDSS = findobj('Tag','ComparisonSelDatasetsLST');

% Retrieve the number of elements in the list
n = numel(lstCompDSS.String);

%%%%%%%%%%%%%% NEW POSITION FOR SELECTED ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the position of the selected reports
posOld = lstCompDSS.Value;

% Shift the values up by one and correct zero indexes to 1
posShifted = min(n, posOld + 1); 

% Calculate the difference in positions betweeen the shifted elements
posDiff = diff(posShifted);

% Find the position of a zero element in the difference array
ZeroDiff = find(posDiff==0);
% Update only if its not empty
if ~isempty(ZeroDiff)
    % Update this difference to 1
    posDiff(ZeroDiff(end)) = 1;
end

% Check if we had a ZeroDiff element, and if we had it, we can shift 
% elements further up the line
if ~isempty(ZeroDiff)
    % Find the first element in the difference vector with a difference 
    % value larger than 1
    LargerDiff = find(posDiff > 1);
    % Update this value by reducing it by 1
    if ~isempty(LargerDiff)
        posDiff(LargerDiff(end)) = posDiff(LargerDiff(end)) + 1;
    end
end
    
% Calculate new positions using the difference array
cumDiff = cumsum(posDiff,'reverse');
posNew = posShifted(end) - [cumDiff,0];

%%%%%%%%%%%%%%% NEW POSITION FOR UNSELECTED ELEMENTS %%%%%%%%%%%%%%%%%%%%%
% Generate a complete index set
allPos = (1:1:n);

% Generate the difference between all and the selected old positions
unselPosOld = setdiff(allPos,posOld,'stable');

% Generate the difference between all and the selected new positions
unselPosNew = setdiff(allPos,posNew,'stable');

%%%%%%%%%%%%%% PLACE IN THE NEW ORDER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a temporary copy for working
tmp = CompDatasetSelection;

% Place the selected elements
tmp(posNew,:) = CompDatasetSelection(posOld,:);

% Place the unselected elements
tmp(unselPosNew,:) = CompDatasetSelection(unselPosOld,:); 

%%%%%%%%%%%%%% ACTUALIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Actualize the global variable
CompDatasetSelection = tmp; 

% Actualize the listbox
lst = getReports(CompDatasetSelection);

% Change the list in the list viewer
if numel(lst)==0
    % Generate an empty listbox
    lstCompDSS.String = ' ';
    lstCompDSS.Max = 1;
    lstCompDSS.Value = 1;
else
    % Place the string array, augment the maximum selection to the
    % maximum number and select the first value
    lstCompDSS.String = lst;
    lstCompDSS.Max = numel(lst);
    lstCompDSS.Value = posNew;
end

end
