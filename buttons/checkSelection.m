function [passed, indx, dataType] = checkSelection()

% Function is responsible for controlling that the current selection
% performed in the dropdown menus is valid, otherwise it throws an error

%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global dropLists

% Initialize variables
passed = false;
indx = 0;
dataType = 0;

%%%%%%%%%%%%%%%% CHECK THAT DROPDOWNS ARE CORRECTLY SELECTED %%%%%%%%%%%%
if ~isfield(dropLists.PrufObj,'SelectedIDs') || ...
   ~isfield(dropLists.Shielding,'SelectedIDs') || ...
   ~isfield(dropLists.Report,'SelectedIDs')
    errordlg('Please select a Report first!','Selection Error','modal');
    return;
end

% Retrieve the selected values in the dropdown menus
dd = findobj('Tag','DatatypeDD');
selectedDatatype = dd.Value;
dd = findobj('Tag','ProjectDD');
selectedProject = dd.Value;
dd = findobj('Tag','PrufObjDD');
selectedPrufObj = dropLists.PrufObj.SelectedIDs(dd.Value);
dd = findobj('Tag','ShieldingDD');
selectedShielding = dropLists.Shielding.SelectedIDs(dd.Value);
dd = findobj('Tag','ReportDD');
selectedReport = dropLists.Report.SelectedIDs(dd.Value);

% Create a boolean vector identifiying which report is being selected
% by the user with the current constellation of Project, PrufObj,
% Shielding and Report selection
selected = and(and(and(dropLists.Project.ID==selectedProject, ...
                       dropLists.PrufObj.ID==selectedPrufObj),...
                       dropLists.Shielding.ID==selectedShielding),...
                       dropLists.Report.ID==selectedReport);

% Check that only one element has been selected
if sum(selected) ~= 1
    % Print a dialog message where we notify the user that the current
    % selection selects more than just one dataset
    errordlg('Current specification for project selection selects more than 1 dataset! Please specify another project!',...
             'Multiple selected datasets','modal');
    return;
end

% Find the new index
indx = find(selected);

% Pass out the selected Datatype
dataType = selectedDatatype;

% Change the passed boolean to true
passed = true;
end
