function removeDataset(src, callbackdata)
% Function removes the selected datasets from the Selected Datasets list

%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global DatasetSelection

% Check if the CompDatasetSelection variable is empty
if isempty(DatasetSelection)
    % Return
    return;
end

% Retrieve the value of the list box, highlighting which elements are
% currently selected
lstDSS = findobj('Tag','SelDatasetsLST');
selDS = lstDSS.Value; 

% Update the global variable
DatasetSelection(selDS,:) = [];

% Actualize the listbox
lst = getReports(DatasetSelection);

% Change the list in the list viewer
if numel(lst)==0
    % Generate an empty listbox
    lstDSS.String = ' ';
    lstDSS.Max = 1;
    lstDSS.Value = 1;
else
    % Place the string array, augment the maximum selection to the
    % maximum number and select the first value
    lstDSS.String = lst;
    lstDSS.Max = numel(lst);
    lstDSS.Value = 1;
end

end

