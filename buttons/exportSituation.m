function exportSituation(~,~)

% Function takes care of exporting the data the user has selected in
% building a Vertical / Horizontal Situation to an Excel file of choosing. 
% The function copies a template Excel Worksheet into the file and renames
% it correspondingly

%% %%%%%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configurations
global Configs
% Data
global Knoten
global Bauteile
global Vorsatz
global VUbertragung
global ExportSituation
% Figures
global fSituationPlots
global fCalcVertSituation
global fCalcHorizSituation

%% %%%%%%%%%%%%%%%%%%%% FIND OUT SITUATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch lower(ExportSituation.Situation)
    case 'vertical'
        VertSituation = true;
        sit = 'Vertical';
        templateFile = Configs.Template.Situation.Vertical;
    case 'horizontal'
        VertSituation = false;
        sit = 'Horizontal';
        templateFile = Configs.Template.Situation.Horizontal;
end

% Check if we have Airborne and Impact data
if isfield(ExportSituation,'Airborne')
    Luftschall = true;
else
    Luftschall = false;
end
if isfield(ExportSituation,'Impact')
    Trittschall = true;
else
    Trittschall = false;
end

%% %%%%%%%%%%%%%%%%%%% SELECT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open a menu for selecting a file
[filename,path] = uigetfile('*.xlsx',['Select Excel file for exporting',...
                                      ' the',10,sit,' Situation']);

% If we have not selected a file, return
if isnumeric(filename)
    return;
end

% Compute the full path
ExcelFile = [path,filename];

%% %%%%%%%%%%%%%%%%%%% OPEN THE FILES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the file where we want to store the situation and the template file
[success,ex,exWB,~] = openExcelFile(ExcelFile,1);
% Vertical: 1, Horizontal: 2
[success2,~,exWB2,exSH2] = openExcelFile(templateFile,1,ex);

% Check if we can continue
if ~success || ~success2
    % Throw error message
    return;
end

% Number of sheets in the target workbook
n = exWB.Sheets.Count;

% Get the correct sheets
template = exSH2{1,1};
target = exWB.Sheets.Item(n);

% Retrieve the names of all other sheets in the workbook
shNames = cell(n,1);
for i=1:n
    shNames{i,1} = exWB.Sheets.Item(i).Name;
end

% Rename the copied worksheet by asking the user for a name
[SituationName,sheetName,success] = askSituationName(shNames);
if ~success
    closeExcel(ex,exWB,exWB2);
    return;
end
% Copy the worksheet over and rename it
template.Copy([],target);
target = exWB.Sheets.Item(n+1);
target.Name = sheetName;

%% %%%%%%%%%%%%%%%%%%%% COORDINATES IN EXCEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define coordinates of the data for the first cell of each block, then 
% define an offset matrix for calculating position where to store the data
% Format:   [Row, Column]
title = [1,5];
info = [4,2];
geom = [11,2];
knot = [11,8];
baut = [20,4];
path = [19,18];
air = [34,2];
imp = [35,24];
y = 35;
uberAir = [5,y];
uberImp = [22,y];
vuberAir = [33,y];
vuberImp = [51,y];
imag = [1,60];

%% %%%%%%%%%%%%%%%%%%%% PRINT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%% TITLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
setCell(target,title,SituationName);

%% %%%%%%%%%%%%%%%%%% GENERAL INFORMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i=0;
setCell(target,info+[i,0],Configs.User);                      i=i+1;
setCell(target,info+[i,0],datestr(now,'yyyy/mm/dd HH:MM'));      i=i+1;
setCell(target,info+[i,0],Configs.Version.Installed);

%% %%%%%%%%%%%%%%%%%%%%%%%%%% GEOMETRY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if VertSituation
    setCell(target,geom,      ExportSituation.Geometry.Lx);
    setCell(target,geom+[1,0],ExportSituation.Geometry.Ly);
    setCell(target,geom+[2,1],ExportSituation.Geometry.Lz(1));
    setCell(target,geom+[2,2],ExportSituation.Geometry.Lz(2));
else
    setCell(target,geom,      ExportSituation.Geometry.Lx);
    setCell(target,geom+[1,1],ExportSituation.Geometry.Ly(1));
    setCell(target,geom+[1,2],ExportSituation.Geometry.Ly(2));
    setCell(target,geom+[2,0],ExportSituation.Geometry.Lz);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% KNOTEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
typeIDMap = {'Kreuz','TStoss','Anders'};
ausMap = {'Vert','Horiz'};

% Create the list of partition Bauteil ID
if VertSituation
    partID = ExportSituation.Structure.KnotenBauteil;
    bautID = [4,4,4,4];
else
    partID = [2, ExportSituation.Structure.KnotenBauteil(2),...
              4, ExportSituation.Structure.KnotenBauteil(4)];
    bautID = ExportSituation.Structure.KnotenBauteil([1,5,3,6]);
end

% Print the data to the Excel file
for i=1:4
    kID = ExportSituation.Structure.Knoten(i);
    if kID~=0
        knotS = knot + [i-1,0];
        % Find a match in the database
        b = (Knoten.ID==kID);
        % Set the data
        setCell(target,knotS,kID);
        setCell(target,knotS+[0,1],typeIDMap{Knoten.TypeID(b)});
        setCell(target,knotS+[0,2],ausMap{Knoten.Ausrichtung(b)});
        setRange(target,knotS+[0,3],1,4,Knoten.Bauteile(b,:));
        setRange(target,knotS+[0,7],1,4,Knoten.Anisotropie(b,:));
        setCell(target,knotS+[0,11],partID(i));
        setCell(target,knotS+[0,12],bautID(i));
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% BAUTEILE / VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:10
    j = i - floor(i/6);
    bID = ExportSituation.Structure.Bauteile(j);
    if bID~=0
        bautS = baut + [i-1,0];
        setCell(target,bautS,bID);
        setCell(target,bautS+[0,1],ExportSituation.Structure.Anisotropie(j));
        setCell(target,bautS+[0,2],Bauteile.Name(Bauteile.ID==bID));
        
        vID = ExportSituation.Structure.Vorsatz(i);
        if vID~=0
            setCell(target,bautS+[0,6],vID);
            setCell(target,bautS+[0,7],Vorsatz.Name(Vorsatz.ID==vID));
        end
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%% PATHWAYS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:4
    for j=1:4
        p = ExportSituation.Pathways(i,j);
        if p
            setCell(target,path + [i-1,j-1],1*p);
        end
    end
end

%% %%%%%%%%%%%%%%%%%%%%%% AIRBORNE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Luftschall
    % Print the direct pathway
    p = ExportSituation.Pathways(:,1);
    if any(p)
        indx = find(p,1);
        vals = ExportSituation.Airborne.Transmission.Values{indx,1};
        printRL(target,air,vals,1);
    end
    % Iterate over the Knoten
    for i=1:4
        if ExportSituation.Structure.Knoten(i)~=0
            % Iterate over the pathways
            for j=2:4
                if ExportSituation.Pathways(i,j)
                    % Calculate new starting cell
                    airS = air + [0,(i-1)*3+j-1];
                    % Extract the values
                    vals = ExportSituation.Airborne.Transmission.Values{i,j};
                    % Calculate SNR % C-values
                    printRL(target,airS,vals,1);
                end
            end
        end
    end
    % Print the sum of the Knoten 
    for i=1:4
        if ExportSituation.Structure.Knoten(i)~=0 && any(ExportSituation.Pathways(i,2:4))
            airS = air+[0,12+i];
            % Extract the values
            vals = ExportSituation.Airborne.Transmission.Values{i,5};
            % Calculate SNR % C-values
            printRL(target,airS,vals,1);
        end
    end
    % Print the Overall Flanken
    if isfield(ExportSituation.Airborne.Transmission,'Flanking')
        airS = air+[0,17];
        vals = ExportSituation.Airborne.Transmission.Flanking;
        printRL(target,airS,vals,1);
    end
    % Print the Overall
    airS = air+[0,19];
    vals = ExportSituation.Airborne.Transmission.Total;
    printRL(target,airS,vals,1);
end

%% %%%%%%%%%%%%%%%%%%%%%%%% IMPACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Trittschall && VertSituation
    % Print the direct pathway
    p = ExportSituation.Pathways(:,1);
    if any(p)
        indx = find(p,1);
        vals = ExportSituation.Impact.Transmission.Values{indx,1};
        printRL(target,imp,vals,2);
    end
    % Iterate over the Knoten
    for i=1:4
        if ExportSituation.Pathways(i,2)
            impS = imp + [0,i];
            vals = ExportSituation.Impact.Transmission.Values{i,2};
            printRL(target,impS,vals,2);
        end
    end
    % Print the Overall Flanken
    if isfield(ExportSituation.Impact.Transmission,'Flanking')
        impS = imp + [0,5];
        vals = ExportSituation.Impact.Transmission.Flanking;
        printRL(target,impS,vals,2);
    end
    % Print the overall
    impS = imp + [0,7];
    vals = ExportSituation.Impact.Transmission.Total;
    printRL(target,impS,vals,2);
end

if Trittschall && ~VertSituation
   % Print the Fd, Ff - Pathways for Knoten 1
   indx = [3,4];
   for i=1:2
       p = ExportSituation.Pathways(1,indx(i));
       if p
           vals = ExportSituation.Impact.Transmission.Values{1,indx(i)};
           printRL(target,imp+[0,i-1],vals,2);
       end
   end
   % Print the fD, fF - Pathways for Knoten 1
   indx = [2,5];
   for i=1:2
       p = ExportSituation.Impact.Ubertragung.Pathways(1,indx(i));
       if p
           vals = ExportSituation.Impact.Transmission.Values{1,indx(i)};
           printRL(target,imp+[0,i+2],vals,2);
       end
   end
   % Print the Fx - for Knoten 1
   if isfield(ExportSituation.Impact.Transmission.Flanking,'Fx')
       vals = ExportSituation.Impact.Transmission.Flanking.Fx;
       printRL(target,imp+[0,2],vals,2);
   end
   % Print the fX - for Knoten 1
   if isfield(ExportSituation.Impact.Transmission.Flanking,'fX')
       vals = ExportSituation.Impact.Transmission.Flanking.fX;
       printRL(target,imp+[0,5],vals,2);
   end
end

%% %%%%%%%%%%%%%%%%%%%%%%% UBERTRAGUNG DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Anonymous function for retrieving and structuring the data correctly
getUber = @(field,i,j)([ExportSituation.(field).Ubertragung.ID(i,j),...
                        ExportSituation.(field).Ubertragung.Area(i,j),...
                        ExportSituation.(field).Ubertragung.Length(i,j),...
                        ExportSituation.(field).Ubertragung.Values{i,j}]);
                    
% Luftschall Ubertragungen
if Luftschall
    % Print the Dd - Ubertragung
    p = ExportSituation.Airborne.Ubertragung.Pathways(:,1);
    if any(p)
        indx = find(p,1);
        setRange(target,uberAir,1,24,getUber('Airborne',indx,1));
    end
    % Print the Df, Fd, Ff - Ubertragungen for each Knoten
    for i=1:4
        if ExportSituation.Structure.Knoten(i)~=0
            % Iterate over the pathways
            for j=2:4
                if ExportSituation.Airborne.Ubertragung.Pathways(i,j)
                    % Calculate new starting cell
                    uberAirS = uberAir + [(i-1)*3+j-1,0];
                    setRange(target,uberAirS,1,24,getUber('Airborne',i,j));
                end
            end
        end
    end 
end

% Trittschall Ubertragungen %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if Trittschall && VertSituation
    % Print the Dd - Ubertragung
    p = ExportSituation.Impact.Ubertragung.Pathways(:,1);
    if any(p)
        indx = find(p,1);
        setRange(target,uberImp,1,24,getUber('Impact',indx,1));
    end
    % Print the Df - Ubertragungen for each Knoten
    for i=1:4
        if ExportSituation.Impact.Ubertragung.Pathways(i,2)
            uberImpS = uberImp + [i,0];
            setRange(target,uberImpS,1,24,getUber('Impact',i,2));
        end
    end
end
if Trittschall && ~VertSituation
   % Print the Fd, Ff, fF, fD - Ubertragungen
   indx = [3,4,2,5];
   for i=1:4
       p = ExportSituation.Impact.Ubertragung.Pathways(1,indx(i));
       if p
           setRange(target,uberImp+[i-1,0],1,23,getUber('Impact',1,indx(i)));
       end
   end
end

%% %%%%%%%%%%%%%%%%%%%%%% VUBERTRAGUNG DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% Anonymous function for retrieving and structuring the data correctly
getVUber = @(field,i,j,k)(...
    [ExportSituation.(field).VUbertragung.ID(i,j,k),...
     VUbertragung.Anisotropie(VUbertragung.ID==ExportSituation.(field).VUbertragung.ID(i,j,k)),...
     ExportSituation.(field).VUbertragung.Values{i,j,k}]);
 
% Luftschall data
if Luftschall
    % Print the VUbertragung for the Bauteil Bottom/First Room
    if VertSituation    % Bottom Room
        indx = [2,4];   % Pathways to be considered (Df, Ff)
        side = 2;       % Ending side of pathway
    else                % First Room
        indx = [3,4];   % Pathways to be considered (Fd, Ff)
        side = 1;       % Starting side of pathway
    end
    for i=1:4
        % Check if there is a Vorsatzschale
        if ExportSituation.Structure.Vorsatz(i)
            p = ExportSituation.Airborne.VUbertragung.Pathways(i,indx,side);
            if any(p)
                j = indx(find(p,1));
                vuberAirS = vuberAir + [i-1,0];
                setRange(target,vuberAirS,1,23,getVUber('Airborne',i,j,side));
            end
        end
    end
    % Print the VUbertragung for the Partition Bottom/First Room
    if VertSituation   % Bottom Room
        indx = 3;      % Pathways to be considered (Fd)
    else               % First Room
        indx = 2;      % Pathways to be considered (Df)
    end
    if ExportSituation.Structure.Vorsatz(5)
        % Dd - Pathway
        j = 1;
        p = ExportSituation.Airborne.VUbertragung.Pathways(:,j,side);
        if any(p)
            i = find(p,1);
            setRange(target,vuberAir+[4,0],1,23,getVUber('Airborne',i,j,side));
        end
        % Flanken Pathway for Knoten 1/3 and Knoten 2/4
        k = [1,3; 2,4];     % Knoten to be considered
        for j=1:2
            p = ExportSituation.Airborne.VUbertragung.Pathways(k(j,:),indx,side);
            if any(p)
                i = k(j,find(p,1));
                setRange(target,vuberAir+[4+j,0],1,23,getVUber('Airborne',i,indx,side));
            end
        end
    end
    % Print the VUbertragung for the Partition Top/Second Room
    if VertSituation        % Top Room
        indx = 2;           % Pathways to be considered (Df)
        side = 1;           % Starting side
    else                    % Second Room
        indx = 3;           % Pathways to be considered (Fd)
        side = 2;           % Ending side
    end
    if ExportSituation.Structure.Vorsatz(6)
        % Dd - Pathway
        j = 1; 
        p = ExportSituation.Airborne.VUbertragung.Pathways(:,j,side);
        if any(p)
            i = find(p,1);
            setRange(target,vuberAir+[7,0],1,23,getVUber('Airborne',i,j,side));
        end
        % Flanken Pathway for Knoten 1/3 and Knoten 2/4
        k = [1,3; 2,4];
        for j=1:2
            p = ExportSituation.Airborne.VUbertragung.Pathways(k(j,:),indx,side);
            if any(p)
                i = k(j,find(p,1));
                setRange(target,vuberAir+[7+j,0],1,23,getVUber('Airborne',i,indx,side));
            end
        end
    end
    % Print the VUbertragung for the Bauteile Top/Second Room
    if VertSituation    % Top Room
        indx = [3,4];   % Pathways to be considered (Fd, Ff)
        side = 1;       % Starting side of pathway
    else                % Second Room
        indx = [2,4];   % Pathways to be considered (Df, Ff)
        side = 2;       % Ending side of pathway
    end
    for i=1:4
        % Check if there is a Vorsatzschale
        if ExportSituation.Structure.Vorsatz(i+6)
            p = ExportSituation.Airborne.VUbertragung.Pathways(i,indx,side);
            if any(p)
                j = indx(find(p,1));
                vuberAirS = vuberAir + [i+9,0];
                setRange(target,vuberAirS,1,23,getVUber('Airborne',i,j,side));
            end
        end
    end
end

% Trittschall Data
if Trittschall && VertSituation
    % Print the VUbertragung for the Vorsatzschale (6) Oben only if it
    % exists
    if ExportSituation.Structure.Vorsatz(6)
        % Print the Dd - Ubertragung
        side = 1;
        indx = 1;
        p = ExportSituation.Impact.VUbertragung.Pathways(:,indx,side);
        if any(p)
            i = find(p,1);
            setRange(target,vuberImp,1,23,getVUber('Impact',i,indx,side));
        end
        % Print the Df - Ubertragungen for Knoten 1/3 and Knoten 2/4
        indx = 2;           % Pathway Df to be considered
        k = [1,3; 2,4];  % Knoten to be considered
        for j=1:2
            p = ExportSituation.Impact.VUbertragung.Pathways(k(j,:),indx,side);
            if any(p)
                i = k(j,find(p,1));
                setRange(target,vuberImp+[j,0],1,23,getVUber('Impact',i,indx,side));
            end
        end
    end
    
    % Print the Receiver side VUbertragungen only if they have not been 
    % printed yet
    if ~Luftschall
        % Print the VUbertragung for the Bauteil Bottom/First Room
        indx = [2,4];   % Pathways to be considered (Df, Ff)
        side = 2;       % Ending side of pathway
        for i=1:4
            % Check if there is a Vorsatzschale
            if ExportSituation.Structure.Vorsatz(i)
                p = ExportSituation.Impact.VUbertragung.Pathways(i,indx,side);
                if any(p)
                    j = indx(find(p,1));
                    vuberAirS = vuberAir + [i-1,0];
                    setRange(target,vuberAirS,1,23,getVUber('Impact',i,j,side));
                end
            end
        end
        % Print the VUbertragung for the Partition Bottom/First Room
        if ExportSituation.Structure.Vorsatz(5)
            % Dd - Pathway
            j = 1;
            p = ExportSituation.Impact.VUbertragung.Pathways(:,j,side);
            if any(p)
                i = find(p,1);
                setRange(target,vuberAir+[4,0],1,23,getVUber('Impact',i,j,side));
            end
        end
    end
end
if Trittschall && ~VertSituation
    % Print the VUbertragung for the Vorsatzschale (1) 1st Room Floor
    if ExportSituation.Structure.Vorsatz(1)
        % Print the Fx - VUbertragung
        indx = [3,4];
        side = 1;
        p = ExportSituation.Impact.VUbertragung.Pathways(1,indx,side);
        if any(p)
            i = indx(find(p,1));
            setRange(target,vuberImp,1,23,getVUber('Impact',1,i,side));
        end
    end
    % Print the VUbertragung for the Vorsatzschale (7) 2nd Room Floor
    if ExportSituation.Structure.Vorsatz(7)
        % Print the fX - VUbertragung
        indx = [2,5];
        side = 1;
        p = ExportSituation.Impact.VUbertragung.Pathways(1,indx,side);
        if any(p)
            i = indx(find(p,1));
            setRange(target,vuberImp+[1,0],1,23,getVUber('Impact',1,i,side));
        end
    end
    
    % Print the Receiver side VUbertragungen only if they have not been 
    % printed yet
    if ~Luftschall
        % Define the receiver side
        side = 2;
        % Print the Fd - VUbertragung
        if ExportSituation.Impact.VUbertragung.Pathways(1,3,side)
            vuberAirS = vuberAir + [8,0];
            setRange(target,vuberAirS,1,23,getVUber('Impact',1,3,side));
        end
        % Print the Ff - VUbertragung
        if ExportSituation.Impact.VUbertragung.Pathways(1,4,side)
            vuberAirS = vuberAir + [10,0];
            setRange(target,vuberAirS,1,23,getVUber('Impact',1,4,side));
        end
        % Print the fD - VUbertragung
        if ExportSituation.Impact.VUbertragung.Pathways(1,2,side)
            vuberAirS = vuberAir + [5,0];
            setRange(target,vuberAirS,1,23,getVUber('Impact',1,2,side));
        end
        % Print the fF - VUbertragung
        if ExportSituation.Impact.VUbertragung.Pathways(1,5,side)
            vuberAirS = vuberAir + [0,0];
            setRange(target,vuberAirS,1,23,getVUber('Impact',1,5,side));
        end
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%% PRINT IMAGE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save the figure as an image
if VertSituation
    print(fCalcVertSituation,'-clipboard','-dbitmap');
else
    print(fCalcHorizSituation,'-clipboard','-dbitmap');
end

% Copy the image to the Excel sheet
target.get('Cells',imag(1),imag(2)).Select;
target.get('Pictures').get('Paste').Select();

% Select back the first cell
target.get('Range','A1').Select;

%% %%%%%%%%%%%%%%%%%%%%%%% FINISH OFF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Close the Excel workbooks and the COM-server
closeExcel(ex,exWB,exWB2);

% Tell the user the export worked without problems
msgbox(['The',32,sit,32,'Situation Export successful!'],'Export Completed');

% Close the figure
fSituationPlots.Visible = 'off';

end