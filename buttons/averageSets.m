function averageSets(~,~)

% Function performs the calculation necessary for doing an averaging of
% multiple acoustic datasets

%% %%%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%
global Data
global CompDatasetSelection
global CompareDatasetsData

%% %%%%%%%%%%%%%%%%%%%%%% DECLARE HANDLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab = findobj('Tag','AveragingTAB');
tbl = findobj('Parent',tab,'Tag','AveragingTABLE');
ax = findobj('Parent',tab,'Tag','AveragingAXES');
but1 = findobj('Parent',tab,'Tag','ExportAvgOrigBUT');
but2 = findobj('Parent',tab,'Tag','ExportAvgUberBUT');
ch = findobj('Parent',ax,'Type','Line');
leg = findobj('Type','Legend','Tag','AveragingLEG');

% Clear the children in any case and the legend
delete(ch(:));
delete(leg);

% Retrieve the number of datasets
n = size(CompDatasetSelection,1);

% Hide every graphical object (table, axes and button
tbl.Visible = 'off';
ax.Visible = 'off';
but1.Visible = 'off';
but2.Visible = 'off';

% Avoid performing any action if no elements or too much elem. are selected
if n==0 || n>39
    if n>39
        % Throw error to user
        errordlg('Too many datasets selected, maximum is 39 datasets!',...
                 'Max. number of datasets exceeded!','modal');
    end
    return;
end

%% %%%%%%%%%%%%%%%%%%% CHECK FOR EQUAL PARENT CONDITIONS %%%%%%%%%%%%%%%%%
passed = checkParentProperties(CompDatasetSelection,'all');
% If the test is passed we continue, otherwise we return
if ~passed
    return;
end

% Retrieve the initial dbType
dbType = CompDatasetSelection(1,1);
% Initialize the DBType array
DBType = {'Air','Impact','AirAVG'};
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% COLORMAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a colormap and retrieve the colors to be used for plotting
colors = repmat([lines(7); prism(6)],[3,1]);
colorsRGB = colors(1:n,:);
colorAvgRGB = [0,0,0];
% Get the equivalent hex value
colorsHEX = rgb2hex(colorsRGB);
colorAvgHEX = rgb2hex(colorAvgRGB);

% Define a color generation function for HTML
colorCell = @(text,color) ['<html><table border=0 width=1000',...
                           ' margin=0 padding=0 bgcolor=',color,...
                           '><tr><td>',text,'</td></tr></table></html>'];

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define a table generation function for placing the content in the middle
% of the table
alignCell = @(text,width) ['<html><table border=0 width=',width,...
                           ' margin=0 padding=0><tr><td align="center"',...
                           ' valign="middle">',text,'</td></tr>',...
                           '</table></html>'];
                       
% Retrieve the frequency dependant values
freq = Data.(DBType{dbType}).MeasFreq;
m = numel(freq);
freq_head = cell(1,m);
                       
% Initialize the variables to be used for the table
if any(dbType==[1,3])
    % Define values to print for Airborne and Airborne Average data
    row_head = {'Plot Color','Plot ID','Datatype','SNR','C','C_tr',...
                'C_50-3150','C_tr_50-3150','C_50-5000','C_tr_50-5000',...
                'C_100-5000','C_tr_100-5000'};
    % Add the frequency dependant values, repeat for each frequency
    for i=1:m
        % Retrieve and print to frequency data
        freq_head{1,i} = strcat('TL(',num2str(freq(i),'%.0f'),'Hz)');
    end
else
    % Define values to print for impact data
    row_head = {'Plot Color','Plot ID','Datatype','SNR','C_I','C_I_50-2500'};
    % Add the frequency dependant values, repeat for each frequency
    for i=1:m
        % Retrieve and print to frequency data
        freq_head{1,i} = strcat('NISPL(',num2str(freq(i),'%.0f'),'Hz)');
    end
end
% Concatenate the values
row_head = [row_head,freq_head];
% Define delta i
di = 2+1;
% Define cells for storing column heads and table data
rows = numel(row_head);
col_head = cell(n+di,1);
tblData = cell(rows,n+di);

% Specify the column widths
col_width = cell(1,n+di);
col_width{1} = 75;
for i=2:n+di
    col_width{i} = 80;
end

% Prepare the first column with the row heads MANUALLY
for j=1:rows
    % Input the row head
    tblData{j,1} = row_head{j};
end

%% %%%%%%%%%%%%% Print column with averages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize row count and column count
j=1; i=2;
% Retrieve the averaged data
avgData = avgDatasets(CompDatasetSelection);

% Retrieve the column width and set the column head
w = num2str(col_width{1,2},'%.0f');
col_head{i,1} = 'Averaged | Values';
% Print the data to the table
tblData{j,i} = colorCell('',colorAvgHEX);                 j=j+1;
tblData{j,i} = alignCell('0',w);                          j=j+1;
tblData{j,i} = alignCell(avgData.DataType,w);             j=j+1;
tblData{j,i} = alignCell(num2str(avgData.SNR,'%.1f'),w);
% Print the C-values to the table depending on Airborne/Impact
if any(dbType==[1,3])
    for k=1:8
        tblData{j+k,i} = alignCell(num2str(avgData.C(k),'%.0f'),w);
    end
elseif dbType==2
    for k=1:2
        tblData{j+k,i} = alignCell(num2str(avgData.C(k),'%.0f'),w);
    end
end
% Update the j-values
j = j+k;
% Print the frequency dependant data
for g=1:m
    tblData{j+g,i} = alignCell(num2str(avgData.Values(g),'%.1f'),w);
end

% Print data for editing (without HTML shit) (in numbers)
j=1; i=3;
col_head{i,1} = 'Edited | Averaged | Values';
tblData{j,i} = colorCell('',colorAvgHEX);        j=j+1;
tblData{j,i} = '0';                              j=j+1;
tblData{j,i} = avgData.DataType;                 j=j+1;
tblData{j,i} = num2str(avgData.SNR,'%.1f');
if any(dbType==[1,3])
    for k=1:8
        tblData{j+k,i} = num2str(avgData.C(k),'%.0f');
    end
elseif dbType==2
    for k=1:2
        tblData{j+k,i} = num2str(avgData.C(k),'%.0f');
    end
end
% Update the j value
j = j + k;
% Print the frequency dependant data
for g=1:m
    tblData{j+g,i} = num2str(avgData.Values(g),'%.1f');
end

%% %%%%%%%%%%%%%%%%%%%%% Print columns with original data %%%%%%%%%%%%%%%%
% Go through the original data
for i=1+di:n+di
    % Retrieve the datatype
    dbType = CompDatasetSelection(i-di,1);
    % Specify the DB
    DB = Data.(DBType{dbType});
    
    % Retrieve the indx
    indx = CompDatasetSelection(i-di,2);
    % Retrieve a string with the column width
    w = num2str(col_width{1,i},'%.0f');
    % Retrieve the report name and split it for better rendering
    col_head{i,1} = strSplit(DB.Report{indx,1},13,'|');
    
    % Initialize row count
    j = 1;
    % Define the plot color
    tblData{j,i} = colorCell('',colorsHEX(i-di,:));         j=j+1;
    % Define a plot ID
    tblData{j,i} = alignCell(num2str(i-di,'%.0f'),w);       j=j+1;
    % Retrieve the data type
    tblData{j,i} = alignCell(DB.DataType{indx,1},w);        j=j+1;
    % Retrieve the SNR
    tblData{j,i} = alignCell(num2str(DB.SNR(indx),'%.1f'),w);        
    % Retrieve the C values
    if any(dbType==[1,3])
        for k=1:8
            tblData{j+k,i} = alignCell(num2str(DB.C(indx,k),'%.0f'),w);
        end
    elseif dbType==2
        for k=1:2
            tblData{j+k,i} = alignCell(num2str(DB.C(indx,k),'%.0f'),w);
        end
    end
    % Update the j-value
    j = j+k;
    % Retrieve the TL / NISPL values
    if any(dbType==[1,3])
        TL = DB.TL(indx,:);
        % Print the TL values to the table
        for k=1:m
            tblData{j+k,i} = alignCell(num2str(TL(k),'%.1f'),w);
        end
    else
        NISPL = DB.NISPL(indx,:);
        % Print the NISPL values to the table
        for k=1:m
            tblData{j+k,i} = alignCell(num2str(NISPL(k),'%.1f'),w);
        end
    end
end

% Put data in the table
tbl.ColumnName = col_head;
tbl.ColumnWidth = col_width;
tbl.RowName = [];
tbl.Data = tblData;
% Make the third column available for editing
col_Edit = false(1,numel(col_head));
col_Edit(3) = true;
tbl.ColumnEditable = col_Edit;

% Set the table to visible
tbl.Visible = 'on';

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hold the axes
hold(ax,'on');
% Initialize the legend handle and string
leg_h = gobjects(2*n+1,1);
leg_s = cell(2*n+1,1);
leg_b = false(2*n+1,1);

%% Plot the averaged data
% Select the correct database from where to draw the data
if any(dbType==[1,3])
   % Airborne Data
   yDataField = 'TL';
   yLabel = 'Schalldammmass TL [dB]';
   lgn_pos = 'northwest';
elseif dbType==2
   % Impact Data
   yDataField = 'NISPL';
   yLabel = 'Trittschallpegel NISPL [dB]';
   lgn_pos = 'northeast';
end
% Retrieve the x-values and y-values
x = DB.MeasFreq;
y = avgData.Values;
% Plot the data
h_avg = plot(ax,x,y,'Color',colorAvgRGB,...
                    'LineStyle','--',...
                    'LineWidth',1.5);
% Prepare the legend
leg_h(1) = h_avg;
leg_s{1,1} = ['0 (',avgData.DataType,')'];
leg_b(1) = true;

%% Plot the original data
% Retrieve the important informations to be displayed in the plot
% Define the delta in indexing
di = 1;
% Define after how many plots the linestyle changes
PlotCycle = 13;
LineStyle = {'-','--','-.'};
% Go throught the original data
for i=1+di:n+di
    % Calculate new iterator
    j = i-di;
    % Retrieve the dbType 
    dbType = CompDatasetSelection(j,1);
    % Retrieve the correct database
    DB = Data.(DBType{dbType});
    % Retrieve the indx
    indx = CompDatasetSelection(j,2);
    % Retrieve the color
    color = colorsRGB(j,:);
    % Retrieve the y-values 
    y = DB.(yDataField)(indx,:);
    % Get the flags
    flag = any(DB.Flag(indx,:,:),3);
    % Get the linestyle index
    lineIndx = (mod(j,PlotCycle)>0) + floor((j)/PlotCycle);
    % Plot the data
    h = plot(ax,x,y,'Color',color,...
                    'LineStyle',LineStyle{lineIndx});
    % Prepare the legend
    leg_h(2*i-2) = h;
    leg_s{2*i-2,1} = [num2str(j,'%.0f'),' (',DB.DataType{indx,1},')'];
    leg_b(2*i-2) = true;
    
    % Plot the flags if necessaray
    if any(flag)
        h = plot(ax,x(flag),y(flag),'MarkerEdgeColor',color,...
                                    'MarkerFaceColor',color,...
                                    'Marker','x',...
                                    'LineStyle','none');
        leg_h(2*i-1) = h;
        leg_s{2*i-1,1} = [num2str(j,'%.0f'),' (Flag)'];
        leg_b(2*i-1) = true;
    end
end

% Place the legend
legend(ax,leg_h(leg_b),leg_s(leg_b),'Location','northwest',...
                                    'Tag','AveragingLEG',...
                                    'Location',lgn_pos);
           
% Enable the gridlines
ax.XGrid = 'on';
ax.YGrid = 'on';
% Specify the scale
ax.XScale = 'log';
% Specify the limits of the x-axis
ax.XLim = [min(x), max(x)];
% Specify the x-ticks
ax.XTick = x(2:3:end);
% Specify the x-label and y-label
ax.XLabel.String = 'Frequenz f [Hz]';
ax.YLabel.String = yLabel;
% Unhold the axes and set them to visible
hold(ax,'off');
ax.Visible = 'on';
% Put the averaging plot on top
uistack(h_avg,'top');

% Set the zoom property
z = zoom(tab.Parent.Parent);
setAllowAxesZoom(z,ax,true);

% Show the export button
but1.Visible = 'on';
but2.Visible = 'on';

%% Set the focus on the selected tab to averaging
% Find the tab group
tabParent = findobj('Tag','topCompTAB');
% Set the selected value to the averaging tab
tabParent.SelectedTab = tab;

%% %%%%%%%%%%%%% STORE DATA TO GLOBAL VARIABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%
% The global variable CompareDatasetsData has three fields, one for averaged
% one for energy subtraction data and one for level subtraction data
% Store the averaged data under the Average field

% Copy the avgData to the data
data = avgData;

% Add the IDs of the datasets used
data.IDs = CompDatasetSelection;
% Store the Messungstype Type-ID
if any(dbType==[1,3])
    data.TypeID = 1;    % Luftschall
else
    data.TypeID = 2;    % Trittschall
end
% Store the OriginalDB-ID
data.OriginalDBID = getOriginalDBID(CompDatasetSelection,'A');

% Store the data under the global variable
CompareDatasetsData.Average = data;

end