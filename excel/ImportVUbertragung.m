function varargout = ImportVUbertragung(~,~)

% Function extract the data from the excel database for VUbertragungschalen...
% ubertragung for further use in the program

%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data variables
global VUbertragung
% Configurations structure
global Configs

% Initialize flag if passed or not
varargout{1} = true;

%% %%%%%%%%%%%%%%%%%%%%% CHECK FOR CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the excel file for modifications through hashing and open the excel
% file only if modification have been made to the file otherwise load the
% data from the storage file,check also the storage file for modifications
storageFile = [Configs.Database.VUbertragung,'.mat'];
% Retrieve the hashes
sameHash = checkHash(Configs.Database.VUbertragung, storageFile, Configs.Hashing);

% If the file and the storage file have not changed, we load from storage,
% otherwise we load by importing from the excel database
if prod(sameHash)
    % Load the data from the storage file
    h = load(storageFile,'VUbertragung');
    VUbertragung = h.VUbertragung;
    return;
end

%% %%%%%%%%%%%%%%%%%%% READ FROM EXCEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'Z';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Knoten
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.VUbertragung,1);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

% Extract the sheet of interest and the last row
exSheet = exSH{1,1};
lastrow = exSH{1,2};

% Compile the range to be read from the worksheet
range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
               lastColumn, num2str(lastrow,'%.0f'));

% Read range out of excel worksheet as a Cell array
DB = exSheet.get('Range',range).Value;

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%%%%%% CREATE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a tracker for tracking the excel row in which the dataset is stored
VUbertragung.ExcelRow = (firstRow:1:(size(DB,1)+firstRow-1))';

% Extract the dataset IDs from the cells
VUbertragung.ID = cell2mat(DB(:,1));
% Check which datasets have a proper ID and select only those
VUbertragung.Considered = ~isnan(VUbertragung.ID);

% Clear Data from not used rows
DB = DB(VUbertragung.Considered,:);

% Update the first two parameters
VUbertragung.ExcelRow = VUbertragung.ExcelRow(VUbertragung.Considered);
VUbertragung.ID = VUbertragung.ID(VUbertragung.Considered);

%%%%%%%%%%%%%%%%% EXTRACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Import Vorsatzschalen ID
VUbertragung.Vorsatz = cell2mat(DB(:,2));
% Import Type-ID
VUbertragung.TypeID = cell2mat(DB(:,3));
% Import Anisotropie
VUbertragung.Anisotropie = cell2mat(DB(:,4));
% Import Original DB IDs
VUbertragung.OriginalID = getOriginalID(DB(:,5));
% Original DB ID string
VUbertragung.OriginalIDstr = DB(:,5);
% Import acoustic values
VUbertragung.Values = cell2mat(DB(:,6:26));

% Create a VUbertragung dropdown field
VUbertragung.Dropdown = dropdownNames(VUbertragung,'vubertragung');

%% %%%%%%%%%%% SAVE THE DATA TO A MAT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the file first
delete(storageFile);
% Save data
save(storageFile,'VUbertragung');
% Hide file
fileattrib(storageFile,'+w +h +a','');
% Compute the new hashes of the files
checkHash(Configs.Database.VUbertragung, storageFile, Configs.Hashing);

end