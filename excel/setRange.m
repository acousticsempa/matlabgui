function setRange(sheet,position,rows,columns,data)

% Function sets the value for a range in the selected sheet
% The position passed in as a 1x2 array
% position = [StartRow, StartColumn]

% Find the cells first
sC = sheet.get('Cells',position(1),position(2));
eC = sheet.get('Cells',position(1)+rows-1,position(2)+columns-1);

% Set the range
sheet.get('Range',sC,eC).Value = data;

end