function varargout = ImportUbertragung(~,~)

% Function extract the data from the excel database for Ubertragung for further
% use in the program

%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data variables
global Ubertragung
% Configurations structure
global Configs

% Initialize flag if passed or not
varargout{1} = true;

%% %%%%%%%%%%%%%%%%%%%%% CHECK FOR CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% file only if modification have been made to the file otherwise load the
% data from the storage file,check also the storage file for modifications
storageFile = [Configs.Database.Ubertragung,'.mat'];
% Retrieve the hashes
sameHash = checkHash(Configs.Database.Ubertragung, storageFile, Configs.Hashing);

% If the file and the storage file have not changed, we load from storage,
% otherwise we load by importing from the excel database
if prod(sameHash)
    % Load the data from the storage file
    h = load(storageFile,'Ubertragung');
    Ubertragung = h.Ubertragung;
    return;
end

%%%%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'AE';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Knoten
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Ubertragung,1);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

% Extract the sheet of interest and the last row
exSheet = exSH{1,1};
lastrow = exSH{1,2};

% Compile the range to be read from the worksheet
range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
               lastColumn, num2str(lastrow,'%.0f'));

% Read range out of excel worksheet as a Cell array
DB = exSheet.get('Range',range).Value;

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%%%% CREATE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a tracker for tracking the excel row in which the dataset is stored
Ubertragung.ExcelRow = (firstRow:1:(size(DB,1)+firstRow-1))';

% Extract the dataset IDs from the cells
Ubertragung.ID = cell2mat(DB(:,1));
% Check which datasets have a proper ID and select only those
Ubertragung.Considered = ~isnan(Ubertragung.ID);

% Clear Data from not used rows
DB = DB(Ubertragung.Considered,:);

% Update the first two parameters
Ubertragung.ExcelRow = Ubertragung.ExcelRow(Ubertragung.Considered);
Ubertragung.ID = Ubertragung.ID(Ubertragung.Considered);

%%%%%%%%%%%%%%%%% EXTRACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Import Knoten IDs
Ubertragung.Knoten = cell2mat(DB(:,2));
% Import Type IDs
Ubertragung.TypeID = cell2mat(DB(:,3));
% Import start and endraum
Ubertragung.Raume = cell2mat(DB(:,4:5));
% Import start and endbauteil
Ubertragung.Bauteile = cell2mat(DB(:,6:7));
% Import Trennbauteilfläche
Ubertragung.Area = cell2mat(DB(:,8));
% Import Länge Stossstelle
Ubertragung.Length = cell2mat(DB(:,9));
% Import Original DB IDs
Ubertragung.OriginalID = getOriginalID(DB(:,10));
% Import Original DB IDs as string
Ubertragung.OriginalIDstr = DB(:,10);
% Import acoustic values
Ubertragung.Values = cell2mat(DB(:,11:31));

% Create a Ubertraung dropdown field
Ubertragung.Dropdown = dropdownNames(Ubertragung,'ubertragung');

%% %%%%%%%%%%% SAVE THE DATA TO A MAT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delete(storageFile);
% Save data
save(storageFile,'Ubertragung');
% Hide file
fileattrib(storageFile,'+w +h','');
% Compute the new hashes of the files
checkHash(Configs.Database.Ubertragung, storageFile, Configs.Hashing);

end