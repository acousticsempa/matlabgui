function varargout = ImportVorsatz(~,~)

% Function extract the data from the excel database for Vorsatz for further
% use in the program

%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data variables
global Vorsatz
% Configurations structure
global Configs

% Initialize flag if passed or not
varargout{1} = true;

%% %%%%%%%%%%%%%%%%%% CHECK FOR CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the excel file for modifications through hashing and open the excel
% file only if modification have been made to the file otherwise load the
% data from the storage file,check also the storage file for modifications
storageFile = [Configs.Database.Vorsatz,'.mat'];
% Retrieve the hashes
sameHash = checkHash(Configs.Database.Vorsatz, storageFile, Configs.Hashing);

% If the file and the storage file have not changed, we load from storage,
% otherwise we load by importing from the excel database
if prod(sameHash)
    % Load the data from the storage file
    h = load(storageFile,'Vorsatz');
    Vorsatz = h.Vorsatz;
    return;
end

%% %%%%%%%%%%%%%%%%%%% READ FROM EXCEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'E';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Knoten
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Vorsatz,1);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

% Extract the sheet of interest and the last row
exSheet = exSH{1,1};
lastrow = exSH{1,2};

% Compile the range to be read from the worksheet
range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
               lastColumn, num2str(lastrow,'%.0f'));

% Read range out of excel worksheet as a Cell array
DB = exSheet.get('Range',range).Value;

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%% CREATE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a tracker for tracking the excel row in which the dataset is stored
Vorsatz.ExcelRow = (firstRow:1:(size(DB,1)+firstRow-1))';

% Extract the dataset IDs from the cells
Vorsatz.ID = cell2mat(DB(:,1));
% Check which datasets have a proper ID and select only those
Vorsatz.Considered = ~isnan(Vorsatz.ID);

% Clear Data from not used rows
DB = DB(Vorsatz.Considered,:);

% Update the first two parameters
Vorsatz.ExcelRow = Vorsatz.ExcelRow(Vorsatz.Considered);
Vorsatz.ID = Vorsatz.ID(Vorsatz.Considered);

%%%%%%%%%%%%%%%%% EXTRACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Import Name
Vorsatz.Name = DB(:,2);
% Import Kurzbeschreibung
Vorsatz.KurzBeschr = DB(:,3);
% Import Bauteile on which the Vorsatzschale can be used
Vorsatz.Bauteile = getBauteileID(DB(:,4));
% Import Detailed description
Vorsatz.DetBeschr = DB(:,5);

% Create a Vorsatz dropdown field
Vorsatz.Dropdown = dropdownNames(Vorsatz,'vorsatz');

%% %%%%%%%%%%% SAVE THE DATA TO A MAT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the file first
delete(storageFile);
% Save the data
save(storageFile,'Vorsatz');
% Hide the file
fileattrib(storageFile,'+w +h','');
% Compute the new hashes of the files
checkHash(Configs.Database.Vorsatz, storageFile, Configs.Hashing);

end