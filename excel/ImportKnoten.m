function varargout = ImportKnoten(~,~)

% Function extract the data from the excel database for Knoten for further
% use in the program

%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data variables
global Knoten
% Configurations structure
global Configs

% Initialize the varargout (flag that we were able to load the stuff
varargout{1} = true;

%% %%%%%%%%%%%%%%%%%%%%%%% CHECK FOR CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the excel file for modifications through hashing and open the excel
% file only if modification have been made to the file otherwise load the
% data from the storage file,check also the storage file for modifications
storageFile = [Configs.Database.Knoten,'.mat'];
% Retrieve the hashes
sameHash = checkHash(Configs.Database.Knoten, storageFile, Configs.Hashing);

% If the file and the storage file have not changed, we load from storage,
% otherwise we load by importing from the excel database
if prod(sameHash)
    % Load the data from the storage file
    h = load(storageFile,'Knoten');
    Knoten = h.Knoten;
    return;
end

%%%%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'N';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Knoten
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Knoten,1);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

% Extract the sheet of interest and the last row
exSheet = exSH{1,1};
lastrow = exSH{1,2};

% Compile the range to be read from the worksheet
range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
               lastColumn, num2str(lastrow,'%.0f'));

% Read range out of excel worksheet as a Cell array
DB = exSheet.get('Range',range).Value;

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%%%%%%%%%% CREATE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a tracker for tracking the excel row in which the dataset is stored
Knoten.ExcelRow = (firstRow:1:(size(DB,1)+firstRow-1))';

% Extract the dataset IDs from the cells
Knoten.ID = cell2mat(DB(:,1));
% Check which datasets have a proper ID and select only those
Knoten.Considered = ~isnan(Knoten.ID);

% Clear Data from not used rows
DB = DB(Knoten.Considered,:);

% Update the first two parameters
Knoten.ExcelRow = Knoten.ExcelRow(Knoten.Considered);
Knoten.ID = Knoten.ID(Knoten.Considered);

%%%%%%%%%%%%%%%%% EXTRACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Import type ID
Knoten.TypeID = cell2mat(DB(:,2));
% Import names
Knoten.Name = DB(:,3);
% Import KurzBeschreibung
Knoten.KurzBeschr = DB(:,4);
% Import Ausrichtung
Knoten.Ausrichtung = cell2mat(DB(:,5));
% Import Bauteile IDs
Knoten.Bauteile = cell2mat(DB(:,6:9));
% Import Anisotropie IDs
Knoten.Anisotropie = cell2mat(DB(:,10:13));
% Import DetBeschreibung
Knoten.DetBeschr = DB(:,14);

% Create a Knoten Dropdown field 
Knoten.Dropdown = dropdownNames(Knoten,'knoten');

%% %%%%%%%%%%% SAVE THE DATA TO A MAT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the file first
delete(storageFile);
% Save data
save(storageFile,'Knoten');
% Hide file
fileattrib(storageFile,'+w +h','');
% Compute the new hashes of the files
checkHash(Configs.Database.Knoten, storageFile, Configs.Hashing);

end