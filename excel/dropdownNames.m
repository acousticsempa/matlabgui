function dd = dropdownNames(data,type)

% Function takes in a structure and uses the data in that structure to
% create the dropdown name for different types
% type: 'knoten','bauteil','vorsatz','uebrtragung','vubertragung'

% Define global variables
global Knoten
global Vorsatz

% First check that we have enough arguments
if nargin~=2
    error('Not enough arguments provided.!');
end

% Define valid types
validType = {'knoten','bauteil','vorsatz','ubertragung','vubertragung'};

% Check that the type is a char and is a valid string
if ~ischar(type) || ~any(strcmpi(type,validType))
    error(['type argument must be a string.\nValid arguments are:\n',...
           '''knoten'',''bauteil'',''vorsatz'',''ubertragungu'',',...
           '''vubertragung''.']);
end

% Retrieve the IDs (since each type has it)
ID = data.ID;
% Find out the number of elements
n = numel(ID);

% Mapping between cell column size and type
m = [2,2,2,11,3];
% Find out how many columns we need
m = m(strcmpi(type,validType));

% Initialize the cell array
d = cell(n,m);

% Switch between the cases and retrieve the data
switch lower(type)
    case {'knoten','bauteil','vorsatz'}
        % Define which columns are to be converted to string
        c = [true,false];
        % Define the format for conversion
        f = {'%.0f',''};
        % Define the delimieters
        del = {'-'};
        
        % Retrieve the data and store it in the cells
        d(:,1) = num2cell(ID);
        d(:,2) = data.Name;
        
    case 'ubertragung'
        % Define which columns are to be converted to string
        c = ([1,0,0,0,1,0,1,0,1,0,1] == 1);
        % Define the format for conversion
        f = {'%.0f','','','','%.0f','','%.0f','','%.0f','','%.0f'};
        % Define delimiters
        del = {'-','-','-','','','','-','','',''};
        
        % Retrieve the data and store it in the cells
        d(:,1) = num2cell(ID);
        % Find the Knotenname
        [b,indx] = ismember(data.Knoten,Knoten.ID);
        d(b,2) = Knoten.Name(indx(b),1);
        d(~b,2) = {'KNOTENMISSING'};
        % Define the Luftschall, Trittschall sigla
        str = {'LS','TS'};
        d(:,3) = str(data.TypeID);
        % Define Raume and Bauteile
        d(:,4) = {'R'};
        d(:,5) = num2cell(data.Raume(:,1));
        d(:,6) = {'B'};
        d(:,7) = num2cell(data.Bauteile(:,1));
        d(:,8) = {'R'};
        d(:,9) = num2cell(data.Raume(:,2));
        d(:,10) = {'B'};
        d(:,11) = num2cell(data.Bauteile(:,2));
        
    case 'vubertragung'
        % Define which columns are to be convert to string
        c = [true,false,false,false];
        % Define the format for conversion
        f = {'%.0f','','',''};
        % Define delimiters
        del = {'-','-','-'};
        
        % Retrieve the data and store it in the cells
        d(:,1) = num2cell(ID);
        % Find the Vorsatzname
        [b,indx] = ismember(data.Vorsatz,Vorsatz.ID);
        d(b,2) = Vorsatz.Name(indx(b),1);
        d(~b,2) = {'VORSATZMISSING'};
        typeID = {'LS';'TS'};
        anisID = {'Parallel';'Senkrecht';'Direkt'};
        d(:,3) = typeID(data.TypeID);
        d(:,4) = anisID(data.Anisotropie);
end

% Convert the numeric data to a string
for i=1:m
    % Check if this column needs to be converted
    if c(i)
        % Iterate over the elements and perform the conversion
        for j=1:n
            d{j,i} = num2str(d{j,i},f{i});
        end
    end
end

% Initialize the return cell array
dd = cell(n,1);
% Concatenate the strings
for i=1:n
    dd{i,1} = strjoin(d(i,:),del);
end

end