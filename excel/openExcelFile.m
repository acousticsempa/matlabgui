function [success,ex,exWB,exSH] = openExcelFile(ExcelFile,sheets,ex)

% Function takes care of opening up an Excel File identified by the path
% passed in as a string. 
% The function takes also care of checking that the file is not opened by
% someone else, in which case it returns a boolean variable indicating that
% it could not open the file

% Initialize the variables
success = false;
exWB = [];
exSH = [];
if nargin < 3
    ex = [];
end

%% %%%%%%%%%%%%%%%%%%%%%%% INPUT CONTROL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that the filename passed in is a string
if ~ischar(ExcelFile)
    % Throw error
    error('Invalid arguments for filename passed in.');
end

% Check if the user has passed in which sheets to open
if nargin > 1
    % Check that it is an array
    if ~isnumeric(sheets) || any(sheets < 1)
        % Throw error
        error('Invalid arguments for sheets passed in.');
    end
else
    sheets = 1;
end

%% %%%%%%%%%%%%%%%%%%%%%%% OPEN FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 3
    % Create the COM-server with an Excel application
    ex = actxserver('Excel.Application');
    % Hide the alerts
    ex.DisplayAlerts = false;
    % Hide the Excel window
    ex.Visible = 0;
end

% Check if the Excel file exists, if not we need to create it
if ~(exist(ExcelFile,'file')==2)
    % Create the specific workbook
    wb = ex.Workbook.Add();
    % Save the workbook with the same name as specified by the user
    wb.SaveAs(ExcelFile);
    % Close the workbook
    wb.Close();
end

% Open the Excel file
exWB = ex.Workbooks.Open(ExcelFile);

% Check if the fiel is read-only, in which case we need to prompt the user
% to close the file, to be able ot store the data
if exWB.ReadOnly
    % Close the workbook and quit the actxserver
    exWB.Close;
    ex.Quit;
    % Throw an error to the user to tell him to close the file
    errordlg(['To store/access the data please close the ',ExcelFile,' file',...
              ' and repeat the operation!'],...
              'File already open - Error','modal');
    return;
end

%% %%%%%%%%%%%%%%%%%%%%% OPEN THE SHEETS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of sheets to retrieve
n = numel(sheets);
% Initialize the Excel-Sheets array
exSH = cell(n,2);

for i=1:n
    % Open the Excel Sheet
    exSH{i,1} = exWB.Sheets.get('Item',sheets(i));
    % Find the last used row
    exSH{i,2} = exSH{i,1}.get('UsedRange').get('Rows').get('Count');
    % Total number of sheets in the file
    exSH{i,3} = exWB.Sheets.Count;
end

% Update the success variable
success = true;

end