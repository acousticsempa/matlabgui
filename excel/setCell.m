function setCell(sheet,position,data)

% Position is a vector with the format:
% Position = [row, column]

% Function sets the value for a cell in the selected sheet
sheet.get('Cells',position(1),position(2)).Value = data;

end