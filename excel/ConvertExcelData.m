function varargout = ConvertExcelData(~,~)
% Functions extracts data from the original database and converts the
% Airborne (D) measurement type datasets into Airborne (R') measurement
% type dataset.
% It stores the resulting database into a new excel file, which will be
% updated every time the application launches, thus always having up to
% date Data

% Configurations structure
global Configs

% Initialize flag is passed or not 
varargout{1} = true;

%% %%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'HG';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%% CONVERTED DATABASE %%%%%%%%%%%%%%
% Check if the converted file exists and if it exists delete it
if exist(Configs.Database.Converted,'file') == 2
    % Delete the file
    delete(Configs.Database.Converted);
end
% Copy the Original database to the new name
copyfile(Configs.Database.Original,Configs.Database.Converted,'f');

% %%%%%%%%%%%%%%% OPEN CONVERTED DATABASE %%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Bauteile
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Converted,1);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

% Extract the sheet and the last row
exSheet = exSH{1,1};
lastRow = exSH{1,2};

% Compile the range to be read from the worksheet
range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
               lastColumn, num2str(lastRow,'%.0f'));
           
% Read range out of excel worksheet as a Cell array
DB = exSheet.get('Range',range).Value;


%% Iterate over the rows and check whether the datatype for the given
% dataset is set to a certain value and if it is, we have to modify some
% data
% Define the offset between the index in the database and the row number in
% excel
Offset = firstRow - 1;
% Specify the measurement frequencies
freq = [50,63,80,100,125,160,200,250,315,400,500,630,800,1000,1250,...
            1600,2000,2500,3150,4000,5000];
        
% Iterate over all entries in the database
for i=1:lastRow-Offset
    % Extract the datatype for the given dataset
    DType = DB(i,17);
    
    % Check if the datatype is D, if it is we need to do some calculations
    if strcmpi(DType,'D')
        % Retrieve the TL data
        TL_D = cell2mat(DB(i,27:47));
        % Retrieve the area
        A = cell2mat(DB(i,13));
        % Retrieve the receiver room volume
        V = cell2mat(DB(i,12));
        
        % Update the TL_D values to TL_R values
        TL_R = TL_D - 10*log10(V/A) + 4.9;
        % Round to one decimal place
    	TL_R = fix(10*TL_R + 0.5)/10;
    
        % %%%%% Calculate new Rw and C, C_tr,... %%%%%%
        % Calculate Rw + Rw (decimal)
        [~,Rw] = Rw_rating(freq,TL_R,1);
    
        % Calculate Rw + C_(100-3150) and Rw + Ctr_(100-3150)
        RwC = RwC_rating(freq,TL_R,[100,3150]);
        RwCtr = RwCtr_rating(freq,TL_R,[100,3150]);
    
        % Calculate Rw + C_50-3150 and Rw + Ctr_50-3150
        RwC_50_3150 =   RwC_rating(freq,TL_R,[50,3150]);
        RwCtr_50_3150 = RwCtr_rating(freq,TL_R,[50,3150]);
    
        % Calculate Rw + C_50-5000 and Rw + Ctr_50-5000
        RwC_50_5000 =   RwC_rating(freq,TL_R,[50,5000]);
        RwCtr_50_5000 = RwCtr_rating(freq,TL_R,[50,5000]);
    
        % Calculate Rw + C_100-5000 and Rw + Ctr_100-5000
        RwC_100_5000 =   RwC_rating(freq,TL_R,[100,5000]);
        RwCtr_100_5000 = RwCtr_rating(freq,TL_R,[100,5000]);
        
        % Calculate the C values using the Rwdec value
        RwRound = floor(Rw);
        C = RwC - RwRound;
        Ctr = RwCtr - RwRound;
        C_50_3150 = RwC_50_3150 - RwRound;
        Ctr_50_3150 = RwCtr_50_3150 - RwRound;
        C_50_5000 = RwC_50_5000 - RwRound;
        Ctr_50_5000 = RwCtr_50_5000 - RwRound;
        C_100_5000 = RwC_100_5000 - RwRound;
        Ctr_100_5000 = RwCtr_100_5000 - RwRound;
        % Create a matrix with all C-values
        CValues = [C, Ctr, C_50_3150, Ctr_50_3150, C_50_5000, Ctr_50_5000,...
                           C_100_5000,Ctr_100_5000];
        
        %% Write this new data into the excel file
        % Calculate the excel row
        exRow = num2str(i + Offset,'%.0f');
        
        % Write the new Datatype to the column Q
        rng = ['Q', exRow];
        exSheet.get('Range',rng).Value = 'R''';
        
        % Write the SNR value to the column R
        rng = ['R', exRow];
        exSheet.get('Range',rng).Value = Rw;
        
        % Write C-values to the columns S:Z
        rng = ['S', exRow, ':Z', exRow];
        exSheet.get('Range',rng).Value = CValues;
        
        % Write TL_R to the columns AA:AU
        rng = ['AA', exRow, ':AU', exRow];
        exSheet.get('Range',rng).Value = TL_R;
    end
end

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

end