function closeExcel(ex,varargin)

% Function takes care of saving and closing the COM-server

% Iterate over all workbooks
for i=1:numel(varargin)
    % Save and close the workbook
    varargin{i}.Save;
    varargin{i}.Close;
end

% Close the server
ex.Quit;

% Delete the handle
delete(ex);

end