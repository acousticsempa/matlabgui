function varargout = ImportExcelData(~,~)
% Function extract the data from the excel database for further use 
% in the GUI

%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data
global Data
global walls
% Configurations structure
global Configs

% Initialize flag if passed or not
varargout{1} = true;

%% %%%%%%%%%%%%%%%% CHECK FOR CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the hash value (SHA512) of the file, to see if anything has changed
% from last time we run this script, the function returns a boolean
% variable indicating if the file is the same (true) or has had some
% changes (false)
storageFile = [Configs.Database.Converted,'.mat'];
% Retrieve the hashes
sameHash = checkHash(Configs.Database.Original,...
                     Configs.Database.Converted,...
                     storageFile,...
                     Configs.Hashing);

% If none of the files have changed we can load the data from memory
if prod(sameHash)
    % Retrieve data from the storageFile
    h = load(storageFile,'Data','walls');
    Data = h.Data;
    walls = h.walls;
    return;
end

%% %%%%%%%%%%%%%%%% PERFORM TYPE CONVERSION ON DATABASE %%%%%%%%%%%%%%%
% Function performs the conversion of Airborne (D) measurement data
% into Airborne (R') measurement data
pC = ConvertExcelData();
% Check if we were able to perform the conversion
if ~pC
    varargout{1} = false;
    return;
end

%% %%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'HG';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Bauteile
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Converted,[1,2,3]);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

%%%%%%%%%%%%%%%%%% EXTRACT DATA TO NATIVE DATA STRUCTURES %%%%%%%%%%%%%%%%
% Procedure is repeated for 'Airborne' and for 'Impact'
fieldName = {'Air','Impact','AirAVG'};

%%%%%%%%%%%%%%%%%% READ OUT EXCEL DATA TO MATRIX %%%%%%%%%%%%%%%%%%%%%%%%%
% Iterate over the sheets
for i=1:numel(fieldName)
    % Retrieve the last row used in the spreadsheet
    lastrow = exSH{i,2};

    % Compile the range to be read from the worksheet
    range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
                   lastColumn, num2str(lastrow,'%.0f'));
                 
    % Read range out of excel worksheet as a Cell array
    DB = exSH{i,1}.get('Range',range).Value;
    
    % Create a tracker for tracking the excel row in which the dataset is stored
    D.ExcelRow = (firstRow:1:(size(DB,1)+firstRow-1))';

    % Extract the dataset IDs from the cells
    D.ID = cell2mat(DB(:,1));
    % Check which datasets have a proper ID and select only those
    D.Considered = ~isnan(D.ID);

    % Clear Data from not used rows
    DB = DB(D.Considered,:);

    % Update the first two parameters
    D.ExcelRow = D.ExcelRow(D.Considered);
    D.ID = D.ID(D.Considered);

    %%%%%%%%%%%%%%%%% EXTRACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Import report characteristics 
    % (Report Number, Report Date, Report Remarks, Client, Test Date)
    D.Report = getCommonString(DB(:,2:6),'/',[2,3,4,5]);
    
    % Import source and receiving room specifications
    D.SR = getRoom(DB(:,7:9));
    D.RR = getRoom(DB(:,10:12));
    
    % Date in which the import has been done
    D.ImportDate = DB(:,14);
    
    % Retrieve links to other datasets
    D.Link.Air = cell2mat(DB(:,15));
    D.Link.Impact = cell2mat(DB(:,16));
    
    % Retrieve the datatype of the dataset
    D.DataType = getCommonString(DB(:,17),'/'); 
    
    % Retrieve the SNR value
    D.SNR = cell2mat(DB(:,18));
    
    % Retrieve the deviations
    D.Deviation.Sum = cell2mat(DB(:,69));
    D.Deviation.Max = cell2mat(DB(:,70));
    D.Deviation.Freq = getFreq(DB(:,71));
            
    % Retrieve the test data
    D.Test.Title = DB(:,72);
    D.Test.Description = getCommonString(cleanCR(DB(:,73)),'/');
    D.Test.Object = getCommonString(cleanCR(DB(:,74)),'/');
    
    % Retrieve the walls types (using the encoding format)
    D.Walls = getWallsID(DB(:,81:92));
    
    % Retrieve the shielding conditions
    D.Shielding = cell2mat(DB(:,93:108));

    % Specify a MeasFreq element indicating the measuring frequencies used
    % in this tests
    D.MeasFreq = [50,63,80,100,125,160,200,250,315,400,500,630,...
                  800,1000,1250,1600,2000,2500,3150,4000,5000];
              
    % Data available only in the 'Airborne' and 'Impact' datasheets
    if sum(strcmpi(fieldName{i},{'Air','Impact'}))
        % Retrieve the flag for the measurements
        D.Flag = cell2mat(DB(:,48:68));
        % Manipulate the flag to a boolean (true if flag is present)
        D.Flag = ~isnan(D.Flag);
        
        % Retrieve additional data on the source and receiving room
        D.Test.SR = cell2mat(DB(:,75:77));
        D.Test.RR = cell2mat(DB(:,78:80));
        
        % Retrieve the L2, T, L2sb, Lb measurement values
        D.L2 = cell2mat(DB(:,130:150));
        D.T = cell2mat(DB(:,151:171));
        D.L2sb = cell2mat(DB(:,172:192));
        D.Lb = cell2mat(DB(:,193:213));
        
        % Retrieve the project and PrufObj properties
        D.Project = DB(:,214);
        D.PrufObj = DB(:,215);
        
    % Data available in 'AirAVG', but needs additional cleaning
    else
        % Retrieve the flag for the measurements
        D.Flag = getDoubleFlags(DB(:,48:68));
        
        % Retrieve additional data on the source and receiving room
        D.Test.SR = DB(:,75:77);
        D.Test.RR = DB(:,78:80);
        
        % Retrieve the project and PrufObj properties
        D.Project = DB(:,155);
        D.PrufObj = DB(:,156);
    end
    
    %%%%%%%%%%%%%%%%% EXTRACT SHEET SPECIFIC DATA %%%%%%%%%%%%%%%%%%%%%%%%
    switch fieldName{i}
        case 'Air'
            %%%%%%%%%%% AIRBORNE ONLY DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Retrieve the area
            D.Area = cell2mat(DB(:,13));
            % Retrieve the C-values
            D.C = cell2mat(DB(:,19:26));
            % Retrieve the TL values
            D.TL = cell2mat(DB(:,27:47));
            % Retrieve the L1 data
            D.L1 = cell2mat(DB(:,109:129));
        case 'Impact'
            %%%%%%%%%%% IMPACT ONLY DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Retrieve the C-values
            D.C = cell2mat(DB(:,19:20));
            % Retrieve the NISPL values 
            D.NISPL = cell2mat(DB(:,27:47));
        case 'AirAVG'
            %%%%%%%%%% AIRBORNE AVERAGE ONLY DATA %%%%%%%%%%%%%%%%%%%%%%%%
            % Retrieve the area
            D.Area = DB(:,13);
            % Retrieve the C-values
            D.C = cell2mat(DB(:,19:26));
            % Retrieve the TL values
            D.TL = cell2mat(DB(:,27:47));
            % Retrieve the TL and TL2 data
            D.TL1 = cell2mat(DB(:,113:133));
            D.TL2 = cell2mat(DB(:,134:154));
    end

    % Store the struct in the correct field
    Data.(fieldName{i}) = D;
    
    % Clear the dummy struct (so that on the next run its clear
    clearvars D
end

%% After the application has retrieved the data we can close the workbooks
% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%% SAVE THE DATA TO A MAT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the file first
delete(storageFile);
% Store the data
save(storageFile,'Data','walls');
% Hide the file
fileattrib(storageFile,'+w +h','');
% Update the hashes for the new files
checkHash(Configs.Database.Original,...
          Configs.Database.Converted,...
          storageFile,...
          Configs.Hashing);

end