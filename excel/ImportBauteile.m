function varargout = ImportBauteile(~,~)

% Function extract the data from the excel database for Bauteile for further
% use in the program

%% %%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Data variables
global Bauteile
% Configurations structure
global Configs

% Initialize flag if passed or not
varargout{1} = true;

%% %%%%%%%%%%%%%%%%%% CHECK FOR CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check the excel file for modifications through hashing and open the excel
% file only if modification have been made to the file otherwise load the
% data from the storage file,check also the storage file for modifications
storageFile = [Configs.Database.Bauteile,'.mat'];
% Retrieve the hashes
sameHash = checkHash(Configs.Database.Bauteile, storageFile, Configs.Hashing);

% If the file and the storage file have not changed, we load from storage,
% otherwise we load by importing from the excel database
if prod(sameHash)
    % Load the data from the storage file
    h = load(storageFile,'Bauteile');
    Bauteile = h.Bauteile;
    return;
end

%% %%%%%%%%%%%%%%%%% DEFINE THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify the first and last column of the database
firstColumn = 'A';
lastColumn = 'E';
firstRow = 2;

%% %%%%%%%%%%%%%%%%% ACCESS THE EXCEL FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the COM-server and the workbooks for Bauteile
[success,ex,exWB,exSH] = openExcelFile(Configs.Database.Bauteile,1);

% Check if we can continue
if ~success
    varargout{1} = false;
    return;
end

% Extract the sheet and the last row
exSheet = exSH{1,1};
lastrow = exSH{1,2};

% Compile the range to be read from the worksheet
range = strcat(firstColumn, num2str(firstRow,'%.0f'),':',...
               lastColumn, num2str(lastrow,'%.0f'));

% Read range out of excel worksheet as a Cell array
DB = exSheet.get('Range',range).Value;

% Save and close the workbook and the COM-server
closeExcel(ex,exWB);

%% %%%%%%%%%%%%%%%%%%%%%%%%%% CREATE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a tracker for tracking the excel row in which the dataset is stored
Bauteile.ExcelRow = (firstRow:1:(size(DB,1)+firstRow-1))';

% Extract the dataset IDs from the cells
Bauteile.ID = cell2mat(DB(:,1));
% Check which datasets have a proper ID and select only those
Bauteile.Considered = ~isnan(Bauteile.ID);

% Clear Data from not used rows
DB = DB(Bauteile.Considered,:);

% Update the first two parameters
Bauteile.ExcelRow = Bauteile.ExcelRow(Bauteile.Considered);
Bauteile.ID = Bauteile.ID(Bauteile.Considered);

%%%%%%%%%%%%%%%%% EXTRACT DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Import type ID
Bauteile.TypeID = cell2mat(DB(:,2));
% Import names
Bauteile.Name = DB(:,3);
% Import KurzBeschreibung
Bauteile.KurzBeschr = DB(:,4);
% Import DetBeschreibung
Bauteile.DetBeschr = DB(:,5);

% Create a Bauteile dropdown field
Bauteile.Dropdown = dropdownNames(Bauteile,'bauteil');

%% %%%%%%%%%%% SAVE THE DATA TO A MAT FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the file first
delete(storageFile);
% Store data
save(storageFile,'Bauteile');
% Hide the file
fileattrib(storageFile,'+w +h','');
% Compute the new hashes of the files
checkHash(Configs.Database.Bauteile, storageFile, Configs.Hashing);

end