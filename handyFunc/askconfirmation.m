function proceed = askconfirmation(object,type)

% Function proceeds to ask the user for confirmation to continue with the
% modification, deletion behaviour and returns a variable that indicates if
% the user desires to proceed

% Check that the type is a char and is one of the valid types
if ~ischar(type) || ~any(strcmpi(type,{'create','modify','delete'}))
    % Throw error
    error(['Invalid argument for type passed in. Valid arguments are',...
           ' ''create'', ''modify'', ''delete''']);
end

% Check that the object is a string
if ~ischar(object)
    error('Invalid argument for object. Only strings are accepted.');
end

% Switch between the different types of confirmation
switch lower(type)
    % Creation of object
    case 'create'
        q = questdlg(['You are about to create a new ',object,'!',...
                      ' Do you wish to continue?'],...
                     ['Create ',object],...
                      'No','Yes','Yes');
    case 'modify'
        q = questdlg(['You are about to overwrite the selected ',object,...
                      ' with new data!',...
                      ' Do you wish to continue?'],...
                     ['Modify ',object],...
                      'No','Yes','Yes');
    case 'delete'
        q = questdlg(['You are about to delete the selected ',object,'!',...
                      ' Do you wish to continue?'],...
                     ['Delete ',object],...
                      'No','Yes','Yes');
end

% Return if the user has accepted or not
switch lower(q)
    case 'yes'
        proceed = true;
    case 'no'
        proceed = false;
end