function [in] = cleanCR(in)
% Function takes either a cell array or a string and substitutes the 
% Carriage-Return New-Line (CR-NL) sequence with '. '

% Check if the input is a char array
if ischar(in)
    % Find where the carriage returna and the newlines are
    cr = (in==13);
    nl = (in==10);
    % Substitute
    in(cr) = '.';
    in(nl) = ' ';
else
    % Deal with cell array of characters arrays
    % Retrieve the size of the array
    [n,m] = size(in);
    
    % Iterate over all cells
    for j=1:m
        for i=1:n
            % Retrieve the string
            str = in{i,j};
            % Find where the carriage return and the newlines are
            cr = (str==13);
            nl = (str==10);
            % Substitute
            str(cr) = '.';
            str(nl) = ' ';
            % Store back into cell array
            in{i,j} = str;
        end
    end
end

