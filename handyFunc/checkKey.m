function passed = checkKey(key,type)

% Function takes care of determining if the Key being pressed is of the
% type desired by the programmer
% Type can be chosen as:
%   'delete':       Keys allowed: 'delete','backspace'
%   'enter':        Keys allowed: 'enter'

% Check if type is a string
if ~ischar(type) || ~any(strcmpi(type,{'delete','enter'}))
    % Throw error
    error(['Wrong argument for type passed in. Valid values for type are:',...
           ' ''delete'', ''enter''.']);
end

% Differentiate between the different cases
switch lower(type)
    % Deletion keys
    case 'delete'
        passed = any(strcmpi(key,{'delete','backspace'}));
    
    % Enter keys
    case 'enter'
        passed = any(strcmpi(key,{'return'}));
end