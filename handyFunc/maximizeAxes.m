function maximizeAxes(ax,OuterPosition)

% Function takes care of maximizing the axes, to the maximum size they can
% achieve and still remain within the position passed in by the user
% This is achieved by calculating the inner position using the TightInset
% property...

% Get the tight-inset
TightInset = ax.TightInset;

% Calculate the new "inner" Position of the axes
Position = [OuterPosition(1) + TightInset(1),...
            OuterPosition(2) + TightInset(2),...
            OuterPosition(3) - TightInset(1) - TightInset(3),...
            OuterPosition(4) - TightInset(2) - TightInset(4)];
        
% Change the axes position
ax.Position = Position; 

end