function writeXMLconfig(XMLfile)

% Function writes the XML file containing the information about the
% configuration of the application

% Global variables
global Configs

%% %%%%%%%%%%%%%%%%%%%%%% ANONYMOUS FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve node
getTag = @(node,tag)(node.getElementsByTagName(tag).item(0));
% Create node
newTag = @(doc,tag)(doc.createElement(tag));
% Add text to node
addTxt = @(doc,node,txt)(node.appendChild(doc.createTextNode(txt)));
% Append children
appendChild = @(parent,child)(parent.appendChild(child));

%% %%%%%%%%%%%%%%%%%%%%%%% WRITE XML FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize the XML document
xmlDoc = com.mathworks.xml.XMLUtils.createDocument('leichtbauprufstand');

% Retrieve the first level node
LbPs = getTag(xmlDoc,'leichtbauprufstand');

% Create the second level nodes
DB = newTag(xmlDoc,'databases');
temp = newTag(xmlDoc,'templates');
diary = newTag(xmlDoc,'diary');
hash = newTag(xmlDoc,'hashing');
test = newTag(xmlDoc,'testing');

% Create the third level nodes
o = newTag(xmlDoc,'original');
c = newTag(xmlDoc,'converted');
k = newTag(xmlDoc,'knoten');
b = newTag(xmlDoc,'bauteile');
v = newTag(xmlDoc,'vorsatz');
u = newTag(xmlDoc,'ubertragung');
vu = newTag(xmlDoc,'vubertragung');
s = newTag(xmlDoc,'situation');

% Create the fourth level nodes
ve = newTag(xmlDoc,'vertical');
ho = newTag(xmlDoc,'horizontal');

% Add the data to the nodes
addTxt(xmlDoc,o,Configs.Database.Original);
addTxt(xmlDoc,c,Configs.Database.Converted);
addTxt(xmlDoc,k,Configs.Database.Knoten);
addTxt(xmlDoc,b,Configs.Database.Bauteile);
addTxt(xmlDoc,v,Configs.Database.Vorsatz);
addTxt(xmlDoc,u,Configs.Database.Ubertragung);
addTxt(xmlDoc,vu,Configs.Database.VUbertragung);
addTxt(xmlDoc,ve,Configs.Template.Situation.Vertical);
addTxt(xmlDoc,ho,Configs.Template.Situation.Horizontal);
addTxt(xmlDoc,diary,Configs.Diary);
addTxt(xmlDoc,hash,Configs.Hashing);
if Configs.Testing
    addTxt(xmlDoc,test,'true');
else
    addTxt(xmlDoc,test,'false');
end

% Add attributes to the nodes
hash.setAttribute('algorithms','MD2,MD4,MD5,SHA1,SHA256,SHA512');
test.setAttribute('values','true,false');

% Append the children to the parent nodes
appendChild(DB,o);
appendChild(DB,c);
appendChild(DB,k);
appendChild(DB,b);
appendChild(DB,v);
appendChild(DB,u);
appendChild(DB,vu);
appendChild(LbPs,DB);
appendChild(s,ve);
appendChild(s,ho);
appendChild(temp,s);
appendChild(LbPs,temp);
appendChild(LbPs,diary);
appendChild(LbPs,hash);
appendChild(LbPs,test);

% Be sure to delete the current .xml file, so that we can store it
if exist(XMLfile,'file')==2
    delete(XMLfile);
end
% Write the xml file
xmlwrite(XMLfile,xmlDoc);
% Change the permissions
fileattrib(XMLfile,'+w','');

end