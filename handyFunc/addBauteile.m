function out = addBauteile(in,bID,indx)

% Function takes in: 
% in:       an array of size nxm containing Bauteile ID and NaN
% bID:      an array of size 1xk containing Bauteile IDs
% indx:     an index that indicates at which point the bID should be
%           inserted in the in matrix

% Check that the size of bID is kx1
if size(bID,1)~=1
    % Transpose
    bID = bID';
    % Repeat the check
    if size(bID,1)~=1
        % Throw error
        error('The argument bID should be an array of size 1xk.');
    end
end

% Get the size of the in matrix
[n,m] = size(in);

% Get the size of the bID array
k = numel(bID);

% Initialize the size of the matrix
nnew = n;
mnew = max(k,m);

% Update the sizes of the matrix
if indx > n
    nnew = n+1;
    indx = n+1;
end
  
% Create a matrix capable of containing the data
out = NaN(nnew,mnew);

% Copy the old data in the new array
out(1:n,1:m) = in;

% Add the new data at the specified index, but first clear the row
out(indx,:) = NaN;
out(indx,1:k) = bID;

% Check if we can cut some columns off at the end
b = all(isnan(out),1);
% Keep only columns that don't have only NaN
out = out(:,~b);

end