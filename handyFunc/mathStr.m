function str = mathStr(strin)

% Function takes a string an keeps only math relevant characters 
% Math relevant characters: 
%   digits:         0,1,2,3,4,5,6,7,8,9
%   operators:      +-*/
%   decimals:       .,
%   literals:       Inf,NaN

% Transform to lower case
str = lower(strin);

% Create index vector
n = numel(str);
indx = 1:n;

% Find out the digits
b_dig = isstrprop(str,'digit');

% Find out the operators
b_o1 = (str=='+');
b_o2 = (str=='-');
b_o3 = (str=='*');
b_o4 = (str=='/');

% Find out the decimals
b_d1 = (str==',');
b_d2 = (str=='.');

% Find location of literals
b_l1 = false(1,n);
b_l2 = false(1,n);
indx_l1 = strfind(str,'inf');
indx_l2 = strfind(str,'nan');
if ~isempty(indx_l1)
    % Replace the literals in the original string
    for i=1:numel(indx_l1)
        strin(indx_l1(i):indx_l1(i)+2) = 'Inf';
    end
    % Find boolean vector of characters to keep
    indx_l1 = repmat(indx_l1,1,3) + repmat([0,1,2],numel(indx_l1),1);
    b_l1 = ismember(indx,indx_l1(:));
end
if ~isempty(indx_l2)
    % Replace the literals in the original string
    for i=1:numel(indx_l2)
        strin(indx_l2(i):indx_l2(i)+2) = 'NaN';
    end
    % Find boolean vector of characters to keep
    indx_l2 = repmat(indx_l2,1,3) + repmat([0,1,2],numel(indx_l2),1);
    b_l2 = ismember(indx,indx_l2(:));
end

% Compound the booleans into a boolean matrix and find if any element is
% true
b = any([b_dig; b_o1; b_o2; b_o3; b_o4; b_d1; b_d2; b_l1; b_l2],1);

% Return only selected characters
str = strin(b);

end