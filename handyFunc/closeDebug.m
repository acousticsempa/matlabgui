% Function performs a check to close open actxservers

% Check if ex and exWB exist
if exist('ex','var') && exist('exWB','var')
    % Close the excel
    closeExcel(ex,exWB);
end

% Check if ex2 and exWB2 exist
if exist('ex2','var') && exist('exWB2','var')
    closeExcel(ex2,exWB2);
end

% Check if ol exists
if exist('ol','var')
    % Close the server
    ol.release;
end