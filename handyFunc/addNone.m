function [listOut,IDsOut] = addNone(list,IDs)
% Function takes a list of dropdown values and a list of IDs and adds a 
% None element at the beginning and shifts around the index values

% Check if no arguments are passed in in which case we initialize the list
% to empty and the IDs to empty as well
if nargin < 2
    list = {};
    IDs = [];
end

% Check if the IDs if a vertical array
if size(IDs,1)==1
    IDs = transpose(IDs);
end

% Read the size of the list
n = numel(list); 

% Initialize new cell array
listOut = cell(n+1,1);
% Copy the old data
listOut(2:end,1) = list;
% Add the None element
listOut{1,1} = 'None';

% Update the IDs vector
IDsOut = [0; IDs];

end

