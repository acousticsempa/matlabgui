function passed = checkName(Names,newNames,object)

% Function takes care of checking the new names against existing names, for
% different situations, the valid types are
% type:     'create':   check new name against existing names
%           'modify':   check modified name against existing names and
%                       itself
% The function returns an array indicating if it found the name within the
% names cell array and at which index it was found, and if no name was
% found it return an array of 0s.

% Initialize output variables
passed = false;

% Check that the object is a string
if ~ischar(object)
    error('Invalid argument for object. Only strings are accepted.');
end

% Check if newNames is a string, in which case we need to place it in a
% cell array of size 1
if ischar(newNames)
    nam = cell(1,1);
    nam{1,1} = newNames;
    newNames = nam;
end    

% Get the size of the newNames cell array
n = numel(newNames);

% Check if we have a match for each element in the newNames
% cell array and at which position this match was found
b = ismember(newNames,Names);

% Check that the names are not just empty space and tabs
bempty = false(n,1);
for i=1:n
    % Get the string
    str = newNames{i};
    % Find only non-graphic chars
    str = str(isstrprop(str,'graphic'));
    if isempty(str) || strcmpi(str,'')
        bempty(i) = true;
    end
end

% Compound valid names
valid = all(~[b,bempty],2);

% Check if we have one no-valid name and throw an error if we have at least
% one non-valid name
if any(~valid)
    errordlg([object,'-Name already exists, please choose another name!'],...
              'Duplicate name','modal');
    return;
end

% If all the code has been executed without entering an error-dialog, then
% we have passed
passed = true;

end