function updateXML = readXMLconfig(XMLfile)

% Function reads the XML file containing the information about the
% configuration of the application

% Global variables
global Configs

%% %%%%%%%%%%%%%%%%%%%%%% ANONYMOUS FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve node
getTag = @(node,tag)(node.getElementsByTagName(tag).item(0));
% Retrieve value
getVal = @(node,tag)(char(node.getElementsByTagName(tag).item(0).getFirstChild.getData));

%% %%%%%%%%%%%%%%%%%%%%%%% READ XML FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the XML document
xmlDoc = xmlread(XMLfile);

% Initialize variable to keep track of whether or not we need to update the
% XML file
updateXML = false;

% %%%%%%%%%%%%%%%%%% GET PRIMARY NODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LbPs = getTag(xmlDoc,'leichtbauprufstand');

% Get secondary nodes
if ~isempty(LbPs)
    DB = getTag(LbPs,'database');
    template = getTag(LbPs,'template');
    diary = getTag(LbPs,'diary');
    hash = getTag(LbPs,'hashing');
    test = getTag(LbPs,'testing');

    % Get tertiary nodes
    if ~isempty(DB)
        if ~isempty(getTag(DB,'original'))
            Configs.Database.Original = getVal(DB,'original');
        else
            updateXML = true;
        end
        
        if ~isempty(getTag(DB,'converted'))
            Configs.Database.Converted = getVal(DB,'converted');
        else
            updateXML = true;
        end
        
        if ~isempty(getTag(DB,'knoten'))
            Configs.Database.Knoten = getVal(DB,'knoten');
        else
            updateXML = true;
        end
        
        if ~isempty(getTag(DB,'bauteile'))
            Configs.Database.Bauteile = getVal(DB,'bauteile');
        else
            updateXML = true;
        end
        
        if ~isempty(getTag(DB,'vorsatz'))
            Configs.Database.Vorsatz = getVal(DB,'vorsatz');
        else
            updateXML = true;
        end
        
        if ~isempty(getTag(DB,'ubertragung'))
            Configs.Database.Ubertragung = getVal(DB,'ubertragung');
        else
            updateXML = true;
        end
        
        if ~isempty(getTag(DB,'vubertragung'))
            Configs.Database.VUbertragung = getVal(DB,'vubertragung');
        else 
            updateXML = true;
        end
    else
        updateXML = true;
    end
    
    if ~ isempty(template)
        if ~isempty(getTag(template,'situation'))
            situation = getTag(template,'situation');
            if ~isempty(getTag(situation,'vertical'))
                Configs.Template.Situation.Vertical =getVal(situation,'vertical');
            else
                updateXML = true;
            end
            if ~isempty(getTag(situation,'horizontal'))
                Configs.Template.Situation.Horizontal = getVal(situation,'horizontal');
            else
                updateXML = true;   
            end
        else
            updateXML = true;
        end
    else
        updateXML = true;
    end
    
    if ~isempty(diary)
        Configs.Diary = char(diary.getFirstChild.getData);
    else
        updateXML = true;
    end
    
    if ~isempty(hash)
        Configs.Hashing = char(hash.getFirstChild.getData);
    else
        updateXML = true;
    end
    
    if ~isempty(test)
        if strcmpi(char(test.getFirstChild.getData),'false')
            Configs.Testing = false;
        else
            Configs.Testing = true;
        end
    else
        updateXML = true;
    end
    
else
    updateXML = true;
end

end