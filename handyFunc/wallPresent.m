function passed = wallPresent(ID,SR,ER)

% Function checks that between the Startraum and Endraum there is a wall,
% otherwise it doesn't make any sense

% Global variables
global Knoten

% Index of Bauteil to be checked for every pair of start/endraum 
comb = [1,2; 1,3; 1,4; 2,3; 2,4; 3,4];
% Clockwise bauteile
cw = {[2], [2,3], [2,3,4], [3], [3,4], [4]};
% Anticlockwise bauteile
acw = {[1,4,3], [1,4], [1], [2,1,4], [2,1], [3,2,1]};

% Create and ordered vector for start/endraum 
p = [min(SR,ER), max(SR,ER)];

% Find the index to the combination represented by the inputs
[~,indx] = ismember(p,comb,'rows');

% Find the vector of cw and acw bauteile
cw = cw{1,indx};
acw = acw{1,indx};

%% Find the bautile for the Knoten
bauteileID = Knoten.Bauteile(Knoten.ID==ID,:);

% Retrieve the bauteileID of clockwise and anticlockwise walls
cwID = bauteileID(1,cw);
acwID = bauteileID(1,acw);

% Check that any of the walls actually exists
b_cw = any(cwID);
b_acw = any(acwID);

% Both the clock and anticlockwise way should have a wall in between
passed = (b_cw && b_acw);
