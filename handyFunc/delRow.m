function out = delRow(in,i)

% Function removes th i-th row from a cell array of size nxm and returns
% the cell array without that row 
% We can only remove one row at a time

% Retrieve size of the cell array
[n,~] = size(in);

% Check that the indices to remove are valid
if (max(i)>n) || (min(i)<1)
    error('Index out of bounds');
end

% Calculate the indices to keep
indx = setdiff(1:1:n,i);

% Copy the old values in the new cell array
out = in(indx,:);

end