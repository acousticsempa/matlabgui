function str = preZeros(numb,formStr,nZeros)
% Function takes a number and a formatting, as well as the number of
% preceding zeros we want to have and returns the correct string

% Cap the number of zeros to 9
if nZeros > 9
    nZeros = 9;
end

% Define a units scale
power10 = 10.^[1,2,3,4,5,6,7,8,9,10];

% Find how many units the number has
unit = find(numb < power10,1);
% If there is no unit, we limit the number of zeros to 9
if isempty(unit)
    unit = 9;
end

% Convert the number to string using the given format
str = num2str(numb,formStr);

% Calculate how many zeros we need to put in front of it
zerosNeeded = nZeros - unit;

% Attach the needed zeros to the front
zerosStr = {'0','00','000','0000','00000','000000',...
            '0000000','00000000','000000000'};

% Attach zeros only if needed
if zerosNeeded > 0
    str = strcat(zerosStr{zerosNeeded},str);
end

end

