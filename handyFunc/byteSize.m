function byteSize(var)
% Function return the size of a variable in whatever measure 

% Extract the information 
varInfo = whos('var');

% Extract the size in bytes
b = varInfo.bytes;

% Base measure
base = 1000;        % Can be 1024 or 1000

% Decide unit to use when printing
if base == 1024
    % Set base to B
    baseUnit = 'B';
elseif base == 1000
    % Set base to iB
    baseUnit = 'iB';
else
    % Set the base to unknown
    baseUnit = '?B';
end
        

% Print info based on the scale
if b < base
    % Print the value in bytes
    fprintf('Size of variable: %.0f %s',b, baseUnit);
elseif b < base^2
    % Print the value in kylobytes
    fprintf('Size of variable: %.1f K%s', b/base, baseUnit);
elseif b < base^3
    % Print the value in megabytes
    fprintf('Size of variable: %.1f M%s', b/base^2, baseUnit);
else
    % Print the value in gigabytes
    fprintf('Size of variable: %.1f G%s', b/base^3, baseUnit);
end

% Print a newline
fprintf('\n');

end