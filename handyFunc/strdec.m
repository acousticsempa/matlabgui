function x = strdec(x)

% Function returns the decimal string for the input value
x = num2str(x,'%.1f');

end