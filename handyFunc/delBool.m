function out = delBool(out,i)

% Function sets the i-th element of the in vector to false and cuts values
% of the array equal to false that are at the end of the vector

% Get the size of the out array
n = numel(out);

% Set the i-th value to false
if (max(i)<n+1) && (min(i)>0)
    for j=1:numel(i)
        out(i(j)) = false;
    end
end

% Initialize the index
i = n;
% Initialize the conditions
lastFalse = true;

% Cut values equal to false that are at the end of the vector
while lastFalse && i>0
    % Check if the i-th value is false
    if ~out(i)
        out(i) = [];
    else
        lastFalse = false;
    end
    % Diminish the counter
    i = i-1;
end

end