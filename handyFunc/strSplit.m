function [out] = strSplit(in,split,delim)
% Function takes a string and the number of characters after which a
% certain delimeter needs to be inserted and return the constructed string
% %%%%%%%%% INPUT %%%%%%%%%%%
% in:       Input string
% split:    Number of characters after which the delimiter is inserted
% delim:    The delimiter that is inserted into the string

% If the input string is not a string we return an empty string
if ~ischar(in)
    % Return a space
    out = ' ';
    return;
end

% Check inputs if zero or negative split or if the delimiter is not a
% string we return the input string
if split < 1 || ~ischar(delim)
    % Return the input string
    out = in;
    % Return
    return;
end

% 

% Retrieve the size of the string
n = numel(in);

% Calculate the modulus and the integer division with the split
modStr = mod(n,split);
intStr = floor(n/split);

% Calculate value for for-loop
k = (modStr>=1);

% Go through the various cases
if intStr > 0
    % String has a size larger or equal to the split
    % Initialize the out string
    out = in(1:split);
    
    % Repeat the process for the length of the string
    for i=1:(k + intStr - 1)
        % Calculate next indexex
        ind_s = i*split+1;
        ind_e = min(n,(i+1)*split);
        
        % Compose the string
        out = [out,delim,in(ind_s:ind_e)];
    end
else
    % String needs no modifications
    out = in;
end

end

