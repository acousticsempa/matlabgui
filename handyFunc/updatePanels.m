function updatePanels(origin)

% Function takes care of updating the panels depending on which object is
% calling the function and which panels are visible

% Panels
global pBauteil
global pVorsatz
global pUbertragung
global pVUbertragung

% Check the the input argument is valid
if nargin < 1
    error('Please pass in an argument');
end
if ~ischar(origin)
    error('The argument needs to be a string');
end
if ~strcmpi(origin,{'Knoten','Bauteil','Vorsatz'})
    error(['Invalid argument passed in. Valid arguments are:\n',...
           '''Knoten'',''Bauteil'',''Vorsatz''.']);
end

% Update Bauteil panel if it is visible
if any(strcmpi(origin,'Knoten')) &&...
       strcmpi(pBauteil.Visible,'on')
    pBauteil_Populate(pBauteil.Data.BauteilID);
end

% Update Vorsatz panel if it is visible
if any(strcmpi(origin,{'Knoten','Bauteil'})) &&...
       strcmpi(pVorsatz.Visible,'on')
    pVorsatz_Populate(pVorsatz.Data.VorsatzID);
end

% Update Ubertragung panel if visible
if any(strcmpi(origin,{'Knoten','Bauteil'})) &&...
        strcmpi(pUbertragung.Visible,'on')
    pUbertragung_Populate(pUbertragung.Data.UbertragungID);
end

% Update VUbertragung panel if visible
if any(strcmpi(origin,{'Knoten','Bauteil','Vorsatz'})) &&...
       strcmpi(pVUbertragung.Visible,'on')
   pVUbertragung_Populate(pVUbertragung.Data.VUbertragungID);
end

end