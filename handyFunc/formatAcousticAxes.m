function formatAcousticAxes(ax)

% Function takes an axes element as input and formats it according to the
% format being used for representing acoustic datasets

% Global variables
global Data

%% %%%%%%%%%%%%%%%%%%% FORMAT %%%%%%%%%%%%%%%%%%%%%
% Define the X-Label
ax.XLabel.String = 'Frequenz f [Hz]';

% Enable the gridlines
ax.XGrid = 'on';
ax.YGrid = 'on';

% Specify the scale
ax.XScale = 'log';

% Specify the limits of the x-axis
x = Data.Air.MeasFreq;
ax.XLim = [min(x), max(x)];

% Specify the x-ticks
ax.XTick = x(2:3:end);

end