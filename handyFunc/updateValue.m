function newVal = updateValue(oldIDs,newIDs,oldVal)

% Function returns the value at which a dropdown has to be set taking into
% account the previous value --> and corresponding ID it was set before so
% that we get the same ID

% Check if the oldIDs is empty, in which case just return 1
if isempty(oldIDs)
    newVal = 1;
    return;
end

% If we have only two arguments passed in, then we are looking for a
% specific ID in the oldIDs array
if nargin < 3
    % Find the position of the inputted ID (second argument) within the 
    % ID list (first argument)
    b = (oldIDs == newIDs);
else
    % Retrieve the old ID
    oldID = oldIDs(oldVal);

    % Check if we find the oldID inside the newIDs vector
    b = (newIDs == oldID);
end

% If we have a match (single match, then return the indx, otherwise return
% the first position
if sum(b)==1
    % Calculate the new value
    newVal = find(b);
else
    newVal = 1;
end

end