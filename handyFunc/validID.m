function passed = validID(ID,type)

% The function simply checks that the ID is not zero and returns an error
% message in case the ID is zero

% Initialize the output value
passed = false;

% Check that type is a string
if ~ischar(type)
    error('Please enter a string as the second argument!');
end

% Check if any of the IDs is 0
if any(ID==0)
    errordlg(['No',32,type,' selected! Please select a',32,type,...
              ' and repeat the operation!'],...
             [type,' not valid - Error'],'modal');
    return;
end

% Change if passed
passed = true;

end