function readXMLversion()

% Function reads the XML file containing informations about the version of
% the software being used and stores it in the Config structure

% Global variables
global Configs

% File 
XMLfile = 'version.xml';

%% %%%%%%%%%%%%%%%%% ANONYMOUS FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve node
getTag = @(node,tag)(node.getElementsByTagName(tag).item(0));
% Retrieve value
getVal = @(node,tag)(char(node.getElementsByTagName(tag).item(0).getFirstChild.getData));

%% %%%%%%%%%%%%%%%%%%% INITIALIZE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Configs.Version.Installed = 'MISSING';
Configs.Version.Branch = 'MISSING';

%% %%%%%%%%%%%%%%%%% READ THE XML FILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open the file
xmlDoc = xmlread(XMLfile);

% Get the primary nodes
app = getTag(xmlDoc,'application');

% Get the secondary nodes
if ~isempty(app)
    installed = getTag(app,'installed');
    branch = getTag(app,'branch');
    
    % Get the values
    if ~isempty(installed)
        Configs.Version.Installed = getVal(app,'installed');
    end
    if ~isempty(branch)
        Configs.Version.Branch = getVal(app,'branch');
    end
end

end