function [name,sheetName,success] = askSituationName(shNames)

% Function performs the duty to ask the user for the Situation name and the
% sheet name to use

success = false;

%% %%%%%%%%%%%%%%%%%%%% DEFINE THE DIALOG %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the options
options.Resize = 'on';
options.WindowStyle = 'modal';

% Define the text to display
quest = {'Enter Situation Name:','Enter Excel Sheet Name:'};
title = 'Situation Name';
name = '';
sheetName = '';

% Add the empty name to the list of forbidden names
shNames = [shNames;{''}];

% Initialize a variable for avoiding getting stuck in the while loop
cancel = false;
match = true;

% Continue to ask the user until 
while ~cancel && match 
    % Prepare the defaults
    defaults = {name,sheetName};
    
    % Display the input dialog
    answers = inputdlg(quest,title,1,defaults,options);

    % Check if its empty
    if isempty(answers)
        cancel = true;
    else
        % Parse the inputs
        name = answers{1};
        sheetName = answers{2};

        % Parse the answers and check that none is empty
        if ~any(strcmpi(sheetName,shNames))
            match = false;
        end
    end
end

% Check if we decided to cancel, in which case we were not successful 
if cancel
    return;
end

% Update the boolean variable
success = true;

end