function varargout = checkHash(varargin)

% The function uses flexible input variables to repeat the process of
% checking the hashes for an undefined number of files
% %%% INPUT %%%
% varargin:     (1xn) cell array containing the names of the files for
%               which we want to compare the hash value
% 
% %%% OUTPUT %%%
% sameSHA:      (nx1) boolean array indicating if the present hash value
%               of the file is the same as the stored hash value for that
%               file

% Check if the last argument being passed in is a hashing algorithm
possibleHashingAlgorithms = {'MD2', 'MD4', 'MD5', 'SHA1', 'SHA256',...
                             'SHA384', 'SHA512'};
% See if we have chosen an algorithm
chosenAlgorithm = strcmpi(varargin{end},possibleHashingAlgorithms);
% If we have chosen one
if sum(chosenAlgorithm)==1
    % Indicate which algorithm we have chosen
    HashAlgo = possibleHashingAlgorithms{chosenAlgorithm};
    % Indicate that we need to avoid the last input in the varargin
    % variable
    IgnoreLast = true;
else
    % Use as default the SHA512 algorithm
    HashAlgo = 'SHA512';
    % Consider last input
    IgnoreLast = false;
end

% Retrieve the number of elements in varargin
n = numel(varargin);
% Update to take into account when we pass in a hashing algorithm
n = n - IgnoreLast;

% Create boolean array indicating if the SHA value is remained the same or
% if the file has been modified
sameHash = false(n,1);

% Repeat the process for all inputs
for i=1:n
    % First get the content out of varargin
    fileToCheck = varargin{i};
    
    % Check if the file passed in exists, if not we return a false anyway
    if exist(fileToCheck,'file')==2
        % Calculate the SHA value of the original file
        cmd = strcat('CertUtil -hashfile',32,fileToCheck,32,HashAlgo);
        % Run the command and store the results
        [~,cmdout] = dos(cmd);
        % Separate the output and retrieve the hash value
        cmdout = strsplit(cmdout,char(10));
        % Retrieve the second argument and remove the spaces, thus
        % obtaining the hash
        hashNew = strrep(cmdout{2},' ','');
        
        % Retrieve the last modification time of the file
        timeNew = getFileData(fileToCheck,'datenum');
        
        % Since we are checking the hash value for files, we will store
        % the last computed hash-value in a data file with the same name 
        % and extension as the file for which we are calculating the hash
        % value

        % Hash storage file name
        HashFileName = [fileToCheck,'.',HashAlgo,'.mat'];
        
        % Read in the content of the shaFileName, first check if it exists
        if exist(HashFileName,'file')==2
            % If it exists, we can read the data (stored under the variable
            % hashNew from previous function call)
            h = load(HashFileName);
            hashOld = h.hashNew;
            timeOld = h.timeNew;
            
            % Check if the hashes are the same
            if strcmpi(hashNew,hashOld) && (timeNew==timeOld)
                % Update the boolean vector, saying that the file is the 
                % same as the SHA hashes are the same
                sameHash(i) = true;
            end
            
            % Delete the old file, so that we can store a new one
            delete(HashFileName);
        end
        
        % Update the file with the new hash value
        save(HashFileName,'hashNew','timeNew');
        
        % Set the attribute of the file to hidden
        fileattrib(HashFileName,'+w +h','');
    end
end

% Check if the user wants output
if nargout == 1
    varargout{1} = sameHash;
end

end
