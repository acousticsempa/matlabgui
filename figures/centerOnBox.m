function pos = centerOnBox(box,pos,scaleH,scaleV)

% Function takes as inputs a box (defined using the position array notation
% common to Matlab) and a figure with a current Position and centers the
% figure on the box center position

% Check the inputs 
if nargin==2
    % We have only passed in a box and a position, the scale is thus 1
    scaleH = 1; 
    scaleV = 1;
elseif nargin==3
    % We have passed in only one position array and two scaling factor
    % We need to switch the variables
    scaleV = scaleH; 
    scaleH = pos; 
    pos = box;
elseif nargin==4
    % We have passed in both the horizontal and vertical scale and two
    % position arrays
else
    % Throw error because not enough arguments have been passed in
    errordlg('Not enough input argument!','Arguments error','modal');
    return;
end

% First check which measure system we are using (both positions should be
% in the same units system
if max(box)<=1
    boxNormalized = true;
else
    boxNormalized = false;
end
if max(pos)<=1
    figNormalized = true;
else
    figNormalized = false;
end

% Check that we have the same system
if xor(boxNormalized,figNormalized)
    % Throw error
    errordlg('Both positions should have the same units!','Units error','modal');
    return;
end

% Scale the position with the current scaling passed in
pos(3) = pos(3)*scaleH; 
pos(4) = pos(4)*scaleV;

% Calculations for normalized values and pixel units
if boxNormalized
    %% Calculation for normalized units
    % Calculate the middle point of the box
    mh_Box = box(1)+0.5*box(3);
    mv_Box = box(2)+0.5*box(4);
    
    % Center the figure on this point
    pos = [mh_Box-0.5*pos(3), mv_Box-0.5*pos(4), pos(3:4)];
else
    %% Calculation for pixel units
    % Calculate the middle point of the box
    mh_Box = box(1)+round(0.5*box(3));
    mv_Box = box(2)+round(0.5*box(4));
    
    % Center the figure on this point
    pos = [mh_Box-round(0.5*pos(3)), mv_Box-round(0.5*pos(4)), pos(3:4)];
end