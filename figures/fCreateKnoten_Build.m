function fCreateKnoten_Build()
% Function defines the elements in the fCreateKnoten window 
% 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fCreateKnoten
global AvailableScreen

% Handler array containing all figure
global h_fig


%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
radio = [100,20];
drop = [150,20];
button = [150,20];
text = [200,20];
textKur = [200,35];
textDet = [200,60];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width =  max([label(1)+3*radio(1)+3*intra(1),...
                  label(1)+text(1)+intra(1),...
                  label(1)+textKur(1)+intra(1),...
                  label(1)+textDet(1)+intra(1),...
                  label(1)+2*drop(1)+2*intra(1),...
                  2*button(1)+intra(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = max([9*label(2),...
                  4*drop(2)+textDet(2)+textKur(2)+text(2)+2*radio(2)])+...
                  9*intra(2) + button(2)+ marg(2) + marg(4);
              
% Account for the image to be displayed at the end
WidthToHeight = 1;
% Calculate the height of the axes (starting at Bauteil 2)
h = max([7*label(2),...
         2*drop(2)+textDet(2)+textKur(2)+text(2)+2*radio(2)])+...
         6*intra(2);
% Retrieve image dimensions through proportions
imageDim = [WidthToHeight,1]*h;
% Augment the width with the corresponding image width
min_width = min_width + imageDim(1);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the figure position
pos = centerOnBox(AvailableScreen,[0,0,min_width,min_height]);

% Create a new full screen figure
fCreateKnoten = figure('Name','Create Knoten','NumberTitle','off',...
                       'ToolBar','none','MenuBar','none',...
                       'Units','pixels','Resize','off',...
                       'Position',pos,...
                       'CloseRequestFcn',@closeFigure,...
                       'Visible','off');
% Add figure to a handler list for closing
h_fig = [h_fig,fCreateKnoten];

% Create property for storage of figure related data
addprop(fCreateKnoten,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Knoten-Typ','Ausrichtung','Knoten-Name','Kurzbeschreibung',...
           'Det. Beschreibung',...
           'Bauteil 1','Bauteil 2','Bauteil 3','Bauteil 4'};
tags = {'KnotenTyp','Ausrichtung','KnotenName','KurzBeschreibung',...
        'DetBeschreibung','Bauteil1','Bauteil2','Bauteil3','Bauteil4'};
lbl_tag = strcat(tags,'LBL');
% Create the label elements iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',fCreateKnoten,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the first group of radio buttons
rad_txt = {'Kreuzstoss','T-Stoss','Anders'};
tags = {'Kreuzstoss','TStoss','Anders'};
rad_tag1 = strcat(tags,'RAD');
% Create the radio button group
knotenType = uibuttongroup('Parent',fCreateKnoten,'Units','pixels',...
                           'Visible','on','BorderType','none',...
                           'Tag','KnotenTypRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_txt)
    uicontrol('Parent',knotenType,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Tag',rad_tag1{i});
end

% Define the text to be displayed in the second group of radio buttons
rad_txt = {'Vertikal','Horizontal'};
tags = {'Vertikal','Horizontal'};
rad_tag2 = strcat(tags,'RAD');
% Create the radio button group
ausrichtung = uibuttongroup('Parent',fCreateKnoten,'Units','pixels',...
                            'Visible','on','BorderType','none',...
                            'Tag','AusrichtungRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_txt)
    uicontrol('Parent',ausrichtung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@fCreateKnoten_Populate,...
              'Tag',rad_tag2{i});
end

%% %%%%%%%%%%%%%%%%%%%% EDITABLE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the tags of the editable text fields
tags = {'KnotenName','KurzBeschreibung','DetBeschreibung'};
txt_tag = strcat(tags,'TXT');
txt_max = [1,100,100];
% Create the dropdown menus iteratively
for i=1:numel(txt_tag)
    uicontrol('Parent',fCreateKnoten,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Min',0,'Max',txt_max(i),...
              'Tag',txt_tag{i});
end
                    
%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the first set of dropdowns
txt = ' ';
tags = {'Bauteil1','Bauteil2','Bauteil3','Bauteil4'};
dd_tag1 = strcat(tags,'DD');
% Create the dropdown menus iteratively
for i=1:numel(dd_tag1)
    uicontrol('Parent',fCreateKnoten,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@fCreateKnoten_Populate,...
              'String',txt,'Tag',dd_tag1{i});
end

% Define which text needs to be displayed in the first set of dropdowns
txt = {'Keine Anisotropie','Anisotropie parallel zur Knotenrichtung',...
       'Anisotropie senkrecht zur Knotenrichtung'};
tags = {'Anisotropie1','Anisotropie2','Anisotropie3','Anisotropie4'};
dd_tag2 = strcat(tags,'DD');
% Create the dropdown menus iteratively
for i=1:numel(dd_tag2)
    uicontrol('Parent',fCreateKnoten,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@fCreateKnoten_Populate,...
              'String',txt,'Tag',dd_tag2{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'Cancel','Create Knoten'};
tags = {'Cancel','CreateKnoten'};
button_tag = strcat(tags,'BUT');
button_fcn = {'closeFigure','exportKnoten'}; 
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',fCreateKnoten,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% AXES FOR IMAGE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the axes for image
ax = axes('Parent',fCreateKnoten,'Units','pixels','Visible','on',...
                'Tag','KnotenPreviewAX',...
                'Color',0.94*ones(1,3));
% Hide the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 8; 
nV = 10;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = round(max(label(1),(button(1)-2*intra(1))/3));
h2 = round(max([(radio(1)-intra(1))/2,...
                (text(1)-5*intra(1))/6,...
                (textKur(1) -5*intra(1))/6,...
                (textDet(1)-5*intra(1))/6,...
                (drop(1)-2*intra(1))/3,...
                (button(1)-2*intra(1))/3]));
shareH = [h1; h2*ones(6,1); imageDim(1)]/(pos(3)-marg(1)-marg(3)-6*intra(1));
    
d = max(label(2),drop(2));
r = max(label(2),radio(2));
shareV = [button(2), d, d, d, d, max(label(2),textDet(2)),...
                     max(label(2),textKur(2)), max(label(2),text(2)),...
                     r, r]'/(pos(4)-marg(2)-marg(4)-9*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have fCreateKnoten as Parent
obj = findobj('Parent',fCreateKnoten);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 10;
for j=0:8
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j+1});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 5;
for j=0:3
    % Retrieve the first dropdown and place it correctly
    dd = findobj(obj,'Tag',dd_tag1{j+1});
    dd.Position = BoundingBox(boxes,[2,4],i-j,nH);
    % Retrieve the second dropdown and place it correctly
    dd = findobj(obj,'Tag',dd_tag2{j+1});
    dd.Position = BoundingBox(boxes,[5,7],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,7],10,nH);
% Place the group
knotenType.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],3,1,zeros(1,4),intra);
for j=0:2
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',knotenType,'Tag',rad_tag1{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,3);
end

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,7],9,nH);
% Place the group
ausrichtung.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],3,1,zeros(1,4),intra);
for j=0:1
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',ausrichtung,'Tag',rad_tag2{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,3);
end

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
txt = findobj(obj,'Tag','KnotenNameTXT');
txt.Position = BoundingBox(boxes,[2,7],8,nH);
txt = findobj(obj,'Tag','KurzBeschreibungTXT');
txt.Position = BoundingBox(boxes,[2,7],7,nH);
txt = findobj(obj,'Tag','DetBeschreibungTXT');
txt.Position = BoundingBox(boxes,[2,7],6,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','CancelBUT');
but.Position = BoundingBox(boxes,[2,4],1,nH);
but = findobj(obj,'Tag','CreateKnotenBUT');
but.Position = BoundingBox(boxes,[5,7],1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Position the axes
ax.Position = BoundingBox(boxes,nH,[1,nV],nH);