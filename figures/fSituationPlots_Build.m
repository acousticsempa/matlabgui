function fSituationPlots_Build()

% Function takes care of building the figure for displaying the graphs with
% the data from the vertical situation

%% %%%%%%%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fSituationPlots
global AvailableScreen

% Handler array for containing all figures
global h_fig

%% %%%%%%%%%%%%%%%%%%%%%%%%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the figure
fSituationPlots = figure('Name','Vertical Situation - Luftschall + Trittschall',...
                         'NumberTitle','off',...
                         'ToolBar','none','MenuBar','none',...
                         'Units','pixels','Visible','off',...
                         'Position',centerOnBox(AvailableScreen,0.7,0.7),...
                         'CloseRequestFcn',@closeFigure);
% Specify the function for resizing
fSituationPlots.ResizeFcn = @fSituationPlots_Resize;
                     
% Add the figure to a handler for closing
h_fig = [h_fig,fSituationPlots];

% Add a property to the figure to store data
addprop(fSituationPlots,'Data');

%% %%%%%%%%%%%%%%%%%%%%%%% LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lbl_txt = {'Vertical Situation - Luftschall + Trittschall'};
tags = {'Title'};
lbl_tag = strcat(tags,'LBL');
lbl_font = [15];
lbl_weight = [1];
weight = {'normal','bold'};
for i=1:numel(lbl_tag)
    uicontrol('Parent',fSituationPlots,'Visible','on',...
              'Units','pixels','Style','text',...
              'HorizontalAlignment','center',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',lbl_font(i),...
              'FontWeight',weight{lbl_weight(i)+1});
end    

%% %%%%%%%%%%%%%%%%%%%%%%% AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% LUFTSCHALL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the axes iteratively
for k=1:4
    % Calculate the k-string
    kstr = num2str(k,'%.0f');
    % Initialize the axes
    ax = axes('Parent',fSituationPlots,'Units','pixels','Visible','on',...
              'Tag',['Knoten',kstr,'AX']);
          
    % Define title and Y-Label
    ax.Title.String = ['Luftschall - Knoten',32,kstr];
    ax.YLabel.String = 'Schalldämmmass R [dB]';
    % Format the axes
    formatAcousticAxes(ax);
end

% Create the final axes for a summary
ax = axes('Parent',fSituationPlots,'Units','pixels','Visible','on',...
          'Tag','SummaryAX');
% Specify the title and Y-Label
ax.Title.String = 'Luftschall - Alle Knoten';
ax.YLabel.String = 'Schalldämmmass R [dB]';
% Format the axes 
formatAcousticAxes(ax);

%%%%%%%%%%%%%%%%%%%%%%%%%% TRITTSCHALL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ax = axes('Parent',fSituationPlots,'Units','pixels','Visible','on',...
          'Tag','TrittschallAX');
% Specify title and y-label
ax.Title.String = 'Trittschall - Alle Knoten';
ax.YLabel.String = 'Trittschallpegel L_n [dB]';
% Format the axes
formatAcousticAxes(ax);

%% %%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but_txt = {'Export to Excel Worksheet'};
tags = {'export'};
but_tag = strcat(tags,'BUT');
but_fcn = {'exportSituation'};
but_fcn = strcat('@',but_fcn);
for i=1:numel(but_tag)
    uicontrol('Parent',fSituationPlots,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%% RESIZE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fSituationPlots_Resize(fSituationPlots);

end