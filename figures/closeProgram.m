function closeProgram(~,~)

% Function takes care of closing all figures created for the GUI 

% Configurations
global Configs
% Handler array
global h_fig

% Iterate over all elements in the array and delete them
for i=1:numel(h_fig)
    delete(h_fig(i));
end

% Close the diary file
diary('off');

%% %%%%%%%%%%%%%%%% SEND EMAIL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform this operation only if we are not testing
if ~Configs.Testing
    % Check that the file has a length larger than 0
    DiaryFile = fopen(Configs.Diary);
    fseek(DiaryFile,0,'eof');
    DiaryLength = ftell(DiaryFile);
    fclose(DiaryFile);
    
    % Send email only if file has a length > 0
    if DiaryLength > 0
        % Open an outlook server
        ol = actxserver('Outlook.Application');
        % Create new mail
        mail = ol.CreateItem('olMail');
        % Specify subject
        mail.Subject = 'Error Log from LeichtbauPrufstand - Application';
        % Specify receiver of mail
        mail.To = 'matteo.empa@gmail.com';
        % Specify the body format
        mail.BodyFormat = 'olFormatHTML';
        % Specify the body
        mail.HTMLBody = ['Dear Matteo,<br><br>Attached you can find the',...
                         ' log file for the session terminated at ',...
                         datestr(datetime('now')),'!<br><br>',...
                         'As it turns out my use of the application',...
                         ' generated some logs you should go over!'];
        % Specify the attachement
        mail.attachments.Add(Configs.Diary);
        % Send message
        mail.Send;
        % Close server
        ol.release;
    end
end
% Delete diary file
delete(Configs.Diary);

%% %%%%%%%%%%%%%%%%%% Clear global variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars -global

% Return
return;
end

