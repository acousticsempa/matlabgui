function varargout = fCalcHorizSituation_Populate(src,~)

% Function takes care of populating the CalcHorizSituation with the correct
% Bauteile from the chosen Knoten

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fCalcHorizSituation
% Data
global Knoten
global Bauteile
global Vorsatz

%% %%%%%% RETRIEVE THE OBJECTS IN THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the objects which have as parent the panel
obj = fCalcHorizSituation.Children;

% Partition dropdown
pDD = findobj(obj,'Tag','PartitionDD');

% Text fields
Lx = findobj(obj,'Tag','LxTXT');
Lz = findobj(obj,'Tag','LzTXT');
FRLy = findobj(obj,'Tag','FRLyTXT');
SRLy = findobj(obj,'Tag','SRLyTXT');

% Radio buttons
rad = findobj(obj,'Tag','AnisotropieRAD');
aniso(1) = findobj(rad.Children,'Tag','AnisoNoneRAD');
aniso(2) = findobj(rad.Children,'Tag','AnisoXRAD');
aniso(3) = findobj(rad.Children,'Tag','AnisoZRAD');

% Knoten dropdowns
kDD(1) = findobj(obj,'Tag','Knoten1DD');
kDD(2) = findobj(obj,'Tag','Knoten2DD');
kDD(3) = findobj(obj,'Tag','Knoten3DD');
kDD(4) = findobj(obj,'Tag','Knoten4DD');

% Bauteile dropdowns
bDD(1) = findobj(obj,'Tag','Bauteil1DD');
bDD(2) = findobj(obj,'Tag','Bauteil2DD');
bDD(3) = findobj(obj,'Tag','Bauteil3DD');
bDD(4) = findobj(obj,'Tag','Bauteil4DD');
sbDD(1) = findobj(obj,'Tag','Bauteil5DD');
sbDD(2) = findobj(obj,'Tag','Bauteil6DD');

% Vorsatzschalen lower room
vDD(1) = findobj(obj,'Tag','VorsatzFR1DD');
vDD(2) = findobj(obj,'Tag','VorsatzFR2DD');
vDD(3) = findobj(obj,'Tag','VorsatzFR3DD');
vDD(4) = findobj(obj,'Tag','VorsatzFR4DD');
vDD(5) = findobj(obj,'Tag','VorsatzFRPartitionDD');

% Vorsatzschalen upper room
vDD(6) = findobj(obj,'Tag','VorsatzSRPartitionDD');
vDD(7) = findobj(obj,'Tag','VorsatzSR1DD');
vDD(8) = findobj(obj,'Tag','VorsatzSR2DD');
vDD(9) = findobj(obj,'Tag','VorsatzSR3DD');
vDD(10) = findobj(obj,'Tag','VorsatzSR4DD');

% Axes
ax = findobj(obj,'Tag','HorizSituationAX');

%% %%%%%%%%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
ov = @(x)(ones(1,x));

elem =  [pDD, aniso,     kDD,     bDD, sbDD,      vDD, Lx,Lz,FRLy,SRLy];
upLev = [  1, 1,1,1, 2*ov(4), 3*ov(4),  4,4, 5*ov(10),         6*ov(4)];

% Find which element called this function
b = (src==elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

%% %%%%%%%%%%%%%% INITIALIZE PARTITION DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%
% if the source calling the function is not any of the figure elements,
% that means we need to initialize the figure dropdowns with actual data to
% select from
% We need to find all Bauteile that are valid as a partition (in this case
% all the decken)
if updateLevel == 0
   % Find all the Wande
   bP = (Bauteile.TypeID == 1);
   
   % Check that we have at least one match, if we don't have it, we need to
   % throw an error and close the figure
   if ~any(bP)
       % Define the varargout variable as false
       varargout{1} = false;
       % Throw the error
       errordlg(['Please define at least ONE Wand to calculate a',...
                 ' horizontal situation!!'],'No decke - Error','modal');
       return;
   end
   
   % Update the dropdown
   pDD.String = Bauteile.Dropdown(bP);
   % Try to reuse the previously selected partition bauteil
   if isfield(fCalcHorizSituation.Data,'Partition') && ...
      isfield(fCalcHorizSituation.Data,'Bauteile')
       pDD.Value = updateValue(fCalcHorizSituation.Data.Partition,...
                               Bauteile.ID(bP),...
                               pDD.Value);
   end
   
   % Update global variable
   fCalcHorizSituation.Data.Partition = Bauteile.ID(bP);
   
   % Initialize the fields for storing the situation 
   fCalcHorizSituation.Data.Bauteile = zeros(1,9);
   fCalcHorizSituation.Data.Anisotropie = zeros(1,9);
   fCalcHorizSituation.Data.Vorsatz = zeros(1,10);
end

% Retrieve the Partition Bauteil ID
PartitionID = fCalcHorizSituation.Data.Partition(pDD.Value);
% Retrieve the Anisotropie of Partition Bauteil
PartitionAnisotropie = aniso(1).Value*0 + ...
                       aniso(2).Value*1 + ...
                       aniso(3).Value*3;

% Update the Bauteile field of the global structure with the currently
% selected partition and the current anisotropie
fCalcHorizSituation.Data.Bauteile(5) = PartitionID;
fCalcHorizSituation.Data.Anisotropie(5) = PartitionAnisotropie;

% Define mapping of global anisotropie to local Knoten Anisotropie
AnisoMap = [0,1,3;...   % Global anisotropie direction
            0,2,1;...   % Knoten 1/3 local anisotropie ID
            0,1,2];     % Knoten 2/4 local anisotropie ID

% Find the correct anisotropie for Knoten 1/3 and Knoten 2/4
bA = (PartitionAnisotropie == AnisoMap(1,:));
aniso13 = AnisoMap(2,bA);
aniso24 = AnisoMap(3,bA);

%% %%%%%%%%%%%%%%%%% UPDATE KNOTEN DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source of the calling is the Partition dropdown or the 
% Partition anisotropie radio buttons, then the user has
% selected a new partition bauteil and we need to find all Knoten that
% contain that Bauteil as Bauteil 1/3 and have the correct anisotropie 
if updateLevel < 2    
    % Find Knoten with a valid Ausrichtung
    bK13 = (Knoten.Ausrichtung == 1);
    bK24 = (Knoten.Ausrichtung == 2);
    
    % Find Knoten with a valid Bauteil
    bB1 = (Knoten.Bauteile(:,2) == PartitionID);
    bB24 = (Knoten.Bauteile == PartitionID);
    bB3 = (Knoten.Bauteile(:,4) == PartitionID);
    
    % Find Bauteile with a valid Anisotropie
    bB1(:,:,2) = (Knoten.Anisotropie(:,2) == aniso13);
    bB24(:,:,2) = (Knoten.Anisotropie == aniso24);
    bB3(:,:,2) = (Knoten.Anisotropie(:,4) == aniso13);
    
    % Find the Knotens with valid Bauteile
    bB1 = any(all(bB1,3),2);
    bB24 = any(all(bB24,3),2);
    bB3 = any(all(bB3,3),2);
    
    % Compound with Ausrichtung to find valid Knoten
    bK1 = all([bK13,bB1],2);
    bK24 = all([bK24,bB24],2);
    bK3 = all([bK13,bB3],2);
   
    % Update the dropdown menus for Knotens 1
    [dd,kIDs] = addNone(Knoten.Dropdown(bK1),Knoten.ID(bK1));    
    kDD(1).String = dd;
    % Try to update to the previous value
    if isfield(fCalcHorizSituation.Data,'Knoten') && ...
       (numel(fCalcHorizSituation.Data.Knoten)>=1)
        kDD(1).Value = updateValue(fCalcHorizSituation.Data.Knoten{1},...
                                   kIDs,kDD(1).Value);
    end
    % Update the global structure
    fCalcHorizSituation.Data.Knoten{1} = kIDs;
    
    % Update the dropdown menus for Knotens 3
    [dd,kIDs] = addNone(Knoten.Dropdown(bK3),Knoten.ID(bK3));    
    kDD(3).String = dd;
    % Try to update to the previous value
    if isfield(fCalcHorizSituation.Data,'Knoten') && ...
       (numel(fCalcHorizSituation.Data.Knoten)>=3)
        kDD(3).Value = updateValue(fCalcHorizSituation.Data.Knoten{3},...
                                   kIDs,kDD(3).Value);
    end
    % Update the global structure
    fCalcHorizSituation.Data.Knoten{3} = kIDs;

    % Update the dropdown menus for Knotens 2/4
    [dd,kIDs] = addNone(Knoten.Dropdown(bK24),Knoten.ID(bK24));   
    for i=2:2:4
        kDD(i).String = dd;
        % Try to update to the previous value
        if isfield(fCalcHorizSituation.Data,'Knoten') && ...
           (numel(fCalcHorizSituation.Data.Knoten)>=i)
            kDD(i).Value = updateValue(fCalcHorizSituation.Data.Knoten{i},...
                                       kIDs,kDD(i).Value);
        end
        % Update the global structure
        fCalcHorizSituation.Data.Knoten{i} = kIDs;
    end
end

% Retrieve the Knoten IDs
fCalcHorizSituation.Data.KnotenID = zeros(1,4);
for i=1:4
    fCalcHorizSituation.Data.KnotenID(i) = ...
        fCalcHorizSituation.Data.Knoten{i}(kDD(i).Value);
end

%% %%%%%%%%%%%% UPDATE KNOTEN BAUTEIL DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source calling the function is a Knoten dropdown, we need to take
% care of finding out which of the Bauteil 1/3 is a valid ceiling / floor
% if a ceiling / floor has already been selected, if not we can choose
% whatever bauteil 1/3 we want
if updateLevel < 3
    % Find the caller
    caller = (kDD == src);
    
    % Check if we have a specific caller or if we are just initializing
    if ~any(caller)
        caller = true(1,4);
    end
    
    % %%%%%%%%%%%%% Knoten 1/3 %%%%%%%%%%%%%%%%
    % Define valid dropdown values
    dd = {'Bauteil 1','Bauteil 3'};
    IDs = [1,3];
    
    for i=1:2:3
        if caller(i)
            % Find the Knoten ID
            kID = fCalcHorizSituation.Data.Knoten{i}(kDD(i).Value);
            % Boolean vector for identifying the Knoten
            bK = (Knoten.ID == kID);
            % Check if we have a match with the Knoten
            if any(bK)
                % Retrieve the present Bauteile
                bB = (Knoten.Bauteile(bK,[1,3]) ~= 0);
            else
                bB = false(1,2);
            end
            % Prepare the dropdowns
            if all(bB)
                vdd = dd(bB);
                vID = IDs(bB);
            else
                if any(bB)
                    vdd = dd(bB);
                    vID = IDs(bB);
                    [vdd,vID] = addNone(vdd,vID);
                else
                    [vdd,vID] = addNone();
                end
            end
            % Update the dropdowns
            bDD(i).String = vdd;
            if isfield(fCalcHorizSituation.Data,'KnotenBauteil') && ...
                        (numel(fCalcHorizSituation.Data.KnotenBauteil)>=i)
                bDD(i).Value = updateValue(...
                        fCalcHorizSituation.Data.KnotenBauteil{i},...
                        vID,bDD(i).Value);
            else
                bDD(i).Value = 1;
            end
            % Update the global data variable
            fCalcHorizSituation.Data.KnotenBauteil{i} = vID;
        end
    end
    
    % %%%%%%%%%%%%% Knoten 2/4 %%%%%%%%%%%%%%%%%%
    % Define valid dropdown values
    dd = {'Bauteil 1','Bauteil 2','Bauteil 3','Bauteil 4'};
    IDs = [1,2,3,4];
    
    for i=2:2:4
        if caller(i)
            % Find the Knoten ID
            kID = fCalcHorizSituation.Data.Knoten{i}(kDD(i).Value);
            % Boolean vector for identifying the Knoten
            bK = (Knoten.ID == kID);
        
            % Check if we have a match with the Knoten
            if any(bK)
                % Retrieve the correct Bauteile
                bB = (Knoten.Bauteile(bK,:) == PartitionID);
                bA = (Knoten.Anisotropie(bK,:) == aniso24);
                % Compound to find valid Bauteile
                bB = all([bB;bA],1);
            
                % Update the dropdowns
                bDD(i).String = dd(bB);
                if isfield(fCalcHorizSituation.Data,'KnotenBauteil') && ...
                        (numel(fCalcHorizSituation.Data.KnotenBauteil)>=i)
                    bDD(i).Value = updateValue(...
                             fCalcHorizSituation.Data.KnotenBauteil{i},...
                             IDs(bB),bDD(i).Value);
                else
                    bDD(i).Value = 1;
                end
                % Update the global data variable
                fCalcHorizSituation.Data.KnotenBauteil{i} = IDs(bB);
            else
                % None dropdown
                [bdd,bID] = addNone();
                bDD(i).String = bdd;
                bDD(i).Value = 1;
                fCalcHorizSituation.Data.KnotenBauteil{i} = bID;
            end
        end
    end
end

% Retrieve the Bauteil identifying the partition / ceiling / floor
fCalcHorizSituation.Data.KnotenBauteilID = zeros(1,6);
for i=1:4
    % Retrieve the Bauteil pos
    bID = fCalcHorizSituation.Data.KnotenBauteil{i}(bDD(i).Value);
    % Retrieve the Knoten ID
    kID = fCalcHorizSituation.Data.KnotenID(i);
    % Map zero values to the correct bID for Knoten 1/3
    if bID==0 && kID~=0
        % Define the ID map
        IDmap = [1,3];
        % Find the Knoten match
        bK = (Knoten.ID == kID);
        % Find the bauteile
        baut = find(Knoten.Bauteile(bK,[1,3])==0,1);
        bID = IDmap(baut);
    end
    % Store this value in the array
    fCalcHorizSituation.Data.KnotenBauteilID(i) = bID;
end

%% %%%%%%%%%%%% UPDATE KNOTEN BAUTEIL DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source calling the function is a BAUTEIL dropdown, we need find
% the valid Bauteile that can be used as walls for the case of Knoten 2/4
if updateLevel < 4
    % Find the caller if it is a Bauteile Dropdown
    callerB = (bDD([2,4]) == src);
    % Find the caller if it is a Knoten Dropdown
    callerK = (kDD([2,4]) == src);
    % Compound the caller
    caller = any([callerB; callerK],1);
    
    % Check if we have a specific caller or if we are just initializing
    if ~any(caller)
        caller = true(1,2);
    end

    % Define valid dropdown values
    dd = {'Bauteil 1','Bauteil 2','Bauteil 3','Bauteil 4'};
    IDs = [1,2,3,4];
    % Define mapping between selected partition bauteil and available
    % Bauteile for selection
    pIDmap = [0, 1, 0, 1;...        % Bauteil 1
              1, 0, 1, 0;...        % Bauteil 2
              0, 1, 0, 1;...        % Bauteil 3
              1, 0, 1, 0];          % Bauteil 4
    pIDmap = (pIDmap == 1);
    
    % Define an ID map for the for-loop
    IDmap = [2,4];
    % Iterate over the Bauteile Submenus
    for i=1:2
        if caller(i)
            % Get the correct index
            j = IDmap(i);
            % Find the Knoten ID
            kID = fCalcHorizSituation.Data.KnotenID(j);
            % Boolean vector for identifying the Knoten
            bK = (Knoten.ID == kID);
            % Check if we have a match
            if any(bK)
                % Find existing Bauteile
                bB = (Knoten.Bauteile(bK,:) ~= 0);
                % Retrieve the Bauteil which has been selected as Partition
                pID = fCalcHorizSituation.Data.KnotenBauteilID(j);
                % Mapping between chosen Bauteil and available Bauteile for
                % further selection
                bV = pIDmap(pID,:);
                % Compound to find valid Bauteile for selection
                bB = all([bB; bV],1);
                % Initialize the dropdown list
                if all(bB(bV))
                    vdd = dd(bB);
                    vID = IDs(bB);
                else
                    if any(bB(bV))
                        vdd = dd(bB);
                        vID = IDs(bB);
                        [vdd,vID] = addNone(vdd,vID);
                    else
                        [vdd,vID] = addNone();
                    end
                end
            else
                [vdd,vID] = addNone();
            end
            
            % Update the dropdowns
            sbDD(i).String = vdd;
            if isfield(fCalcHorizSituation.Data,'KnotenBauteil') && ...
                        (numel(fCalcHorizSituation.Data.KnotenBauteil)>=4+i)
            	sbDD(i).Value = updateValue(...
                             fCalcHorizSituation.Data.KnotenBauteil{4+i},...
                             vID,sbDD(i).Value);
            else
            	sbDD(i).Value = 1;
            end
            % Update the global data variable
            fCalcHorizSituation.Data.KnotenBauteil{4+i} = vID;
        end
    end
end

% Retrieve the Bauteil identifying the Wande
for j=1:2
    % Retrieve the Partition pos
    pID = fCalcHorizSituation.Data.KnotenBauteilID(j*2);
    % Retrieve the Bauteil pos
    bID = fCalcHorizSituation.Data.KnotenBauteil{j+4}(sbDD(j).Value);
    % Retrieve the Knoten ID
    kID = fCalcHorizSituation.Data.KnotenID(j*2);
    % Find out the Bauteil position for bID = 0
    if bID==0 && kID~=0
        % Define a mapping between pID and Valid Bauteile
        IDmap = [2,4; 1,3; 2,4; 1,3];
        % Find the Knoten match
        bK = (Knoten.ID == kID);
        % Find the first Bauteil equal to 0
        baut = find(Knoten.Bauteile(bK,IDmap(pID,:))==0,1);
        bID = IDmap(pID,baut);
    end
    fCalcHorizSituation.Data.KnotenBauteilID(j+4) = bID;
end
     
%% %%%%%%%%%%%%%%%%%%% RETRIEVE BAUTEILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefine the anisotropie mapping for vertical elements
x = 1000;
AnisoMap = [0,1,2,3;...   % Global anisotropie direction
            0,2,1,x;...   % Knoten 1/3 local anisotropie ID
            0,x,1,2];     % Knoten 2/4 local anisotropie ID

%%%%%%%%%%%%%%%%%%% KNOTEN 1/3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the current Knoten and Bauteile selected in the dropdowns and
% store them in the Bauteile and Anisotropie structure
for i=1:2:3
    % Retrieve the Knoten selected
    kID = fCalcHorizSituation.Data.Knoten{i}(kDD(i).Value);
    % Find a match
    bK = (Knoten.ID == kID);
    
    % Check if we have a match with the Knoten
    if any(bK)
        % Find out which Bauteil has been selected as the ceiling / floor
        switch fCalcHorizSituation.Data.KnotenBauteilID(i)
            case 1
                % Selected Bauteil 1 as FR and 3 as SR
                indx = [1,3];
            case 3
                % Selected Bauteil 3 as FR and 1 as SR
                indx = [3,1];
        end
        % Retrieve the Bauteile and the anisotropie
        baut = Knoten.Bauteile(bK,indx);
        anis = Knoten.Anisotropie(bK,indx);
        
        % Calculate the corresponding anisotropie ID
        [~,pos] = ismember(anis,AnisoMap(2,:));
        anis = AnisoMap(1,pos);
    else
        % When no match, we set everything to 0
        baut = [0,0];
        anis = [0,0];
    end
    
    % Update the global variable
    fCalcHorizSituation.Data.Bauteile(i) = baut(1);
    fCalcHorizSituation.Data.Bauteile(i+5) = baut(2);
    fCalcHorizSituation.Data.Anisotropie(i) = anis(1);
    fCalcHorizSituation.Data.Anisotropie(i+5) = anis(2);
end

%%%%%%%%%%%%%%%%%%%%% KNOTEN 2/4 %%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the current Knoten and Bauteile selected in the dropdowns and 
% store them in the Bauteile and Anisotropie structure
for i=2:2:4
    % Retrieve the Knoten selected
    kID = fCalcHorizSituation.Data.Knoten{i}(kDD(i).Value);
    % Find a match
    bK = (Knoten.ID == kID);
    
    % Check if we have a match with the Knoten
    if any(bK)
        % Find out which Bauteil has been selected as the wall and define
        % the correct indexes for retrieve the Bauteile IDs
        switch fCalcHorizSituation.Data.KnotenBauteilID(4+i/2)
            case 1
                indx = [1,3];
            case 2
                indx = [2,4];
            case 3
                indx = [3,1];
            case 4
                indx = [4,2];
        end
        % Retrieve the Bauteile and the anisotropie
        baut = Knoten.Bauteile(bK,indx);
        anis = Knoten.Anisotropie(bK,indx);
        
        % Calculate the corresponding anisotropie ID
        [~,pos] = ismember(anis,AnisoMap(2,:));
        anis = AnisoMap(1,pos);  
    else
        baut = [0,0];
        anis = [0,0];
    end
    
    % Update the global variable
    fCalcHorizSituation.Data.Bauteile(i) = baut(1);
    fCalcHorizSituation.Data.Bauteile(i+5) = baut(2);
    fCalcHorizSituation.Data.Anisotropie(i) = anis(1);
    fCalcHorizSituation.Data.Anisotropie(i+5) = anis(2);
end
        

%% %%%%%%%%%%%%%%%%%%% UPDATE VORSATZSCHALE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source calling the function has an update Level smaller than 3 we 
% need to update the dropdown menus
if updateLevel < 5
    % Mapping between Vorsatzschalen index and Bauteil index
    bauIndx = [1,2,3,4,5,5,6,7,8,9];
    for i=1:10
        % Retrieve the Bauteil ID
        bID = fCalcHorizSituation.Data.Bauteile(bauIndx(i));
        % Find valid Vorsatzschalen
        bV = any(Vorsatz.Bauteile == bID,2);
        % Prepare the dropdowns
        [dd,vIDs] = addNone(Vorsatz.Dropdown(bV),Vorsatz.ID(bV));
            
        % Update the dropdown
        vDD(i).String = dd;
        % Try to update to the previous value
        if isfield(fCalcHorizSituation.Data,'VorsatzDD') && ...
           (numel(fCalcHorizSituation.Data.VorsatzDD)>=i)
            vDD(i).Value = updateValue(fCalcHorizSituation.Data.VorsatzDD{i},...
                                       vIDs,vDD(i).Value);
        end
        % Update the global structure
        fCalcHorizSituation.Data.VorsatzDD{i} = vIDs;
        fCalcHorizSituation.Data.Vorsatz(i) = vIDs(vDD(i).Value);
    end
end

%% %%%%%%%%%%%%%%%%%% UPDATE SELECT VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source has an updateLevel of 3, we can update a single Vorsatz
% value in the Vorsatz structure
if updateLevel == 5
    % Find out the caller
    caller = (vDD == src);
    % Retrieve the selected value and store the selected Vorsatz
    fCalcHorizSituation.Data.Vorsatz(caller) = ...
        fCalcHorizSituation.Data.VorsatzDD{caller}(vDD(caller).Value);
end 

%% %%%%%%%%%%%%%%%%%% UPDATE SIZE OF STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
if updateLevel == 6
    % Only check that the inputted string is a valid one, and if not, then
    % throw an error and maintain the old value
    caller = (src == [Lx,Lz,FRLy,SRLy]);
    % Define defaults values when we don't input a string
    defaults = {'7.0','7.0','3.0','3.0'};
    % Try to convert the string and if we have an error, we use the default
    % stringa and we throw an error
    val = str2double(src.String);
    if isnan(val) || isinf(val) || (val < 0)
        % Reset the default string
        src.String = defaults{caller};
        % Throw an error
        errordlg('Invalid length! Please input a valid length.',...
                 'Invalid length - Error','modal');
    end
end

%% %%%%%%%%%%%%%%%%%%% UPDATE 3D-PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In any case update the 3D-Plot of the situation, by retrieving the data
% about which Bauteils and which Vorsatzschalen are used
lx = str2double(Lx.String);
lz = str2double(Lz.String);
lyFR = str2double(FRLy.String);
lySR = str2double(SRLy.String);

% Plot the 3D-Plot
plotHorizontalSituation(ax,fCalcHorizSituation.Data.Bauteile,...
                           fCalcHorizSituation.Data.Anisotropie,...
                           fCalcHorizSituation.Data.Vorsatz,...
                           lx,lyFR,lySR,lz);

% Correct the position of the axes to take advantage of all the available 
% space, save space for the legend
legPos = ax.Legend.Position;
axPos = fCalcHorizSituation.Data.AxesPosition;

% Calculate the new legend position
legPos = [axPos(1) + axPos(3) - legPos(3),...
          axPos(2) + 0.5*(axPos(4) - legPos(4)),...
          legPos(3),legPos(4)];
ax.Legend.Position = legPos;

% Calculate new axes position
ax.Position = [axPos(1), axPos(2),...
               axPos(3) - legPos(3),...
               axPos(4)];
           
%% %%%%%%%%%%%%%%%%%% KEEP TRACK OF CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fCalcHorizSituation.Data.Changed = true;

%% %%%%%%%%%%%%%%%%%% UPDATE THE VARARGOUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The first output argument tells the caller if we passed the population or
% not
varargout{1} = true;

end