function [box] = BoundingBox(boxes,indH,indV,nH)
% Function calculates the bounding box for any number of inputted boxes
% 
% INPUT variables:
% indH:    Indexes of the GUI skeleton in the horizontal direction of
%          elements to be considered in calculating the bounding box
% indV:    Indexes of the GUI skeleton in the vertical direction of
%          elements to be considered in calculating the bounding box
% 
% OUTPUT variables: 
% box:      Array, describing the position of the bounding box bounding
%           all the passed in boxes, format: [left, bottom, width, height]

% Transform to matrices
H = repmat(indH,[numel(indV),1]);
V = repmat(indV',[1,numel(indH)]);

% First calculate the indexes of the boxes being considered
boxInd = ((V-1)*nH) + H;
boxInd = boxInd(:);

% Create a new matrix representing the boxes we consider
boxesPos = boxes(boxInd,:);

% Retrieve the maximum coordinates
box = [min(boxesPos(:,1)),...                 % Minimum left coordinate
       min(boxesPos(:,2)),...                 % Minimum bottom coordinate
       max(boxesPos(:,1) + boxesPos(:,3)),... % Maximum right coordinate
       max(boxesPos(:,2) + boxesPos(:,4))];   % Maximum top coordinate
  
% Calculate the corresponding width and height
box(3) = box(3) - box(1);
box(4) = box(4) - box(2);

end
