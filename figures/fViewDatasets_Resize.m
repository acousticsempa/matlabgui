function fViewDatasets_Resize(src,~)
% Function takes care of resizing all elements within the function for
% a new function size


%% %%%%%%%%%%%%%%%% SPECIFY SKELETON OF UI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the number of horizontal elements and the number of vertical
% elements
nH = 8;
nV = 8;

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]

% Spaces in between elements
intra = [10, 10];

% Minimum sizes
drop = [150,20];
button = [150,20];
label = [150,20];
textKur = [300,40];
textDet = [300,80];
tab = [900,500];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = max([label(1) + drop(1) + max(textKur(1),textDet(1)) + 2*intra(1),...
                 3*button(1) + textDet(1) + 3*intra(1),...
                 tab(1)]) + ...
            marg(1) + marg(3);
% Minimum height
ld = max(label(2),drop(2));
min_height = ld + intra(2) + max(ld,(textKur(2)-intra(2))/2) + 2*intra(2) + ...
             ld + intra(2) + max(ld + 2*button(2) + 2*intra(2), textDet(2)) +...
             intra(2) + tab(2) + marg(2) + marg(4);

% Check if the figure needs to be resized, so that the minimum sizes are
% respected
fpos = src.Position;
w = max(min_width, fpos(3));
h = max(min_height, fpos(4));
src.Position = [fpos(1:2), w, h];

% Check that the position is within the boundaries of the desktop
src.Position = withinScreen(src.Position);

% Calculate fpos relative to window
fpos = src.Position;
fpos(1:2) = 0;

%% %%%%%%%%%%%%%%%%%% GET ELEMENT POSITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate shares of each element of the GUI to the total share
shareH = ones(nH,1)/nH;
b = max(button(2),(textDet(2)-2*intra(2))/3);
d1 = max(ld, (textDet(2)-2*intra(2))/3);
d2 = max(ld, (textKur(2)-2*intra(2))/2);
shareV = [0, b, b, d1, ld, d2, d2, ld]/(fpos(4) - marg(2) -marg(4) - 7*intra(2));
shareV(1) = 1 - sum(shareV(2:end));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(fpos, nH, nV, marg, intra, shareH, shareV);

%%%%%%%%%%%%%%%%%%%% FIND OBJECT THAT HAVE AS PARENT THE FIGURE %%%%%%%%%%
obj = findobj('Parent',src);

%% %%%%%%%%%%%%%%%%%% PLACE TEXT ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV; 
% Place the dropdown labels
lb = findobj(obj,'Tag','DatatypeLB');
lb.Position = BoundingBox(boxes,1,i,nH);    i=i-1;
lb = findobj(obj,'Tag','ProjectLB');
lb.Position = BoundingBox(boxes,1,i,nH);    i=i-1;
lb = findobj(obj,'Tag','PrufObjLB');
lb.Position = BoundingBox(boxes,1,i,nH);    i=i-1;
lb = findobj(obj,'Tag','ShieldingLB');
lb.Position = BoundingBox(boxes,1,i,nH);    i=i-1;
lb = findobj(obj,'Tag','ReportLB');
lb.Position = BoundingBox(boxes,1,i,nH);

% Place the labels for the preview elements
lb = findobj(obj,'Tag','TestDescriptionLB');
lb.Position = BoundingBox(boxes,[4,8],nV,nH);
lb = findobj(obj,'Tag','TestObjectLB');
lb.Position = BoundingBox(boxes,[4,8],nV-3,nH);

% Place the preview elements
txt = findobj(obj,'Tag','TestDescriptionTXT');
txt.Position = BoundingBox(boxes,[4,8],nV-[1,2],nH);
txt = findobj(obj,'Tag','TestObjectTXT');
txt.Position = BoundingBox(boxes,[4,8],nV-[4,6],nH);

%% %%%%%%%%%%%%%%%%%% PLACE TABS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the 'topTAB' element
topTab = findobj(obj,'Tag','topTAB');
topTab.Position = BoundingBox(boxes,[1,nH],1,nH);

%% %%%%%%%%%%%%%%%%%% PLACE DROPDOWN ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
dd = findobj(obj,'Tag','DatatypeDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);    i=i-1;
dd = findobj(obj,'Tag','ProjectDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);    i=i-1;
dd = findobj(obj,'Tag','PrufObjDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);    i=i-1;
dd = findobj(obj,'Tag','ShieldingDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);    i=i-1;
dd = findobj(obj,'Tag','ReportDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);

%% %%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','BackBUT');
but.Position = BoundingBox(boxes,1,3,nH);
but = findobj(obj,'Tag','DataViewerBUT');
but.Position = BoundingBox(boxes,2,3,nH);
but = findobj(obj,'Tag','SelectorBUT');
but.Position = BoundingBox(boxes,3,3,nH);
but = findobj(obj,'Tag','ExportDatasetBUT');
but.Position = BoundingBox(boxes,2,2,nH);
but = findobj(obj,'Tag','CalculatorBUT');
but.Position = BoundingBox(boxes,3,2,nH);

end
