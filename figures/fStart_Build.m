function fStart_Build()
% Function defines the elements in the Staring window 
% 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fStart
global AvailableScreen

% Handler array containing all figure
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]

% Spaces in between elements
intra = [10, 10];

% Minimum sizes
but = [200,40];
title = [300,40];
check = [20,20];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the number of horizontal elements and the number of vertical
% elements
nH = 4;
nV = 10;

% Minimum width
min_width = max([title(1),3*but(1)+2*intra(1)]) + ...
            marg(1) + marg(3);
% Minimum height
min_height = 8*but(2) + 2*title(2) + (nV-1)*intra(2) + ...
             marg(2) + marg(4);

%% %%%%%%%%%%%%%%%%%%%% DEFINE FIGURE SIZES (in pixels) %%%%%%%%%%%%%%%%%%%
% Calculate the available fullscreen
fullScreen();

% Center a figure with a width of 400 and a height of 400 on the center of
% the screen
pos = centerOnBox(AvailableScreen,[0,0,min_width,min_height]);

%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a new full screen figure
fStart = figure('Name','View Measured Datasets','NumberTitle','off',...
                'ToolBar','none','MenuBar','none',...
                'Units','pixels','Resize','off',...
                'Position',pos,...
                'CloseRequestFcn',@closeProgram,...
                'Visible','off');

% Add figure to a handler list for closing
h_fig = [h_fig,fStart];

%% %%%%%%%%%%%%%%%%%% INITIALIZE LABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to display
lbl_txt = {'Acoustic Datasets Database'};
tags = {'StartTitle'};
lbl_tag = strcat(tags,'LBL');
for i=1:numel(lbl_txt)
    uicontrol('Parent',fStart,'Visible','on','Units','pixels',...
              'Style','edit','String',lbl_txt{i},...
              'Enable','inactive',...
              'BackgroundColor',0.939*ones(1,3),...
              'HorizontalAlignment','center',...
              'FontSize',18,'FontWeight','bold',...
              'Tag',lbl_tag{i});
end

% Define titles
lbl_txt = {'Create','View','Update DB','Calculate Situation'};
tags = {'Create','View','Update','CalcSituation'};
lbl_tag = strcat(tags,'LBL');
for i=1:numel(lbl_txt)
    uicontrol('Parent',fStart,'Visible','on','Units','pixels',...
              'HorizontalAlignment','center',...
              'Enable','inactive',...
              'BackgroundColor',0.939*ones(1,3),...
              'Style','edit','String',lbl_txt{i},...
              'FontSize',14,'FontWeight','bold',...
              'Tag',lbl_tag{i});
end

%% %%%%%%%%%%%%%%%%%% INITIALIZE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'Datasets','Knoten','Bauteil',...
              'Vorsatz','Ubertragung','VorUbertrag',...
              'Bauteil','Knoten','Vorsatz',...
              'Knoten','Bauteil','Vorsatz',...
              'Ubertragung','VorUbertrag',...
              'Calculate Vertical Situation','Calculate Horizontal Situation'};
tags = {'ViewDatasets','ViewKnoten','ViewBauteil',...
        'ViewVorsatz','ViewUbertragung','ViewVUbertragung',...
        'CreateBauteil','CreateKnoten','CreateVorsatz',...
        'UpdateKnoten','UpdateBauteil','UpdateVorsatz',...
        'UpdateUbertragung','UpdateVUbertragung',...
        'CalcVertSituation','CalcHorizSituation'};
button_tag = strcat(tags,'BUT');
button_fcn = {'StartViewDatasets','StartViewer','StartViewer',...
              'StartViewer','StartViewer','StartViewer',...
              'StartCreator','StartCreator','StartCreator',...
              'updateDatabase','updateDatabase','updateDatabase',...
              'updateDatabase','updateDatabase',...
              'StartCalcSituation','StartCalcSituation'}; 
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',fStart,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'FontSize',14,...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%% CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the checkboxes to be used for understanding if the user also wants
% to see the parent panels
tags = {'Bauteil','Vorsatz','Ubertragung','VUbertragung'};
tip = ['Show parent',10,'objects for:',10];
che_tag = strcat(tags,'CHE');
for i=1:numel(che_tag)
    uicontrol('Parent',fStart,'Visible','on','Units','pixels',...
              'Style','checkbox','String','',...
              'TooltipString',[tip,tags{i}],...
              'Tag',che_tag{i});
end

%% %%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate fpos relative to window
pos = fStart.Position;
pos(1:2) = 0;

%% %%%%%%%%%%%%%%%%%% GET ELEMENT POSITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate shares of each element of the GUI to the total share
shareH = [but(1),but(1)-intra(1)-check(1),check(1),but(1)]...
            /(pos(3) - marg(1) - marg(3) - (nH-1)*intra(1));
shareV = [but(2),title(2),but(2)*ones(1,7),title(2)]...
            /(pos(4) - marg(2) -marg(4) - (nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

%%%%%%%%%%%%%%%%%%%% FIND OBJECT THAT HAVE AS PARENT THE FIGURE %%%%%%%%%%
obj = findobj('Parent',fStart);

%% %%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
i = nV;
lbl = findobj(obj,'Tag','StartTitleLBL');
lbl.Position = BoundingBox(boxes,[1,nH],i,nH);

% Subtitles
i = nV-1;
j = 1;
lbl = findobj(obj,'Tag','CreateLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);       j=j+1;
lbl = findobj(obj,'Tag','ViewLBL');
lbl.Position = BoundingBox(boxes,[j,j+1],i,nH);       j=j+2;
lbl = findobj(obj,'Tag','UpdateLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

% Subtitle
lbl = findobj(obj,'Tag','CalcSituationLBL');
lbl.Position = BoundingBox(boxes,[1,nH],2,nH);

%% %%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creation buttons
i = nV-3;
j = 1;
but = findobj(obj,'Tag','CreateKnotenBUT');   
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','CreateBauteilBUT');      
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','CreateVorsatzBUT');      
but.Position = BoundingBox(boxes,j,i,nH);

% View buttons
i = nV-2;
j = 2;
but = findobj(obj,'Tag','ViewDatasetsBUT');
but.Position = BoundingBox(boxes,[j,j+1],i,nH);         i=i-1;
but = findobj(obj,'Tag','ViewKnotenBUT');   
but.Position = BoundingBox(boxes,[j,j+1],i,nH);         i=i-1;
but = findobj(obj,'Tag','ViewBauteilBUT');   
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','ViewVorsatzBUT');
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','ViewUbertragungBUT');   
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','ViewVUbertragungBUT');   
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;

% Update buttons
i = nV-3;
j = 4;
but = findobj(obj,'Tag','UpdateKnotenBUT');
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','UpdateBauteilBUT');
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','UpdateVorsatzBUT');
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','UpdateUbertragungBUT');
but.Position = BoundingBox(boxes,j,i,nH);               i=i-1;
but = findobj(obj,'Tag','UpdateVUbertragungBUT');
but.Position = BoundingBox(boxes,j,i,nH);               i=i-2;

% Calculation buttons
rSub = 2;
subboxes = BoxPositions(BoundingBox(boxes,[1,nH],1,nH),rSub,1,...
                        zeros(1,4),intra);
but = findobj(obj,'Tag','CalcVertSituationBUT');
but.Position = BoundingBox(subboxes,1,1,rSub);
but = findobj(obj,'Tag','CalcHorizSituationBUT');
but.Position = BoundingBox(subboxes,2,1,rSub);

%% %%%%%%%%%%%%%%%%%% PLACE CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Viewer Checkboxes
i = nV-4;
j = 3;
che = findobj(obj,'Tag','BauteilCHE');
che.Position = BoundingBox(boxes,j,i,nH);       i=i-1;
che = findobj(obj,'Tag','VorsatzCHE');
che.Position = BoundingBox(boxes,j,i,nH);       i=i-1;
che = findobj(obj,'Tag','UbertragungCHE');
che.Position = BoundingBox(boxes,j,i,nH);       i=i-1;
che = findobj(obj,'Tag','VUbertragungCHE');
che.Position = BoundingBox(boxes,j,i,nH);

end
