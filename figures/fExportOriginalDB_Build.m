function fExportOriginalDB_Build()
% Function build the window for exporting averaged data to the original
% database

%% %%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
global fExportOriginalDB
global fCompareDatasets

% Handler array of all created figures
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
drop = [100,20];
text = [120,20];
textDet = [400,80];
button = [150,20];
table = [416,220];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width =  max([label(1)+text(1)+intra(1),...
                  label(1)+drop(1)+intra(1),...
                  textDet(1),...
                  table(1),...
                  2*button(1)+intra(1)]) + marg(1) + marg(3);
% Minimum height
min_height = max([4*label(2)...
                  label(2)+2*drop(2)+text(2)]) + 7*intra(2) +...
                  table(2)+button(2)+2*textDet(2) + marg(2) + marg(4);

%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a new full screen figure
fExportOriginalDB = figure('Name','Export Averaged Datasets to Original Database',...
                           'NumberTitle','off',...
                           'ToolBar','none','MenuBar','none',...
                           'Units','pixels','Resize','off',...
                           'CloseRequestFcn',@closeFigure,...
                           'Visible','off',...
                           'WindowStyle','modal');
% Add figure to the global handler array
h_fig = [h_fig, fExportOriginalDB];

% Center the ExportOriginalDB figure on the CompareDatasets figure and
% scale it horizontally and vertically
pos = [0,0,min_width,min_height];
fExportOriginalDB.Position = centerOnBox(fCompareDatasets.Position,pos);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%% LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the labels
lbl_txt = {'Report Name','Test Description','Test Object',...
           'SNR, C und Transmissionswerte R/L'};
tags = {'ReportName','TestDescr','TestObj','table'};
lbl_tag = strcat(tags,'LBL');
% Create the labels iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',fExportOriginalDB,'Style','text',...
              'HorizontalAlignment','left',...
              'Visible','on','Units','pixels',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the dropdown menus
dd_txt = ' ';
tags = {'ExportTestDescr','ExportTestObj'};
dd_tag = strcat(tags,'DD');
drop_fcn = {'selExportTestDescr','selExportTestObj'};
drop_fcn = strcat('@',drop_fcn);
% Create the dropdown menus iteratively
for i=1:numel(dd_tag)
    uicontrol('Parent',fExportOriginalDB,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'String',dd_txt,'Tag',dd_tag{i},...
              'Callback',eval(drop_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%%%% EDITABLE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%
txt_txt = ' ';
tags = {'ReportName','ExportTestDescr','ExportTestObj'};
txt_tag = strcat(tags,'TXT');
rows = [1,2,2];
% Create the text fields iteratively
for i=1:numel(txt_tag)
    uicontrol('Parent',fExportOriginalDB,'Style','edit','Visible','on',...
              'Units','pixels',...
              'HorizontalAlignment','left','Max',rows(i),...
              'String',txt_txt,'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the buttons
but_txt = {'Export Data','Cancel'};
tags = {'Export','Cancel'};
but_tag = strcat(tags,'BUT');
but_fcn = {'exportOriginalDB','closeFigure'};
but_fcn = strcat('@',but_fcn);
% Create the buttons iteratively
for i=1:numel(but_txt)
    uicontrol('Parent',fExportOriginalDB,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb = uitable('Parent',fExportOriginalDB,'Visible','on',...
             'RowName',[],'ColumnName',[],'RowStriping','on',...
             'Units','pixels','Tag','ValuesTABLE');
         
%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 3; 
nV = 8;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h = label(1);
shareH = [h; 0; 0]/(pos(3)-marg(1)-marg(3)-2*intra(1));
shareH(2:3) = (1-sum(shareH))/2;

l = label(2);
d = max(label(2),drop(2));
t = max(label(2),text(2));
b = button(2);
shareV = [b,0,l,textDet(2),d,textDet(2),d,t]'...
            /(pos(4)-marg(2)-marg(4)-7*intra(2));
shareV(2) = 1-shareV(1)-sum(shareV(3:end));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have fExportOriginalDB as Parent
obj = findobj('Parent',fExportOriginalDB);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
lbl = findobj(obj,'Tag','ReportNameLBL');
lbl.Position = BoundingBox(boxes,1,i,nH);       i=i-1;
lbl = findobj(obj,'Tag','TestDescrLBL');
lbl.Position = BoundingBox(boxes,1,i,nH);       i=i-2;
lbl = findobj(obj,'Tag','TestObjLBL');
lbl.Position = BoundingBox(boxes,1,i,nH);       i=i-2;
lbl = findobj(obj,'Tag','tableLBL');
lbl.Position = BoundingBox(boxes,[1,nH],i,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV-1;
dd = findobj(obj,'Tag','ExportTestDescrDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);        i=i-2;
dd = findobj(obj,'Tag','ExportTestObjDD');
dd.Position = BoundingBox(boxes,[2,3],i,nH);

% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
txt = findobj(obj,'Tag','ReportNameTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);       i=i-2;
txt = findobj(obj,'Tag','ExportTestDescrTXT');
txt.Position = BoundingBox(boxes,[1,3],i,nH);       i=i-2;
txt = findobj(obj,'Tag','ExportTestObjTXT');
txt.Position = BoundingBox(boxes,[1,3],i,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','CancelBUT');
but.Position = BoundingBox(boxes,2,1,nH);
but = findobj(obj,'Tag','ExportBUT');
but.Position = BoundingBox(boxes,3,1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb.Position = BoundingBox(boxes,[1,nH],2,nH);

end