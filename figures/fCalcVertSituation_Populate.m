function varargout = fCalcVertSituation_Populate(src,~)

% Function takes care of populating the CalcVertSituation with the correct
% Bauteile from the chosen Knoten

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fCalcVertSituation
% Data
global Knoten
global Bauteile
global Vorsatz

%% %%%%%% RETRIEVE THE OBJECTS IN THE PANEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve all the objects which have as parent the panel
obj = fCalcVertSituation.Children;

% Partition dropdown
pDD = findobj(obj,'Tag','PartitionDD');

% Text fields
Lx = findobj(obj,'Tag','LxTXT');
Ly = findobj(obj,'Tag','LyTXT');
LRLz = findobj(obj,'Tag','LRLzTXT');
URLz = findobj(obj,'Tag','URLzTXT');

% Radio buttons
rad = findobj(obj,'Tag','AnisotropieRAD');
aniso(1) = findobj(rad.Children,'Tag','AnisoNoneRAD');
aniso(2) = findobj(rad.Children,'Tag','AnisoXRAD');
aniso(3) = findobj(rad.Children,'Tag','AnisoYRAD');

% Knoten dropdowns
kDD(1) = findobj(obj,'Tag','Knoten1DD');
kDD(2) = findobj(obj,'Tag','Knoten2DD');
kDD(3) = findobj(obj,'Tag','Knoten3DD');
kDD(4) = findobj(obj,'Tag','Knoten4DD');

% Bauteile dropdowns
bDD(1) = findobj(obj,'Tag','Bauteil1DD');
bDD(2) = findobj(obj,'Tag','Bauteil2DD');
bDD(3) = findobj(obj,'Tag','Bauteil3DD');
bDD(4) = findobj(obj,'Tag','Bauteil4DD');

% Vorsatzschalen lower room
vDD(1) = findobj(obj,'Tag','VorsatzLR1DD');
vDD(2) = findobj(obj,'Tag','VorsatzLR2DD');
vDD(3) = findobj(obj,'Tag','VorsatzLR3DD');
vDD(4) = findobj(obj,'Tag','VorsatzLR4DD');
vDD(5) = findobj(obj,'Tag','VorsatzCeilingDD');

% Vorsatzschalen upper room
vDD(6) = findobj(obj,'Tag','VorsatzFloorDD');
vDD(7) = findobj(obj,'Tag','VorsatzUR1DD');
vDD(8) = findobj(obj,'Tag','VorsatzUR2DD');
vDD(9) = findobj(obj,'Tag','VorsatzUR3DD');
vDD(10) = findobj(obj,'Tag','VorsatzUR4DD');

% Axes
ax = findobj(obj,'Tag','VertSituationAX');

%% %%%%%%%%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which things in the panel need to be updated depending on which
% graphical object is the source of the call
% Create an array with the graphical objects and a corresponding array with
% the updateLevel
ov = @(x)(ones(1,x));

elem =  [pDD, aniso,     kDD,      vDD, Lx,Ly,LRLz,URLz,     bDD];
upLev = [  1, 1,1,1, 2*ov(4), 3*ov(10),         4*ov(4), 5*ov(4)];

% Find which element called this function
b = (src==elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

%% %%%%%%%%%%%%%% INITIALIZE PARTITION DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%
% if the source calling the function is not any of the figure elements,
% that means we need to initialize the figure dropdowns with actual data to
% select from
% We need to find all Bauteile that are valid as a partition (in this case
% all the decken)
if updateLevel == 0
   % Find all the Decken
   bP = (Bauteile.TypeID == 2);
   
   % Check that we have at least one match, if we don't have it, we need to
   % throw an error and close the figure
   if ~any(bP)
       % Define the varargout variable as false
       varargout{1} = false;
       % Throw the error
       errordlg(['Please define at least ONE Decke to calculate a',...
                 ' vertical situation!!'],'No decke - Error','modal');
       return;
   end
   
   % Update the dropdown
   pDD.String = Bauteile.Dropdown(bP);
   % Try to reuse the previously selected partition bauteil
   if isfield(fCalcVertSituation.Data,'Partition') && ...
      isfield(fCalcVertSituation.Data,'Bauteile')
       pDD.Value = updateValue(fCalcVertSituation.Data.Partition,...
                               Bauteile.ID(bP),...
                               pDD.Value);
   end
   
   % Update global variable
   fCalcVertSituation.Data.Partition = Bauteile.ID(bP);
   
   % Initialize the fields for storing the situation 
   fCalcVertSituation.Data.Bauteile = zeros(1,9);
   fCalcVertSituation.Data.Anisotropie = zeros(1,9);
   fCalcVertSituation.Data.Vorsatz = zeros(1,10);
end

% Retrieve the Partition Bauteil ID
PartitionID = fCalcVertSituation.Data.Partition(pDD.Value);
% Retrieve the Anisotropie of Partition Bauteil
PartitionAnisotropie = aniso(1).Value*0 + ...
                       aniso(2).Value*1 + ...
                       aniso(3).Value*2;

% Update the Bauteile field of the global structure with the currently
% selected partition and the current anisotropie
fCalcVertSituation.Data.Bauteile(5) = PartitionID;
fCalcVertSituation.Data.Anisotropie(5) = PartitionAnisotropie;

% Define mapping of global anisotropie to local Knoten Anisotropie
AnisoMap = [0,1,2;...   % Global anisotropie direction
            0,2,1;...   % Knoten 1/3 local anisotropie ID
            0,1,2];     % Knoten 2/4 local anisotropie ID

% Find the correct anisotropie for Knoten 1/3 and Knoten 2/4
bA = (PartitionAnisotropie == AnisoMap(1,:));
aniso13 = AnisoMap(2,bA);
aniso24 = AnisoMap(3,bA);

%% %%%%%%%%%%%%%%%%% UPDATE KNOTEN DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source of the calling is the Partition dropdown or the 
% Partition anisotropie radio buttons, then the user has
% selected a new partition bauteil and we need to find all Knoten that
% contain that Bauteil as Bauteil 1/3 and have the correct anisotropie 
if updateLevel < 2    
    % Find Knoten with Vertical Ausrichtung
    bK = (Knoten.Ausrichtung == 1);
    % Find Knoten with a valid Bauteil
    bB = (Knoten.Bauteile(:,[1,3]) == PartitionID);
    
    % Find Bauteile with a valid Anisotropie
    bB13 = bB;
    bB24 = bB;
    bB13(:,:,2) = (Knoten.Anisotropie(:,[1,3]) == aniso13);
    bB24(:,:,2) = (Knoten.Anisotropie(:,[1,3]) == aniso24);
    
    % Find the Knotens with valid Bauteile
    bB13 = any(all(bB13,3),2);
    bB24 = any(all(bB24,3),2);
    
    % Compound with Ausrichtung to find valid Knoten
    bK13 = all([bK,bB13],2);
    bK24 = all([bK,bB24],2);
   
    % Update the dropdown menus for Knotens 1/3
    [dd,kIDs] = addNone(Knoten.Dropdown(bK13),Knoten.ID(bK13));    
    for i=1:2:3
        kDD(i).String = dd;
        % Try to update to the previous value
        if isfield(fCalcVertSituation.Data,'Knoten') && ...
           (numel(fCalcVertSituation.Data.Knoten)>=i)
            kDD(i).Value = updateValue(fCalcVertSituation.Data.Knoten{i},...
                                       kIDs,kDD(i).Value);
        end
        % Update the global structure
        fCalcVertSituation.Data.Knoten{i} = kIDs;
    end

    % Update the dropdown menus for Knotens 2/4
    [dd,kIDs] = addNone(Knoten.Dropdown(bK24),Knoten.ID(bK24));   
    for i=2:2:4
        kDD(i).String = dd;
        % Try to update to the previous value
        if isfield(fCalcVertSituation.Data,'Knoten') && ...
           (numel(fCalcVertSituation.Data.Knoten)>=i)
            kDD(i).Value = updateValue(fCalcVertSituation.Data.Knoten{i},...
                                       kIDs,kDD(i).Value);
        end
        % Update the global structure
        fCalcVertSituation.Data.Knoten{i} = kIDs;
    end
end

% Retrieve the Knoten IDs
fCalcVertSituation.Data.KnotenID = zeros(1,4);
for i=1:4
    fCalcVertSituation.Data.KnotenID(i) = ...
        fCalcVertSituation.Data.Knoten{i}(kDD(i).Value);
end

%% %%%%%%%%%%%% UPDATE KNOTEN BAUTEIL DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source calling the function is a Knoten dropdown, we need to take
% care of finding out which of the Bauteil 1/3 is a valid ceiling / floor
% if a ceiling / floor has already been selected, if not we can choose
% whatever bauteil 1/3 we want
if updateLevel < 3
    % Find the caller
    caller = (kDD == src);
    
    % Check if we have a specific caller or if we are just initializing
    if any(caller)
        % Define start and end of for loop
        k = find(caller);
        n = k;
    else
        k = 1;
        n = 4;
    end
    
    % Define the valid dropdown values
    dd = {'Bauteil 1','Bauteil 3'};
    IDs = [1,3];
    
    % Repeat the process for multiple dropdowns
    for i=k:n
        % Find the Knoten ID
        kID = fCalcVertSituation.Data.Knoten{i}(kDD(i).Value);
        % Boolean vector for identifying the Knoten
        bK = (Knoten.ID == kID);
        
        % Check if we have a match with the Knoten
        if any(bK)
            % Retrieve the correct Bauteile
            bB = (Knoten.Bauteile(bK,[1,3]) == PartitionID);
            
            % Check Bauteile for Knoten 1/3
            if mod(i,2)
                % Retrieve the correct anisotropie
                bA = (Knoten.Anisotropie(bK,[1,3]) == aniso13);
            else
                bA = (Knoten.Anisotropie(bK,[1,3]) == aniso24);
            end
            % Compound to find valid Bauteile
            bB = all([bB;bA],1);
            
            % Update the dropdowns
            bDD(i).String = dd(bB);
            bDD(i).Value = 1;
            % Update the global data variable
            fCalcVertSituation.Data.KnotenBauteil{i} = IDs(bB);
        else
            % None dropdown
            [bdd,bID] = addNone();
            bDD(i).String = bdd;
            bDD(i).Value = 1;
            fCalcVertSituation.Data.KnotenBauteil{i} = bID;
        end
    end
end

% Retrieve the Bauteil identifying the partition
fCalcVertSituation.Data.KnotenBauteilID = zeros(1,4);
for i=1:4
    fCalcVertSituation.Data.KnotenBauteilID(i) = ...
        fCalcVertSituation.Data.KnotenBauteil{i}(bDD(i).Value);
end

% Redefine the anisotropie mapping for vertical elements
x = 1000;
AnisoMap = [0,1,2,3;...   % Global anisotropie direction
            0,2,x,1;...   % Knoten 1/3 local anisotropie ID
            0,x,2,1];     % Knoten 2/4 local anisotropie ID

% Retrieve the current Knoten and Bauteile selected in the dropdowns and
% store them in the Bauteile and Anisotropie structure
for i=1:4
    % Retrieve the Knoten selected
    kID = fCalcVertSituation.Data.Knoten{i}(kDD(i).Value);
    % Find a match
    bK = (Knoten.ID == kID);
    
    % Check if we have a match with the Knoten
    if any(bK)
        % Retrieve the Bauteile and the anisotropie
        baut = Knoten.Bauteile(bK,[4,2]);
        anis = Knoten.Anisotropie(bK,[4,2]);
        
        % Calculate the corresponding anisotropie ID
        if mod(i,2) % Knoten 1/3
            [~,pos] = ismember(anis,AnisoMap(2,:));
        else
            [~,pos] = ismember(anis,AnisoMap(3,:));
        end
        anis = AnisoMap(1,pos);
    else
        % When no match, we set everything to 0
        baut = [0,0];
        anis = [0,0];
    end
    
    % Update the global variable
    fCalcVertSituation.Data.Bauteile(i) = baut(1);
    fCalcVertSituation.Data.Bauteile(i+5) = baut(2);
    fCalcVertSituation.Data.Anisotropie(i) = anis(1);
    fCalcVertSituation.Data.Anisotropie(i+5) = anis(2);
end

%% %%%%%%%%%%%%%%%%%%% UPDATE VORSATZSCHALE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source calling the function has an update Level smaller than 3 we 
% need to update the dropdown menus
if updateLevel < 3
    % Mapping between Vorsatzschalen index and Bauteil index
    bauIndx = [1,2,3,4,5,5,6,7,8,9];
    for i=1:10
        % Retrieve the Bauteil ID
        bID = fCalcVertSituation.Data.Bauteile(bauIndx(i));
        % Find valid Vorsatzschalen
        bV = any(Vorsatz.Bauteile == bID,2);
        % Prepare the dropdowns
        [dd,vIDs] = addNone(Vorsatz.Dropdown(bV),Vorsatz.ID(bV));
            
        % Update the dropdown
        vDD(i).String = dd;
        % Try to update to the previous value
        if isfield(fCalcVertSituation.Data,'VorsatzDD') && ...
           (numel(fCalcVertSituation.Data.VorsatzDD)>=i)
            vDD(i).Value = updateValue(fCalcVertSituation.Data.VorsatzDD{i},...
                                       vIDs,vDD(i).Value);
        end
        % Update the global structure
        fCalcVertSituation.Data.VorsatzDD{i} = vIDs;
        fCalcVertSituation.Data.Vorsatz(i) = vIDs(vDD(i).Value);
    end
end

%% %%%%%%%%%%%%%%%%%% UPDATE SELECT VORSATZ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the source has an updateLevel of 3, we can update a single Vorsatz
% value in the Vorsatz structure
if updateLevel == 3
    % Find out the caller
    caller = (vDD == src);
    % Retrieve the selected value and store the selected Vorsatz
    fCalcVertSituation.Data.Vorsatz(caller) = ...
        fCalcVertSituation.Data.VorsatzDD{caller}(vDD(caller).Value);
end 

%% %%%%%%%%%%%%%%%%%% UPDATE SIZE OF STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%
if updateLevel == 4
    % Only check that the inputted string is a valid one, and if not, then
    % throw an error and maintain the old value
    caller = (src == [Lx,Ly,LRLz,URLz]);
    % Define defaults values when we don't input a string
    defaults = {'7.0','7.0','3.0','3.0'};
    % Try to convert the string and if we have an error, we use the default
    % stringa and we throw an error
    val = str2double(src.String);
    if isnan(val) || isinf(val) || (val < 0)
        % Reset the default string
        src.String = defaults{caller};
        % Throw an error
        errordlg('Invalid length! Please input a valid length.',...
                 'Invalid length - Error','modal');
    end
end

%% %%%%%%%%%%%%%%%%%%% UPDATE 3D-PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In any case update the 3D-Plot of the situation, by retrieving the data
% about which Bauteils and which Vorsatzschalen are used
lx = str2double(Lx.String);
ly = str2double(Ly.String);
lzLR = str2double(LRLz.String);
lzUR = str2double(URLz.String);

% Plot the 3D-Plot
plotVerticalSituation(ax,fCalcVertSituation.Data.Bauteile,...
                         fCalcVertSituation.Data.Anisotropie,...
                         fCalcVertSituation.Data.Vorsatz,...
                         lx,ly,lzLR,lzUR);

% Correct the position of the axes to take advantage of all the available 
% space, save space for the legend
legPos = ax.Legend.Position;
axPos = fCalcVertSituation.Data.AxesPosition;

% Calculate the new legend position
legPos = [axPos(1) + axPos(3) - legPos(3),...
          axPos(2) + 0.5*(axPos(4) - legPos(4)),...
          legPos(3),legPos(4)];
ax.Legend.Position = legPos;

% Calculate new axes position
ax.Position = [axPos(1), axPos(2),...
               axPos(3) - legPos(3),...
               axPos(4)];
           
%% %%%%%%%%%%%%%%%%%% KEEP TRACK OF CHANGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fCalcVertSituation.Data.Changed = true;

%% %%%%%%%%%%%%%%%%%% UPDATE THE VARARGOUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The first output argument tells the caller if we passed the population or
% not
varargout{1} = true;

end