function fExportVUbertragung_Build()

% Function defines the elements in the fExportVUbertragung figure 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fExportVUbertragung
global AvailableScreen

% Handler array containing all figure
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
radio = [80,20];
check = [80,20];
drop = [100,20];
text = [200,20];
button = [150,20];      % Large button
table = [500,110];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width =  max([label(1)+2*radio(1)+2*intra(1),...
                  label(1)+3*check(1)+3*intra(1),...
                  label(1)+drop(1)+intra(1),...
                  label(1)+text(1)+intra(1),...
                  table(1),...
                  label(1)+2*button(1)+2*intra(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = max([6*label(2),...
                  label(2)+text(2)+drop(2)+check(2)+2*radio(2)])+...
                  7*intra(2) + table(2)+button(2)+ marg(2) + marg(4);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the figure position
pos = centerOnBox(AvailableScreen,[0,0,min_width,min_height]);

% Create a new full screen figure
fExportVUbertragung = figure('Name','Export Vorsatz-Ubertragung',...
                             'NumberTitle','off',...
                             'ToolBar','none','MenuBar','none',...
                             'Units','pixels','Resize','off',...
                             'Position',pos,...
                             'CloseRequestFcn',@closeFigure,...
                             'Visible','off',...
                             'WindowStyle','modal');
% Add figure to a handler list for closing
h_fig = [h_fig,fExportVUbertragung];

% Add property to figure for storage of dropdowns, values, etc
addprop(fExportVUbertragung,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Messungstyp','Strahlungstyp','Anisotropie','Vorsatzschale',...
           'Original DB-ID','Pegelreduktion (dR/dL-Werte)'};
tags = {'Messungstyp','Stahlungstyp','Anisotropie','Vorsatz',...
        'OriginalDBID','RLWerte'};
lbl_tag = strcat(tags,'LBL');
for i=1:numel(lbl_txt)
    uicontrol('Parent',fExportVUbertragung,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the Messungstyp group of radio buttons
rad_txt = {'Luftschall','Trittschall'};
tags = {'Luftschall','Trittschall'};
rad_tag1 = strcat(tags,'RAD');
messung = uibuttongroup('Parent',fExportVUbertragung,'Units','pixels',...
                        'Visible','on','BorderType','none',...
                        'Tag','MessungRAD');
for i=1:numel(rad_tag1)
    uicontrol('Parent',messung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Enable','inactive',...
              'Tag',rad_tag1{i});
end

% Define the text to be displayed in the Strahlungtyp group of radio buttons
rad_txt = {'Luft','Tritt'};
tags = {'Luft','Tritt'};
tip = {sprintf('Luftschall-Anregung\nLuftschall-Abstrahlung\nTrittschall-Abstrahlung'),...
               'Trittschall-Anregung'};
rad_tag2 = strcat(tags,'RAD');
strahlung = uibuttongroup('Parent',fExportVUbertragung,'Units','pixels',...
                          'Visible','on','BorderType','none',...
                          'Tag','StrahlungRAD');
for i=1:numel(rad_tag2)
    uicontrol('Parent',strahlung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'TooltipString',tip{i},...
              'Tag',rad_tag2{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the check buttons
che_txt = {'Parallel','Senkrecht','Direkt'};
tags = {'AnisoParall','AnisoSenk','AnisoDir'};
tip = {sprintf('G�ltig f�r Weg parallel\nzur Anisotropierichtung'),...
       sprintf('G�ltig f�r Weg senkrecht\nzur Anisotropierichtung'),...
               'G�ltig f�r Direktdurchgang'};
che_tag = strcat(tags,'CHE');
for i=1:numel(che_txt)
    uicontrol('Parent',fExportVUbertragung,'Visible','on','Units','pixels',...
              'Style','checkbox','String',che_txt{i},...
              'TooltipString',tip{i},...
              'Tag',che_tag{i});
end
%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the dropdown
tags = {'Vorsatz'};
dd_tag = strcat(tags,'DD');
for i=1:numel(dd_tag)
    uicontrol('Parent',fExportVUbertragung,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'String',' ','Tag',dd_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed
tags = {'OriginalDB'};
txt_tag = strcat(tags,'TXT');
for i=1:numel(txt_tag)
    uicontrol('Parent',fExportVUbertragung,'Style','text',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb = uitable('Parent',fExportVUbertragung,'Visible','on',...
             'RowName',[],'ColumnName',[],'RowStriping','on',...
             'Units','pixels','Tag','RLWerteTABLE');

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display
but_txt = {'Cancel','Export Vorsatz-Ubertragung'};
tags = {'Cancel','Export'};
but_tag = strcat(tags,'BUT');
but_fcn = {'closeFigure','exportVUbertragung'};
but_fcn = strcat('@',but_fcn);
for i=1:numel(but_txt)
    uicontrol('Parent',fExportVUbertragung,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 7; 
nV = 8;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = label(1);
h2 = round(max([(radio(1)-2*intra(1))/3,...
                (check(1)-intra(1))/2,...
                (drop(1)-5*intra(1))/6,...
                (text(1)-5*intra(1))/6,...
                (table(1)-h1-5*intra(1))/6,...
                (button(1)-2*intra(1))/3]));
shareH = [h1, h2*ones(1,6)]...
            /(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));

l = label(2);
t = max(l,text(2));
d = max(l,drop(2));
c = max(l,check(2));
r = max(l,radio(2));
shareV = [button(2), 0, l, t, d, c, r, r]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));
shareV(2) = 1-shareV(1)-sum(shareV(3:end));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have fExportVUbertragung as Parent
obj = findobj('Parent',fExportVUbertragung);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV+1;
for j=1:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end
% Modify the last position
lbl.Position = BoundingBox(boxes,[1,nH],i-j,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the subdivision of the radio groups
rSub = 2;

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,nH],nV,nH);
% Place the group
messung.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
for j=1:rSub
    % Retrieve the radios and place it correctly
    rad = findobj(messung.Children,'Tag',rad_tag1{j});
    rad.Position = BoundingBox(subboxes,j,1,rSub);
end

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,nH],nV-1,nH);
% Place the group
strahlung.Position = box;
for j=1:rSub
    % Retrieve the radios and place it correctly
    rad = findobj(strahlung.Children,'Tag',rad_tag2{j});
    rad.Position = BoundingBox(subboxes,j,1,rSub);
end

%% %%%%%%%%%%%%%%%%%%% PLACE CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:3
    % Retrieve the checkboxes and place tehm correctly
    che = findobj(obj,'Tag',che_tag{j});
    che.Position = BoundingBox(boxes,[2,3]+(j-1)*2,nV-2,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Place Vorsatz DD
dd = findobj(obj,'Tag','VorsatzDD');
dd.Position = BoundingBox(boxes,[2,nH],5,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 4;
for j=1:1
    % Retrieve the text element and place it
    txt = findobj(obj,'Tag',txt_tag{j});
    txt.Position = BoundingBox(boxes,[2,nH],i,nH);
end

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','CancelBUT');
but.Position = BoundingBox(boxes,[2,4],1,nH);
but = findobj(obj,'Tag','ExportBUT');
but.Position = BoundingBox(boxes,[5,7],1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb.Position = BoundingBox(boxes,[1,nH],2,nH);