function fullScreen()

% Function return the position vector indicating the full screen as
% specified in this file

%% Define a global available variable
global AvailableScreen

%% Windows taskbar
% Height of the windows taskbar
winTaskBar = 40;
% Consider windows taskbar
B_winTaskBar = true;

%% Figure top bar
% Height of the figure bar
figureBar = 30;
% Consider the figure bar
B_figureBar = true;

%% Figure borders
% Margins at left, bottom and right
borders = 8;
% Consider the borders
B_borders = true;

%% Get the full screen size
fullscreen = get(0,'ScreenSize');

%% Calculate the correction values
% Correction left
c_Left = B_borders*borders; 

% Correction bottom
c_Bottom = B_borders*borders + B_winTaskBar*winTaskBar; 

% Correction width
c_Width = -2*B_borders*borders;

% Correction height
c_Height = -B_borders*borders - B_winTaskBar*winTaskBar - B_figureBar*figureBar; 

% Create the correction vector
fullscreen_Correction = [c_Left, c_Bottom, c_Width, c_Height];

%% Store the corrected value in the globally available variable
AvailableScreen = fullscreen + fullscreen_Correction;