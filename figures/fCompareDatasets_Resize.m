function fCompareDatasets_Resize(src, callbackdata)
% Function takes care of resizing all elements within the function for
% a new function size

%%%%%%%%%%%%%%%%%% SPECIFY SKELETON OF UI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the number of horizontal elements and the number of vertical
% elements
nH = 13;
nV = 11;

%%%%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg_fig = 15*ones(4,1);    % [left, bottom, right, top]

% Spaces in between elements
intra = [10, 10];

% Minimum sizes
min_list = [300,150];
min_button = [150,20];
min_button_small = [50,20];
min_title = [150,20];
min_tab = [900,500];

%%%%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width = max([2*min_list(1) + min_button_small(1) + 2*intra(1);...
                 4*min_button(1) + 3*intra(1);...
                 2*min_title(1) + intra(1);...
                 min_tab(1)]) + ...
            marg_fig(1) + marg_fig(3);
        
% Minimum height
min_height = min_tab(2) + 3*min_button(2) + 5*intra(2) + ...
             max([min_list(2);
                  4*min_button_small(2) + 3*intra(2)]) + ...
             marg_fig(2) + marg_fig(4);

% Check if the figure needs to be resized, so that the minimum sizes are
% respected
fpos = src.Position;
w = max(min_width, fpos(3));
h = max(min_height, fpos(4));
src.Position = [fpos(1:2), w, h];

% Check that the position is within the boundaries of the desktop
src.Position = withinScreen(src.Position);

% Calculate fpos relative to window
fpos = src.Position;
fpos(1:2) = 0;

%%%%%%%%%%%%%%%%%%%% GET ELEMENT POSITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate shares of each element of the GUI to the total share
shareH = ones(nH,1)/nH;
h = max([min_list(2) - 5*intra(2),6*min_button_small(2)])/6;
shareV = [0, min_button(2), min_button(2), min_button(2), ...
          h, h, h, h, h, h, min_title(2)]...
             /(fpos(4) - marg_fig(2) -marg_fig(4) - 8*intra(2));
shareV(1) = 1 - sum(shareV(2:end));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(fpos, nH, nV, marg_fig, intra, shareH, shareV);

%%%%%%%%%%%%%%%%%%%% FIND OBJECT THAT HAVE AS PARENT THE FIGURE %%%%%%%%%%
obj = findobj('Parent',src);

%%%%%%%%%%%%%%%%%%%% PLACE LABEL ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 11;
lb = findobj(obj,'Tag','SelDatasetsLB');
lb.Position = BoundingBox(boxes,[1,6],i,nH);
lb = findobj(obj,'Tag','ComparisonSelDatasetsLB');
lb.Position = BoundingBox(boxes,[8,13],i,nH);

%%%%%%%%%%%%%%%%%%%% PLACE LIST ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = [5,10];
lst = findobj(obj,'Tag','SelDatasetsLST');
lst.Position = BoundingBox(boxes,[1,6],i,nH);
lst = findobj(obj,'Tag','ComparisonSelDatasetsLST');
lst.Position = BoundingBox(boxes,[8,13],i,nH);

%% %%%%%%%%%%%%%%%%%% PLACE BUTTON ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 4;
% Buttons for operating on the datasets from which datasets for comparison
% can be drawn from
but = findobj(obj,'Tag','gotof2BUT');
but.Position = BoundingBox(boxes,[1,2],i,nH);
but = findobj(obj,'Tag','RemoveDatasetBUT');
but.Position = BoundingBox(boxes,[3,4],i,nH);
but = findobj(obj,'Tag','ClearDatasetsBUT');
but.Position = BoundingBox(boxes,[5,6],i,nH);

% Buttons for performing calculation on the datasets selected for
% comparison
i = 4;
but = findobj(obj,'Tag','CompareDatasetBUT');
but.Position = BoundingBox(boxes,[8,10],i,nH);
but = findobj(obj,'Tag','AverageDatasetBUT');
but.Position = BoundingBox(boxes,[11,13],i,nH);
% One line below
i = i-1;
but = findobj(obj,'Tag','EnergySubtractionBUT');
but.Position = BoundingBox(boxes,[8,9],i,nH);       i=i-1;
but = findobj(obj,'Tag','LevelSubtractionBUT');
but.Position = BoundingBox(boxes,[8,9],i,nH);

% Place button for adding or removing datasets from comparison list
i = 9;
but = findobj(obj,'Tag','ShiftUpBUT');
but.Position = BoundingBox(boxes,7,i,nH);   i = i-1;
but = findobj(obj,'Tag','AddSetBUT');
but.Position = BoundingBox(boxes,7,i,nH);   i = i-1;
but = findobj(obj,'Tag','RemoveSetBUT');
but.Position = BoundingBox(boxes,7,i,nH);   i = i-1;
but = findobj(obj,'Tag','ShiftDownBUT');
but.Position = BoundingBox(boxes,7,i,nH);

%% %%%%%%%%%%%%%%%%%% PLACE SLIDERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 3;
txt = findobj(obj,'Tag','EnergyThresholdLBL');
txt.Position = BoundingBox(boxes,[10,11],i,nH);
sli = findobj(obj,'Tag','EnergyThresholdSLI');
sli.Position = BoundingBox(boxes,[12,13],i,nH);

%% %%%%%%%%%%%%%%%%%% PLACE TAB ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab = findobj(obj,'Tag','topCompTAB');
tab.Position = BoundingBox(boxes,[1,13],1,nH);

end