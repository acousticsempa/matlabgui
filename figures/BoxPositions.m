function [boxes] = BoxPositions(box, nH, nV, margins, inter, shareH, shareV)
% Function calculates the positions for putting multiple boxes in a given 
% space, considering margins to the side, in between, above and below
%
% Explanation of the input variables
% box:      Standard position array [left, bottom, width, height]
% nH:       Number of elements in the horizontal direction
% nV:       Number of elements in the vertical direction
% margins:  Array containing the margins around the box
%           [margin_Left, margin_Bottom, margin_Right, margin_Top]
% inter:    Array containing the interspaces between the elements
%           [interspace_Horizontal, interspace_Vertical]
% shareH:   Share of the usable width taken by each box
% shareV:   Share of the usable height taken by each box

%%%%%%%%%%%%%%%%% CONTROL THE INPUTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In case only the 6th argument has been specified
switch nargin
    case 6
        % Case in which only one share has been specified
        if numel(shareH) == nH
            % If the number of element corresponds, we just need to specify
            % the share for the vertical elements
            shareV = ones(nV,1)/nV;
        else
            % In this case we need to specify the share for the horizontal
            % elements and copy the input to the input to the vertical
            % elements share
            shareV = shareH; 
            shareH = ones(nH,1)/nH;
        end
    case 7
        % Check that the shares have the correct size
        if numel(shareH) ~= nH 
            % Throw error
            fprintf('Please input a shareH vector with correct size!\n');
        end
        if numel(shareV) ~= nV
            % Throw error
            fprintf('Please input a shareV vector with correct size!\n');
        end
    otherwise
        % Case in which the shares haven't been specified
        shareH = ones(nH,1)/nH;
        shareV = ones(nV,1)/nV;
end


%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the usable width and height
usable_width = box(3) - margins(1) - margins(3) - (nH-1)*inter(1);
usable_height = box(4) - margins(2) - margins(4) - (nV-1)*inter(2);

% Calculate the box width and heights
box_width = usable_width*shareH;
box_height = usable_height*shareV;

% Initialize the boxes variable
boxes = zeros(nH*nV,4);

% Iterate over all boxes and return their position based on the inputs
% Iterate over the rows
for j=1:nV
    % Iterate over the columns
    for i=1:nH
        % Calculate the box position
        boxes(i+(j-1)*nH,1) = box(1) + margins(1) + ...
                              sum(box_width(1:(i-1))) + (i-1)*inter(1);
        boxes(i+(j-1)*nH,2) = box(2) + margins(2) + ...
                              sum(box_height(1:(j-1))) + (j-1)*inter(2);
        boxes(i+(j-1)*nH,3) = box_width(i);
        boxes(i+(j-1)*nH,4) = box_height(j);
    end
end

% Round to nearest integer (for pixels)
if max(boxes(:)) > 1
    boxes = round(boxes,0);
end

end