function fExportOriginalDB_Populate()

% Function is called when the user wants to export the averaged data to the
% Original DB and needs to populate the fExportOriginalDB figure

%% %%%%%%%%%%%%%%%%%%%%% GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fExportOriginalDB
% Data
global Data
global CompareDatasetsData
global ExportOriginalDBData
                     
% Extract the relevant data from the global structure
data = CompareDatasetsData.Average;

% Only Air field of the Data global structure is available, otherwise we
% have a problem
DB = Data.Air;
% Number of projects is set to two by default
n = 2;

%% %%%%%%%%%%%%%%%%% POPULATE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create empty cell holders for test description and test object
testDescr = cell(n,1); 
testObj = cell(n,1);

% For each selected report retrieve the project description and the object
% description, repeat for all reports
for i=1:n
    % Retrieve the project index
    indx = data.IDs(i,2);
    % Retrieve the project description and the test object
    testDescr{i,1} = DB.Test.Description{indx,1};
    testObj{i,1} = DB.Test.Object{indx,1};
end

% Retrieve the unique test description and unique test objects
[~,testDescr] = getUniques(testDescr);
[~,testObj] = getUniques(testObj);

% %%%%%%%%% Create a substring for display in the dropdown menus %%%%%%%%%
% Define the substring length
substrLength = 60;
% Retrieve number of different test descriptions and test objects
k = numel(testDescr);
m = numel(testObj);
% Initialize cell arrays for dropdown menus
testDescrDD = cell(k,1); 
testObjDD = cell(m,1);

% Go through all the test descriptions
for i=1:k
    % Get the reduced size string
    str = testDescr{i,1}; 
    s = min(substrLength, numel(str));
    testDescrDD{i,1} = ['(',num2str(i,'%.0f'),') ',str(1:s)];
end
% Go through all the test objects
for j=1:m
    % Get the reduced size string
    str = testObj{j,1};
    s = min(substrLength, numel(str));
    testObjDD{j,1} = ['(',num2str(j,'%.0f'),') ',str(1:s)];
end

% %%%%%%%%%%%% PLACE THE STRINGS IN THE DROPDOWN MENUS %%%%%%%%%%%%%%%%%%
% Find all objects in the figure
obj = findobj('Parent',fExportOriginalDB);
% Find the dropdown menus and populate them
dd = findobj(obj,'Tag','ExportTestDescrDD');
dd.String = testDescrDD;
dd.Value = 1;
dd = findobj(obj,'Tag','ExportTestObjDD');
dd.String = testObjDD;
dd.Value = 1; 

% Initialize the text fields with the first dropdown option
txt = findobj(obj,'Tag','ExportTestDescrTXT');
txt.String = testDescr{1,1};
txt = findobj(obj,'Tag','ExportTestObjTXT');
txt.String = testObj{1,1};

% Store the dropdown texts for later
ExportOriginalDBData.TestDescrDD = testDescr;
ExportOriginalDBData.TestObjDD = testObj;

%% %%%%%%%%%%%%%%%% POPULATE THE REPORT NAME %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retreive the report names and concatenate them with a / separator
repN = cell(1,n);
for i=1:n
    % Retrieve the report name
    repN{1,i} = DB.Report{data.IDs(i,2),1};
end
% Concatenate the report names
str = strjoin(repN,'/');

% Place the string in the report name text field
txt = findobj(obj,'Tag','ReportNameTXT');
txt.String = str;

%% %%%%%%%%%%%%%%%%% POPULATE THE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MeasFreq = [50,63,80,100,125,160,200,250,315,400,500,630,...
            800,1000,1250,1600,2000,2500,3150,4000,5000];
% Prefix in the string
cstr = {'C','Ctr','C50-3150','Ctr50-3150','C50-5000',...
        'Ctr50-5000','C100-5000','Ctr100-5000'};

% Specify the column head
col_head = {'SNR / C','[dB]','','Freq [Hz]','R [dB]','','Freq [Hz]','R [dB]'};
% Create table data
tblData = cell(11,8);
% Create table data
tblData{1,1} = 'SNR';
tblData{1,2} = data.SNR;
di = 1;
for i=1+di:numel(cstr)+di
    tblData{i,1} = cstr{i-di};
    tblData{i,2} = data.C(i-di);
end
for i=1:11
    tblData{i,4} = MeasFreq(i);
    tblData{i,5} = data.Values(i);
end
for i=1:10
    tblData{i,7} = MeasFreq(i+11);
    tblData{i,8} = data.Values(i+11);
end

% Retrieve the table
tb = findobj(obj,'Tag','ValuesTABLE');
% Put data in the table
tb.ColumnName = [];
tb.RowName = [];

% Specify Column name
tb.ColumnName = col_head;

% Specify column width
wtext = 65;
wdata = 60;
wspace = 20;
tb.ColumnFormat = {[],'bank',[],[],'bank',[],[],'bank'};
colWidth = {wtext,wdata,wspace,wtext,wdata,wspace,wtext,wdata};
tb.ColumnWidth = colWidth;

tb.Data = tblData;
end

