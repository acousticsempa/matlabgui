function fCreateVorsatz_Build()
% Function defines the elements in the fCreateVorsatz window 
% 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fCreateVorsatz
global AvailableScreen

% Handler array containing all figure
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
drop = [100,20];
button = [150,20];
but = [60,20];
text = [200,20];
textKur = [200,35];
textDet = [200,60];
list = [200,60];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width =  label(1) + ...
             max([text(1) + intra(1),...
                  textKur(1) + intra(1),...
                  textDet(1) + intra(1),...
                  drop(1)+but(1) + 2*intra(1),...
                  list(1) + intra(1),...
                  2*button(1) + 2*intra(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = button(2) + 5*intra(2) + ...
             max([5*label(2),...
                  list(2)+max(drop(2),but(2))+textDet(2)+textKur(2)+text(2)])+...
             + marg(2) + marg(4);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the figure position
pos = centerOnBox(AvailableScreen,[0,0,min_width,min_height]);

% Create a new full screen figure
fCreateVorsatz = figure('Name','Create Vorsatz','NumberTitle','off',...
                        'ToolBar','none','MenuBar','none',...
                        'Units','pixels','Resize','off',...
                        'Position',pos,...
                        'CloseRequestFcn',@closeFigure,...
                        'Visible','off');
% Add figure to a handler list for closing
h_fig = [h_fig,fCreateVorsatz];

% Add a Data property for storage of the data in the figure
addprop(fCreateVorsatz,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Vorsatz-Name','Kurzbeschreibung',...
           'Det. Beschreibung','Bauteile'};
tags = {'VorsatzName','Kurzbeschreibung','DetBeschreibung','Bauteile'};
lbl_tag = strcat(tags,'LBL');
for i=1:numel(lbl_txt)
    uicontrol('Parent',fCreateVorsatz,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%%% EDITABLE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the tags of the editable text fields
tags = {'VorsatzName','KurzBeschreibung','DetBeschreibung'};
txt_tag = strcat(tags,'TXT');
txt_max = [1,100,100];
for i=1:numel(txt_tag)
    uicontrol('Parent',fCreateVorsatz,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Min',0,'Max',txt_max(i),...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the first dropdown
tags = {'Bauteile'};
dd_tag = strcat(tags,'DD');
for i=1:numel(dd_tag)
    uicontrol('Parent',fCreateVorsatz,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'KeyPressFcn',@fCreateVorsatz_Populate,...
              'String',' ','Tag',dd_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'Cancel','Create Vorsatz','Add'};
tags = {'Cancel','CreateVorsatz','AddBauteil'};
button_tag = strcat(tags,'BUT');
button_fcn = {'closeFigure','exportVorsatz','fCreateVorsatz_Populate'}; 
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',fCreateVorsatz,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%% LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the lists
txt = ' ';
tags = 'SelBauteile';
list_tag = strcat(tags,'LST');
uicontrol('Parent',fCreateVorsatz,'Style','listbox','Min',0,...
          'Visible','on','Units','pixels',...
          'String',txt,'Tag',list_tag,...
          'KeyPressFcn',@fCreateVorsatz_Populate);

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 5; 
nV = 6;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
w = round(max([(text(1)-3*intra(1))/4,...
               (textKur(1)-3*intra(1))/4,...
               (textDet(1)-3*intra(1))/4,...
               (drop(1)-2*intra(1))/3,...
               (but(1)),...
               (list(1)-3*intra(1))/4,...
               (button(1)-intra(1))/2]));            
shareH = [label(1),w, w, w, w]...
            /(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));
shareV = [button(2), list(2), max(drop(2),but(2)), textDet(2), textKur(2), text(2)]...
            /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have fCreateKnoten as Parent
obj = fCreateVorsatz.Children;

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV+1;
for j=1:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV+1;
for j=1:3
    txt = findobj(obj,'Tag',txt_tag{j});
    txt.Position = BoundingBox(boxes,[2,nH],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dd = findobj(obj,'Tag','BauteileDD');
dd.Position = BoundingBox(boxes,[2,4],3,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lst = findobj(obj,'Tag','SelBauteileLST');
lst.Position = BoundingBox(boxes,[2,nH],2,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','AddBauteilBUT');
but.Position = BoundingBox(boxes,nH,3,nH);
but = findobj(obj,'Tag','CancelBUT');
but.Position = BoundingBox(boxes,[2,3],1,nH);
but = findobj(obj,'Tag','CreateVorsatzBUT');
but.Position = BoundingBox(boxes,[4,5],1,nH);