function fCalcVertSituation_Resize(src,~)

% Function takes care of resizing all elements withing the figure for a new
% figure size

% Global variables
global fCalcVertSituation

%% %%%%%%%%%%%%%%%%% SPECIFY SKELETON OF UI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 7; 
nV = 25;

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
title = [160,20];
subtitle = [160,20];
radio = [60,20];
check = [30,20];
label = [75,20];
space = [20,10];
drop = [150,20];
graph = [700,400];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
t = title(1);
st = subtitle(1);
s = space(1);
l = label(1);
r = radio(1);
d = drop(1);
min_width =  max([t, 2*st + s + 2*intra(1), l + 3*r + 4*intra(1),...
                  2*l + 2*d + s + 4*intra(1)]) + ...
             + s + 2*intra(1) + ...
             max([t,2*l + 2*d + s + 4*intra(1),graph(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
t = title(2);
st = subtitle(2);
l = label(2);
d = max(l,drop(2));
r = max(l,radio(2));
c = max(l,check(2));
s = space(2);
min_height = 5*t + 10*d + st + r + 5*c + 3*s + (nV-1)*intra(2) + ...
                 + marg(2) + marg(4);

% Check if the figure needs to be resized, so that the minimum sizes are
% respected
pos = src.Position;
src.Position = [pos(1:2), min_width, min_height];

% Check that the position is within the boundaries of the desktop
src.Position = withinScreen(src.Position);

% Calculate pos relative to window
pos = src.Position;
pos(1:2) = 0;

%% %%%%%%%%%%%%%%%%%% GET ELEMENT POSITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate shares of each element of the GUI to the total share
l = label(1);
d = drop(1);
shareH = [l,d,space(2),l,d,space(1),0]...
            /(pos(3)-marg(1)-marg(3)-(nH-1)*intra(1));
shareH(end) = 1-sum(shareH(1:end-1));

d = max(label(2),drop(2));
d4 = d*ones(1,4);
d5 = d*ones(1,5);
t = title(2);
s = space(2);
st = subtitle(2);
r = radio(2);
shareV = [d5, t, s, d5, st, t, s, d4, t, s, r, d, t, t]...
            /(pos(4) - marg(2) -marg(4) - (nV-1)*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Create functions for quick calculation of subboxes and subpositions
subBoxes = @(box,rSub)(BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra));
subPos = @(box,subbox,k,rSub)([box(1),box(2),0,0] + BoundingBox(subbox,k,1,rSub));

%%%%%%%%%%%%%%%%%%%% FIND OBJECT THAT HAVE AS PARENT THE FIGURE %%%%%%%%%%
obj = src.Children;

%% %%%%%%%%%%%%%%%%%% PLACE LABEL ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Place the title
i = nV;
j = 1;
lbl = findobj(obj,'Tag','VertSituationLBL');
lbl.Position = BoundingBox(boxes,[j,nH-1],i,nH);

% Place the Bauteil Label
i = i-1;
j = 1;
lbl = findobj(obj,'Tag','SelectPartitionLBL');
lbl.Position = BoundingBox(boxes,[j,nH-1],i,nH);    i=i-1;
lbl = findobj(obj,'Tag','PartitionLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);           i=i-1;
lbl = findobj(obj,'Tag','AnisotropieLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

% Place the Geometry Label
i = i+2;
j = nH;
lbl = findobj(obj,'Tag','DefineGeometryLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i = i-1;
rSub = 5;
k = 1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
lbl = findobj(obj,'Tag','PartitionGeomLBL');
lbl.Position = subPos(box,subbox,k,rSub);     k=k+1;
lbl = findobj(obj,'Tag','LxLBL');
lbl.Position = subPos(box,subbox,k,rSub);     k=k+2;
lbl = findobj(obj,'Tag','LyLBL');
lbl.Position = subPos(box,subbox,k,rSub);     i=i-1;
k = 1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
lbl = findobj(obj,'Tag','RoomHeightLBL');
lbl.Position = subPos(box,subbox,k,rSub);     k=k+1;
lbl = findobj(obj,'Tag','LRLzLBL');
lbl.Position = subPos(box,subbox,k,rSub);     k=k+2;
lbl = findobj(obj,'Tag','URLzLBL');
lbl.Position = subPos(box,subbox,k,rSub);

% Place the Knoten Labels
i = i-2;
j = 1;
lbl = findobj(obj,'Tag','SelectKnotenLBL');
lbl.Position = BoundingBox(boxes,[j,nH-1],i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Knoten1LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Knoten2LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Knoten3LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Knoten4LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

% Place the Knoten Partition Bauteil labels
i = i+3;
j = 4;
lbl = findobj(obj,'Tag','Bauteil1LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Bauteil2LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Bauteil3LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','Bauteil4LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

% Place the Vorsatz labels for Lower Room
i = i-2;
j = 1;
lbl = findobj(obj,'Tag','SelectVorsatzLBL');
lbl.Position = BoundingBox(boxes,[j,nH-1],i,nH);    i=i-1;
lbl = findobj(obj,'Tag','LowerRoomLBL');
lbl.Position = BoundingBox(boxes,[j,j+1],i,nH);    i=i-1;
lbl = findobj(obj,'Tag','LRWall1LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','LRWall2LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','LRWall3LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','LRWall4LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','LRCeilingLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

% Place the Vorsatz labels for Upper Room
i = i+5;
j = 4;
lbl = findobj(obj,'Tag','UpperRoomLBL');
lbl.Position = BoundingBox(boxes,[j,j+1],i,nH);    i=i-1;
lbl = findobj(obj,'Tag','URWall1LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','URWall2LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','URWall3LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','URWall4LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
lbl = findobj(obj,'Tag','URFloorLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

% Place the Pathways labels
i = i-2;
j = 1;
lbl = findobj(obj,'Tag','SelectPathwaysLBL');
lbl.Position = BoundingBox(boxes,[j,nH-1],i,nH);    i=i-1;
lbl = findobj(obj,'Tag','PathPartitionLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);        i=i-1;
lbl = findobj(obj,'Tag','PathKnoten1LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);        i=i-1;
lbl = findobj(obj,'Tag','PathKnoten2LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);        i=i-1;
lbl = findobj(obj,'Tag','PathKnoten3LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);        i=i-1;
lbl = findobj(obj,'Tag','PathKnoten4LBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

%% %%%%%%%%%%%%%%%%% EDIT TEXT ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV-(1)-(1);
j = nH;
k = 3;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
txt = findobj(obj,'Tag','LxTXT');
txt.Position = subPos(box,subbox,k,rSub);     k=k+2;
txt = findobj(obj,'Tag','LyTXT');
txt.Position = subPos(box,subbox,k,rSub);     i=i-1;
k = 3;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
txt = findobj(obj,'Tag','LRLzTXT');
txt.Position = subPos(box,subbox,k,rSub);     k=k+2;
txt = findobj(obj,'Tag','URLzTXT');
txt.Position = subPos(box,subbox,k,rSub);

%% %%%%%%%%%%%%%% PLACE RADIO BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV-(1)-(2);
j = 2;
radParent = findobj(obj,'Tag','AnisotropieRAD');
box = BoundingBox(boxes,[j,nH-1],i,nH);
radParent.Position = box;
% Position the inner elements of the radio group
rSub = 3;
subboxes = BoxPositions([0,0,box(3),box(4)],rSub,1,zeros(1,4),intra);
rad = findobj(radParent.Children,'Tag','AnisoNoneRAD');
rad.Position = BoundingBox(subboxes,1,1,rSub);
rad = findobj(radParent.Children,'Tag','AnisoXRAD');
rad.Position = BoundingBox(subboxes,2,1,rSub);
rad = findobj(radParent.Children,'Tag','AnisoYRAD');
rad.Position = BoundingBox(subboxes,3,1,rSub);

%% %%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Place the Partition Dropdown
i = nV-(1)-(1);
j = 2;
dd = findobj(obj,'Tag','PartitionDD');
dd.Position = BoundingBox(boxes,[j,j+3],i,nH);

% Place the Knoten Dropdowns
i = i-4;
j = 2;
dd = findobj(obj,'Tag','Knoten1DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','Knoten2DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','Knoten3DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','Knoten4DD');
dd.Position = BoundingBox(boxes,j,i,nH);
i = i+3;
j = 5;
dd = findobj(obj,'Tag','Bauteil1DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','Bauteil2DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','Bauteil3DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','Bauteil4DD');
dd.Position = BoundingBox(boxes,j,i,nH);

% Place the Vorsatz Dropdowns
i = i-4;
j = 2;
dd = findobj(obj,'Tag','VorsatzLR1DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzLR2DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzLR3DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzLR4DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzCeilingDD');
dd.Position = BoundingBox(boxes,j,i,nH);
i = i+4;
j = 5;
dd = findobj(obj,'Tag','VorsatzUR1DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzUR2DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzUR3DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzUR4DD');
dd.Position = BoundingBox(boxes,j,i,nH);    i=i-1;
dd = findobj(obj,'Tag','VorsatzFloorDD');
dd.Position = BoundingBox(boxes,j,i,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define spacing 
rSub = 3;

i = nV-(1)-(3+1)-(5+1)-(7+1)-(1);
j = [2,4];
k=1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
che = findobj(obj,'Tag','DdCHE');
che.Position = subPos(box,subbox,k,rSub);     i=i-1;

k=1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
che = findobj(obj,'Tag','Knoten1DfCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten1FdCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten1FfCHE');
che.Position = subPos(box,subbox,k,rSub);     i=i-1;

k=1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
che = findobj(obj,'Tag','Knoten2DfCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten2FdCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten2FfCHE');
che.Position = subPos(box,subbox,k,rSub);     i=i-1;

k=1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
che = findobj(obj,'Tag','Knoten3DfCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten3FdCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten3FfCHE');
che.Position = subPos(box,subbox,k,rSub);     i=i-1;

k=1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
che = findobj(obj,'Tag','Knoten4DfCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten4FdCHE');
che.Position = subPos(box,subbox,k,rSub);     k=k+1;
che = findobj(obj,'Tag','Knoten4FfCHE');
che.Position = subPos(box,subbox,k,rSub);

%% %%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 1;
j = nH;
rSub = 9;
k = 1;
dk = floor(rSub/2) - 1;
box = BoundingBox(boxes,j,i,nH);
subbox = subBoxes(box,rSub);
but = findobj(obj,'Tag','LuftschallBUT');
but.Position = subPos(box,subbox,[k,k+dk],rSub);     k=k+ceil(rSub/2);
but = findobj(obj,'Tag','TrittschallBUT');
but.Position = subPos(box,subbox,[k,k+dk],rSub);

%% %%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ax = findobj(obj,'Tag','VertSituationAX');
box = BoundingBox(boxes,nH,[2,nV-4],nH);

% If we don't have a legend, we can take up the whole space
if isempty(ax.Legend)
    ax.Position = box;
else
    % Retrieve legend size
    legPos = ax.Legend.Position;
    % Position the legend relatively to the axes
    ax.Legend.Position = [box(1) + box(3) - legPos(3),...
                          box(2) + 0.5*(box(4) - legPos(4)),...
                          legPos(3),legPos(4)];
    % Update the axes position
    ax.Position = [box(1),box(2),box(3)-legPos(3),box(4)];
end
% Store the data for later
fCalcVertSituation.Data.AxesPosition = box;

end