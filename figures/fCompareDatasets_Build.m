function fCompareDatasets_Build()
% Function builds the GUI corresponding to the specifications

%% %%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fViewDatasets
global fCompareDatasets

% Handler array containing all figure
global h_fig

%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a new full screen figure
fCompareDatasets = figure('Name','Compare Measured Datasets',...
                          'NumberTitle','off',...
                          'ToolBar','none','MenuBar','none',...
                          'Units','pixels','Resize','on',...
                          'CloseRequestFcn',@closeFigure,...
                          'Visible','off');
% Set the SizeChangedFcn
fCompareDatasets.SizeChangedFcn = @fCompareDatasets_Resize;

% Add figure to the global handler array
h_fig = [h_fig,fCompareDatasets];

% Get the position and outer position from the other figure
fCompareDatasets.OuterPosition = fViewDatasets.OuterPosition;

%% %%%%%%%%%%%%%%%%%% INITIALIZE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the labels
lbl_txt = {'Selected Datasets','For Comparison Selected Datasets'};
tags = {'SelDatasets','ComparisonSelDatasets'};
lbl_tag = strcat(tags,'LB');
font_size = 12;

% Create the labels iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',fCompareDatasets,'Style','text',...
              'HorizontalAlignment','left',...
              'Visible','on','Units','pixels',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',font_size,'FontWeight','bold');
    
end

%% %%%%%%%%%%%%%%%%%% INITIALIZE LISTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the lists
list_txt = {' ',' '};
tags = {'SelDatasets','ComparisonSelDatasets'};
list_tag = strcat(tags,'LST');

% Create the lists iteratively
for i=1:numel(list_txt)
    uicontrol('Parent',fCompareDatasets,'Style','listbox','Min',0,...
              'Visible','on','Units','pixels',...
              'String',list_txt{i},'Tag',list_tag{i});
end

%% %%%%%%%%%%%%%%%%%% INITIALIZE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the buttons
but_txt = {'Back','Remove Datasets','Clear Datasets',...
           'Remove','Add','Shift Up','Shift Down',...
           'Compare Datasets',...
           'Average Datasets','Energy Subtraction','Noise Level Subtraction'};
tags = {'gotof2','RemoveDataset','ClearDatasets',...
        'RemoveSet','AddSet','ShiftUp','ShiftDown',...
        'CompareDataset',...
        'AverageDataset','EnergySubtraction','LevelSubtraction'};
but_tag = strcat(tags,'BUT');
but_fcn = {'ViewDatasetsCompareDatasets','removeDataset',...
           'clearDatasets','removeSet','addSet','shiftUpSet',...
           'shiftDownSet',...
           'compareSets','averageSets','energySubtract','levelSubtract'};
but_fcn = strcat('@',but_fcn);
% Create the buttons iteratively
for i=1:numel(but_txt)
    uicontrol('Parent',fCompareDatasets,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',but_txt{i},...
              'Tag',but_tag{i},...
              'Callback',eval(but_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%% INITIALIZE SLIDERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text that needs to be displayed in the sliders
txt = {'Meas. Threshold: 3.0dB'};
tags = {'EnergyThreshold'};
sli_tag = strcat(tags,'SLI');
lbl_tag = strcat(tags,'LBL');
sli_fcn = {'energyThreshold'};
sli_fcn = strcat('@',sli_fcn);

% Create the sliders iteratively
for i=1:numel(sli_tag)
    uicontrol('Parent',fCompareDatasets,'Visible','on','Units','pixels',...
              'Style','text','String',txt{i},...
              'Tag',lbl_tag{i});
    uicontrol('Parent',fCompareDatasets,'Visible','on','Units','pixels',...
              'Style','slider',...
              'Min',0,'Max',5,'Value',3,...
              'SliderStep',[0.02,0.2],...
              'Tag',sli_tag{i},...
              'Callback',eval(sli_fcn{i}),...
              'Interruptible','off','BusyAction','cancel');
end

%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE TABS %%%%%%%%%%%%%%%%%%%%%&&&&&&%%%%
% Define the tabs names
compTabs_txt = {'Comparison','Averaging','Energy Subtraction',...
                'Noise Level Subtraction'};
compTabs_tag = {'Comparison','Averaging','Energy','Level'};
compTabs_tag = strcat(compTabs_tag,'TAB');

% Initialize the tab-group
compTabsPar = uitabgroup('Parent',fCompareDatasets,'Units','pixels',...
                         'Tag','topCompTAB');
% Iterate over the tabs
for i=1:numel(compTabs_txt)
    uitab('Parent',compTabsPar,'Units','pixels',...
          'Title',compTabs_txt{i},'Tag',compTabs_tag{i});
end

end

