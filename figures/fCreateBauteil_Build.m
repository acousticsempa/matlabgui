function fCreateBauteil_Build()
% Function defines the elements in the fCreateBauteil window 
% 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fCreateBauteil
global AvailableScreen

% Handler array containing all figure
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [120,20];
check = [100,20];
button = [150,20];
text = [200,20];
textKur = [200,35];
textDet = [200,60];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width =  max([2*check(1)+2*intra(1),...
                  text(1)+intra(1),...
                  textKur(1)+intra(1),...
                  textDet(1)+intra(1),...
                  2*button(1)+2*intra(1)]) + ...
             label(1) + marg(1) + marg(3);
% Minimum height
min_height = max([4*label(2),...
                  check(2)+text(2)+textDet(2)+textKur(2)])+...
                  4*intra(2) + button(2)+ marg(2) + marg(4);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the figure position
pos = centerOnBox(AvailableScreen,[0,0,min_width,min_height]);

% Create a new full screen figure
fCreateBauteil = figure('Name','Create Bauteil','NumberTitle','off',...
                       'ToolBar','none','MenuBar','none',...
                       'Units','pixels','Resize','off',...
                       'Position',pos,...
                       'CloseRequestFcn',@closeFigure,...
                       'Visible','off');
% Add figure to a handler list for closing
h_fig = [h_fig,fCreateBauteil];


%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Bauteil-Typ','Bauteil-Name','Kurzbeschreibung',...
           'Det. Beschreibung'};
tags = {'BauteilType','BauteilName','Kurzbeschreibung','DetBeschreibung'};
lbl_tag = strcat(tags,'LBL');
% Create the label elements iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',fCreateBauteil,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%% CHECK ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the check buttons
che_txt = {'Wand','Decke'};
tags = {'Wand','Decke'};
che_tag = strcat(tags,'CHE');
% Define the check buttons iteratively
for i=1:numel(che_txt)
    uicontrol('Parent',fCreateBauteil,'Visible','on','Units','pixels',...
              'Style','checkbox','String',che_txt{i},...
              'Tag',che_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%% EDITABLE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the tags of the editable text fields
tags = {'BauteilName','KurzBeschreibung','DetBeschreibung'};
txt_tag = strcat(tags,'TXT');
txt_max = [1,100,100];
% Create the dropdown menus iteratively
for i=1:numel(txt_tag)
    uicontrol('Parent',fCreateBauteil,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String','','HorizontalAlignment','left',...
              'Min',0,'Max',txt_max(i),...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'Cancel','Create Bauteil'};
tags = {'Cancel','CreateBauteil'};
button_tag = strcat(tags,'BUT');
button_fcn = {'closeFigure','exportBauteil'}; 
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',fCreateBauteil,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 3; 
nV = 5;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
w = round(max([check(1),button(1),...
               (text(1)-intra(1))/2, (textKur(1)-intra(1))/2,...
               (textDet(1)-intra(1))/2]));            
shareH = [label(1),w, w]/(pos(3)-marg(1)-marg(3)-2*intra(1));
shareV = [button(2), max(label(2),[textDet(2),textKur(2),text(2),check(2)])]...
            /(pos(4)-marg(2)-marg(4)-4*intra(2));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have fCreateKnoten as Parent
obj = findobj('Parent',fCreateBauteil);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
for j=0:nV-2
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j+1});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE CHECKBOXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=0:1
    % Retrieve the checkboxes and place tehm correctly
    che = findobj(obj,'Tag',che_tag{j+1});
    che.Position = BoundingBox(boxes,2+j,nV,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT FIELDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 4;
txt = findobj(obj,'Tag','BauteilNameTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);       i=i-1;
txt = findobj(obj,'Tag','KurzBeschreibungTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);       i=i-1;
txt = findobj(obj,'Tag','DetBeschreibungTXT');
txt.Position = BoundingBox(boxes,[2,3],i,nH);       i=i-1;

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 2;
but = findobj(obj,'Tag','CancelBUT');
but.Position = BoundingBox(boxes,i,1,nH);       i=i+1;
but = findobj(obj,'Tag','CreateBauteilBUT');
but.Position = BoundingBox(boxes,i,1,nH);