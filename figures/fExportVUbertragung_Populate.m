function fExportVUbertragung_Populate(~,~)

% Function populates the ExportVorsatz window with the actual data 
% to write to the database

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fExportVUbertragung
% Data
global Vorsatz
global CompareDatasetsData

%% %%%%%%%%%%%%%%%%% FIND GRAPHICS OBJECTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = fExportVUbertragung.Children;

% Find radio buttons
Mgr = findobj(obj,'Tag','MessungRAD');
l = findobj(Mgr.Children,'Tag','LuftschallRAD');
t = findobj(Mgr.Children,'Tag','TrittschallRAD');

% Find table
tbl = findobj(obj,'Tag','RLWerteTABLE');
% Find dropdowns
vDD = findobj(obj,'Tag','VorsatzDD');
% Find text objects
tO = findobj(obj,'Tag','OriginalDBTXT');
 
%% %%%%%%%%%%%%%%%%% Extract the data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% When the call originates we need to extract the data from where it has
% been stored when the user calculated or modified the calculated values
% under the NoiseLevelSubtraction tab in the fCompareDatasets window!
% The data is conviniently stored in the CompareDatasetsData global
% structure in the field: LevelSubtraction
data = CompareDatasetsData.LevelSubtraction;

%% %%%%%%%%%%%%%%%%%%% POPULATE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% SET RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If the underlying data is Luftschall, then select the Luftschall radio,
% otherwise select the Trittschall radio
if data.TypeID==1
    l.Value = 1;
else
    t.Value = 1;
end

%%%%%%%%%%%%%%%%%%%% SET TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MeasFreq = [50,63,80,100,125,160,200,250,315,400,500,630,...
            800,1000,1250,1600,2000,2500,3150,4000,5000];
% Prefix in the string
if data.TypeID==1
    pref = 'dR';
else
    pref = 'dL';
end

% Create table data
tblData = cell(5,11);
% Create table data
for i=1:11
    tblData{1,i} = [pref,' (',num2str(MeasFreq(i),'%.0f'),'Hz)'];
    tblData{2,i} = num2str(data.Values(i),'%.1f');
end
for i=1:10
    tblData{4,i} = [pref,' (',num2str(MeasFreq(i+11),'%.0f'),'Hz)'];
    tblData{5,i} = num2str(data.Values(i+11),'%.1f');
end

% Put data in the table
tbl.ColumnName = [];
tbl.RowName = [];
w = 68;
tbl.ColumnWidth = repmat({w},1,11);
tbl.Data = tblData;

%% %%%%%%%%%%%%%%%%%% PREPARE VORSATZ DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the valid Vorsatz 
b = (Vorsatz.ID ~= 0);

% Check if we have a match
if any(b)
    % Retrieve the dropdown text
    dd = Vorsatz.Dropdown(b);
    % Retrieve the IDs 
    data.Vorsatz = Vorsatz.ID(b);
else
    [dd,data.Vorsatz] = addNone();
end

% Update the dropdown menu
vDD.String = dd;
if vDD.Value > numel(vDD.String)
    vDD.Value = 1;
end

%% %%%%%%%%%%%%%%%%%% PRINT THE ORIGINAL DATABASE ID %%%%%%%%%%%%%%%%%%%%%
% Place the string
tO.String = data.OriginalDBID;

%% %%%%%%%%%%%%%%%%%% STORE GLOBAL VARIABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fExportVUbertragung.Data = data;

end