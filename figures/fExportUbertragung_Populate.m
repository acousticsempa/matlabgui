function varargout = fExportUbertragung_Populate(src,~)

% Function populates the ExportUbertragung window with the actual data 
% to write to the database

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fViewDatasets
global fExportUbertragung
% Data
global Data
global Knoten
global CompareDatasetsData

%% %%%%%%%%%%%%%%% RETRIEVE GRAPHICAL OBJECTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = fExportUbertragung.Children;

% Knotentyp radio buttons
gr = findobj(obj,'Tag','KnotenTypRAD');
kK = findobj(gr.Children,'Tag','KreuzstossRAD');
tK = findobj(gr.Children,'Tag','TStossRAD');
aK = findobj(gr.Children,'Tag','AndersRAD');

% Ausrichtung radio buttons
gr = findobj(obj,'Tag','AusrichtungRAD');
v = findobj(gr.Children,'Tag','VertikalRAD');
h = findobj(gr.Children,'Tag','HorizontalRAD');

% Messungstyp radio buttons
Mgr = findobj(obj,'Tag','MessungRAD');
l = findobj(Mgr.Children,'Tag','LuftschallRAD');
t = findobj(Mgr.Children,'Tag','TrittschallRAD');

% Dropdown Menus
kDD = findobj(obj,'Tag','KnotenDD');
srDD = findobj(obj,'Tag','StartraumDD');
erDD = findobj(obj,'Tag','EndraumDD');
sbDD = findobj(obj,'Tag','StartbauteilDD');
ebDD = findobj(obj,'Tag','EndbauteilDD');

% Text fields
tF = findobj(obj,'Tag','FlacheTXT');
tL = findobj(obj,'Tag','LangeTXT');

% Table
tbl = findobj(obj,'Tag','RLWerteTABLE');

% Axes
ax = findobj(obj,'Tag','KnotenPreviewAX');

%% %%%%%%%%%%%%%%%%%%% UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find out which element of the figure is calling the _Populate function
% and depending on that decide which actions have to be performed
elem = [kK,tK,aK, v,h, kDD, srDD,erDD];
upLev = [1, 1, 1, 1,1,   2,    3,   3];

% Find which element called this function
b = (src == elem);
% Retrieve the update level
updateLevel = upLev(b);

% Check if the updateLevel is not empty, otherwise initialize to 0
if isempty(updateLevel)
    % Choose the updateLevel so that all the panel gets updated
    updateLevel = 0;
end

%% %%%%%%%%%%%%%%%%%%%% RETRIEVE DATA TO DISPLAY %%%%%%%%%%%%%%%%%%%%%%%%%
% If the source calling the _Populate function has an updateLevel smaller
% than 1, that means that first we have to retrieve the data to be
% displayed in the figure
if updateLevel < 1
    %% %%%%%%%%%%%%%%%%%%%% ORIGIN: fViewDatasets %%%%%%%%%%%%%%%%%%%%%%%%%
    % When the origin of the call is the fViewDatasets figure, then we need 
    % to extract the data from the selected dataset
    if src==fViewDatasets
        % Check that dropdowns in fViewDatasets are correctly selected
        % Use the function to check that the dropdowns are correctly selected
        % Function also throws the necessary error warnings
        [passed, indx, selDT] = checkSelection();
        % Prepare passed for output
        varargout{1,1} = passed;
        
        % Continue only if passed
        if ~passed
            return;
        end
        
        % Retrieve the correct database
        dbType = {'Air','Impact','AirAVG'};
        DB = Data.(dbType{1,selDT});
        
        % Retrieve the TypeID
        typeID = [1,2,1];       % LS, TS, LS
        data.TypeID = typeID(selDT);
        
        % Trennbauteilfläche
        data.Area = 0;
        if selDT==1
            data.Area = DB.Area(indx,1);
        elseif selDT==3
            % Retrieve the string and split it by /
            str = strsplit(DB.Area{indx,1},'/');
            % Evaluate the string to an array, average and store
            data.Area = mean([eval(str{1,1}),eval(str{1,2})]);
        end
        
        % Länge stossstelle
        data.Length = 0;
        if isfield(DB,'Length')
            data.Length = DB.Length(indx,1);
        end
        
        % Original-DB-ID composed of
        % x.y:
        %   x:  ID of selected report (in original database)
        %   y:  Case type of selected report (1: Air, 2: Impact, 3: AirAVG)
        % Retrieve ID
        ID = DB.ID(indx,1);
        % Define the appendinx for the OriginalDBID
        appx = {'M','M','A'};
        % Retrieve the Original-DB-ID string
        data.OriginalDBID = getOriginalDBID([selDT,ID],appx{selDT});
        
        % Transmission values
        if any(selDT==[1,3])
            data.Values = DB.TL(indx,:);
        else
            data.Values = DB.NISPL(indx,:);
        end
    end
    
    %% %%%%%%%%%%%%%%%%% ORIGIN: fCompareDatasets %%%%%%%%%%%%%%%%%%%%%%%%%
    % When the origin of the call is not fViewDatasets, then the origin must 
    % be fCompareDatasets, we have to find out whether the call originated 
    % in the AveragingTAB or in the EnergyTAB
    if strcmpi(src.Tag,'AveragingTAB')
        % The data is stored in the CompareDatasetsData.Average field
        data = CompareDatasetsData.Average;
    elseif strcmpi(src.Tag,'EnergyTAB')
        % The data is stored in the CompareDatasetsData.EnergySubtraction field
        data = CompareDatasetsData.EnergySubtraction;
    end
    
    % Try to add the Knoten Structure to the variable
    if isfield(fExportUbertragung.Data,'Knoten')
        data.Knoten = fExportUbertragung.Data.Knoten;
    end
    
    %% Store the data in the global variable
    fExportUbertragung.Data = data;
end

%% %%%%%%%%%%%%%%%%%% UPDATE MESSUNGSTYP RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve the data
data = fExportUbertragung.Data;

% The Messungstyp radio buttons are updated when the figure is created as
% they depend on which type of measurement was at the origin of the data
if updateLevel < 1
    % Two cases: 
    % Luftschall:      can be used on Vertikal and Horizontal knoten
    % Trittschall:     can be used only on vertikal knoten
    if data.TypeID == 1
        % Set the Luftschall-Radio to 1
        l.Value = 1;
        % Make both vertikal and horizontal selectable
        v.Enable = 'on';
        h.Enable = 'on';
    else
        % Set the Trittschall to 1
        t.Value = 1;
        % Make both vertikal and horizontal not-selectable and select vertikal
        v.Enable = 'inactive';
        h.Enable = 'inactive';
        v.Value = 1;
    end
end

%% %%%%%%%%%%%%%%%%%% UPDATE UBERTRAGUNG INFORMATION %%%%%%%%%%%%%%%%%%%%%%
% Update the Ubertragung information only if the updateLevel is smaller
% than 1 (when the figure is initialized)
if updateLevel < 1
    % Update the text for Area and Length
    tF.String = data.Area;
    tL.String = data.Length;
    
    % Update the table %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Prefix in the string
    pref = 'R';
    if data.TypeID == 2
        pref = 'L';
    end

    % Define x-coordinates for table values
    x = [50,63,80,100,125,160,200,250,315,400,500,630,...
         800,1000,1250,1600,2000,2500,3150,4000,5000];
 
    % Create table data
    tblData = cell(5,11);
    for i=1:11
        tblData{1,i} = [pref,' (',num2str(x(i),'%.0f'),'Hz)'];
        tblData{2,i} = num2str(data.Values(i),'%.1f');
    end
    for i=1:10
        tblData{4,i} = [pref,' (',num2str(x(i+11),'%.0f'),'Hz)'];
        tblData{5,i} = num2str(data.Values(i+11),'%.1f');
    end

    % Put data in the table
    tbl.ColumnName = [];
    tbl.RowName = [];
    w = 65;
    tbl.ColumnWidth = repmat({w},1,11);
    tbl.Data = tblData;
end

%% %%%%%%%%%%%%%%%%%% UPDATE KNOTEN DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Knoten dropdown if the updateLevel is smaller than 2
if updateLevel < 2
    % Calculate the TypeID of the Knoten using the radio buttons
    TypeID = kK.Value*1 + tK.Value*2 + aK.Value*3;
    % Calculate the Ausrichtung ID
    AusrichtungID = v.Value*1 + h.Value*2;
    
    % Find Knoten that have the same TypeID and AusrichtungID as selected
    bT = (Knoten.TypeID == TypeID);
    bA = (Knoten.Ausrichtung == AusrichtungID);
    % Compound boolean vectors
    b = all([bT,bA],2);
    
    % Try to retrieve old IDs
    if isfield(fExportUbertragung.Data,'Knoten')
        kIDold = fExportUbertragung.Data.Knoten;
    else
        kIDold = [];
    end
    
    % Prepare the dropdown for the Knoten
    if any(b)
        % Retrieve the Knoten that respect the properties
        dd = Knoten.Dropdown(b);
        fExportUbertragung.Data.Knoten = Knoten.ID(b);
    else
        [dd,fExportUbertragung.Data.Knoten] = addNone();
    end
    % Update the list and the value
    kDD.String = dd;
    kDD.Value = updateValue(kIDold,fExportUbertragung.Data.Knoten,kDD.Value);
end

%% %%%%%%%%%%%%%%%%%%% PLOT THE KNOTEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Knoten need to be replotted only when the updateLevel is smaller than
% 3
if updateLevel < 3
    % Retrieve the Knoten ID
    kID = fExportUbertragung.Data.Knoten(kDD.Value);
    % Check if we have a match
    b = (Knoten.ID == kID);
    % Initialize the teile and aniso arrays
    teile = zeros(1,4);
    aniso = zeros(1,4);
    % Check if we have a match and retrieve the data for that Knoten
    if any(b)
        teile = Knoten.Bauteile(b,:);
        aniso = Knoten.Anisotropie(b,:);
    end
    
    % Plot the Knoten
    plotKnoten(ax,teile,aniso);
end

%% %%%%%%%%%%%%%%%%% UPDATE RAUME DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Raume dropdown and the Bauteile dropdowns only if the user 
% changes the selection on the raume
if updateLevel < 4
    %% CHECK THAT START AND ENDRAUM ARE NOT EQUAL %%%%%%%%%%%%%%%%%%%%%%%%
    % Check if the Startraum and Endraum are equal 
    if srDD.Value == erDD.Value
        % Initialize the vector of different possible values
        v = [1,2,3,4];
        
        % We have to find a new value for either the Endraum or the
        % Startraum
        if src == srDD
            % Find new value for the Endraum
            v = v(v ~= srDD.Value);
            erDD.Value = v(1);
            
        elseif src == erDD
            % Find new value for the Startraum
            v = v(v ~= erDD.Value);
            srDD.Value = v(1);
        end
    end
    
    %% RETRIEVE VALID BAUTEILE TO SHOW IN THE DROPDOWNS %%%%%%%%%%%%%%%%%%
    % Mapping of Raum to Bauteil
    RtoB = [1,2; 2,3; 3,4; 1,4];
    % Define the Bauteile dropdown list
    ddBaut = {'Bauteil 1','Bauteil 2','Bauteil 3','Bauteil 4'};
    
    % Find the Knoten ID
    kID = fExportUbertragung.Data.Knoten(kDD.Value);
    % See if we have a match
    b = (Knoten.ID == kID);
    % Find out which Bauteile exist in the Knoten
    bB = false(1,4);
    if any(b)
        bB = (Knoten.Bauteile(b,:) > 0);
    end
    
    %%%%%%%%%%%%%%% Startbauteil dropdown %%%%%%%%%%%%%%%%%%%%%%
    sbID = RtoB(srDD.Value,:);
    sbID = sbID(bB(sbID));
    % Find out old list
    try bIDprev = fExportUbertragung.Data.Startbauteil;
    catch
        bIDprev = [];
    end
    % Check if we have a match
    if ~isempty(sbID)
        dd = ddBaut(sbID);
    else
        [dd,sbID] = addNone();
    end
    % Store the Bauteile IDs
    fExportUbertragung.Data.Startbauteil = sbID;
    % Update the dropdown
    sbDD.String = dd;
    sbDD.Value = updateValue(bIDprev,sbID,sbDD.Value);
    
    %%%%%%%%%%%%%%%%% Endbauteil dropdown %%%%%%%%%%%%%%%%%%%%%%%%%%
    ebID = RtoB(erDD.Value,:);
    ebID = ebID(bB(ebID));
    % Find out old list
    try bIDprev = fExportUbertragung.Data.Endbauteil;
    catch
        bIDprev = [];
    end
    % Check if we have a match
    if ~isempty(ebID)
        dd = ddBaut(ebID);
    else
        [dd,ebID] = addNone();
    end
    % Store the Bauteile IDs
    fExportUbertragung.Data.Endbauteil = ebID;
    % Update the dropdown
    ebDD.String = dd;
    ebDD.Value = updateValue(bIDprev,ebID,ebDD.Value);
end

end