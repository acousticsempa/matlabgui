function pos = withinScreen(intBox)

% Function controls that the internalBox is within the limits of the
% external box and places it accordingly

% Use the global available Screen
global AvailableScreen

% Store it under the extBox name
extBox = AvailableScreen;

% Initialize variables
dh = 0;
dhReserve = 0;
dv = 0;
dvReserve = 0;
dhSize = 0;
dvSize = 0;

% Check the left boundary
if intBox(1) < extBox(1)
    % Shift horizontally
    dh = extBox(1) - intBox(1);
else
    dhReserve = intBox(1)-extBox(1);
end

% Check the bottom boundary
if intBox(2) < extBox(2)
    % Shift vertically
    dv = extBox(2) - intBox(2);
else
    dvReserve = intBox(2)-extBox(2);
end

% Check the right border
int_Rpos = intBox(1)+intBox(3)+dh;
ext_Rpos = extBox(1)+extBox(3);
if int_Rpos > ext_Rpos
    % Check if using dhReserve is enough or if we need to shrink as well
    if dhReserve > (int_Rpos-ext_Rpos)
        % Shift simply to the left
        dh = ext_Rpos - int_Rpos;
    else
        % Use up the shifting from the reserve
        dh = -dhReserve; 
        % And shrink for the rest
        dhSize = ext_Rpos - int_Rpos + dhReserve;
    end
end

% Check the top border
int_Tpos = intBox(2)+intBox(4)+dv;
ext_Tpos = extBox(2)+extBox(4);
if int_Tpos > ext_Tpos
    % Check if using dvReserve is enough or if we need to shrink as well
    if dvReserve > (int_Tpos-ext_Tpos)
        % Shift simply to the bottom
        dv = ext_Tpos - int_Tpos;
    else
        % Use up the shifting from the reserve
        dv = -dvReserve;
        % And shrink for the rest
        dvSize = ext_Tpos - int_Tpos + dvReserve;
    end
end
% Return the new position
pos = intBox + [dh, dv, dhSize, dvSize];

end

