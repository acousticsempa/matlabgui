function fCreateKnoten_Populate(src,~)

% Function populates the dropdown menus for the CreateKnoten window

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fCreateKnoten
% Data
global Bauteile

%% %%%%%%%%%%%% RETRIEVE OBJECT HANDLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = fCreateKnoten.Children;

% Ausrichtung radio buttons
gr = findobj(obj,'Tag','AusrichtungRAD');
v = findobj(gr.Children,'Tag','VertikalRAD');
h = findobj(gr.Children,'Tag','HorizontalRAD');

% Bauteile / Anisotropie Dropdown menus
b1 = findobj(obj,'Tag','Bauteil1DD');
b2 = findobj(obj,'Tag','Bauteil2DD');
b3 = findobj(obj,'Tag','Bauteil3DD');
b4 = findobj(obj,'Tag','Bauteil4DD');
a1 = findobj(obj,'Tag','Anisotropie1DD');
a2 = findobj(obj,'Tag','Anisotropie2DD');
a3 = findobj(obj,'Tag','Anisotropie3DD');
a4 = findobj(obj,'Tag','Anisotropie4DD');
% Create arrays of handles
bDD = [b1, b2, b3, b4];
aDD = [a1, a2, a3, a4];

% Find axes
ax = findobj(obj,'Tag','KnotenPreviewAX');

%% %%%%%%%%%%%% CALCULATE UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate which element is calling this function and update the figure
% depending on the source of the call
elem =  [v,h,     bDD,     aDD];
upLev = [1,1, 2,2,2,2, 2,2,2,2];

% Find which element called this function
b = (src == elem);
% Retrieve the update level
updateLevel = upLev(b);

% Retrieve the update Level
if isempty(updateLevel)
    updateLevel = 0;
end

%% %%%%%%%%%%%%% DROPDOWNS FOR BAUTEILE 2/4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform this operation when the source has an updateLevel smaller than 1
if updateLevel < 1
    % Specify the bauteil Type ID
    TypeID = 1;     % Always a Wand
    
    % Find which bauteile have the corresponding TypeID
    b = (Bauteile.TypeID == TypeID);
    % Retrieve the corresponding dropdown texts
    dd = Bauteile.Dropdown(b);
    % Retrieve the indexes of the bauteile shown in the dropdown list
    IDs = Bauteile.ID(b);
    % Add the None element with ID==0 to the dropdown
    [dd,IDs] = addNone(dd,IDs);
    
    % Update the dropdowns
    for i=2:2:4
        % Update the list
        bDD(i).String = dd;
        % Try retrieving the previous list of BauteileIDs
        try bIDprev = fCreateKnoten.Data.Bauteil{i};
        catch
            bIDprev = [];
        end
        % Set the value
        bDD(i).Value = updateValue(bIDprev,IDs,bDD(i).Value);
    end
   
    % Store the data in the global data structure
    fCreateKnoten.Data.Bauteil{2} = IDs;
    fCreateKnoten.Data.Bauteil{4} = IDs;
end

%% %%%%%%%%%%%%% DROPDOWNS FOR BAUTEILE 1/3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform this operation when the source has an updateLevel smaller than 2
if updateLevel < 2
    % Specify the bauteil Type ID for these Bauteile
    % If Vertikal is selected:  TypeID==2: Decke
    % If Horizontal is selected: TypeID==1: Wand
    TypeID = v.Value*2 + h.Value*1;

    % Find which bauteile have the corresponding TypeID
    b = (Bauteile.TypeID == TypeID);
    % Retrieve the corresponding dropdown texts
    dd = Bauteile.Dropdown(b);
    % Retrieve the indexes of bauteile shown in the dropdown list
    IDs = Bauteile.ID(b);
    % Add the None element with ID==0 to the dropdown
    [dd,IDs] = addNone(dd,IDs);

    % Update the dropdowns
    for i=1:2:3
        % Update the list
        bDD(i).String = dd;
        % Try retrieving the previous list of BauteileIDs
        try bIDprev = fCreateKnoten.Data.Bauteil{i};
        catch
            bIDprev = [];
        end
        % Set the value
        bDD(i).Value = updateValue(bIDprev,IDs,bDD(i).Value);
    end
    
    % Store the data in the global data structure
    fCreateKnoten.Data.Bauteil{1} = IDs;
    fCreateKnoten.Data.Bauteil{3} = IDs;
end

%% %%%%%%%%%%%%%%%%% UPDATE KNOTEN DRAWING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform this action only if the updateLevel is smaller than 3
if updateLevel < 3
    % Retrieve the Bauteile IDs for plotting
    teile = zeros(1,4);
    % Iterate over the dropdowns
    for i=1:4
        teile(i) = fCreateKnoten.Data.Bauteil{i}(bDD(i).Value);
    end
  
    % Retrieve the Anisotropie ID for plotting
    aniso = zeros(1,4);
    % Iterate over the dropdowns
    for i=1:4
        aniso(i) = aDD(i).Value - 1;
    end
    
    % Plot the Knoten 
    plotKnoten(ax,teile,aniso);
end