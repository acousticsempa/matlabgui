function fViewDatasets_Build()
% Function defines the elements in the ViewDatasets window 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fViewDatasets
global AvailableScreen

% Handler array containing all figure
global h_fig

%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a new full screen figure
fViewDatasets = figure('Name','View Measured Datasets','NumberTitle','off',...
                       'ToolBar','none','MenuBar','none',...
                       'Units','pixels','Resize','on',...
                       'OuterPosition',centerOnBox(AvailableScreen,0.5,0.7),...
                       'CloseRequestFcn',@closeFigure,...
                       'Visible','off');
% Set the SizeChangedFcn
fViewDatasets.SizeChangedFcn = @fViewDatasets_Resize;

% Add figure to a handler list for closing
h_fig = [h_fig,fViewDatasets];

%% %%%%%%%%%%%%%%%%%%%% BUILD THE TABS INTERFACE %%%%%%%%%%%%%%%%%%%%%%%%%
% Define the tabs names
topTabs_txt = {'Aufbau','Daten','Graphen'};
topTabs_tag = {'AufbauTAB','DatenTAB','GraphenTAB'};
% Initialize the tab-group
topTabsPar = uitabgroup('Parent',fViewDatasets,'Units','pixels','Tag','topTAB');
% Iterate over the tabs
for i=1:numel(topTabs_txt)
    uitab('Parent',topTabsPar,'Units','pixels',...
          'Title',topTabs_txt{i},'Tag',topTabs_tag{i});
end
% Add resizing function for the DatenTAB
tab = findobj('Tag','DatenTAB');
tab.SizeChangedFcn = @resizeDataTab;

% Define subtab names
AufbauTabs_txt = {'3D','Grundriss','Langsschnitt','Querschnitt'};
AufbauTabs_tag = {'3DTAB','GrundTAB','LangsTAB','QuerTAB'};
% Create tab group for the 'Aufbau' tab
AufbauTabsPar = uitabgroup('Parent',findobj('Tag','AufbauTAB'));
for i=1:numel(AufbauTabs_txt)
    uitab('Parent',AufbauTabsPar,...
          'Title',AufbauTabs_txt{i},'Tag',AufbauTabs_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%% INITIALIZE DROPDOWN CONTROLLER %%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the dropdown menus
txt = {{'Airborne','Impact','Airborne Averaged'},' ',' ',' ',' '};
tags = {'DatatypeDD','ProjectDD','PrufObjDD','ShieldingDD','ReportDD'};
drop_fcn = {'selDatatype','selProject','selPrufObj','selShielding','selReport'};
drop_fcn = strcat('@',drop_fcn);
% Create the dropdown menus iteratively
for i=1:numel(txt)
    uicontrol('Parent',fViewDatasets,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'String',txt{i},'Tag',tags{i},...
              'Callback',eval(drop_fcn{i}));
end
                    
%% %%%%%%%%%%%%%%%%%%%% TEXT ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
txt_txt = {'Datatype','Project','Pr�fobject','Shielding','Report'};
txt_tag = {'DatatypeLB','ProjectLB','PrufObjLB','ShieldingLB','ReportLB'};
% Create the text elements iteratively
for i=1:numel(txt_txt)
    uicontrol('Parent',fViewDatasets,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',txt_txt{i},'Tag',txt_tag{i});
    txt = findobj('Tag',txt_tag{i});
    txt.FontSize = txt.FontSize*1.3;
end

% Define which texts needs to be displayed in the preview boxes
txt_txt = {'Test Description','Test Object'};
txt_tag = {'TestDescriptionLB','TestObjectLB'};
for i=1:numel(txt_txt)
    uicontrol('Parent',fViewDatasets,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',txt_txt{i},'Tag',txt_tag{i});
    txt = findobj('Tag',txt_tag{i});
    txt.FontSize = txt.FontSize*1.3;
end

%% %%%%%%%%%%%%%%% PREVIEW ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the preview text to display
preview_txt = {' ',' '};
preview_tag = {'TestDescriptionTXT','TestObjectTXT'};
for i=1:numel(preview_txt)
    uicontrol('Parent',fViewDatasets,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',preview_txt{i},...
              'Tag',preview_tag{i});
end

%% %%%%%%%%%%%%%%%%%% INITIALIZE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'View Dataset','Select Dataset','Comparison Mode',...
              'Export Dataset','Cancel'};
tags = {'DataViewer','Selector','Calculator','ExportDataset','Back'};
button_tag = strcat(tags,'BUT');
button_fcn = {'viewData','selectDataset','ViewDatasetsCompareDatasets',...
              'ViewDatasetsExportUbertragung','closeFigure'};
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',fViewDatasets,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end