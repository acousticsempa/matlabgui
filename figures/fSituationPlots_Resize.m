function fSituationPlots_Resize(src,~)

% Function takes care of resizing all elements within the figure for a new
% figure size

%% %%%%%%%%%%%%%%%%%% SPECIFY SKELETON OF UI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define skeleton of the figure
nH = 2;
nV = 5;

%% %%%%%%%%%%%%%%%%%% DECLARE MARGINS AND MINIMUM WIDTHS %%%%%%%%%%%%%%%%%
% Margins for element bordering with the figure border
marg = 15*ones(4,1); 
% Spaces in between elements
intra = [10,10];

% Minimum sizes
ax = [300,300];
but = [100,20];
tit = [100,20];

%% %%%%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%
min_width = max([2*ax(1) + intra(1),...
                 but(1),   tit(1)]) + marg(1) + marg(3);
min_height = but(2) + 3*ax(2) + tit(2) + (nV-1)*intra(2) + marg(2) + marg(4);

% Get the figure position
pos = src.Position;
w = max(pos(3),min_width);
h = max(pos(4),min_height);
src.Position = [pos(1:2), w, h];

% Check that the position is within the boundaries of the desktop
src.Position = withinScreen(src.Position);

% Calculate the position relative to the window
pos = src.Position;
pos(1:2) = 1;

%% %%%%%%%%%%%%%%%%%%%%%%% CALCULATE SHARES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shareH = ones(1,nH)/nH;
shareV = [but(2), zeros(1,nV-2), tit(2)]...
                /(pos(4)-marg(2)-marg(4)-(nV-1)*intra(2));
shareV(2:end-1) = (1-sum(shareV([1,end])))/(nV-2);

% Calculate the box position
boxes = BoxPositions(pos,nH,nV,marg,intra,shareH,shareV);

%% %%%%%%%%%%%%%%%%%%%%% GRAPHIC OBJECTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve graphic object in the figure
obj = src.Children;

%% %%%%%%%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
j = [1,nH];
lbl = findobj(obj,'Tag','TitleLBL');
lbl.Position = BoundingBox(boxes,j,i,nH);

%% %%%%%%%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV-1;
j = 1;
% Repeat the operation for the different axes
ax = findobj(obj,'Tag','Knoten1AX');
maximizeAxes(ax,BoundingBox(boxes,j,i,nH));     j=j+1;
ax = findobj(obj,'Tag','Knoten2AX');
maximizeAxes(ax,BoundingBox(boxes,j,i,nH));     i=i-1; j=j-1;
ax = findobj(obj,'Tag','Knoten3AX');
maximizeAxes(ax,BoundingBox(boxes,j,i,nH));     j=j+1;
ax = findobj(obj,'Tag','Knoten4AX');
maximizeAxes(ax,BoundingBox(boxes,j,i,nH));     i=i-1; j=j-1;
ax = findobj(obj,'Tag','SummaryAX');
maximizeAxes(ax,BoundingBox(boxes,j,i,nH));     j=j+1;
ax = findobj(obj,'Tag','TrittschallAX');
maximizeAxes(ax,BoundingBox(boxes,j,i,nH));

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 1;
j = [1,nH];
rSub = 3;
but = findobj(obj,'Tag','exportBUT');
subboxes = BoxPositions(BoundingBox(boxes,j,i,nH),rSub,1,zeros(1,4),[0,0]);
but.Position = BoundingBox(subboxes,ceil(rSub/2),1,rSub);

end