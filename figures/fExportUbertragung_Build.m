function fExportUbertragung_Build()

% Function defines the elements in the fExportUbertragung figure 

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
% Figures and controllers
global fExportUbertragung
global AvailableScreen

% Handler array containing all figure
global h_fig

%% %%%%%% DECLARE MARGINS AND MINIMUM WIDTHS (in pixels) %%%%%%%%%%%%%%%%%%
% Margins for elements bordering with the figure border
marg = 15*ones(4,1);    % [left, bottom, right, top]
% Spaces in between elements
intra = [10, 10];

% Minimum sizes
label = [125,20];
radio = [100,20];
drop = [100,20];
text = [200,20];
button = [150,20];
table = [500,110];

%% %%%%%%%%%%%%%%%%%% CALCULATE MINIMUM SIZE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum width
min_width =  max([label(1)+3*radio(1)+3*intra(1),...
                  label(1)+2*drop(1)+2*intra(1),...
                  label(1)+text(1)+intra(1),...
                  table(1),...
                  2*button(1)+intra(1)]) + ...
             + marg(1) + marg(3);
% Minimum height
min_height = max([9*label(2),...
                  2*text(2)+3*drop(2)+3*radio(2)+label(2)])+...
                  10*intra(2) + table(2)+button(2)+ marg(2) + marg(4);
              
% Account for the image to be displayed at the end
WidthToHeight = 1;
% Calculate the height of the axes (starting at Bauteil 2)
h = max([9*label(2),...
         3*drop(2)+3*radio(2)+2*text(2)+label(2)]) + 8*intra(2);
% Retrieve image dimensions through proportions
imageDim = [WidthToHeight,1]*h;
% Augment the width with the corresponding image width
min_width = min_width + imageDim(1);
                            
%% %%%%%%%%%%%%%%%%%%%% INITIALIZE THE FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the figure position
pos = centerOnBox(AvailableScreen,[0,0,min_width,min_height]);

% Create a new full screen figure
fExportUbertragung = figure('Name','Export Ubertragung','NumberTitle','off',...
                            'ToolBar','none','MenuBar','none',...
                            'Units','pixels','Resize','off',...
                            'Position',pos,...
                            'CloseRequestFcn',@closeFigure,...
                            'Visible','off',...
                            'WindowStyle','modal');
% Add figure to a handler list for closing
h_fig = [h_fig,fExportUbertragung];

% Attach data structure for storage of figure related data
addprop(fExportUbertragung,'Data');

%% %%%%%%%%%%%%%%%%%%%% LABELS ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the text boxes
lbl_txt = {'Knoten-Typ','Ausrichtung','Knoten-Name','Messungstyp',...
           'Startraum / Startbauteil','Endraum / Endbauteil',...
           'Trennbauteilfläche [m2]','Länge Stossstelle [m]',...
           'Transmissionswerte (R/L-Werte)'};
tags = {'KnotenTyp','Ausrichtung','KnotenName','Messungstyp',...
        'StartRaumBauteil','EndRaumBauteil',...
        'Flache','Lange','RLWerte'};
lbl_tag = strcat(tags,'LBL');
% Create the label elements iteratively
for i=1:numel(lbl_txt)
    uicontrol('Parent',fExportUbertragung,'Visible','on','Units','pixels',...
              'Style','text','HorizontalAlignment','left',...
              'String',lbl_txt{i},'Tag',lbl_tag{i},...
              'FontSize',10.5);
end

%% %%%%%%%%%%%%%%%%%%% RADIO ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text to be displayed in the Knotentyp group of radio buttons
rad_txt = {'Kreuzstoss','T-Stoss','Anders'};
tags = {'Kreuzstoss','TStoss','Anders'};
rad_tag1 = strcat(tags,'RAD');
% Create the radio button group
knotenType = uibuttongroup('Parent',fExportUbertragung,'Units','pixels',...
                           'Visible','on','BorderType','none',...
                           'Tag','KnotenTypRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_txt)
    uicontrol('Parent',knotenType,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@fExportUbertragung_Populate,...
              'Tag',rad_tag1{i});
end

% Define the text to be displayed in the Ausrichtung group of radio buttons
rad_txt = {'Vertikal','Horizontal'};
tags = {'Vertikal','Horizontal'};
rad_tag2 = strcat(tags,'RAD');
% Create the radio button group
ausrichtung = uibuttongroup('Parent',fExportUbertragung,'Units','pixels',...
                            'Visible','on','BorderType','none',...
                            'Tag','AusrichtungRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_txt)
    uicontrol('Parent',ausrichtung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Callback',@fExportUbertragung_Populate,...
              'Tag',rad_tag2{i});
end

% Define the text to be displayed in the Messungstyp group of radio buttons
rad_txt = {'Luftschall','Trittschall'};
tags = {'Luftschall','Trittschall'};
rad_tag3 = strcat(tags,'RAD');
% Create the radio button group
messung = uibuttongroup('Parent',fExportUbertragung,'Units','pixels',...
                        'Visible','on','BorderType','none',...
                        'Tag','MessungRAD');
% Define the radio buttons iteratively
for i=1:numel(rad_txt)
    uicontrol('Parent',messung,'Visible','on','Units','pixels',...
              'Style','radiobutton','String',rad_txt{i},...
              'Enable','inactive',...
              'Tag',rad_tag3{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed in the first dropdown
txt = ' ';
tags = 'Knoten';
dd_tag1 = strcat(tags,'DD');
uicontrol('Parent',fExportUbertragung,'Style','popupmenu',...
          'Visible','on','Units','pixels',...
          'Callback',@fExportUbertragung_Populate,...
          'String',txt,'Tag',dd_tag1);

% Define which text needs to be displayed in the second set of dropdowns
txt = {'Raum 1','Raum 2','Raum 3','Raum 4'};
tags = {'Startraum','Endraum'};
dd_tag2 = strcat(tags,'DD');
dd_val = [1,2];
% Create the dropdown menus iteratively
for i=1:numel(dd_tag2)
    uicontrol('Parent',fExportUbertragung,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'Callback',@fExportUbertragung_Populate,...
              'String',txt,'Tag',dd_tag2{i},'Value',dd_val(i));
end

% Define which text needs to be displayed in the third set of dropdowns
txt = {'Bauteil 1','Bauteil 2'};
tags = {'Startbauteil','Endbauteil'};
dd_tag3 = strcat(tags,'DD');
% Create the dropdown menus iteratively
for i=1:numel(dd_tag3)
    uicontrol('Parent',fExportUbertragung,'Style','popupmenu',...
              'Visible','on','Units','pixels',...
              'String',txt,'Tag',dd_tag3{i},'Value',1);
end

%% %%%%%%%%%%%%%%%%%%%% EDITABLE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define which text needs to be displayed
txt = ' ';
tags = {'Flache','Lange'};
txt_tag = strcat(tags,'TXT');
% Create the dropdown menus iteratively
for i=1:numel(txt_tag)
    uicontrol('Parent',fExportUbertragung,'Style','edit',...
              'Visible','on','Units','pixels',...
              'String',txt,'HorizontalAlignment','left',...
              'Min',0,'Max',1,...
              'Tag',txt_tag{i});
end

%% %%%%%%%%%%%%%%%%%%%%%%%%% TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb = uitable('Parent',fExportUbertragung,'Visible','on',...
             'RowName',[],'ColumnName',[],'RowStriping','on',...
             'Units','pixels','Tag','RLWerteTABLE');

%% %%%%%%%%%%%%%%%%%%%%%%%%%% BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the button text to display 
button_txt = {'Cancel','Export Ubertragung'};
tags = {'Cancel','exportUbertragung'};
button_tag = strcat(tags,'BUT');
button_fcn = {'closeFigure','exportUbertragung'}; 
button_fcn = strcat('@',button_fcn);
for i=1:numel(button_txt)
    uicontrol('Parent',fExportUbertragung,'Visible','on','Units','pixels',...
              'Style','pushbutton',...
              'Interruptible','off','BusyAction','cancel',...
              'String',button_txt{i},...
              'Tag',button_tag{i},...
              'Callback',eval(button_fcn{i}));
end

%% %%%%%%%%%%%%%%%%%%%% AXES FOR IMAGE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the axes for image
ax = axes('Parent',fExportUbertragung,'Units','pixels','Visible','on',...
          'Tag','KnotenPreviewAX',...
          'Color',0.94*ones(1,3));
% Disable the axes
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
ax.ZAxis.Visible = 'off';

%% %%%%%%%%%%%%%%%%%%%% PLACE THE ELEMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define UI skeleton
nH = 8; 
nV = 11;

% Calculate pos relative to window
pos(1:2) = 0;

% Get elements positions 
% Calculate shares of each element of the GUI to the total share
h1 = label(1);
h2 = round(max([(radio(1)-intra(1))/2,...
                (drop(1)-intra(1))/2,...
                (text(1)-5*intra(1))/6,...
                (button(1)-2*intra(1))/3]));
shareH = [h1; h2*ones(6,1); 0]/(pos(3)-marg(1)-marg(3)-7*intra(1));
shareH(end) = 1-sum(shareH(1:end-1));

l = label(2);
d = max(label(2),drop(2));
t = max(label(2),text(2));
r = max(label(2),radio(2));
shareV = [button(2), 0, l, t, t, d, d, r, d, r, r]'...
            /(pos(4)-marg(2)-marg(4)-10*intra(2));
shareV(2) = 1-shareV(1)-sum(shareV(3:end));

% Calculate the absolute positions for all the GUI Elements
boxes = BoxPositions(pos, nH, nV, marg, intra, shareH, shareV);

% Find all the objects that have fCreateKnoten as Parent
obj = findobj('Parent',fExportUbertragung);

%% %%%%%%%%%%%%%%%%%%% PLACE LABELS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = nV;
for j=0:3
    % Retrieve the label and place it correctly
    lbl = findobj(obj,'Tag',lbl_tag{j+1});
    lbl.Position = BoundingBox(boxes,1,i-j,nH);
end
% Place the label for Flache and Lange over a larger portion of the figure
i = 7;
lbl = findobj(obj,'Tag','StartRaumBauteilLBL');
lbl.Position = BoundingBox(boxes,[1,3],i,nH);   i=i-1;
lbl = findobj(obj,'Tag','EndRaumBauteilLBL');
lbl.Position = BoundingBox(boxes,[1,3],i,nH);   i=i-1;
lbl = findobj(obj,'Tag','FlacheLBL');
lbl.Position = BoundingBox(boxes,[1,3],i,nH);   i=i-1;
lbl = findobj(obj,'Tag','LangeLBL');
lbl.Position = BoundingBox(boxes,[1,3],i,nH);   i=i-1;
lbl = findobj(obj,'Tag','RLWerteLBL');
lbl.Position = BoundingBox(boxes,[1,7],i,nH);

%% %%%%%%%%%%%%%%%%%%% PLACE RADIOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,7],nV,nH);
% Place the group
knotenType.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],3,1,zeros(1,4),intra);
for j=0:2
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',knotenType,'Tag',rad_tag1{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,3);
end

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[2,7],nV-1,nH);
% Place the group
ausrichtung.Position = box;
for j=0:1
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',ausrichtung,'Tag',rad_tag2{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,3);
end

% Get the box where the radio button will be placed
box = BoundingBox(boxes,[4,7],nV-3,nH);
% Place the group
messung.Position = box;
% Calculate the position of subboxes
subboxes = BoxPositions([0,0,box(3),box(4)],2,1,zeros(1,4),intra);
for j=0:1
    % Retrieve the radios and place it correctly
    rad = findobj('Parent',messung,'Tag',rad_tag3{j+1});
    rad.Position = BoundingBox(subboxes,j+1,1,2);
end

%% %%%%%%%%%%%%%%%%%%% PLACE DROPDOWNS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Place KnotenName dd
dd = findobj(obj,'Tag',dd_tag1);
dd.Position = BoundingBox(boxes,[2,7],nV-2,nH);
% Place Raum and Bauteil
i = nV-4;
for j=0:1
    dd = findobj(obj,'Tag',dd_tag2{j+1});
    dd.Position = BoundingBox(boxes,[4,5],i-j,nH);
    dd = findobj(obj,'Tag',dd_tag3{j+1});
    dd.Position = BoundingBox(boxes,[6,7],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%% PLACE TEXT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i = 5;
for j=0:1
    % Retrieve the first dropdown and place it correctly
    txt = findobj(obj,'Tag',txt_tag{j+1});
    txt.Position = BoundingBox(boxes,[4,7],i-j,nH);
end

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE BUTTONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
but = findobj(obj,'Tag','CancelBUT');
but.Position = BoundingBox(boxes,[2,4],1,nH);
but = findobj(obj,'Tag','exportUbertragungBUT');
but.Position = BoundingBox(boxes,[5,7],1,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE TABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tb.Position = BoundingBox(boxes,[1,nH-1],2,nH);

%% %%%%%%%%%%%%%%%%%%%%%%% PLACE AXES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ax.Position = BoundingBox(boxes,nH,[1,nV],nH);