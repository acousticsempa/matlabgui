function closeFigure(src,~)
% Function hides the figure calling it (or the figure in which the button
% is

% Find type of the src object passed in an climb up the ancestry chain
% until we reach a parent that is a figure
while strcmpi(src.Type,'Figure')~=1
    % Set the src object to the parent of the current src object
    src = src.Parent;
end

% Hide the figure
src.Visible = 'off';

return;
end