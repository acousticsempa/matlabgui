function fCreateVorsatz_Populate(src,callbackdata)

% Function populates the CreateVorsatz window with the actual data 
% to write to the database

%% %%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%
% Figures
global fCreateVorsatz
% Data
global Bauteile

%% %%%%%%%%%%%%%%%%% FIND GRAPHICS OBJECTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
obj = fCreateVorsatz.Children;

% Find dropdowns
bDD = findobj(obj,'Tag','BauteileDD');
% Find buttons
but = findobj(obj,'Tag','AddBauteilBUT');
% Find lists
lst = findobj(obj,'Tag','SelBauteileLST');

%% %%%%%%%%%%%%%%%%%%%%%%%% RETRIEVE THE FIGURE DATA %%%%%%%%%%%%%%%%%%%%%
data = fCreateVorsatz.Data;

%% %%%%%%%%%%%%%%%%%%%%%%%% CALCULATE UPDATE LEVEL %%%%%%%%%%%%%%%%%%%%%%%
% Create mapping between elements and updateLevel
elem = [bDD, but, lst];
upLev = [ 1,   2,   3];

% Find the update Level
upLev = upLev(src == elem);

% If we have not found the source
if isempty(upLev)
    upLev = 0;
end

%% %%%%%%%%%%%%%%%%%%%%%%%% BAUTEILE DROPDOWN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Actualize the dropdown list only if the update level is 0
if upLev == 0
    % Retrieve the full list of list of Bauteile and store a structure
    % containing the corresponding IDs
    dd = Bauteile.Dropdown;
    data.Bauteile = Bauteile.ID;
    
    % Update the Bauteile Dropdown
    bDD.String = dd;
    if bDD.Value > numel(dd)
        bDD.Value = 1;
    end
end

%% %%%%%%%%%%%%%%%%%%%%%% UPDATE LIST %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the list when the update Level is 0 (initialize) or 2 (add
% Bauteil) or 3 (delete Bauteil)

% Initialize the list
if isfield(data,'BauteileList')
    lstID = data.BauteileList;
else
    lstID = [];
end

% Add Bauteil: 
% Through pressing of return key or through clicking on the button
if ((upLev == 1) && checkKey(callbackdata.Key,'enter')) || (upLev == 2)
    % Find out which bauteil is selected
    bID = data.Bauteile(bDD.Value);
    % Add this bauteil to the Bauteile_List
    lstID = [lstID; bID];
    % Keep only unique values
    lstID = unique(lstID,'rows','stable');
end

% Delete Bauteile
if upLev == 3
    % Check that the key being pressed is a delete key
    if checkKey(callbackdata.Key,'delete')
        % Delete teh selected elements from the list
        vals = lst.Value;
        % Update the list of IDs
        lstID(vals) = [];
    end
end

%%%%%%%%%%%%%%%%% PRINT THE LIST %%%%%%%%%%%%%%%%%%%%%  
% Check that only existing IDs are being used
[b,pos] = ismember(lstID, Bauteile.ID);

% Check if we have at least a match
if any(b)
    % Get the list names
    dd = Bauteile.Dropdown(pos(b),1);
    % Update the global variable
    lstID = lstID(b);
else
    % None list
    [dd,lstID] = addNone();
end

% Update the list and the value if needed
lst.String = dd;
lst.Max = numel(dd);
lst.Value = unique(min(lst.Value,numel(dd)),'sorted');

% Update the data variable
data.BauteileList = lstID;

%% %%%%%%%%%%%%%%%%%%%%% UPDATE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the data property of the figure where we store the data associated
% with the figure
fCreateVorsatz.Data = data;

end