function [freq] = getFreq(cellArray)

% Function goes through all the cells in the cell array and returns the 
% frequency value based on the unit

% Retrieve the size of the cellArray
n = numel(cellArray);

% Initialize the return array
freq = zeros(n,1); 

% Iterate over the cells
for i=1:n
    % Retrieve the string and replace the spaces and 'HZ' with nothing
    str = strrep(cellArray{i,1},' ','');
    str = strrep(str,'Hz','');
    str = strrep(str,'hz','');
    
    % Retrieve the last string element indicating the multiple
    % Either it is a number, 'k''K', 'm''M' 'g''G' and evaluate the string
    num = str2double(str(1:end-1));
    post = str(end);
    
    % If its a digit skip this step
    if ~isstrprop(post,'digit')
        % Enact the proper cases
        switch lower(post)
            case 'k'
                num = num*1000;
            case 'm'
                num = num*1000^2;
            case 'g'
                num = num*1000^3;
            otherwise
        end
    end
    
    % Store the number in the frequence array
    freq(i) = num; 
end

end             