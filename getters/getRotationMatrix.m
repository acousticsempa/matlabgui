function [R] = getRotationMatrix(A,B)

% Function returns a rotation matrix for rotating from the A-vector into
% the B-vector

% First normalize the vectors
A = A/norm(A);
B = B/norm(B);

% Reshape to the correct size
A = reshape(A,[3,1]);
B = reshape(B,[3,1]);

% Calculate vector dot product
dP = dot(A,B);
% If the vector dot product is equal to 1 then we have the same coordinates
% so we can avoid performing the calculations
if dP==1
    R = eye(3);
    return;
end
% If the vector dot product is equal to 0 then we have an inverted
% coordinate system (rotated by 180 degrees)
if dP==-1
    R = -eye(3);
    R(3,3) = 1;
    return;
end
    
cP = cross(A,B);

% Calculate trigonometric value
c = dP;         % Cosinus of theta
s = norm(cP);   % Sinus of theta

% Calculate the rotation matrix in the plane
G = [c, -s, 0;...
     s,  c, 0;...
     0,  0, 1];
 
% Calculate the basis vectors of the plane
u = A;
v = (B - dP*A)/(norm(B - dP*A));
w = cP;

% Basis matrix in the plane of transformation
F = [u, v, w];

% Calculate the complete rotation matrix
% It should be F*G*inv(F), but given that F is an orthogonal matrix
% it has the property that inv(F) == transpose(F)
R = F*G*transpose(F);

end