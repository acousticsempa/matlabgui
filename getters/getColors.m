function colors = getColors(n)

% Function generates a set of color which are optimally spaced, so that the
% user can distinguish them correctly

% Maximum number available
nmax = 21;

% Check if we ask for more
if n>nmax
    error('Maximum number of available colors: %i',nmax);
end

%% %%%%%%%%%%%%%%%%%%%%% DEFINE COLORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colors define according to the paper by Kenneth L.Kelly
% "Twenty-Two Colors of Maximum Contrast"
colors = [243, 195,   0;...     % Vivid Yellow
          135,  86, 146;...     % Strong Purple
          243, 132,   0;...     % Vivid Orange
          161, 202, 241;...     % Very Light Blue, Baby blue
          190,   0,  50;...     % Vivid Red
          194, 178, 128;...     % Grayish Yellow
          132, 132, 132;...     % Medium Gray
            0, 136,  86;...     % Vivid Green
          230, 143, 172;...     % Strong Purplish Pink
            0, 103, 165;...     % Strong Blue, bright blue
          249, 147, 121;...     % Strong Yellowish Pink
           96,  78, 151;...     % Strong Violet
          246, 166,   0;...     % Vivid Orange Yellow
          179,  68, 108;...     % Strong Purplish Red
          220, 211,   0;...     % Vivid Greenish Yellow
          136,  45,  23;...     % Strong Redish Brown
          141, 182,   0;...     % Vivid Yellow Green
          101,  69,  34;...     % Deep Yellowish Brown
          226,  88,  34;...     % Vivid Reddish Orange
           43,  61,  38;....    % Dark Olive Green
           34,  34,  34]/255;   % Black
       
% Define colors using Matlab colormaps
% map = [lines(7); prism(6); hsv(6)];
% map = hsv(n);

% Cut the map short
colors = colors(1:n,:);

end