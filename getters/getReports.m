function [out] = getReports(in)
% The function takes a nx2 matrix as input and retrieves and returns a
% string which completely identifiey the report for display in the
% listboxes
% INPUTS: 
% in:   [Air/Impact, ReportID], is a unique identifier for a report
%
% OUTPUTS: 
% out:  {'str1','str2'} is a cell array containing the string to be 
%       presented in the listbox, corresponding with the specified 
%       reports.

%%%%%%%%%%%%%%%%%%%%%% DECLARE GLOBAL VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%
global Data

% Get the size of input array
n = size(in,1); 

% Initialize a list array for storing the strings
out = cell(n,1);

% Iterate over the elements in the input array to retrieve the identifying
% string
for i=1:n
    % Depending on which type it is we have to specify certain variables to
    % retrieve the data from the correct database
    if in(i,1)==1
        % Define Airborne specific variables
        DBType = 'AIR____';
        field = 'Air';
    elseif in(i,1)==2
        % Define Impact specific variables
        DBType = 'IMP____';
        field = 'Impact';
    elseif in(i,1)==3
        % Define Airborne Average specific variables
        DBType = 'AIRAVG';
        field = 'AirAVG';
    end
    % Retrieve the index
    indx = in(i,2);
    % Retrieve the important data about the dataset
    ReportString = Data.(field).Report{indx,1};
    ID = Data.(field).ID(indx);
    DataType = Data.(field).DataType{indx,1};
    
    % Concatenate the string
    out{i,1} = strcat(DBType,'-',preZeros(ID,'%.0f',3),...
                      '-(',DataType,')',32,'-',32,...
                      ReportString);
end

