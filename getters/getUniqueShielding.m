function [ind, uniqueString, uniqueSetting] = getUniqueShielding(boolArray)

% This function takes a boolean 2D-Array and retrieves the unique
% configurations among all present configurations

%%%%%%%%%%%%%%%%%%% RETRIEVE UNIQUE CONFIGURATIONS %%%%%%%%%%%%%%%%%%%%%%%
[uniqueSetting,~,ind] = unique(boolArray,'rows','stable');

% Initialize the uniqueString cellArray
uniqueString = cell(size(uniqueSetting,1),1);

% False string
falseString = 'FFFFffFFffffFFFF';
trueString = 'TTTTttTTttttTTTT';

%%%%%%%%%%%%%%%%%%% CREATE AN IDENTIFICATION STRING %%%%%%%%%%%%%%%%%%%%%%
for i=1:size(uniqueSetting,1)
    % Retrieve the unique setting and create an idenfification string
    sett = uniqueSetting(i,:);
    
    % Create a string of T & F to indicate the setting
    str = char((~sett).*falseString + sett.*trueString);
    
    % Insert underscores for separation
    uniqueString{i} = strcat('SC',num2str(i,'%.0f'),'_',...
                             str(1:4),'_',...
                             str(5:6),'_',...
                             str(7:8),'_',...
                             str(9:12),'_',...
                             str(13:end));
end
end

