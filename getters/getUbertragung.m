function ID = getUbertragung(Configurations,DirectionIndependent)

% Function takes care of retrieving the correct Ubertragungen for a
% specific configuration, also taking into account that if the
% configuration if not direction dependant, we need to search also for the
% corresponding "mirrored" pathway
%
% The output is in the following format
% 

% Global variables
global Ubertragung

% Check the input size
if nargin~=2
    error(['The number of input for the getUbertragung function is 2:',...
           '''Configurations'' and ''DirectionIndependent''.']);
end

% Check size of configurations matrix
if size(Configurations,2)~=6    
    error('The ''configurations'' argument should be an nx6 matrix.');
end

%% %%%%%%%%%%%%%%%%%%%%% FIND UBERTRAGUNGEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the matrix of all possibilities store in the database
ValidConfigurations = [Ubertragung.Knoten,...
                       Ubertragung.TypeID,...
                       Ubertragung.Raume,...
                       Ubertragung.Bauteile];

% First we search the passed in configurations
[b,pos] = ismember(Configurations,ValidConfigurations,'rows');

% Check if the configurations are DirectionIndependent, in which case we
% need to check also for the reversed pathway
if DirectionIndependent
    % Create the configuration of the reversed pathway
    a = Configurations;
    Configurations(:,[3,5]) = a(:,[4,6]);
    Configurations(:,[4,6]) = a(:,[3,5]);
    
    % Check if we find a match
    [bR,posR] = ismember(Configurations,ValidConfigurations,'rows');
    
    % Compound the vectors using the XOR logic
    b = xor(b,bR);
    pos = pos + posR;
end

% Retrieve the IDs of the Ubertragungen
ID = zeros(size(b));
ID(b) = Ubertragung.ID(pos(b));

end