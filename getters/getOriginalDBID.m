function str = getOriginalDBID(in,orig)

% Function takes as an input a matrix of size nx2 and returns a string
% representing this datasets according to the convention specified in the
% database design
% x.y,x.y-z
%   x:  Dataset-ID
%   y:  Database ID (1:Airborne, 2:Impact, 3:AirborneAverage)
%   z:  Data Origin (M: Measurement, A:Averaging, E:EnergySub, L:LevelSub
% 
% Input format
% First column:     Database-ID
% Second column:    Dataset-ID

% Iterate over the elements in the input matrix
n = size(in,1);
% Preallocate cell array
strs = cell(1,n);
for i=1:n
    % Concatenate the strings
    strs{1,i} = [num2str(in(i,2),'%.0f'),'.',num2str(in(i,1),'%.0f')];
end

% Concatenate the strings and add the Data Origin identifier
str = [strjoin(strs,','),'-',orig];

end