function out = getFileData(filename,datatype)

% This function uses the dir function to retrieve information about a
% specific file, the function retrieve the directory automatically from the
% pathname

% Check if the file exist
if exist(filename,'file')~=2
    error('filename must be the full path to an existing file!');
end
    
% Check that we have a valid datatime
if ~any(strcmpi(datatype,{'name','folder','date','bytes','isdir','datenum'}))
    error('datatype must be a valid option.');
end

% Split the string using the / or \ separators
if ispc
    sep = '\';
elseif ismac
    sep = '/';
end
% Find the last occurrence of the separator and split there
p = find(filename == sep,1,'last');

% If is empty throw error
if isempty(p)
    error('filename must be the full path to the file!');
end

% Get the path and the filename
path = filename(1:p-1);
filename = filename(p+1:end);

% Retrieve the directory information
data = dir(path);

% Find at which index our file lies
i = 0;
found = false;
while i<numel(data) && ~found
    % Update index
    i = i+1;
    % Check if this is the valid index
    if strcmp(data(i).name,filename)
        found = true;
    end
end

% Retrieve the data
out = data(i);

% Retrieve the correct field
out = out.(datatype);

end
