function out = getOriginalID(in)

% Function takes a cell array of size nx1 and returns a matrix of size
% nxmx2 containing in the first nxm matrix the Database ID (1 for Airborne,
% 2 for Impact and 3 for Airborne Average) and in the second matrix the ID
% assigned to the measurement in the old database
% 
% The function also clears letters and - signes used for identification of
% the origin of the data in the database

% Retrieve the size of the array
[n,m] = size(in);

% Check that the m-size is only one and throw error otherwise
if m~=1
    % Throw error
    error('Size of the input cell array must be nx1.');
    return;
end

% Initialize the matrix for storing the IDs
m = 10;
out = NaN(n,m,2);

% Initialize a variable to keep track of the number of IDs in a single cell
counter = 0;

% Iterate over the cells passed in to retrieve the necessary data
for i=1:n
    % Retrieve the string
    str = in{i,1};
    
    % Check if it is a number or a string and conver to string if it is a
    % number
    if isnumeric(str)
        % Convert back to string
        str = num2str(str,'%.1f');
    end
    
    % Remove alphabetic characters and spaces from the string
    str = str(~isstrprop(str,'alpha'));
    str = strrep(str,' ','');
    % Remove the - character used to identify the source of the
    % calculations in the ubertragung database
    str = strrep(str,'-','');
    
    % Separate the string using the , delimiter
    c = strsplit(str,',');
    
    % Check if the last element is empty
    if isempty(c{1,end})
        c = c(1,1:end-1);
    end

    % Retrieve how many elements we have in c
    k = size(c,2);
    
    % Update the counter
    counter = max(k, counter);
    
    % Check if we have to enlarge the out matrix
    if k > m
        % Update the size of the output matrix
        mOld = m;
        m = k+10;
        % Create additional matrix and attach it to the end
        newOut = NaN(n,m,2);
        % Copy the previous values
        newOut(:,1:mOld,:) = out;
        % Switch variables
        out = newOut;
    end
    
    % Iterate over the elements
    for j=1:k
        % Split the string again
        b = strsplit(c{1,j},'.');
        
        % The first part is the index and the second part is the database
        % ID
        % Store the index 
        out(i,j,2) = str2double(b{1,1});
        % Store the database ID
        out(i,j,1) = str2double(b{1,2});
    end
end

% Cut the array being passed out
out = out(:,1:counter,:);

end
        
    
    