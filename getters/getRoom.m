function [in] = getRoom(in)
% This function takes as input a cell array of size nx3 and clean the first
% column, which is in the form of 'Room ABCD' of the spaces and the room or
% raum string

% Retrieve the size of the cell array
[n,~] = size(in);

% Define strings that should be cleaned off the input string
remove_str = {'raum','room',' '};
m = numel(remove_str);

% Iterate over the elements and clean the string
for i=1:n
    % Retrieve the original string
    str = lower(in{i,1});
    
    % Repeat the process for each string to be removed
    for j=1:m
        % Replace the string
        str = strrep(str,remove_str{j},'');
    end
    
    % Copy the cleaned string in the cell array
    in{i,1} = upper(str);
end

end

