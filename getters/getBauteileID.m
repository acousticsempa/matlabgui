function out = getBauteileID(in)

% Function takes a cell array of size nx1 and returns a matrix of size
% nxm containing in each row the indexes of the Bauteile
% 
% The function also clears letters and - signes used for identification of
% the origin of the data in the database

% Retrieve the size of the array
[n,m] = size(in);

% Check that the m-size is only one and throw error otherwise
if m~=1
    % Throw error
    error('Size of the input cell array must be nx1.');
    return;
end

% Initialize the matrix for storing the IDs
m = 10;
out = NaN(n,m);

% Initialize a variable to keep track of the number of IDs in a single cell
counter = 0;

% Iterate over the cells passed in to retrieve the necessary data
for i=1:n
    % Retrieve the string
    str = in{i,1};
    
    % Check if it is a number in which case we have already identified the
    % only index
    if isnumeric(str)
        % Store the index in the output matrix
        out(i,1) = str;
    else
        % We need to process the string to retrieve all indexes
    
        % Separate the string using the , delimiter
        c = strsplit(str,',');
        
        % Check if the last element is empty
        if isempty(c{1,end})
            c = c(1,1:end-1);
        end
        
        % Retrieve how many elements we have in c
        k = size(c,2);
    
        % Update the counter
        counter = max(k, counter);
    
        % Check if we have to enlarge the out matrix
        if k > m
            % Update the size of the output matrix
            mOld = m;
            m = k+10;
            % Create additional matrix and attach it to the end
            newOut = NaN(n,m);
            % Copy the previous values
            newOut(:,1:mOld) = out;
            % Switch variables
            out = newOut;
        end
    
        % Store the indexes in the matrix
        out(i,1:k) = str2double(c);
    end
end

% Cut the array being passed out
out = out(:,1:counter,:);

end
        
    
    