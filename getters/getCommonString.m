function [in] = getCommonString(in,delimiter,column)
% Function takes a cell array (nx1) and processes the input of each cell
% for the case of Airborne Averaged data, which usually has the format
% STR1delimiterSTR2 and returns only str1 if STR1 is equal to STR2, and 
% returns STR1delimiterSTR2 if they are not equal

% Check input
charInput = ischar(in);
if charInput
    % Store the string in a cell array
    inOld = in;
    % Create a cell variable and store the old input
    in = cell(1,1);
    in{1,1} = inOld;
end

% Check that column exists
if ~exist('column','var')
    % Set column to 1
    column = 1;
end

% Get the cell array size
[n,~] = size(in);

% Iterate over all columns
for j=1:numel(column)
    % Get the column index
    col = column(j);
    
    % Iterate over all cells in the column
    for i=1:n
        % Get the string in the cell
        str = in{i,col};
        % Procede with separation only if it is a string
        if ischar(str)
            % Separate the string using the delimiter
            splitStr = strsplit(str,delimiter);
            % Number of splitted elements
            k = numel(splitStr);
    
            % Check that only the splitting has returned an even number of
            % substrings
            if mod(k,2) == 0
                % Calculate the first string half and the second string half
                str1 = strjoin(splitStr(1,1:k/2),delimiter);
                str2 = strjoin(splitStr(1,k/2+1:k),delimiter);
                % Compare
                if strcmpi(str1,str2)
                    % Store only the first one
                    in{i,col} = str1;
                end
            end
        end
    end
end

% If the input is a char return a char
if charInput
    % Retrieve the string in the first cell element and return that
    in = in{1,1};
end

end