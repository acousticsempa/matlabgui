function [out] = getDoubleFlags(in)
% Function takes a cell array (nxm) and analyzes each cell for an input of
% the type 1,1 or , or 1, or ,1 and returns a boolean matrix of the 
% following form (nxmx2)

% Retrieve the dimensions of the cell array
[n,m] = size(in);

% Initialize the out structure
out = false(n,m,2);

% Go through all elements in the cell array and render them correctly in
% the boolean array
for i=1:n
    for j=1:m
        % Retrieve the string in this cell
        str = in{i,j};
        
        % Remove spaces from the string
        str = strrep(str,' ','');
        
        % Switch cases
        switch str
            case '1,'
                % Only first dataset has a flag
                out(i,j,1) = true;
            case ',1'
                % Only second dataset has a flag
                out(i,j,2) = true;
            case '1,1'
                % Both datasets have a flag
                out(i,j,1) = true;
                out(i,j,2) = true;
        end
    end
end
end

