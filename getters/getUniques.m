function [ind, uniques] = getUniques(cellArray)

% Function searches for unique values inside the cellArray and returns a 
% vector of indexes and a unique elements vector. 
% 
% The vector of indexes references at which position in the unique elements
% vector the original value of the cell is stored

%%%%%%%%%%%% INITIALIZE VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the size of the cellArray
n = numel(cellArray);

% Initialize the vector of indices to the size of the cell array
ind = zeros(n,1);

% Clean the cellArray from NaN values
for i=1:n
    % Get the cell element
    el = cellArray{i};
    % Check if the element is a string, if not convert to string
    if isnumeric(el)
        cellArray{i} = num2str(el);
    end
end

%%%%%%%%%%%%%%%%%%% STRING COMPARISON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize the unsorted variable to the number of elements in the
% cell array
unsorted = n;
toSort = cellArray;
counter = 0;

% Initialize the unique cell array for speed
uniques = cell(n,1);

% Repeat the procedure until the unsorted variable reaches 0
while unsorted > 0
   % Increase the counter 
   counter = counter + 1;
   
   % Compare the first element in the queue to the cellArray
   bool = strcmpi(toSort{1},cellArray);
   
   % Add the index to the ind vector
   ind = ind + bool*counter;
   
   % Add the value to the uniques cell array
   uniques{counter,1} = toSort{1};
   
   % Clean the toSort array of this sorted value
   toSort = toSort(~strcmpi(toSort{1},toSort));

   % Count the number of unsorted values
   unsorted = numel(toSort);
end

% Cut the uniques cell array to the number of unique elements retrieved
uniques = uniques(1:counter,1);

end