function out = addOriginalID(out,str,indx)

% Function takes a nxmx2 matrix of originalDB-ID and a OriginalIDs string
% and attaches the str (newIDs-string) to the end of the matrix
% The function also clears letters and - signes used for identification of
% the origin of the data in the database

% Retrieve the size of the array
[n,m,~] = size(out);

% Check if the newIDs is a number or a string and conver to string if it is a
% number
if isnumeric(str)
    % Convert back to string
    str = num2str(str,'%.1f');
end

% Check if the indx is being passed in, if not, we need to set the ind to
% n+1
if nargin < 3
    indx = n+1;
end

% Check that the index is smaller than n+1
if indx > n+1
    error('The passed in index is out of bounds');
end

%% %%%%%%%%%%%%%%%%%% STRING MANIPULATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove alphabetic characters and spaces from the string
str = str(~isstrprop(str,'alpha'));
str = strrep(str,' ','');
% Remove the - character used to identify the source of the
% calculations in the ubertragung database
str = strrep(str,'-','');

% Separate the string using the , delimiter
c = strsplit(str,',');

% Check if the last element is empty
if isempty(c{1,end})
    c = c(1,1:end-1);
end

%% %%%%%%%%%%%%%%%%% MANIPULATION OF ARRAY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieve how many elements we have in c
k = size(c,2);

% Check if we have to enlarge the out matrix
nnew = n;
mnew = m;
if indx > n
    nnew = indx;
end
if k > m
    mnew = k;
end

% Update the size of the output matrix
newOut = NaN(nnew,mnew,2);
% Copy the previous values
newOut(1:n,1:m,:) = out;
% Switch variables 
out = newOut;

% Iterate over the elements
for j=1:k
    % Split the string again
    b = strsplit(c{1,j},'.');
    
    % The first part is the index and the second part is the database
    % ID
    % Clear the row
    out(indx,:,:) = NaN;
    % Store the index
    out(indx,j,2) = str2double(b{1,1});
    % Store the database ID
    out(indx,j,1) = str2double(b{1,2});
end

end