function [out] = getTicks(range,spacing)
% Function returns an array indicating the ticks necessary to recreate 
% the correct spacing as given by the user

% Find the lowest element and largest element
min_range = min(range);
max_range = max(range);

% Calculate the modulus of the min/max ranges with the spacing
m_min = mod(min_range,spacing);
m_max = mod(max_range,spacing);

% Find the nearest value (for the minimum below, for the maximum above)
minTick = min_range - m_min;
maxTick = max_range - m_max + (m_max~=0)*spacing;

% Create the tick array
out = minTick:spacing:maxTick;
end

