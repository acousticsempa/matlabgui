function [out] = getWallsID(cellArray)

% This function performs an encoding between the wall type and a number to 
% perform faster comparisons

% Declare the global variable walls for encoding available also outside of
% this function
global walls

% Encoding of wall types
walls.Types = {'None','Default','Specimen'};
walls.Encoding = [0,1,2];

% Initialize the output array
[n,m] = size(cellArray);
out = zeros(n,m);

% Iterate over all elements
for i=1:n
    for j=1:m
        % Compare and store the corresponding encoding
        out(i,j) = walls.Encoding(strcmpi(cellArray{i,j},....
                                          walls.Types));
    end
end
end
