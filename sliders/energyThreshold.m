function energyThreshold(src, ~)
% Function modifies the label text to reflect the value of the slider

% Get the current value
v = src.Value; 

% Find the label element
lbl = findobj('Tag','EnergyThresholdLBL');

% Modify the string 
lbl.String = ['Meas. Threshold: ',num2str(v,'%.1f'),'dB'];

end

